$j(function() {
    $j('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
$j(function() {
    $j(window).bind("load resize", function() {
        console.log($j(this).width())
        if ($j(this).width() < 768) {
            $j('div.sidebar-collapse').addClass('collapse')
        } else {
            $j('div.sidebar-collapse').removeClass('collapse')
        }
    })
})
