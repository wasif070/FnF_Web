jQuery(document).ready(function() {
    //    tab.init();
    //    friendMessage.init();
    groupMessage.init();
    
    var height=$(".messagePanel").css("height");
    height=height.replace("px","");
    if(height>350){
        $(".messagePanel").css("height","350px");
        $(".messagePanel").css("overflow-y","scroll");
    }
    $(".messagePanel").scrollTop($(".messagePanel")[0].scrollHeight);
    
    $("#emoticonBtn").click(function(){
        alert('$("#emoticonBtn").click(function(){');
        var pos = $(this).offset();
        var div = $("#emoticons_changed");
        var visibility=$("#emoticons_changed").css("display");
                        
        if(visibility=='none'){
            div.css('display','block');
        }else{
            div.css('display','none');
        }

        div.css({
            "left": pos.left-div.width()/2+10,
            "top":  pos.top - div.height() - 10
        });
    });
    
    
});

function scrollDown(){
    $(".jmessage").removeAttr("readonly");
    $(".sendingStatus").text("");
    var height=$(".messagePanel").css("height");
    height=height.replace("px","");
    if(height>350){
        $(".messagePanel").css("height","350px");
        $(".messagePanel").css("overflow-y","scroll");
    }
    $(".messagePanel").scrollTop($(".messagePanel")[0].scrollHeight);
}
function smileyCode(iconCode)
{
    $(".jmessage").removeAttr("readonly");
    var $msgEle = $(".jmessage");
    $msgEle.val($msgEle.val() + iconCode+' ');
    var $ele = $("#emoticons_changed");
    $ele.hide();
    $msgEle.focus();
}

var friendMessage={
    init:function(){
        friendMessage.loadFriendChat();
        $('.jfriendHistory').click(function(){
            friendMessage.getHistory();
        });
        $('#messageForm').submit(function(){ 
            friendMessage.sendFriendMessage();
            return false;
        });
    },
    dynamicSort:function(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    },
    sendFriendMessage:function(){
        var message=$("input[name='message']").val();
        message=message.trim();
        if(message!=null && message.length>0){
            $.ajax({
                url: base_url+"/SendMessage",
                type:'post',
                beforeSend:function(){
                    $('.jmessage').attr("readonly", true);
                },
                data: {
                    message:message,
                    friendName:$("input[name='friendName']").val(),
                    userName:$("input[name='userName']").val()
                },
                success: function(data){
                    $('.jmessage').removeAttr("readonly");
                    $('.jmessage').val("");
                    if(data!=null){
                        var json=eval('('+data+')');
                        var s=json.uId.toString();
                        var m=json.mg.toString();
                        $('.chatPanel').append("<div><span class='cname'>"+s+" : </span>"+m+"</div><div class='cdate'>Sent at "+json.messageDateString+"</div>");
                        scrollDown();
                    }
                }
            }); 
        }
        return false;
    },
    getHistory:function(){
        var numDays=$('.jnumDays').val();
        if(numDays!=null && numDays.length>0){
            $.ajax({
                url: base_url+"/GetFriendHistory",
                type:'post',
                beforeSend:function(){
                    $('.jmessage').attr("readonly", true);
                },
                data: {
                    numDays:numDays,
                    friendName:$("input[name='friendName']").val()
                },
                success: function(data){
                    if(data=='true'){
                        $('.chatPanel').replaceWith("<div class='chatPanel'></div>");
                    }
                },
                error:function(e,m,s){
                //                    alert(e+m+s);
                }
            }); 
        }
        return false;
    },
    loadFriendChat:function(){
        $.ajax({
            url: base_url+"/GetMessage",
            type:'post',
            data: {
                friendName:$("input[name='friendName']").val()
            },
            success: function(data){
                if(data!=null){
                    var jsonData=eval('('+data+')');
                    if(jsonData.success){
                        var jData=eval('('+jsonData.message+')');
                        if(jData.length>1){
                            jData=jData.sort(friendMessage.dynamicSort("messageDate"));
                        }
                        for(var i=0;i<jData.length;i++) {
                            var json = jData[i];
                            var s=json.uId.toString();
                            var m=json.mg.toString();
                            $('.chatPanel').append("<div><span class='cname'>"+s+" : </span>"+m+"</div><div class='cdate'>Sent at "+json.messageDateString+"</div>");
                        }
                        scrollDown();
                    }      
                }
            }
        });
        setTimeout(function(){
            friendMessage.loadFriendChat()
        }, 1000);
    }
}

var groupMessage={
    init:function(){
        groupMessage.loadGroupChat();
        $('#groupMessageForm').submit(function(){ 
            groupMessage.sendGroupMessage();
            return false;
        });
        $('.jgroupHistory').click(function(){
            groupMessage.getHistory();
        });
    },
    sendGroupMessage:function(){
        var message=$("input[name='message']").val();
        message=message.trim();
        if(message!=null && message.length>0){
            $.ajax({
                url: base_url+"/SendGroupMessage",
                type:'post',
                beforeSend:function(){
                    $('.jmessage').attr("readonly", true);  
                },
                data: {
                    message:message,
                    groupId:$("input[name='groupId']").val(),
                    friendName:$("input[name='friendName']").val(),
                    userName:$("input[name='userName']").val()
                },
                success: function(data){
                    $('.jmessage').val("");
                    if(data!=null){
                        var json=eval('('+data+')'); 
                        var s=json.uId.toString();
                        var m=json.mg.toString();
                        $('.chatGroupPanel').append("<div><span class='cname'>"+s+" : </span>"+m+"</div><div class='cdate'>Sent at "+json.messageDateString+"</div>");
                        scrollDown();
                    }
                }
            }); 
        }
        return false;
    },
    getHistory:function(){
        var numDays=$('.jnumDays').val();
        if(numDays!=null && numDays.length>0){
            $.ajax({
                url: base_url+"/GetGroupHistory",
                type:'post',
                beforeSend:function(){
                    $('.jmessage').attr("readonly", true);
                },
                data: {
                    numDays:numDays,
                    groupId:$("input[name='groupId']").val()
                },
                success: function(data){
                    if(data=='true'){
                        $('.chatPanel').replaceWith("<div class='chatPanel'></div>");
                    }
                },
                error:function(e,m,s){
                //                    alert(e+m+s);
                }
            }); 
        }
        return false;
    },
    loadGroupChat:function(){
        $.ajax({
            url: base_url+"/GetGroupMessage",
            type:'post',
            data: {
                groupId:$("input[name='groupId']").val()
            },
            success: function(data){
                if(data!=null){
                    var jsonData=eval('('+data+')');
                    if(jsonData.success){
                        var jData=eval('('+jsonData.message+')');
                        if(jData.length>1){
                            jData=jData.sort(friendMessage.dynamicSort("messageDate"));
                        }
                        for(var i=0;i<jData.length;i++) {
                            var json = jData[i];
                            var s=json.uId.toString();
                            var m=json.mg.toString();
                            $('.chatGroupPanel').append("<div><span class='cname'>"+s+" : </span>"+m+"</div><div class='cdate'>Sent at "+json.messageDateString+"</div>");
                        }
                        scrollDown();
                    }      
                }
            }
        });
        setTimeout(function(){
            groupMessage.loadGroupChat()
        }, 1000);
    }
}

var tab={
    init:function(){
        $('.tab_nav_1').mouseover(function(){
            tab.reset('1');
        });
        $('.tab_nav_2').mouseover(function(){
            tab.reset('2');
        });
        $('.tab_nav_3').mouseover(function(){
            tab.reset('3');
        });
    },
    reset:function(sel){
        $('.tab_body').css('display','none');
        $('.tab_body_'+sel).css('display','block');
        $('.tab_nav').removeClass('sel');
        $('.tab_nav_'+sel).addClass('sel');
    }
}