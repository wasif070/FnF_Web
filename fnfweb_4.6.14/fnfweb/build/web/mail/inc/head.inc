<head>
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8" />
    <title>IPV Mail</title>
    <!--<meta charset="utf-8">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet"/>
    <script src="static/js/behaviour.js" type="text/javascript"></script>
    <script src="static/js/behaviour-rules.js" type="text/javascript"></script>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/spin.js"></script>
    
    <script src="static/js/drag-n-drop.js" type="text/javascript"></script> 

    <script type="text/javascript">
        var $j = jQuery.noConflict();
    </script>
    <script src="js/utils.js" type="text/javascript"></script>
    <script src="js/sb-admin.js" type="text/javascript"></script>
</head>
