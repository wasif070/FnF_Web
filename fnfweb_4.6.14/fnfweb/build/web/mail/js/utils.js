var spinner;
var counter = 0;
jQuery(document).ready(function() {
//    utilities.callSpinner('');
    var opts = {
        lines: 10, // The number of lines to draw
        length: 7, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px                          
    };
//    var target = document.getElementById(obj);
//        alert(target)
    spinner = new Spinner(opts);

    $('.jsignup').on('click', function() {
        utilities.signUpRequest();
        return false;
    });

    $('.t_inputFileUpload').on('click', function() {
        document.getElementById('realFileBrowser').click();
        return false;
    });
});

var utilities = {
    formatURL: '',
    callSpinner: function(obj) {
        var opts = {
            lines: 10, // The number of lines to draw
            length: 7, // The length of each line
            width: 4, // The line thickness
            radius: 10, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px                          
        };
        var target = document.getElementById(obj);
//        alert(target)
//        var spinner = new Spinner(opts).spin(target);
        spinner.spin(target);
    }, stopSpinner: function() {
        spinner.stop();
    }, showAlert: function(obj) {
        alert('alert called')
    }, redirectToLogon: function() {
        window.location = 'logon.jsf';
    }, fileUploadLabelChange: function() {
        alert($("#realFileBrowser").val());
    }, htmlContentRead: function(partRowIndex) {      
        $.ajax({
            type: 'POST',
            url: base_url+'/retrieveDisplayPartContentServlet.svl',
            data: {
                time: new Date().getMilliseconds(),
                partNumber: partRowIndex
            },
            success: function(retVal) {
                console.log('success')
                $('#thisIshtmlValueHolder').html(retVal);
            },
            error: function(e, m, s) {
                alert('error --> ' + e.responseText)
            }
        });
    }, signUpRequest: function() {
        $(".error_message").hide();
        $.ajax({
            type: 'POST',
            url: 'http://38.127.68.201/WebMailServer/signup.action',
//            url: 'http://192.168.1.95:8080/WebMailServer/signup.action',
            data: {
                time: new Date().getMilliseconds(),
                username: $(".userID1").val(),
                fullname: $(".fullName").val(),
                password: $(".exampleInputPassword1").val(),
                confirm_password: $(".exampleInputPassword2").val(),
                phone: $(".phoneNumber").val()
            },
            success: function(retVal) {
                if (retVal != 'SUCCESS') {
                    $("#error_message").html(retVal);
                    $(".error_message").show();
                    return false;
                } else {
                    alert('Account created successfully !');
                    window.location = 'logon.jsf';
                }
            },
            error: function(e, m, s) {                
                $("#error_message").html('Account Creation Failed');
                $(".error_message").show();
                return false;
//                alert("Account creation Failed : " + e.responseText);
            }
        });
        return false;
    }, mailSelection: function() {
        $(".checkBox").each(function(index) {
            if ($(this).prop('checked')) {
                counter = counter + 1;
            }
        });
        if (counter == 0) {
            $(".jMoveMessageButton").hide();
        } else {
            $(".jMoveMessageButton").show();
        }
        counter = 0;
        spinner.stop();
    }, selectedFolderCSS: function() {
        $(".commonLeftMenu").each(function(index) {
            console.log('searching started ... ***');
            if ($(".currentFolderName").attr("title") == $(this).prop('rel')) {
                $(this).addClass('test');
//                console.log('mil gaya :D')
                console.log($(".currentFolderName").attr("title"));
            }
//            console.log($(this).prop('rel'));
//            if($('.currentFolderName').val()){}
        });

    }

};

