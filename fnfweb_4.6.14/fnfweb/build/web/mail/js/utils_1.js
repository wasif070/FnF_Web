var spinner;
var counter = 0;
jQuery(document).ready(function() {
//    utilities.callSpinner('');
    var opts = {
        lines: 10, // The number of lines to draw
        length: 7, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px                          
    };
//    var target = document.getElementById(obj);
//        alert(target)
    spinner = new Spinner(opts);

    $j('.jsignup').on('click', function() {
        utilities.signUpRequest();
        return false;
    });

    $j('.t_inputFileUpload').on('click', function() {
        document.getElementById('realFileBrowser').click();
        return false;
    });
});

var utilities = {
    formatURL: '',
    callSpinner: function(obj) {
        var opts = {
            lines: 10, // The number of lines to draw
            length: 7, // The length of each line
            width: 4, // The line thickness
            radius: 10, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px                          
        };
        var target = document.getElementById(obj);
//        alert(target)
//        var spinner = new Spinner(opts).spin(target);
        spinner.spin(target);
    }, stopSpinner: function() {
        spinner.stop();
    }, showAlert: function(obj) {
        alert('alert called')
    }, redirectToLogon: function() {
        window.location = 'logon.jsf';
    }, fileUploadLabelChange: function() {
        alert($j("#realFileBrowser").val());
    }, signUpRequest: function() {
        $j(".error_message").hide();
        $j.ajax({
            type: 'POST',
            url: 'http://38.127.68.201/WebMailServer/signup.action',
//            url: 'http://192.168.1.95:8080/WebMailServer/signup.action',
            data: {
                time: new Date().getMilliseconds(),
                username: $j(".userID1").val(),
                fullname: $j(".fullName").val(),
                password: $j(".exampleInputPassword1").val(),
                confirm_password: $j(".exampleInputPassword2").val(),
                phone: $j(".phoneNumber").val()
            },
            success: function(retVal) {
                if (retVal != 'SUCCESS') {
                    $j("#error_message").html(retVal);
                    $j(".error_message").show();
                    return false;
                } else {
                    alert('Account created successfully !');
                    window.location = 'logon.jsf';
                }
            },
            error: function(e, m, s) {                
                $j("#error_message").html('Account Creation Failed');
                $j(".error_message").show();
                return false;
//                alert("Account creation Failed : " + e.responseText);
            }
        });
        return false;
    }, mailSelection: function() {
        $j(".checkBox").each(function(index) {
            if ($j(this).prop('checked')) {
                counter = counter + 1;
            }
        });
        if (counter == 0) {
            $j(".jMoveMessageButton").hide();
        } else {
            $j(".jMoveMessageButton").show();
        }
        counter = 0;
        spinner.stop();
    }, selectedFolderCSS: function() {
        $j(".commonLeftMenu").each(function(index) {
            console.log('searching started ... ***');
            if ($j(".currentFolderName").attr("title") == $j(this).prop('rel')) {
                $j(this).addClass('test');
//                console.log('mil gaya :D')
                console.log($j(".currentFolderName").attr("title"));
            }
//            console.log($j(this).prop('rel'));
//            if($j('.currentFolderName').val()){}
        });

    }

};

