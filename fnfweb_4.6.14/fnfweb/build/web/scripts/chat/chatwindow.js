var windowFocus = true;
var username;
var chatHeartbeatCount = 0;
var minChatHeartbeat = 1000;
var maxChatHeartbeat = 33000;
var chatHeartbeatTime = minChatHeartbeat;
var originalTitle;
var blinkOrder = 0;

var chatboxFocus = new Array();
var newMessages = new Array();
var newMessagesWin = new Array();
var chatBoxes = new Array();
var currentDiv="";

$(document).ready(function(){
    tab.init();
    originalTitle = document.title;
    messenger.startChatSession();

    $([window, document]).blur(function(){
        windowFocus = false;
    }).focus(function(){
        windowFocus = true;
        document.title = originalTitle;
    });
});
function smileyCode(iconCode)
{
    var $msgEle = $("#chatbox_"+currentDiv+" .chatboxtextarea");
    $msgEle.val($msgEle.val() + iconCode+' ');
    var $ele = $("#emoticons_changed");
    $ele.hide();
    $msgEle.focus();
}
var messenger={
    restructureChatBoxes:function(){
        align = 0;
        for (x in chatBoxes) {
            chatboxtitle = chatBoxes[x];

            if ($("#chatbox_"+chatboxtitle).css('display') != 'none') {
                if (align == 0) {
                    $("#chatbox_"+chatboxtitle).css('right', '20px');
                } else {
                    width = (align)*(225+7)+20;
                    $("#chatbox_"+chatboxtitle).css('right', width+'px');
                }
                align++;
            }
        } 
    },
    chatWith:function(chatuser) {    
        messenger.createChatBox(chatuser);
        $("#chatbox_"+chatuser+" .chatboxtextarea").focus();
    },
    createChatBox:function(chatboxtitle,minimizeChatBox) {
        if ($("#chatbox_"+chatboxtitle).length > 0) {
            if ($("#chatbox_"+chatboxtitle).css('display') == 'none') {
                $("#chatbox_"+chatboxtitle).css('display','block');
                messenger.restructureChatBoxes();
            }
            $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
            return;
        }

        $(" <div />" ).attr("id","chatbox_"+chatboxtitle)
        .addClass("chatbox")
        .html('<div class="chatboxhead"><div class="chatboxtitle">'+chatboxtitle+'</div><div class="chatboxoptions"><a href="javascript:void(0)" onclick="javascript:messenger.toggleChatBoxGrowth(\''+chatboxtitle+'\')"><span style="font-weight:normal;font-size:12px;color:#696969;" class="glyphicon glyphicon-minus"></span></a> <a href="javascript:void(0)" onclick="javascript:messenger.closeChatBox(\''+chatboxtitle+'\')"><span style="font-weight:normal;font-size:12px;color:#696969;" class="glyphicon glyphicon-remove"></span></a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><div class="jstatusValue"></div><textarea class="chatboxtextarea"  onkeydown="javascript:return messenger.checkChatBoxInputKey(event,this,\''+chatboxtitle+'\');"></textarea><img class="emoticonBtn" onclick="javascript:return messenger.openEmoticon(this,\''+chatboxtitle+'\');" src="'+base_url+'/resources/img/smile_icon.png" alt="EMO"/></div>')
        .appendTo($( "body" ));
			   
        $("#chatbox_"+chatboxtitle).css('bottom', '0px');
	
        chatBoxeslength = 0;

        for (x in chatBoxes) {
            if ($("#chatbox_"+chatBoxes[x]).css('display') != 'none') {
                chatBoxeslength++;
            }
        }

        if (chatBoxeslength == 0) {
            $("#chatbox_"+chatboxtitle).css('right', '20px');
        } else {
            width = (chatBoxeslength)*(225+7)+20;
            $("#chatbox_"+chatboxtitle).css('right', width+'px');
        }
	
        chatBoxes.push(chatboxtitle);

        if (minimizeChatBox == 1) {
            minimizedChatBoxes = new Array();

            if ($.cookie('chatbox_minimized')) {
                minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
            }
            minimize = 0;
            for (j=0;j<minimizedChatBoxes.length;j++) {
                if (minimizedChatBoxes[j] == chatboxtitle) {
                    minimize = 1;
                }
            }

            if (minimize == 1) {
                $('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
                $('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
            }
        }

        chatboxFocus[chatboxtitle] = false;

        $("#chatbox_"+chatboxtitle+" .chatboxtextarea").blur(function(){
            chatboxFocus[chatboxtitle] = false;
            $("#chatbox_"+chatboxtitle+" .chatboxtextarea").removeClass('chatboxtextareaselected');
        }).focus(function(){
            chatboxFocus[chatboxtitle] = true;
            newMessages[chatboxtitle] = false;
            $('#chatbox_'+chatboxtitle+' .chatboxhead').removeClass('chatboxblink');
            $("#chatbox_"+chatboxtitle+" .chatboxtextarea").addClass('chatboxtextareaselected');
        });

        $("#chatbox_"+chatboxtitle).click(function() {
            if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') != 'none') {
                $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
            }
        });

        $("#chatbox_"+chatboxtitle).show();
        if(minimizeChatBox!=1){
            $.post(base_url+"/SessionAdd", {
                friendName: chatboxtitle
            } , 
            function(html){
                var data=eval('('+html+')');
                if(data.success){
                    var messageList=eval('('+data.message+')');
                    if(messageList!=null && messageList.length>0){
                        $.each(messageList, function(i,msg){
                            $("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+msg.uId+'&nbsp;:&nbsp;</span><span class="chatboxmessagecontent">'+msg.mg+'</span></div>');
                            $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
                        });
                    }
                }
            });
        }
    },
    startChatSession:function(){
        $.ajax({
            url: base_url+"/SessionStart",
            cache: false,
            dataType: "json",
            success: function(data) {
                username = data.uId;
                var friends=eval('('+data.friends+')');
                $.each(friends, function(i,item){
                    if (item)	{ // fix strange ie bug
                        chatboxtitle = item.friendName;
                        if ($("#chatbox_"+chatboxtitle).length <= 0) {
                            messenger.createChatBox(chatboxtitle,1);
                        }
                        if(item.messageList!=null && item.messageList.length>0){
                            $.each(item.messageList, function(i,msg){
                                $("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+msg.uId+'&nbsp;:&nbsp;</span><span class="chatboxmessagecontent">'+msg.mg+'</span></div>');
                                $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
                            });
                        }
                    }
                });
		
                for (i=0;i<chatBoxes.length;i++) {
                    chatboxtitle = chatBoxes[i];
                    $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
                    setTimeout('$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);', 100); // yet another strange ie bug
                }
	
                setTimeout('messenger.chatHeartbeat();',chatHeartbeatTime);
		
            }
        });
    },
    chatHeartbeat:function(){
        var itemsfound = 0;
        if (windowFocus == false) {
            var blinkNumber = 0;
            var titleChanged = 0;
            for (x in newMessagesWin) {
                if (newMessagesWin[x] == true) {
                    ++blinkNumber;
                    if (blinkNumber >= blinkOrder) {
                        document.title = x+' says...';
                        titleChanged = 1;
                        break;	
                    }
                }
            }
		
            if (titleChanged == 0) {
                document.title = originalTitle;
                blinkOrder = 0;
            } else {
                ++blinkOrder;
            }

        } else {
            for (x in newMessagesWin) {
                newMessagesWin[x] = false;
            }
        }

        for (x in newMessages) {
            if (newMessages[x] == true) {
                if (chatboxFocus[x] == false) {
                    $('#chatbox_'+x+' .chatboxhead').toggleClass('chatboxblink');
                }
            }
        }
	
        $.ajax({
            url: base_url+"/SessionHeartBeat",
            cache: false,
            dataType: "json",
            success: function(data) {
                if(data.success){
                    var friends=eval('('+data.message+')');
                    $.each(friends, function(i,item){
                        if (item)	{ // fix strange ie bug
                            chatboxtitle = item.friendName;
                            if ($("#chatbox_"+chatboxtitle).length <= 0) {
                                messenger.createChatBox(chatboxtitle,1);
                            }
                            newMessages[chatboxtitle] = true;
                            newMessagesWin[chatboxtitle] = true;
                            if(item.messageList!=null && item.messageList.length>0){
                                $.each(item.messageList, function(i,msg){
                                    $("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+msg.uId+'&nbsp;:&nbsp;</span><span class="chatboxmessagecontent">'+msg.mg+'</span></div>');
                                    $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
                                });
                            }
                        }
                        itemsfound++;
                    });
                }
                
//                else{
//                    if(data.message!=null && data.message.toString().length>0){
//                        var friends=eval('('+data.message+')');
//                        $.each(friends, function(i,item){
//                            if (item)	{ // fix strange ie bug
//                                chatboxtitle = item.friendName;
//                                if ($("#chatbox_"+chatboxtitle).length <= 0) {
//                                    messenger.createChatBox(chatboxtitle,1);
//                                }
//                                newMessages[chatboxtitle] = true;
//                                newMessagesWin[chatboxtitle] = true;
//                                $("#chatbox_"+chatboxtitle+" .jstatusValue").html(item.statusVal);
//                            }
//                            itemsfound++;
//                        }); 
//                    }
//                }
           
                chatHeartbeatCount++;
            
                //                if (itemsfound > 0) {
                //                    chatHeartbeatTime = minChatHeartbeat;
                //                    chatHeartbeatCount = 1;
                //                } else if (chatHeartbeatCount >= 10) {
                //                    chatHeartbeatTime *= 2;
                //                    chatHeartbeatCount = 1;
                //                    if (chatHeartbeatTime > maxChatHeartbeat) {
                //                        chatHeartbeatTime = maxChatHeartbeat;
                //                    }
                //                }
                setTimeout('messenger.chatHeartbeat();',chatHeartbeatTime);
            }
        });
    },
    closeChatBox:function(chatboxtitle) {
        $('#chatbox_'+chatboxtitle).css('display','none');
        messenger.restructureChatBoxes();
        $.post(base_url+"/SessionClose", {
            friendName: chatboxtitle
        } , function(data){	
            });
    },
    toggleChatBoxGrowth:function(chatboxtitle) {
        if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') == 'none') {  
		
            var minimizedChatBoxes = new Array();
		
            if ($.cookie('chatbox_minimized')) {
                minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
            }

            var newCookie = '';

            for (i=0;i<minimizedChatBoxes.length;i++) {
                if (minimizedChatBoxes[i] != chatboxtitle) {
                    newCookie += chatboxtitle+'|';
                }
            }

            newCookie = newCookie.slice(0, -1)


            $.cookie('chatbox_minimized', newCookie);
            $('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','block');
            $('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','block');
            $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
        } else {
            var newCookie = chatboxtitle;
            if ($.cookie('chatbox_minimized')) {
                newCookie += '|'+$.cookie('chatbox_minimized');
            }
            $.cookie('chatbox_minimized',newCookie);
            $('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
            $('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
        }
    },
    openEmoticon:function(obj,chatboxtitle) {       
        currentDiv=chatboxtitle;
        var pos = $(obj).offset();          
        var div = $("#emoticons_changed");     
       var visibility=$("#emoticons_changed").css("display");          
        if(visibility=='none'){          
            div.css('display','block');           
        }else{           
            div.css('display','none');           
        }

        div.css({
            "left": pos.left-div.width()+10,
            "top":  pos.top - div.height() - 10
        });
    },
//    sendChatStatus:function(chatboxtitle){
//        $.post(base_url+"/SendStatusMessage", {
//            friendName: chatboxtitle
//        } , function(data){
////            alert(data);
//        });
//    },
    checkChatBoxInputKey:function(event,chatboxtextarea,chatboxtitle) {
        if(event.keyCode == 13 && event.shiftKey == 0)  {
            message = $(chatboxtextarea).val();
            message = message.replace(/^\s+|\s+$/g,"");
            $(chatboxtextarea).val('');
            $(chatboxtextarea).focus();
            $(chatboxtextarea).css('height','30px');
            if (message != '') {
                $.post(base_url+"/SendMessage", {
                    friendName: chatboxtitle, 
                    message: message
                } , function(data){
                    var json=eval('('+data+')');
                    message = message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
                    $("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+firstNametemp+'&nbsp;:&nbsp;</span><span class="chatboxmessagecontent">'+json.mg+'</span></div>');
                    $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
                });
            }
            chatHeartbeatTime = minChatHeartbeat;
            chatHeartbeatCount = 1;
            
            return false;
        }

        var adjustedHeight = chatboxtextarea.clientHeight;
        var maxHeight = 94;

        if (maxHeight > adjustedHeight) {
            adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
            if (maxHeight)
                adjustedHeight = Math.min(maxHeight, adjustedHeight);
            if (adjustedHeight > chatboxtextarea.clientHeight)
                $(chatboxtextarea).css('height',adjustedHeight+8 +'px');
        } else {
            $(chatboxtextarea).css('overflow','auto');
        }
    }
}

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};

var tab={
    init:function(){
        $('.tab_nav_1').mouseover(function(){
            tab.reset('1');
        });
        $('.tab_nav_2').mouseover(function(){
            tab.reset('2');
        });
        $('.tab_nav_3').mouseover(function(){
            tab.reset('3');
        });
    },
    reset:function(sel){
        $('.tab_body').css('display','none');
        $('.tab_body_'+sel).css('display','block');
        $('.tab_nav').removeClass('sel');
        $('.tab_nav_'+sel).addClass('sel');
    }
}