jQuery(document).ready(function() {
    friends.init();
    groups.init();
    $("#searchContactArea").submit(function() {
        return false;
    });
    $("#searchGroupArea").submit(function() {
        return false;
    });
});

var friends = {
    init: function() {
        friends.loadFriends();
        friends.loadRefreshFriends();
        $('.jFriendStatus').change(function() {
            friends.loadFriends();
        });
        $('.jSearchValue').keyup(function() {
            var val = $(this).val();
            if (val.length > 3 || val.length < 1) {
                friends.loadFriends();
            }
        });
    },
    dynamicSort: function(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function(a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    },
    loadFriends: function() {
        $.ajax({
            url: base_url + "/GetMyFriends",
            type: 'post',
            data: {
                friendShipStatus: $('.jFriendStatus').val(),
                friendSearchValue: $('.jSearchValue').val()
            },
            success: function(data) {
                if (data != null) {
//                     alert(data);
                    $('.chat-table').replaceWith("<table class='chat-table table-striped table-hover'></table>");
                    var jsonData = eval('(' + data + ')');
                    if (jsonData.sucs) {
                        var jData = eval('(' + jsonData.mg + ')');
                        jData = jData.sort(friends.dynamicSort("fn"));
                        for (var i = 0; i < jData.length; i++) {
                            var json = jData[i];
                            var statusImg = "";
                            var imLink = "";
                            if (json.frnS == 1) {
                                //                                imLink="<a href=\""+base_url+"/secure/chat/chat.xhtml?friend="+json.uId+"\" style=\"color:#008;font-weight:bold;\">IM</a>\n\
                                //<a href=\"javascript:void(0)\" onclick=\"javascript:chatWith('//"+json.uId+"')\">IM</a>";


                                //    AITA KI DORKAR? --->      $('input[name="friendName"]').val()
                                if (json.noOfMsg > 0 && json.uId != $('input[name="friendName"]').val()) {
                                    imLink = "<span style='color:#f00;font-size:8pt;position:relative;bottom:5px;'>" + json.noOfMsg + "</span>";
                                }
                                imLink += "<a href=\"javascript:void(0)\" onclick=\"javascript:messenger.chatWith('" + json.uId + "')\" style=\"color:#008;font-weight:bold;\"><img style=\"width: 25px;height: 25px;\" src=\"" + base_url + "/clients_image/messages-icon.png\" /></a>";
                                if (json.psnc == 1) {
                                    statusImg = base_url + '/clients_image/offline.png';
                                } else {
                                    statusImg = base_url + '/clients_image/online.png';
                                }
                            } else {
                                statusImg = base_url + '/clients_image/pending.png';
                            }


                            var device = '';
//  public static final int PC = 1;
//    public static final int ANDROID = 2;
//    public static final int I_PHONE = 3;
//    public static final int WINDOWS = 4;
//    public static final int WINDOWS = 5;


if(json.hasOwnProperty('dvc')&&json.psnc===2){
       if(json.dvc==='1'){
        
        device = "<span class=\"dghjutttyjujk\">PC</span>";
    }else if(json.dvc==='2'){
        
        device = "<span class=\"dghjutttyjujk\">ANDROID</span>";
        
    }else if(json.dvc==='3'){
        
        device = "<span class=\"dghjutttyjujk\">I_PHONE</span>";
        
    }else if(json.dvc==='4'){
        
        device = "<span class=\"dghjutttyjujk\">WINDOWS</span>";
        
    }else if(json.dvc==='5'){
        
        device = "<span class=\"dghjutttyjujk\">WEB</span>";
        
    }
    
    
}


                            var content = "<tr class=\"" + json.uId + "\"><td ><img src=\"" + statusImg + "\" width=\"15\" height=\"15\" style=\"margin-left: 5px;margin-right: 5px;\" />"+
device+"</td>" +
                              
                                    "<td><img src='" + json.prIm + "' width='30' height='30'/></td><td>\n\
<a href=\"" + base_url + "/secure/user/profile/friends_profile.xhtml?friend=" + json.uId + "\"> <font class=\"font-header2-link\">" + json.fn + " " + json.ln + "</font></a></td><td>" + imLink + "</td></tr>";

                            if ($("." + json.uId).length) {
                                $("." + json.uId).replaceWith(content);
                            }
                            else {
                                $('.chat-table').append(content);
                            }
                        }
                    }
                    if (jsonData.numberOfNewRequest != null && jsonData.numberOfNewRequest > 0) {
                        $('.notificationIcon').css("display", 'block');
                        $('.jnotifyValue').html(jsonData.numberOfNewRequest);
                    } else {
                        $('.notificationIcon').css("display", 'none');
                    }
                }
            }
        });
    },
    loadRefreshFriends: function() {

        $.ajax({
            url: base_url + "/GetFriends",
            type: 'post',
            data: {
            },
            success: function(data) {
                if (data != null) {
                    var jsonData = eval('(' + data + ')');
                    if (jsonData.sucs) {
                        var jData = eval('(' + jsonData.mg + ')');
                        for (var i = 0; i < jData.length; i++) {
                            var json = jData[i];
                            if (json.deleted === true) {
                                $("." + json.uId).remove();
                            } else {
                                var statusImg = "";
                                var imLink = "";
                                if (json.frnS == 1) {
                                    if (json.noOfMsg > 0 && json.uId != $('input[name="friendName"]').val()) {
                                        imLink = "<span style='color:#f00;font-size:8pt;position:relative;bottom:5px;'>" + json.noOfMsg + "</span>";
                                    }
                                    imLink += "<a href=\"javascript:void(0)\" onclick=\"javascript:messenger.chatWith('" + json.uId + "')\" style=\"color:#008;font-weight:bold;\"><img style=\"width: 25px;height: 25px;\" src=\"" + base_url + "/clients_image/messages-icon.png\" /></a>";
                                    //imLink+="<a href=\""+base_url+"/secure/chat/chat.xhtml?friend="+json.uId+"\" style=\"color:#008;font-weight:bold;\">IM</a>";
                                    if (json.psnc == 1) {
                                        statusImg = base_url + '/clients_image/offline.png';
                                    } else {
                                        statusImg = base_url + '/clients_image/online.png';
                                    }
                                } else {
                                    statusImg = base_url + '/clients_image/pending.png';
                                }

                                var device = '';
//  public static final int PC = 1;
//    public static final int ANDROID = 2;
//    public static final int I_PHONE = 3;
//    public static final int WINDOWS = 4;
//    public static final int WINDOWS = 5;




if(json.hasOwnProperty('dvc')&&json.psnc===2){
    if(json.dvc==='1'){
        
        device = "<span class=\"dghjutttyjujk\">PC</span>";
    }else if(json.dvc==='2'){
        
        device = "<span class=\"dghjutttyjujk\">ANDROID</span>";
        
    }else if(json.dvc==='3'){
        
        device = "<span class=\"dghjutttyjujk\">I_PHONE</span>";
        
    }else if(json.dvc==='4'){
        
        device = "<span class=\"dghjutttyjujk\">WINDOWS</span>";
        
    }else if(json.dvc==='5'){
        
        device = "<span class=\"dghjutttyjujk\">WEB</span>";
        
    }
    
    
}


                                           var content = "<tr class=\"" + json.uId + "\"><td ><img src=\"" + statusImg + "\" width=\"15\" height=\"15\" style=\"margin-left: 5px;margin-right: 5px;\" />"+
device+"</td>" +
                                        "<td><img src='" + json.prIm + "' width='30' height='30'/></td><td>\n\
<a href=\"" + base_url + "/secure/user/profile/friends_profile.xhtml?friend=" + json.uId + "\"> <font class=\"font-header2-link\">" + json.fn + " " + json.ln + "</font></a></td><td>" + imLink + "</td></tr>";

                                if ($("." + json.uId).length) {
                                    $("." + json.uId).replaceWith(content);
                                }
                                else {
                                    $('.chat-table').append(content);
                                }
                            }

                        }
                    }
                    if (jsonData.numberOfNewRequest != null && jsonData.numberOfNewRequest > 0) {
                        $('.notificationIcon').css("display", 'block');
                        $('.jnotifyValue').html(jsonData.numberOfNewRequest);
                    } else {
                        $('.notificationIcon').css("display", 'none');
                    }
                }
            }
        });
        setTimeout(function() {
            friends.loadRefreshFriends()
        }, 3000);
    }
}

var groups = {
    init: function() {
        groups.loadGroups();
        groups.loadRefreshGroups();
        $('.jGroupSearchValue').keyup(function() {
            var val = $(this).val();
            if (val.length > 3 || val.length < 1) {
                groups.loadGroups();
            }
        });
    },
    dynamicSort: function(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function(a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    },
    loadGroups: function() {
        $.ajax({
            url: base_url + "/GetMyGroups",
            type: 'post',
            data: {
                groupSearchValue: $('.jGroupSearchValue').val()
            },
            success: function(data) {
                if (data != null) {
                    $('.group-chat-table').replaceWith("<table class='group-chat-table  table-striped table-hover'></table>");
                    var jsonData = eval('(' + data + ')');
                    if (jsonData.sucs) {
                        var jData = eval('(' + jsonData.mg + ')');
                        jData = jData.sort(groups.dynamicSort("gNm"));
                        for (var i = 0; i < jData.length; i++) {
                            var json = jData[i];
                            var statusImg = "";
                            var imLink = "";
                            imLink += "<a href=\"javascript:void(0)\" onclick=\"javascript:groupmessenger.chatWith('" + json.grpId + "','" + json.gNm + "')\" style=\"color:#008;font-weight:bold;\"><img style=\"width: 25px;height: 25px;\" src=\"" + base_url + "/clients_image/messages-icon.png\" /></a>";
                            //                            imLink="<a href=\""+base_url+"/secure/chat/group_chat.xhtml?grpId="+json.grpId+"\" style=\"color:#008;font-weight:bold;\">IM</a>";
                            if ($("." + json.grpId).length) {
                                $("." + json.grpId).replaceWith("<tr class=\"" + json.grpId + "\"><td><img width='35' height='35'  src=\"" + base_url + "/clients_image/groupIcon.png\" /></td>\n\
<td><a href=\"" + base_url + "/secure/group/details.xhtml?grpId=" + json.grpId + "\"> <font class=\"font-header2-link\">" + json.gNm + "</font></a><br/> <font class=\"font-header3\"> creator :" + json.sAd + " </font></td><td>" + imLink + "</td></tr>");
                            } else {
                                $('.group-chat-table').append("<tr class=\"" + json.grpId + "\"><td><img width='35' height='35'  src=\"" + base_url + "/clients_image/groupIcon.png\" /></td>\n\
<td><a href=\"" + base_url + "/secure/group/details.xhtml?grpId=" + json.grpId + "\"> <font class=\"font-header2-link\">" + json.gNm + "</font></a><br/> <font class=\"font-header3\"> creator :" + json.sAd + " </font></td><td>" + imLink + "</td></tr>");
                            }
                        }
                    }
                }
            }
        });
    },
    loadRefreshGroups: function() {
        $.ajax({
            url: base_url + "/GetGroups",
            type: 'post',
            data: {
            },
            success: function(data) {
                if (data != null) {
                    var jsonData = eval('(' + data + ')');
                    if (jsonData.sucs) {
                        var jData = eval('(' + jsonData.mg + ')');
                        for (var i = 0; i < jData.length; i++) {
                            var json = jData[i];
                            if (json.deleted === true) {
                                alert("deleting group");
                                $("." + json.grpId).remove();

                            } else {

                                var imLink = "";
                                if (json.noOfMsg > 0 && json.grpId != $('input[name="groupId"]').val()) {
                                    imLink = "<span style='color:#f00;font-size:8pt;position:relative;bottom:5px;'>" + json.noOfMsg + "</span>";
                                }
                                imLink += "<a href=\"javascript:void(0)\" onclick=\"javascript:groupmessenger.chatWith('" + json.grpId + "','" + json.gNm + "')\" style=\"color:#008;font-weight:bold;\"><img style=\"width: 25px;height: 25px;\" src=\"" + base_url + "/clients_image/messages-icon.png\" /></a>";
                                //                            imLink+="<a href=\""+base_url+"/secure/chat/group_chat.xhtml?grpId="+json.grpId+"\" style=\"color:#008;font-weight:bold;\">IM</a>";
                                if ($("." + json.grpId).length) {
                                    $("." + json.grpId).replaceWith("<tr class=\"" + json.grpId + "\"><td><img width='35' height='35'  src=\"" + base_url + "/clients_image/groupIcon.png\" /></td>\n\
<td><a href=\"" + base_url + "/secure/group/details.xhtml?grpId=" + json.grpId + "\"> <font class=\"font-header2-link\">" + json.gNm + "</font></a><br/> <font class=\"font-header3\"> creator :" + json.sAd + " </font></td><td>" + imLink + "</td></tr>");
                                } else {
                                    $('.group-chat-table').append("<tr class=\"" + json.grpId + "\"><td><img width='35' height='35'  src=\"" + base_url + "/clients_image/groupIcon.png\" /></td>\n\
<td><a href=\"" + base_url + "/secure/group/details.xhtml?grpId=" + json.grpId + "\"> <font class=\"font-header2-link\">" + json.gNm + "</font></a><br/> <font class=\"font-header3\"> creator :" + json.sAd + " </font></td><td>" + imLink + "</td></tr>");
                                }
                            }
                        }
                    }
                }
            }
        });
        setTimeout(function() {
            groups.loadRefreshGroups()
        }, 3000);
    }
}