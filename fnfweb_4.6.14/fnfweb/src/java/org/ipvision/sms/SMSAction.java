/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.sms;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.ipvision.communication.MainBean;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "smsAction")
@SessionScoped
public class SMSAction implements OnReceiveListner, Serializable {

//  AullAuthontication System
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
//  End
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    private String mbl;
    private String mg;
    boolean isComplete = false;
    private String errorMessage;
    private String friendName;
    private String grpId;
    private boolean actionDisable = false;

    public SMSAction() {
    }

    public void sendSMS() {
//        System.out.println("sendSMS");
        actionDisable = true;
        try {
            String rendom = ServerConstants.getRandromPacketId();
            String key = loggedUserProfile.getUserIdentity() + rendom;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_FRIEND_SMS);
             jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("mg", loggedUserProfile.getUserIdentity() + "," + rendom + "," + mbl + "," + mg);
            System.out.println("sendSMS::" + jsonObject.toString());

//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("actn", ServerConstants.UPDATE);
//            jsonObject.put("type", ServerConstants.SEND_SMS);
//            jsonObject.put("pckId", key);
//            jsonObject.put("sId", loggedUserProfile.getSessionId());
//            jsonObject.put("mbl", mbl);
//            jsonObject.put("uId", loggedUserProfile.getUserIdentity());
//            jsonObject.put("full_name", "24FnF");
//            jsonObject.put("mg", mg);
//            System.out.println("sendSMS::" + jsonObject.toString());
            mainBean.getServerCommunication().sendSMSPacket(jsonObject.toString(), key);
            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onReceivedMessage(String msg) {
//        int actn = 0;
//        String sentPhoneNumbers[];
//        String failedPhoneNumbers[];
//        int sent = 0;
//        int failed = 0;
        try {
            JSONObject jsonObject = new JSONObject(msg);
            System.out.println("Received::" + msg);
            int actn = jsonObject.getInt("actn");
            if (actn == ServerConstants.TYPE_FRIEND_SMS) {
                if (jsonObject.has("sCode")) {
                    int statusCode = jsonObject.getInt("sCode");
                    switch (statusCode) {
                        case ServerConstants.STATUS_IN_PROGRESS:
                            errorMessage = "Processed";
                            break;
                        case ServerConstants.STATUS_NOT_DELEVERED:
                            errorMessage = "Message Not Develived";
                            break;
                        case ServerConstants.STATUS_NOT_ENOUGH_BALANCE:
                            errorMessage = "Sorry! You have not enough balance!";
                            break;
                        case ServerConstants.STATUS_SENT:
                            errorMessage = "Message sent successfuly.";
                            break;
                    }
                }
                System.out.println("Received::" + errorMessage);
                isComplete=true;
            }

//            if (actn == ServerConstants.TYPE_SEND_SMS) {
//                if (jsonObject.has("report")) {
//                    JSONArray jsonArray = jsonObject.getJSONArray("report");
//                    sentPhoneNumbers = new String[jsonArray.length()];
//                    failedPhoneNumbers = new String[jsonArray.length()];
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject inerObject = new JSONObject();
//                        inerObject = (JSONObject) jsonArray.get(i);
//                        if (inerObject.getBoolean("sucs")) {
//                            sentPhoneNumbers[sent++] = inerObject.getString("mbl");
//                        } else {
//                            failedPhoneNumbers[failed++] = inerObject.getString("mbl");
//                        }
//                    }
//                    String notifyString = "";
//                    if (sent > 0) {
//                        notifyString = "mg sent to:";
//                        for (int i = 0; i < sent; i++) {
//                            notifyString = notifyString.concat(sentPhoneNumbers[i]);
//                            if (i + 1 != sent) {
//                                notifyString = notifyString.concat(",");
//                            }
//                        }
//                        notifyString = notifyString.concat("\n");
//                    }
//                    if (failed > 0) {
//                        notifyString = notifyString.concat("cannot send to:");
//                        for (int i = 0; i < failed; i++) {
//                            if (i != 0) {
//                                notifyString = notifyString.concat(",");
//                            }
//                            notifyString = notifyString.concat(failedPhoneNumbers[i]);
//
//                        }
//                    }
//                    errorMessage = notifyString;
//                    isComplete = true;
//                } else if (jsonObject.has("sucs")) {
//                    if (jsonObject.getBoolean("sucs")) {
//                        //
//                    } else {
//                        if (jsonObject.has("mg")) {
//                            errorMessage = jsonObject.getString("mg");
//                            isComplete = true;
//                        } else {
//                            isComplete = true;
//                            errorMessage = "Invalid Session";
//                        }
//                    }
//                }
//            }
        } catch (Exception e) {
        }
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public String getMobilePhone() {
        return mbl;
    }

    public void setMobilePhone(String mbl) {
        this.mbl = mbl;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String mg) {
        this.mg = mg;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getGroupId() {
        return grpId;
    }

    public void setGroupId(String grpId) {
        this.grpId = grpId;
    }

    public boolean isActionDisable() {
        return actionDisable;
    }

    public void setActionDisable(boolean actionDisable) {
        this.actionDisable = actionDisable;
    }
}
