/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.friends;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.ipvision.communication.MainBean;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserHomeAction;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.AppConstants;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "friendsBean")
@SessionScoped
public class FriendsBean implements OnReceiveListner, Serializable {

    static Logger logger = Logger.getLogger(FriendsBean.class.getName());
    // AullAuthontication System
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;

    @ManagedProperty(value = "#{userHomeAction}")
    private UserHomeAction userHomeAction;

    //End
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    String sId;
    String uId;
    String seachValue;
    int actn;
    boolean refreshSearchContact = false;
    //boolean refreshContact = false;
    ArrayList<SearchFriendDTO> searchFriendList;
    String errorMessage;
    boolean isComplete = false;
    // private ArrayList<UserProfile> newFriendList;

    public FriendsBean() {
    }

    public UserProfile getFriendProfile(String friendIdentiy) {
        HashMap<String, UserProfile> myFriends = loggedUserProfile.getFriendlist();
        if (myFriends.containsKey(friendIdentiy)) {
            return myFriends.get(friendIdentiy);
        }
        return null;
    }

    public boolean isMyFriend(String friendIdentiy) {
        if (loggedUserProfile.getFriendlist().containsKey(friendIdentiy)) {
            UserProfile uProfile = loggedUserProfile.getFriendlist().get(friendIdentiy);
            if (uProfile.getFriendShipStatus() == 1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public void addFriend(String friendIdentity) {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        actn = ServerConstants.TYPE_ADD_FRIEND;
        try {
            jsonObject.put("uId", friendIdentity);
            jsonObject.put("actn", actn);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }

        if (searchFriendList != null && searchFriendList.size() > 0) {
            for (int inc = 0; inc < searchFriendList.size(); inc++) {
                SearchFriendDTO s = searchFriendList.get(inc);
                if (s.getUserIdentity().equalsIgnoreCase(friendIdentity)) {
                    searchFriendList.remove(inc);
                    break;
                }
            }
        }
    }

    public String acceptFriend(String friendIdentity) {
        isComplete = false;
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        actn = ServerConstants.TYPE_ACCEPT_FRIEND;
        try {
            jsonObject.put("uId", friendIdentity);
            jsonObject.put("actn", actn);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
        return "/secure/user/profile/friends_profile.xhtml" + AppConstants.REDIRECT + "&friend=" + friendIdentity;
    }

    public String removeFriend(String friendIdentity) {
          FacesContext facesContext = FacesContext.getCurrentInstance();
     
        isComplete = false;
        errorMessage="";
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("uId", friendIdentity);
            jsonObject.put("actn", ServerConstants.TYPE_DELETE_FRIEND);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        
                      //  FacesContext facesContext = FacesContext.getCurrentInstance();
                       //  facesContext.addMessage(null, new FacesMessage(errorMessage));
                        errorMessage="";
                     return "";
                    }
                }
            } catch (InterruptedException ex) {
            }
        }

        return "/secure/user/welcome.xhtml" + AppConstants.REDIRECT;
    }

    public String acceptRequestFriend(UserProfile uprofile) {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uId", uprofile.getUserIdentity());
            jsonObject.put("actn", ServerConstants.TYPE_ACCEPT_FRIEND);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    userHomeAction.getRequestedFriendList().remove(uprofile);
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }

        return "/secure/user/profile/pending_friends.xhtml" + AppConstants.REDIRECT;
        // return "";
    }

    public String removeRequestFriend(UserProfile uprofile) {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uId", uprofile.getUserIdentity());
            jsonObject.put("actn", ServerConstants.TYPE_DELETE_FRIEND);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    userHomeAction.getRequestedFriendList().remove(uprofile);
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }

        return "/secure/user/profile/pending_friends.xhtml" + AppConstants.REDIRECT;
    }

    public void addMultipleFriend() {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uId", getSeachValue());
            jsonObject.put("actn", ServerConstants.TYPE_ADD_MULTIPLE_FRIEND);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public void searchAddressBook() {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("schPm", getSeachValue());
            jsonObject.put("actn", ServerConstants.TYPE_ADDRESS_BOOK);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }
    }

    public void addFriendFromAddressBook() {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("schPm", getSeachValue());
            jsonObject.put("actn", ServerConstants.TYPE_ADD_ADDRESS_BOOK_FRIEND);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }
    }

    public void searchContact() {
        String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("schPm", getSeachValue());
            jsonObject.put("actn", ServerConstants.TYPE_CONTACT_SEARCH);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }
    }

    @Override
    public void onReceivedMessage(String msg) {
        try {
            JSONObject jsonObject = new JSONObject(msg.trim());
            actn = jsonObject.getInt("actn");
            if (actn == ServerConstants.TYPE_CONTACT_SEARCH
                    || actn == ServerConstants.TYPE_ADDRESS_BOOK) {
                if (jsonObject.getBoolean("sucs")) {
                    String sequence = jsonObject.getString("seq");
                    int packetNumber = Integer.valueOf(sequence.split("/")[0]);
                    if (packetNumber == 1) {
                        searchFriendList = new ArrayList<SearchFriendDTO>();
                    }
                    JSONArray friendsJsonArray = jsonObject.getJSONArray("searchedcontaclist");
                    for (int j = 0; j < friendsJsonArray.length(); j++) {
                        SearchFriendDTO friendsProfile = new SearchFriendDTO();
                        friendsProfile.setUserIdentity(friendsJsonArray.getJSONObject(j).getString("uId"));
                        friendsProfile.setFirstName(friendsJsonArray.getJSONObject(j).getString("fn"));
                        friendsProfile.setLastName(friendsJsonArray.getJSONObject(j).getString("ln"));
                        friendsProfile.setPresence(1);
                        friendsProfile.setFriendShipStatus(3);
                        searchFriendList.add(friendsProfile);
                    }
                    refreshSearchContact = true;
                } else {
                    refreshSearchContact = true;
                    errorMessage = "No Contact Found!";
                    searchFriendList = new ArrayList<SearchFriendDTO>();
                }

            } else if (actn == ServerConstants.TYPE_ADD_FRIEND) {
                if (jsonObject.getBoolean("sucs")) {
                    UserProfile profile = new UserProfile();
                    profile.setUserIdentity(jsonObject.getString("uId"));
                    profile.setFirstName(jsonObject.getString("fn"));
                    profile.setLastName(jsonObject.getString("ln"));
                    profile.setGender(jsonObject.getString("gr"));
                    profile.setCountry(jsonObject.getString("cnty"));
                    profile.setMobilePhone(jsonObject.getString("mbl"));
                    profile.setEmail(jsonObject.getString("el"));
                    profile.setPresence(jsonObject.getInt("psnc"));
                    profile.setWhatisInYourMind(jsonObject.getString("wim"));
                    profile.setFriendShipStatus(jsonObject.getInt("frnS"));
                    loggedUserProfile.getFriendlist().put(profile.getUserIdentity(), profile);
                    refreshSearchContact = true;

                    userHomeAction.setRefreshDiv(true);
                    if (userHomeAction.getNewFriendList() == null) {
                        userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                    }
                    userHomeAction.getNewFriendList().add(profile);
                }
                isComplete = true;
            } else if (actn == ServerConstants.TYPE_ADD_ADDRESS_BOOK_FRIEND) {
                if (jsonObject.getBoolean("sucs")) {
                    JSONArray friendsArray = new JSONArray(jsonObject.getString("friendList"));
                    if (friendsArray != null && friendsArray.length() > 0) {
                        for (int inc = 0; inc < friendsArray.length(); inc++) {
                            JSONObject friendObject = friendsArray.getJSONObject(inc);
                            UserProfile profile = new UserProfile();
                            profile.setUserIdentity(friendObject.getString("uId"));
                            profile.setFirstName(friendObject.getString("fn"));
                            profile.setLastName(friendObject.getString("ln"));
                            profile.setGender(friendObject.getString("gr"));
                            profile.setCountry(friendObject.getString("cnty"));
                            profile.setMobilePhone(friendObject.getString("mbl"));
                            profile.setEmail(friendObject.getString("el"));
                            profile.setPresence(friendObject.getInt("psnc"));
                            profile.setWhatisInYourMind(friendObject.getString("wim"));
                            profile.setFriendShipStatus(friendObject.getInt("frnS"));
                            loggedUserProfile.getFriendlist().put(profile.getUserIdentity(), profile);
                        }
                    }
                    refreshSearchContact = true;
                    userHomeAction.setRefreshDiv(true);
                }
                isComplete = true;
            } else if (actn == ServerConstants.TYPE_DELETE_FRIEND) {

                if (jsonObject.getBoolean("sucs")) {
                    String fndId = jsonObject.getString("uId");
                    if (loggedUserProfile.getFriendlist().containsKey(fndId)) {
                        loggedUserProfile.getFriendlist().remove(fndId);
                        userHomeAction.setRefreshDiv(true);
                    }

                    UserProfile friendProfile = new UserProfile();

                    friendProfile.setUserIdentity(fndId);
                    friendProfile.setDeleted(true);
                    if (userHomeAction.getNewFriendList() == null) {
                        userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                    }
                    userHomeAction.getNewFriendList().add(friendProfile);

                    userHomeAction.setRefreshDiv(true);
                    isComplete = true;
                } else {

                    errorMessage = jsonObject.getString("mg");

                }

            } else if (actn == ServerConstants.TYPE_ACCEPT_FRIEND) {

                String fndId = jsonObject.getString("uId");
                if (loggedUserProfile.getFriendlist().containsKey(fndId)) {
                    UserProfile friendProfile = loggedUserProfile.getFriendlist().get(fndId);
                    friendProfile.setFirstName(jsonObject.getString("fn"));
                    friendProfile.setLastName(jsonObject.getString("ln"));
                    friendProfile.setGender(jsonObject.getString("gr"));
                    friendProfile.setCountry(jsonObject.getString("cnty"));
                    friendProfile.setMobilePhone(jsonObject.getString("mbl"));
                    friendProfile.setEmail(jsonObject.getString("el"));
                    friendProfile.setPresence(jsonObject.getInt("psnc"));
                    friendProfile.setWhatisInYourMind(jsonObject.getString("wim"));
                    friendProfile.setFriendShipStatus(jsonObject.getInt("frnS"));

                    String imagePath = "";
                    if (jsonObject.has("prIm")) {
                        imagePath = jsonObject.getString("prIm");
                        if (imagePath.length() > 1) {
                            friendProfile.setProfileImage(imagePath);
                        } else {
                            friendProfile.setProfileImage(null);
                        }
                    } else {
                        friendProfile.setProfileImage(null);
                    }
                    loggedUserProfile.getFriendlist().put(fndId, friendProfile);

                    if (userHomeAction.getNewFriendList() == null) {
                        userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                    }
                    userHomeAction.getNewFriendList().add(friendProfile);
                }
                isComplete = true;
                userHomeAction.setRefreshDiv(true);
            } else if (actn == ServerConstants.TYPE_UPDATE_ADD_FRIEND) {
                if (jsonObject.getBoolean("sucs")) {
                    UserProfile profile = new UserProfile();
                    profile.setUserIdentity(jsonObject.getString("uId"));
                    profile.setFirstName(jsonObject.getString("fn"));
                    profile.setLastName(jsonObject.getString("ln"));
                    profile.setGender(jsonObject.getString("gr"));
                    profile.setCountry(jsonObject.getString("cnty"));
                    profile.setMobilePhone(jsonObject.getString("mbl"));
                    profile.setEmail(jsonObject.getString("el"));
                    profile.setPresence(jsonObject.getInt("psnc"));
                    profile.setWhatisInYourMind(jsonObject.getString("wim"));
                    profile.setFriendShipStatus(jsonObject.getInt("frnS"));
                    String imagePath = "";
                    if (jsonObject.has("prIm")) {
                        imagePath = jsonObject.getString("prIm");
                        if (imagePath.length() > 1) {
                            profile.setProfileImage(imagePath);
                        } else {
                            profile.setProfileImage("");
                        }
                    } else {
                        profile.setProfileImage("");
                    }
                    loggedUserProfile.getFriendlist().put(profile.getUserIdentity(), profile);
                    userHomeAction.setRefreshDiv(true);
                    if (userHomeAction.getNewFriendList() == null) {
                        userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                    }
                    userHomeAction.getNewFriendList().add(profile);
                    userHomeAction.setRefreshDiv(true);
                }
            } else if (actn == ServerConstants.TYPE_UPDATE_ACCEPT_FRIEND) {
                if (jsonObject.getBoolean("sucs")) {
                    String fndId = jsonObject.getString("uId");
                    UserProfile friendProfile = loggedUserProfile.getFriendlist().get(fndId);
                    friendProfile.setFirstName(jsonObject.getString("fn"));
                    friendProfile.setLastName(jsonObject.getString("ln"));
                    friendProfile.setGender(jsonObject.getString("gr"));
                    friendProfile.setCountry(jsonObject.getString("cnty"));
                    friendProfile.setMobilePhone(jsonObject.getString("mbl"));
                    friendProfile.setEmail(jsonObject.getString("el"));
                    friendProfile.setPresence(jsonObject.getInt("psnc"));
                    friendProfile.setWhatisInYourMind(jsonObject.getString("wim"));
                    friendProfile.setFriendShipStatus(jsonObject.getInt("frnS"));

                    String imagePath = "";
                    if (jsonObject.has("prIm")) {
                        imagePath = jsonObject.getString("prIm");
                        if (imagePath.length() > 1) {
                            friendProfile.setProfileImage(imagePath);
                        } else {
                            friendProfile.setProfileImage(null);
                        }
                    } else {
                        friendProfile.setProfileImage(null);
                    }
                    userHomeAction.setRefreshDiv(true);
                    if (userHomeAction.getNewFriendList() == null) {
                        userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                    }
                    userHomeAction.getNewFriendList().add(friendProfile);
                    loggedUserProfile.getFriendlist().put(friendProfile.getUserIdentity(), friendProfile);
                    userHomeAction.setRefreshDiv(true);
                }
            } else if (actn == ServerConstants.TYPE_UPDATE_DELETE_FRIEND) {
                String fndId = jsonObject.getString("uId");
                if (loggedUserProfile.getFriendlist().containsKey(fndId)) {
                    loggedUserProfile.getFriendlist().remove(fndId);
                    userHomeAction.setRefreshDiv(true);
                }
                UserProfile friendProfile = new UserProfile();
                friendProfile.setUserIdentity(fndId);
                friendProfile.setDeleted(true);
                if (userHomeAction.getNewFriendList() == null) {
                    userHomeAction.setNewFriendList(new ArrayList<UserProfile>());
                }
                userHomeAction.getNewFriendList().add(friendProfile);
                userHomeAction.setRefreshDiv(true);
            }
        } catch (Exception e) {
        }
    }

//    public boolean checkRefreshDiv() {
//        if (refreshContact) {
//            refreshContact = false;
//            return true;
//        } else {
//            return false;
//        }
//    }
//    public ArrayList<UserProfile> getFriendsUpdate() {
//        ArrayList<UserProfile> friendListJsonStr = null;
////        Gson gson = new Gson();
//        if (newFriendList != null && newFriendList.size() > 0) {
//            friendListJsonStr = newFriendList;
//            newFriendList = new ArrayList<UserProfile>();
//            return friendListJsonStr;
//        }
//        return friendListJsonStr;
//    }
    public boolean checkSearchRefreshDiv() {
        if (refreshSearchContact) {
            refreshSearchContact = false;
            return true;
        } else {
            return false;
        }
    }

    public String getSessionId() {
        return sId;
    }

    public void setSessionId(String sId) {
        this.sId = sId;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getSeachValue() {
        return seachValue;
    }

    public void setSeachValue(String seachValue) {
        this.seachValue = seachValue;
    }

    public ArrayList<SearchFriendDTO> getSearchFriendList() {
        return searchFriendList;
    }

    public void setSearchFriendList(ArrayList<SearchFriendDTO> searchFriendList) {
        this.searchFriendList = searchFriendList;
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public UserHomeAction getUserHomeAction() {
        return userHomeAction;
    }

    public void setUserHomeAction(UserHomeAction userHomeAction) {
        this.userHomeAction = userHomeAction;
    }
}
