/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.friends;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class SearchFriendDTO implements Serializable {

    private String uId;
    private String fn;
    private String ln;
    private String cnty;
    private int psnc;
    private int frnS;

    public SearchFriendDTO() {
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String ln) {
        this.ln = ln;
    }

    public String getCountry() {
        return cnty;
    }

    public void setCountry(String cnty) {
        this.cnty = cnty;
    }

    public int getPresence() {
        return psnc;
    }

    public void setPresence(int psnc) {
        this.psnc = psnc;
    }

    public int getFriendShipStatus() {
        return frnS;
    }

    public void setFriendShipStatus(int frnS) {
        this.frnS = frnS;
    }
}
