package org.ipvision.net;

/**
 * Listener for Transport events.
 */
public interface OnReceiveListner{

    /**
     * When a new response is received.
     */
    public void onReceivedMessage(String msg);
}
