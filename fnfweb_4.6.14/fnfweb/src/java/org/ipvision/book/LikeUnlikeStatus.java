/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.book;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ipvision.utils.ServletResponseDTO;

/**
 *
 * @author user
 */
@WebServlet(name = "LikeUnlikeStatus", urlPatterns = {"/LikeUnlikeStatus"})
public class LikeUnlikeStatus extends HttpServlet {
//{"stId":16,"actn":183,"sId":"1396512223921nazmul","pckId":"1396512263101","cmnId":10}
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       try {
         //  {"stId":16,"actn":184,"sId":"1396513054758nazmul","pckId":"1396513073720"}
           
         BookAction bookAction = (BookAction) request.getSession().getAttribute("bookAction");
          long stId=Long.parseLong(request.getParameter("stId").substring(10));
          int likeUnlike=Integer.parseInt(request.getParameter("likeUnlike"));                  
           String feedBack=bookAction.likeUnlikeStatus(stId,likeUnlike);
           System.out.println(feedBack);
           out.printf(feedBack);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
