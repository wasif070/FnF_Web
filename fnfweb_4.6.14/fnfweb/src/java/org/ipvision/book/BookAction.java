/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.book;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.ipvision.communication.MainBean;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.ServerConstants;
import org.ipvision.utils.ServletResponseDTO;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONException;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "bookAction")
@SessionScoped
public class BookAction implements OnReceiveListner, Serializable {
// AullAuthontication System

    HashMap<String, JSONObject> feedBackMap = new HashMap<String, JSONObject>();
    static Logger logger = Logger.getLogger(BookAction.class.getName());
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
    //End
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    private Map<Long, NewsFeedDTO> newsFeedMap = new ConcurrentHashMap<Long, NewsFeedDTO>();
    private ArrayList<NewsFeedDTO> newList = new ArrayList<NewsFeedDTO>();

    private Map<Long, NewsFeedDTO> myBookMap = new ConcurrentHashMap<Long, NewsFeedDTO>();
    private ArrayList<NewsFeedDTO> myBookList = new ArrayList<NewsFeedDTO>();

    private Map<Long, CommentsDTO> commentsMap = new ConcurrentHashMap<Long, CommentsDTO>();
    private ArrayList<CommentsDTO> commentsList = new ArrayList<CommentsDTO>();

    private Map<String, LikesDTO> likesMap = new ConcurrentHashMap<String, LikesDTO>();
    private ArrayList<LikesDTO> likesList = new ArrayList<LikesDTO>();

    //  private boolean isRefreshDiv = false;
    boolean isComplete = false;
    boolean isComplete_myBook = false;

    String errorMessage = "";
    private String groupSearchValue;
    private String statusMessage;

    private long newsFeedLowestTime = 1597630808096l;
    private long myBookLowestTime = 1597630808096l;

    public BookAction() {
    }

    public void newsFeedRequest(int action) {
        System.out.println("newsFeedRequest");
        newsFeedLowestTime = 1597630808096l;
        newList.clear();
        newsFeedMap.clear();
        myBookList.clear();
        myBookMap.clear();
        commentsMap.clear();
        commentsList.clear();
        feedBackMap.clear();
        likesMap.clear();

        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", action);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

    }

    public void newsFeedRequestWithTime(int actionType) {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", actionType);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            if (actionType == ServerConstants.TYPE_NEWS_FEED) {

                jsonObject.put("tm", newsFeedLowestTime);
            } else {
                jsonObject.put("tm", myBookLowestTime);
            }

            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
            System.out.println("show more clicked");
        } catch (Exception e) {
        }

    }

    // not  in use any longer
    public ArrayList<NewsFeedDTO> getNewsFeedList() {
//        System.out.println("GroupList");
        Map<Long, NewsFeedDTO> groupHashMap = newsFeedMap;
        ArrayList<NewsFeedDTO> list = new ArrayList<NewsFeedDTO>();
        if (groupHashMap != null && groupHashMap.size() > 0) {
            Set set = groupHashMap.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                NewsFeedDTO dto = (NewsFeedDTO) me.getValue();

                list.add(dto);

            }
        }

        return list;
    }

    public String getNewsFeedUpdate(int actionType) {
        String data = "false";
        Gson gson = new Gson();
        ServletResponseDTO servletResponseDTO = new ServletResponseDTO();
        try {

            if (newList.size() > 0 || myBookList.size() > 0) {
                if (actionType == ServerConstants.TYPE_NEWS_FEED) {

                    data = gson.toJson(newList).toString();
                    newList.clear();
                } else if (actionType == ServerConstants.TYPE_MY_BOOK) {
                    data = gson.toJson(myBookList).toString();
                    myBookList.clear();

                }

                servletResponseDTO.setUpdateType(1);
            } else if (commentsList.size() > 0) {

                data = gson.toJson(commentsList).toString();
                commentsList.clear();

                servletResponseDTO.setUpdateType(2);
            } else if (likesList.size() > 0) {

                data = gson.toJson(likesList).toString();
                likesList.clear();

                servletResponseDTO.setUpdateType(3);
            }

            if (data.equals("false")) {
                servletResponseDTO.setMessage(null);
                servletResponseDTO.setSuccess(false);
            } else {
                servletResponseDTO.setMyId(loggedUserProfile.getUserIdentity());
                servletResponseDTO.setMessage(data);
                servletResponseDTO.setSuccess(true);
            }
        } catch (Exception e) {
        }

        return gson.toJson(servletResponseDTO).toString();
    }

    public String addStatus(String statusMessage) {

        String key = "";
        String feedback = "{\"mg\":\"Unable to update Status. please try again.\",\"sucs\":false}";

        try {

            if (statusMessage.length() > 0) {
                key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("actn", ServerConstants.TYPE_ADD_STATUS);
                jsonObject.put("pckId", key);
                jsonObject.put("sId", loggedUserProfile.getSessionId());
                jsonObject.put("sts", statusMessage);
                mainBean.getNotifyThread().notify_or_start_thread();
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
            } else {

                return "";
            }
        } catch (JSONException e) {
        }

        for (int i = 0; i < 50; i++) {
            try {
                Thread.sleep(100);

                if (feedBackMap.containsKey(key)) {
                    System.out.println("checking feedback pkt Id>" + key + key.length());

                    JSONObject jSONObject = feedBackMap.get(key);
                    feedBackMap.remove(key);

                    if (loggedUserProfile.getProfileImage() != null && loggedUserProfile.getProfileImage().length() > 0) {
                        try {
                            jSONObject.put("prIm", loggedUserProfile.getProfileImage());
                            jSONObject.put("uId", loggedUserProfile.getUserIdentity());

                        } catch (Exception e) {
                        }

                    }

                    return jSONObject.toString();
                }
            } catch (InterruptedException ex) {
            }
        }
        return feedback;
    }

    public String addStatusComment(long stId, String cmn) {
//{"stId":16,"actn":181,"sId":"1396510090303nazmul","cmn":"this is a comment","pckId":"1396510637025"}
        String key = "";
        String feedback = "{\"mg\":\"Unable to post commnet.\",\"sucs\":false}";
        try {

            if (cmn.length() > 0 && stId > 0) {
                key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("actn", ServerConstants.TYPE_ADD_STATUS_COMMENT);
                jsonObject.put("stId", stId);
                jsonObject.put("cmn", cmn);
                jsonObject.put("pckId", key);
                jsonObject.put("sId", loggedUserProfile.getSessionId());
                mainBean.getNotifyThread().notify_or_start_thread();
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
                System.out.println("sent>" + jsonObject.toString());

            } else {

                return "";
            }
        } catch (JSONException e) {
        }

        for (int i = 0; i < 50; i++) {
            try {
                Thread.sleep(100);

                if (feedBackMap.containsKey(key)) {
                    System.out.println("checking feedback pkt Id>" + key + key.length());

                    JSONObject jSONObject = feedBackMap.get(key);
                    feedBackMap.remove(key);

                    if (loggedUserProfile.getProfileImage() != null && loggedUserProfile.getProfileImage().length() > 0) {
                        try {
                            jSONObject.put("prIm", loggedUserProfile.getProfileImage());
                            jSONObject.put("uId", loggedUserProfile.getUserIdentity());

                        } catch (Exception e) {
                        }

                    }

                    return jSONObject.toString();
                }
            } catch (InterruptedException ex) {
            }
        }
        return feedback;
    }

    public String deleteStatus(long stId) {

        String key = "";
        String feedback = "{\"mg\":\"Unable to delete.\",\"sucs\":false}";

        try {

            key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_DELETE_STATUS);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("stId", stId);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);

        } catch (JSONException e) {
        }

        for (int i = 0; i < 50; i++) {
            try {
                Thread.sleep(100);

                if (feedBackMap.containsKey(key)) {
                    System.out.println("checking feedback pkt Id>" + key + key.length());

                    JSONObject jSONObject = feedBackMap.get(key);
                    feedBackMap.remove(key);
                    return jSONObject.toString();
                }
            } catch (InterruptedException ex) {
            }
        }
        return feedback;
    }

    public String deleteStatusComment(long stId, long cmnId) {
////{"stId":16,"actn":183,"sId":"1396512223921nazmul","pckId":"1396512263101","cmnId":10}
        String key = "";
        String feedback = "{\"mg\":\"Unable to delete.\",\"sucs\":false}";

        try {
            key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_DELETE_STATUS_COMMENT);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("stId", stId);
            jsonObject.put("cmnId", cmnId);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
            System.out.println("sending>" + jsonObject.toString());
        } catch (JSONException e) {
        }

        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(100);

                if (feedBackMap.containsKey(key)) {
                    System.out.println("checking feedback pkt Id>" + key + key.length());

                    JSONObject jSONObject = feedBackMap.get(key);
                    feedBackMap.remove(key);
                    return jSONObject.toString();
                }
            } catch (InterruptedException ex) {
            }
        }
        return feedback;
    }

    public String fetchComments(long stId) {

        // {"stId":16,"actn":84,"sId":"1396514979687nazmul","pckId":"1396514996418"}
        String key = "";
        String feedback = "{\"mg\":\"Unable to get comments.\",\"sucs\":false}";
        JSONObject jsonObject = new JSONObject();
        try {
            key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            jsonObject.put("actn", ServerConstants.TYPE_COMMENTS_FOR_STATUS);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("stId", stId);

            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), key);
            System.out.println("sending>" + jsonObject.toString());

        } catch (JSONException e) {
        }

        return feedback;
    }

    public String fetchLikes(long stId) {

        // {"stId":16,"actn":84,"sId":"1396514979687nazmul","pckId":"1396514996418"}
        String key = "";
        String feedback = "{\"mg\":\"Unable to get comments.\",\"sucs\":false}";
        JSONObject jsonObject = new JSONObject();
        try {
            key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            jsonObject.put("actn", ServerConstants.TYPE_LIKES_FOR_STATUS);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("stId", stId);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), key);
            System.out.println("sending>" + jsonObject.toString());
        } catch (JSONException e) {
        }

        return feedback;
    }

    public String likeUnlikeStatus(long stId, int likeUnlike) {
        String key = "";

        String feedback = "{\"mg\":\"Unable to update.\",\"sucs\":false}";

        try {
            key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            if (likeUnlike == 1) {
                jsonObject.put("actn", ServerConstants.TYPE_LIKE_STATUS);
            } else {
                jsonObject.put("actn", ServerConstants.TYPE_UNLIKE_STATUS);
            }
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("stId", stId);

            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
            System.out.println("sending>" + jsonObject.toString());
        } catch (JSONException e) {
        }

        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(100);

                if (feedBackMap.containsKey(key)) {
                    System.out.println("checking feedback pkt Id>" + key + key.length());

                    JSONObject jSONObject = feedBackMap.get(key);
                    feedBackMap.remove(key);
                    return jSONObject.toString();
                }
            } catch (InterruptedException ex) {
            }
        }
        return feedback;
    }

    @Override
    public void onReceivedMessage(String msg) {

        try {
//            logger.info("GroupAction===>" + msg.trim());
            JSONObject jsonObject = new JSONObject(msg.trim());
            System.out.println(jsonObject);
            int actn = jsonObject.getInt("actn");
            if (jsonObject.has("sucs") && !jsonObject.getBoolean("sucs")) {
                 if (jsonObject.has("mg")) {
                    errorMessage = jsonObject.getString("mg");
                }
                 return;
             }

                if (actn == ServerConstants.TYPE_NEWS_FEED) {
                    if (jsonObject.has("newsFeedList")) {
                        JSONArray newsFeedArray = jsonObject.getJSONArray("newsFeedList");

                        for (int j = 0; j < newsFeedArray.length(); j++) {
                            JSONObject jGroup = newsFeedArray.getJSONObject(j);
                            NewsFeedDTO newsFeedDTO = new NewsFeedDTO();

                            newsFeedDTO.setStatusId(jGroup.getLong("stId"));
                            newsFeedDTO.setiLike(jGroup.getInt("iLike"));
                            newsFeedDTO.setUserIdentity(jGroup.getString("uId"));

                            if (jGroup.has("fn")) {
                                newsFeedDTO.setFirstName(jGroup.getString("fn"));
                            }
                            if (jGroup.has("ln")) {
                                newsFeedDTO.setLastName(jGroup.getString("ln"));
                            }

                            newsFeedDTO.setTime(jGroup.getLong("tm"));

                            if (newsFeedLowestTime > newsFeedDTO.getTime()) {
                                newsFeedLowestTime = newsFeedDTO.getTime();

                            }

                            newsFeedDTO.setType(jGroup.getInt("type"));
                            newsFeedDTO.setNumberOfComments(jGroup.getInt("nc"));
                            newsFeedDTO.setNumberOfLikes(jGroup.getInt("nl"));
                            if (newsFeedDTO.getType() == 1) {

                                newsFeedDTO.setImageurl(jGroup.getString("iurl"));
                                newsFeedDTO.setAlbumId(jGroup.getString("albId"));
                                newsFeedDTO.setAlbumName(jGroup.getString("albn"));
                                newsFeedDTO.setImageCaption(jGroup.getString("cptn"));
                            } else {
                                newsFeedDTO.setStatus(jGroup.getString("sts"));
                            }

                            try {
                                if (loggedUserProfile.getFriendlist().containsKey(newsFeedDTO.getUserIdentity())) {

                                    UserProfile uProfile = loggedUserProfile.getFriendlist().get(newsFeedDTO.getUserIdentity());

                                    if (uProfile.getProfileImage() != null && !(uProfile.getProfileImage().length() > 0 && uProfile.getProfileImage().contains("clients_image/male.png") || uProfile.getProfileImage().contains("clients_image/female.png"))) {
                                        newsFeedDTO.setProfileImage(uProfile.getProfileImage());
                                    }

                                } else if (newsFeedDTO.getUserIdentity().equals(loggedUserProfile.getUserIdentity())) {

                                    if (loggedUserProfile.getProfileImage() != null && !(loggedUserProfile.getProfileImage().length() > 0 && loggedUserProfile.getProfileImage().contains("clients_image/male.png") || loggedUserProfile.getProfileImage().contains("clients_image/female.png"))) {
                                        newsFeedDTO.setProfileImage(loggedUserProfile.getProfileImage());
                                    }

                                }

                            } catch (Exception e) {
                            }

                            if (!newsFeedMap.containsKey(newsFeedDTO.getStatusId())) {
                                newsFeedMap.put(newsFeedDTO.getStatusId(), newsFeedDTO);

                                newList.add(newsFeedDTO);
                            }
                        }

                    }

                } else if (actn == ServerConstants.TYPE_UPDATE_ADD_STATUS) {

                    NewsFeedDTO newsFeedDTO = new NewsFeedDTO();
                    newsFeedDTO.setIsNew(1);
                    newsFeedDTO.setStatusId(jsonObject.getLong("stId"));
                    newsFeedDTO.setiLike(0);
                    newsFeedDTO.setUserIdentity(jsonObject.getString("uId"));
                    // newsFeedDTO.setFirstName(jsonObject.getString("fn"));
                    // newsFeedDTO.setLastName(jsonObject.getString("ln"));

                    if (jsonObject.has("fn")) {
                        newsFeedDTO.setFirstName(jsonObject.getString("fn"));
                    }
                    if (jsonObject.has("ln")) {
                        newsFeedDTO.setLastName(jsonObject.getString("ln"));
                    }

                    newsFeedDTO.setTime(jsonObject.getLong("tm"));

                    if (newsFeedLowestTime > newsFeedDTO.getTime()) {
                        newsFeedLowestTime = newsFeedDTO.getTime();

                    }

                    newsFeedDTO.setType(jsonObject.getInt("type"));
                    newsFeedDTO.setNumberOfComments(0);
                    newsFeedDTO.setNumberOfLikes(0);
                    if (newsFeedDTO.getType() == 1) {

                        newsFeedDTO.setImageurl(jsonObject.getString("iurl"));
                        newsFeedDTO.setAlbumId(jsonObject.getString("albId"));
                        newsFeedDTO.setAlbumName(jsonObject.getString("albn"));
                        newsFeedDTO.setImageCaption(jsonObject.getString("cptn"));
                    } else {
                        newsFeedDTO.setStatus(jsonObject.getString("sts"));
                    }

                    try {
                        if (loggedUserProfile.getFriendlist().containsKey(newsFeedDTO.getUserIdentity())) {

                            UserProfile uProfile = loggedUserProfile.getFriendlist().get(newsFeedDTO.getUserIdentity());

                            if (uProfile.getProfileImage() != null && !(uProfile.getProfileImage().length() > 0 && uProfile.getProfileImage().contains("clients_image/male.png") || uProfile.getProfileImage().contains("clients_image/female.png"))) {
                                newsFeedDTO.setProfileImage(uProfile.getProfileImage());

                            }

                            newsFeedDTO.setFirstName(uProfile.getFirstName());
                            newsFeedDTO.setLastName(uProfile.getLastName());

                        } else if (newsFeedDTO.getUserIdentity().equals(loggedUserProfile.getUserIdentity())) {
                            if (loggedUserProfile.getProfileImage() != null && !(loggedUserProfile.getProfileImage().length() > 0 && loggedUserProfile.getProfileImage().contains("clients_image/male.png") || loggedUserProfile.getProfileImage().contains("clients_image/female.png"))) {
                                newsFeedDTO.setProfileImage(loggedUserProfile.getProfileImage());

                            }
                            newsFeedDTO.setFirstName(loggedUserProfile.getFirstName());
                            newsFeedDTO.setLastName(loggedUserProfile.getLastName());

                        }

                    } catch (Exception e) {
                    }

                    if (!newsFeedMap.containsKey(newsFeedDTO.getStatusId())) {
                        newsFeedMap.put(newsFeedDTO.getStatusId(), newsFeedDTO);

                        newList.add(newsFeedDTO);
                    }

                } else if (actn == ServerConstants.TYPE_UPDATE_DELETE_STATUS) {
                    NewsFeedDTO newsFeedDTO = new NewsFeedDTO();
                    newsFeedDTO.setDeleted(true);
                    newsFeedDTO.setStatusId(jsonObject.getLong("stId"));
                    //newsFeedDTO.setUserIdentity(jsonObject.getString("uId"));
                    newsFeedMap.remove(newsFeedDTO.getStatusId());
                    newList.add(newsFeedDTO);
                } else if (actn == ServerConstants.TYPE_MY_BOOK) {
                    if (jsonObject.has("newsFeedList")) {
                        JSONArray newsFeedArray = jsonObject.getJSONArray("newsFeedList");

                        for (int j = 0; j < newsFeedArray.length(); j++) {
                            JSONObject jGroup = newsFeedArray.getJSONObject(j);
                            NewsFeedDTO newsFeedDTO = new NewsFeedDTO();
                            newsFeedDTO.setStatusId(jGroup.getLong("stId"));
                            newsFeedDTO.setiLike(jGroup.getInt("iLike"));
                            newsFeedDTO.setUserIdentity(loggedUserProfile.getUserIdentity());
                            newsFeedDTO.setFirstName(loggedUserProfile.getFirstName());
                            newsFeedDTO.setLastName(loggedUserProfile.getLastName());
                            newsFeedDTO.setTime(jGroup.getLong("tm"));
                            if (myBookLowestTime > newsFeedDTO.getTime()) {
                                myBookLowestTime = newsFeedDTO.getTime();
                            }
                            newsFeedDTO.setType(jGroup.getInt("type"));
                            newsFeedDTO.setNumberOfComments(jGroup.getInt("nc"));
                            newsFeedDTO.setNumberOfLikes(jGroup.getInt("nl"));
                            if (newsFeedDTO.getType() == 1) {
                                newsFeedDTO.setImageurl(jGroup.getString("iurl"));
                                newsFeedDTO.setAlbumId(jGroup.getString("albId"));
                                newsFeedDTO.setAlbumName(jGroup.getString("albn"));
                                newsFeedDTO.setImageCaption(jGroup.getString("cptn"));
                            } else {
                                newsFeedDTO.setStatus(jGroup.getString("sts"));
                            }
                            if (loggedUserProfile.getProfileImage() != null && !(loggedUserProfile.getProfileImage().length() > 0 && loggedUserProfile.getProfileImage().contains("clients_image/male.png") || loggedUserProfile.getProfileImage().contains("clients_image/female.png"))) {
                                newsFeedDTO.setProfileImage(loggedUserProfile.getProfileImage());
                            }
                            if (!myBookMap.containsKey(newsFeedDTO.getStatusId())) {
                                myBookMap.put(newsFeedDTO.getStatusId(), newsFeedDTO);
                                myBookList.add(newsFeedDTO);
                            }
                        }

                    }

                } else if (actn == ServerConstants.TYPE_COMMENTS_FOR_STATUS) {
                    if (jsonObject.has("comments")) {
                        long stId = jsonObject.getLong("stId");
                        System.out.println(jsonObject.toString());
                        JSONArray commentsArray = jsonObject.getJSONArray("comments");

                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject comment = commentsArray.getJSONObject(j);
                            CommentsDTO commentsDTO = new CommentsDTO();
                            commentsDTO.setStatusId(stId);
                            commentsDTO.setCommentId(comment.getLong("cmnId"));
                            commentsDTO.setComment(comment.getString("cmn"));
                            commentsDTO.setUserIdentity(comment.getString("uId"));

                            if (comment.has("fn")) {
                                commentsDTO.setFirstName(comment.getString("fn"));
                            }
                            if (comment.has("ln")) {
                                commentsDTO.setLastName(comment.getString("ln"));
                            }

                            commentsDTO.setTime(comment.getLong("tm"));

                            try {
                                if (loggedUserProfile.getFriendlist().containsKey(commentsDTO.getUserIdentity())) {

                                    UserProfile uProfile = loggedUserProfile.getFriendlist().get(commentsDTO.getUserIdentity());

                                    if (uProfile.getProfileImage() != null && !(uProfile.getProfileImage().length() > 0 && uProfile.getProfileImage().contains("clients_image/male.png") || uProfile.getProfileImage().contains("clients_image/female.png"))) {
                                        commentsDTO.setProfileImage(uProfile.getProfileImage());
                                    }

                                } else if (commentsDTO.getUserIdentity().equals(loggedUserProfile.getUserIdentity())) {

                                    if (loggedUserProfile.getProfileImage() != null && !(loggedUserProfile.getProfileImage().length() > 0 && loggedUserProfile.getProfileImage().contains("clients_image/male.png") || loggedUserProfile.getProfileImage().contains("clients_image/female.png"))) {
                                        commentsDTO.setProfileImage(loggedUserProfile.getProfileImage());
                                    }

                                }

                            } catch (Exception e) {
                            }

                            if (!commentsMap.containsKey(commentsDTO.getCommentId())) {
                                commentsMap.put(commentsDTO.getCommentId(), commentsDTO);
                                commentsList.add(commentsDTO);
                            }
                        }

                    }

                } else if (actn == ServerConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT) {

                    CommentsDTO commentsDTO = new CommentsDTO();
                    commentsDTO.setDeleted(true);
                    commentsDTO.setStatusId(jsonObject.getLong("stId"));
                    commentsDTO.setCommentId(jsonObject.getLong("cmnId"));
                    commentsMap.remove(commentsDTO.getCommentId());
                        commentsList.add(commentsDTO);
                    

                } else if (actn == ServerConstants.TYPE_UPDATE_ADD_STATUS_COMMENT) {

                    CommentsDTO commentsDTO = new CommentsDTO();
                    commentsDTO.setIsNew(1);
                    commentsDTO.setStatusId(jsonObject.getLong("stId"));
                    commentsDTO.setCommentId(jsonObject.getLong("cmnId"));
                    commentsDTO.setComment(jsonObject.getString("cmn"));
                    commentsDTO.setUserIdentity(jsonObject.getString("uId"));
                    if (jsonObject.has("fn")) {
                        commentsDTO.setFirstName(jsonObject.getString("fn"));
                    }
                    if (jsonObject.has("ln")) {
                        commentsDTO.setLastName(jsonObject.getString("ln"));
                    }
                    commentsDTO.setTime(jsonObject.getLong("tm"));

                    try {
                        if (loggedUserProfile.getFriendlist().containsKey(commentsDTO.getUserIdentity())) {
                            UserProfile uProfile = loggedUserProfile.getFriendlist().get(commentsDTO.getUserIdentity());
                            if (uProfile.getProfileImage() != null && !(uProfile.getProfileImage().length() > 0 && uProfile.getProfileImage().contains("clients_image/male.png") || uProfile.getProfileImage().contains("clients_image/female.png"))) {
                                commentsDTO.setProfileImage(uProfile.getProfileImage());
                            }
                            commentsDTO.setFirstName(uProfile.getFirstName());
                            commentsDTO.setLastName(uProfile.getLastName());

                        } else if (commentsDTO.getUserIdentity().equals(loggedUserProfile.getUserIdentity())) {
                            if (loggedUserProfile.getProfileImage() != null && !(loggedUserProfile.getProfileImage().length() > 0 && loggedUserProfile.getProfileImage().contains("clients_image/male.png") || loggedUserProfile.getProfileImage().contains("clients_image/female.png"))) {
                                commentsDTO.setProfileImage(loggedUserProfile.getProfileImage());
                            }

                            commentsDTO.setFirstName(loggedUserProfile.getFirstName());
                            commentsDTO.setLastName(loggedUserProfile.getLastName());

                        }
                    } catch (Exception e) {
                    }
                    if (!commentsMap.containsKey(commentsDTO.getCommentId())) {
                        commentsMap.put(commentsDTO.getCommentId(), commentsDTO);
                        commentsList.add(commentsDTO);
                    }

                } else if (actn == ServerConstants.TYPE_LIKES_FOR_STATUS) {
                    if (jsonObject.has("likes")) {
                        long stId = jsonObject.getLong("stId");
                        System.out.println(jsonObject.toString());
                        JSONArray commentsArray = jsonObject.getJSONArray("likes");

                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject comment = commentsArray.getJSONObject(j);
                            LikesDTO likesDTO = new LikesDTO();
                            likesDTO.setStatusId(stId);
                            likesDTO.setUserIdentity(comment.getString("uId"));
                            if (jsonObject.has("fn")) {
                                likesDTO.setFirstName(jsonObject.getString("fn"));
                            }
                            if (jsonObject.has("ln")) {
                                likesDTO.setLastName(jsonObject.getString("ln"));
                            }

                            String mapkey = stId + likesDTO.getUserIdentity();
                            if (!likesMap.containsKey(mapkey)) {
                                likesMap.put(mapkey, likesDTO);
                                likesList.add(likesDTO);

                            }
                        }

                    }

                } else if (actn == ServerConstants.TYPE_UPDATE_LIKE_STATUS) {

                    LikesDTO likesDTO = new LikesDTO();
                    likesDTO.setIsNew(1);
                    likesDTO.setStatusId(jsonObject.getLong("stId"));
                    likesDTO.setUserIdentity(jsonObject.getString("uId"));

                    if (jsonObject.has("fn")) {
                        likesDTO.setFirstName(jsonObject.getString("fn"));
                    }
                    if (jsonObject.has("ln")) {
                        likesDTO.setLastName(jsonObject.getString("ln"));
                    }
                    String mapkey = likesDTO.getStatusId() + likesDTO.getUserIdentity();
                    if (!likesMap.containsKey(mapkey)) {
                        likesMap.put(mapkey, likesDTO);
                        likesList.add(likesDTO);

                    }

                } else if (actn == ServerConstants.TYPE_UPDATE_UNLIKE_STATUS) {
                    LikesDTO likesDTO = new LikesDTO();
                    likesDTO.setDeleted(true);
                    likesDTO.setStatusId(jsonObject.getLong("stId"));
                    likesDTO.setUserIdentity(jsonObject.getString("uId"));

                    String mapkey = likesDTO.getStatusId() + likesDTO.getUserIdentity();
                        likesMap.remove(mapkey);
                        likesList.add(likesDTO);
                    
                } else if (actn > 176 && actn < 188) {
                    feedBackMap.put(jsonObject.getString("pckId"), jsonObject);
                    System.out.println("set to feedback pkt Id>" + jsonObject.getString("pckId") + jsonObject.getString("pckId").length());
                }
//{"actn":92,"pckFs":4260,"seq":"1/2","likes":[{"uId":"toruFinal","fn":"Torongo","ln":"Azad"},{"uId":"toruFinal","fn":"Torongo","ln":"Azad"}],"sucs":true,"stId":627} 
           
        } catch (Exception e) {
//            System.out.println("GroupAction==>" + e.toString());
        }
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getGroupSearchValue() {
        return groupSearchValue;
    }

    public void setGroupSearchValue(String groupSearchValue) {
        this.groupSearchValue = groupSearchValue;
    }

    public long getNewsFeedLowestTime() {
        return newsFeedLowestTime;
    }

    public void setNewsFeedLowestTime(long newsFeedLowestTime) {
        this.newsFeedLowestTime = newsFeedLowestTime;
    }

    public Map<Long, NewsFeedDTO> getNewsFeedMap() {
        return newsFeedMap;
    }

    public void setNewsFeedMap(Map<Long, NewsFeedDTO> newsFeedMap) {
        this.newsFeedMap = newsFeedMap;
    }

    public ArrayList<NewsFeedDTO> getNewList() {
        return newList;
    }

    public void setNewList(ArrayList<NewsFeedDTO> newList) {
        this.newList = newList;
    }

    public HashMap<String, JSONObject> getFeedBackMap() {
        return feedBackMap;
    }

    public void setFeedBackMap(HashMap<String, JSONObject> feedBackMap) {
        this.feedBackMap = feedBackMap;
    }

    public Map<Long, CommentsDTO> getCommentsMap() {
        return commentsMap;
    }

    public void setCommentsMap(Map<Long, CommentsDTO> commentsMap) {
        this.commentsMap = commentsMap;
    }

    public ArrayList<CommentsDTO> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(ArrayList<CommentsDTO> commentsList) {
        this.commentsList = commentsList;
    }

    public boolean isIsComplete() {
        return isComplete;
    }

    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isIsComplete_myBook() {
        return isComplete_myBook;
    }

    public void setIsComplete_myBook(boolean isComplete_myBook) {
        this.isComplete_myBook = isComplete_myBook;
    }

    public long getMyBookLowestTime() {
        return myBookLowestTime;
    }

    public void setMyBookLowestTime(long myBookLowestTime) {
        this.myBookLowestTime = myBookLowestTime;
    }

}
