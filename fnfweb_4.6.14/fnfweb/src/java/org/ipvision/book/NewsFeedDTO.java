/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.book;

/**
 *
 * @author user
 */
public class NewsFeedDTO {
    private Integer actn;
    private Long stId;
    private String uId;  //uId
    private String sts;
    private String iurl;
    private String albId;
    private String albn;
    private String cptn;
    private Long tm;
    private int type;  //news stype
    private int nc;
    private int nl;
    private Integer iLike;
    private String pckId;
    private Boolean sucs = true;
    private Long pckFs;
    private String mg;
    private String fn;
    private String ln;
    private String prIm;
    private int isNew;
    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }
    
    
       public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String prIm) {
        this.prIm = prIm;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String firstName) {
        this.fn = firstName;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String lastName) {
        this.ln = lastName;
    }

    public Integer getiLike() {
        return iLike;
    }

    public void setiLike(Integer iLike) {
        this.iLike = iLike;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public Long getPacketIdFromServer() {
        return pckFs;
    }

    public void setPacketIdFromServer(Long packetIdFromServer) {
        this.pckFs = packetIdFromServer;
    }

    public Long getStatusId() {
        return stId;
    }

    public void setStatusId(Long statusId) {
        this.stId = statusId;
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean success) {
        this.sucs = success;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public Integer getAction() {
        return actn;
    }

    public void setAction(Integer actn) {
        this.actn = actn;
    }

    public int getNoOfComments() {
        return nc;
    }

    public void setNumberOfComments(int noOfComments) {
        this.nc = noOfComments;
    }

    public int getNoOfLikes() {
        return nl;
    }

    public void setNumberOfLikes(int noOfLikes) {
        this.nl = noOfLikes;
    }

    public String getAlbumId() {
        return albId;
    }

    public void setAlbumId(String albumId) {
        this.albId = albumId;
    }

    public String getAlbumName() {
        return albn;
    }

    public void setAlbumName(String albumName) {
        this.albn = albumName;
    }

    public String getImageCaption() {
        return cptn;
    }

    public void setImageCaption(String imageCaption) {
        this.cptn = imageCaption;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getStatus() {
        return sts;
    }

    public void setStatus(String status) {
        this.sts = status;
    }

    public String getImageurl() {
        return iurl;
    }

    public void setImageurl(String imageurl) {
        this.iurl = imageurl;
    }

    public Long getTime() {
        return tm;
    }

    public void setTime(Long time) {
        this.tm = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
