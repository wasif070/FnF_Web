/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.book;

/**
 *
 * @author user
 */
public class CommentsDTO {

    private Long cmnId;
    private Long stId;

   
    private String cmn;
    private Long tm;


    private String uId;
    private String fn;
    private String ln;
 private String prIm;
 
 private int isNew;
private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }
 
 
       public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String prIm) {
        this.prIm = prIm;
    }
    public Long getStatusId() {
        return stId;
    }

    public void setStatusId(Long stId) {
        this.stId = stId;
    }
    
    
  public Long getCommentId() {
        return cmnId;
    }

    public void setCommentId(Long commentId) {
        this.cmnId = commentId;
    }

    public Long getTime() {
        return tm;
    }

    public void setTime(Long time) {
        this.tm = time;
    }
    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String firstName) {
        this.fn = firstName;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String lastName) {
        this.ln = lastName;
    }

    public String getComment() {
        return cmn;
    }

    public void setComment(String comment) {
        this.cmn = comment;
    }

}
