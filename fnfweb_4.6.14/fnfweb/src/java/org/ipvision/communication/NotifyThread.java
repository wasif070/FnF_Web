/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author user
 */
@ManagedBean(name = "notifyThread")
@SessionScoped
public class NotifyThread implements Serializable {

    @ManagedProperty(value = "#{serverCommunication}")
    private ServerCommunication serverCommunication;
    public ResendPacketThread resendPacketThread = null;

    public void notify_or_start_thread() {
        if (resendPacketThread == null) {
            resendPacketThread = new ResendPacketThread();
            resendPacketThread.setServerCommunication(serverCommunication);
            resendPacketThread.start();
        }

        resendPacketThread.setCounter(1);
        if (resendPacketThread.getState() == Thread.State.WAITING) {
            synchronized (resendPacketThread) {
                resendPacketThread.notify();
            }
        }
    }

    public void stopService() {
        if (resendPacketThread != null) {
            resendPacketThread.stopService();
        }
    }

    public ServerCommunication getServerCommunication() {
        return serverCommunication;
    }

    public void setServerCommunication(ServerCommunication serverCommunication) {
        this.serverCommunication = serverCommunication;
    }
}
