/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author user
 */
@ManagedBean(name = "resendPacketThread")
@SessionScoped
public class ResendPacketThread extends Thread implements Serializable {

    @ManagedProperty(value = "#{serverCommunication}")
    private ServerCommunication serverCommunication;
    int counter = 0;
    ResendPacketDTO finalPacket;
 
    
    boolean running = true;

    @Override
    public void run() {
        while (running) {
            try {
                if (!serverCommunication.getSendPacketStorage().isEmpty()) {
                    for (String key : serverCommunication.getSendPacketStorage().keySet()) {
                        finalPacket = serverCommunication.getSendPacketStorage().get(key);
                        if(finalPacket.getSentCount()<20){
                        
                        if (serverCommunication.serverSocket != null && finalPacket != null) {
                            serverCommunication.serverSocket.send(finalPacket.getPacket());
                            System.out.println("resending>"+new String (finalPacket.getPacket().getData()));
                        }}else{
                        serverCommunication.getSendPacketStorage().remove(key);
                        
                        }

                    }
                }
                Thread.sleep(1000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopService() {
        running = false;
    }

    public ServerCommunication getServerCommunication() {
        return serverCommunication;
    }

    public void setServerCommunication(ServerCommunication serverCommunication) {
        this.serverCommunication = serverCommunication;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
