/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.ipvision.utils.ServerConstants;

/**
 *
 * @author user
 */
@ManagedBean(name = "serverCommunication")
@SessionScoped
public class ServerCommunication implements Serializable {

    static Logger logger = Logger.getLogger(ServerCommunication.class.getName());
    private Map<String, ResendPacketDTO> sendPacketStorage = new ConcurrentHashMap<String, ResendPacketDTO>();

    private Map<String, String> receivedPacketStorage = new ConcurrentHashMap<String, String>();

    DatagramSocket serverSocket;
    // DatagramPacket finalPacket;
    String browserInitialize;

    public ServerCommunication() {
    }

    public Map<String, ResendPacketDTO> getSendPacketStorage() {
        return sendPacketStorage;
    }

    public void setSendPacketStorage(Map<String, ResendPacketDTO> sendPacketStorage) {
        this.sendPacketStorage = sendPacketStorage;
    }

    public DatagramSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(DatagramSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

//    public DatagramPacket getFinalPacket() {
//        return finalPacket;
//    }
//
//    public void setFinalPacket(DatagramPacket finalPacket) {
//        this.finalPacket = finalPacket;
//    }
    
    
        public void sendRequestPacket(String dataPacket, String pckId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.REQUEST_PORT);

            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setSendingTime(System.currentTimeMillis());
            sendPacketStorage.put(pckId, resendPacketDTO);
            serverSocket.send(finalPacket);
            System.out.println(new String(finalPacket.getData()));
        } catch (UnknownHostException e) {
            logger.info("sendRequestPacket: " + e);
        } catch (IOException e) {
            logger.info("sendRequestPacket: " + e);
        }
    }
    
    
    public void sendChatRequestPacket(String dataPacket, String pckId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.CHAT_PORT);

            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setSendingTime(System.currentTimeMillis());
            sendPacketStorage.put(pckId, resendPacketDTO);
            serverSocket.send(finalPacket);
            System.out.println(new String(finalPacket.getData()));
        } catch (UnknownHostException e) {
            logger.info("sendRequestPacket: " + e);
        } catch (IOException e) {
            logger.info("sendRequestPacket: " + e);
        }
    }

    public void sendUpdatePacket(String dataPacket, String pckId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.UPDATE_PORT);
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setSendingTime(System.currentTimeMillis());
            sendPacketStorage.put(pckId, resendPacketDTO);
            serverSocket.send(finalPacket);
        } catch (UnknownHostException e) {
            logger.info("sendUpdatePacket: " + e);
        } catch (IOException e) {
            logger.info("sendUpdatePacket: " + e);
        }
    }

    public void sendPacketConfirmation(String dataPacket) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.CONFIRMATION_PORT);
//            sendPacketStorage.put(pckId, finalPacket);
            serverSocket.send(finalPacket);
            System.out.println(serverIP+":"+ServerConstants.CONFIRMATION_PORT+">"+new String (finalPacket.getData()));
        } catch (UnknownHostException e) {
            logger.info("sendPacketConfirmation: " + e);
        } catch (IOException e) {
            logger.info("sendPacketConfirmation: " + e);
        }
    }

    public void sendCallingPacket(String dataPacket, String pckId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.CALLING_PORT);
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setSendingTime(System.currentTimeMillis());
            sendPacketStorage.put(pckId, resendPacketDTO);
            serverSocket.send(finalPacket);

        } catch (UnknownHostException e) {
            logger.info("sendCallingPacket: " + e);
        } catch (IOException e) {
            logger.info("sendCallingPacket: " + e);
        }
    }

    public void sendKeepAlivePacket(String sId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = sId.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.KEEP_ALIVE_PORT);
//            sendPacketStorage.put(pckId, finalPacket);
            serverSocket.send(finalPacket);

        } catch (UnknownHostException e) {
            logger.info("sendKeepAlivePacket: " + e);
        } catch (IOException e) {
            logger.info("sendKeepAlivePacket: " + e);
        }
    }

    
        public void sendAuthenticationPacket(String packet,String pId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = packet.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.AUTHENTICATION_PORT);
//            sendPacketStorage.put(pckId, finalPacket);
            serverSocket.send(finalPacket);

        } catch (UnknownHostException e) {
            logger.info("sendKeepAlivePacket: " + e);
        } catch (IOException e) {
            logger.info("sendKeepAlivePacket: " + e);
        }
    }
    
    public void sendSMSPacket(String dataPacket, String pckId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(ServerConstants.AUTH_SERVER_IP);
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, ServerConstants.SMS_SERVER_PORT);
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setSendingTime(System.currentTimeMillis());
            sendPacketStorage.put(pckId, resendPacketDTO);
            serverSocket.send(finalPacket);
        } catch (UnknownHostException e) {
            logger.info("sendSMSPacket: " + e);
        } catch (IOException e) {
            logger.info("sendSMSPacket: " + e);
        }
    }

    public String getBrowserInitialize() {
        return browserInitialize;
    }

    public void setBrowserInitialize(String browserInitialize) {
        this.browserInitialize = browserInitialize;
    }

    public Map<String, String> getReceivedPacketStorage() {
        return receivedPacketStorage;
    }

    public void setReceivedPacketStorage(Map<String, String> receivedPacketStorage) {
        this.receivedPacketStorage = receivedPacketStorage;
    }

}
