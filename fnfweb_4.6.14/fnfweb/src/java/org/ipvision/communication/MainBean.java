/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.SocketException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author user
 */
@ManagedBean(name = "mainBean")
@SessionScoped
public class MainBean implements Serializable {
    // AullAuthontication System

    @ManagedProperty(value = "#{serverCommunication}")
    private ServerCommunication serverCommunication;
    @ManagedProperty(value = "#{communicationProvider}")
    private CommunicationProvider communicationProvider;
    @ManagedProperty(value = "#{notifyThread}")
    NotifyThread notifyThread;
    //End
    String browserInitialize;
    static Logger logger = Logger.getLogger(MainBean.class.getName());

    public void setCommunicationSocket() {
        try {
            serverCommunication.setServerSocket(new DatagramSocket());
        } catch (SocketException se) {
            logger.info("MainBean-->", se);
        } catch (IOException e) {
            logger.info("MainBean-->", e);
        }
        communicationProvider.setServerCommunication(serverCommunication);
        try {
            communicationProvider.start();
        } catch (Exception e) {
            logger.info("MainBean-->", e);
        }

        notifyThread.setServerCommunication(serverCommunication);
    }

    public void destroySession() {
        FacesContext sess = FacesContext.getCurrentInstance();
        if (sess != null) {
            HttpServletRequest ss = (HttpServletRequest) sess.getExternalContext().getRequest();
            HttpSession sss = ss.getSession(false);
            sss.invalidate();
        }
    }

    public ServerCommunication getServerCommunication() {
        return serverCommunication;
    }

    public void setServerCommunication(ServerCommunication serverCommunication) {
        this.serverCommunication = serverCommunication;
    }

    public CommunicationProvider getCommunicationProvider() {
        return communicationProvider;
    }

    public void setCommunicationProvider(CommunicationProvider communicationProvider) {
        this.communicationProvider = communicationProvider;
    }

    public NotifyThread getNotifyThread() {
        return notifyThread;
    }

    public void setNotifyThread(NotifyThread notifyThread) {
        this.notifyThread = notifyThread;
    }

    public String getBrowserInitialize() {
        return browserInitialize;
    }

    public void setBrowserInitialize(String browserInitialize) {
        this.browserInitialize = browserInitialize;
    }
}
