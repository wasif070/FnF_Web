/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.Serializable;
import java.net.DatagramPacket;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "keepAliveThread")
@SessionScoped
public class KeepAliveThread extends Thread implements Serializable {

    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
    DatagramPacket finalPacket;
    boolean running = false;
    private String sId;
    long currentLoginTime = System.currentTimeMillis();

    @Override
    public void run() {
        while (running) {
            long diff = System.currentTimeMillis() - currentLoginTime;
            if (diff < 20000) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("actn", ServerConstants.TYPE_KEEP_ALIVE);
                    jsonObject.put("sId", getSessionId());
                    mainBean.getServerCommunication().sendKeepAlivePacket(getSessionId());
                } catch (Exception e) {
//                System.out.println("KeepAliveException:==>" + e.toString());
                }
            } else if (diff > (45 * 1000 * 60)) {
                running = false;
            }
            try {
                Thread.sleep(30000);
            } catch (Exception e) {
            }
        }
    }

    public void stopMe() {
        running = false;
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public String getSessionId() {
        return sId;
    }

    public void setSessionId(String sId) {
        this.sId = sId;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public long getCurrentLoginTime() {
        return currentLoginTime;
    }

    public void setCurrentLoginTime(long currentLoginTime) {
        this.currentLoginTime = currentLoginTime;
    }
}
