/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ipvision.communication;

import java.net.DatagramPacket;

/**
 *
 * @author user
 */
public class ResendPacketDTO {
   private DatagramPacket packet;
   private int sentCount;
   private long sendingTime;

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setPacket(DatagramPacket packet) {
        this.packet = packet;
    }

    public int getSentCount() {
        return sentCount++;
    }

    public void setSentCount(int sentCount) {
        this.sentCount = sentCount;
    }

    public long getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(long sendingTime) {
        this.sendingTime = sendingTime;
    }
   
}
