/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.communication;

import java.io.Serializable;
import java.net.DatagramPacket;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "communicationProvider")
@SessionScoped
public class CommunicationProvider extends Thread implements Serializable {

    static Logger logger = Logger.getLogger(CommunicationProvider.class.getName());
    OnReceiveListner onReceiveListner;
    @ManagedProperty(value = "#{serverCommunication}")
    private ServerCommunication serverCommunication;
    String browserInitialize;
    String sId;
    volatile boolean running = true;

    public CommunicationProvider() {
    }

    public void stopService() {
        running = false;
        try {
            Thread.sleep(1000L);
        } catch (Exception e) {
        }

        try {
            if (serverCommunication.getServerSocket() != null) {
//                logger.info("Socket: attempt to close");
                serverCommunication.getServerSocket().close();
//                logger.info("Socket: closeed");
            }
        } catch (Exception e) {
            logger.info("Socket Exception: " + e);
        }
    }

    @Override
    public void run() {
        logger.info("CommunicationProvider Started");
        while (running) {
            try {
                byte[] buf = new byte[ServerConstants.BUFFER_SIZE];
                DatagramPacket incomingPacket = new DatagramPacket(buf, buf.length);
                serverCommunication.getServerSocket().receive(incomingPacket);
                buf = incomingPacket.getData();
                String receiveString = new String(buf);
                logger.info("Received MSG-->" + receiveString.trim());
                System.out.println("Received MSG-->" + receiveString.trim());

                JSONObject jsonObject = new JSONObject(receiveString.trim());

                if (jsonObject.has("pckFs")) {
                    sendConfirmationMessage(jsonObject.getString("pckFs"));

                }

                if (jsonObject.has("actn")) {
                    int actn = jsonObject.getInt("actn");
                    if (actn < 200) {
                        Map<String, ResendPacketDTO> sendPacketStorage = serverCommunication.getSendPacketStorage();
                        if (jsonObject.has("pckId")) {
                            String key = jsonObject.getString("pckId");
                            if (sendPacketStorage.containsKey(key)) {
                                serverCommunication.getSendPacketStorage().remove(key);
                            }
                        }
                        if (jsonObject.has("pckFs")) {
                            if (serverCommunication.getReceivedPacketStorage().containsKey(jsonObject.getString("pckFs"))) {
                                continue;
                            } else {
                                serverCommunication.getReceivedPacketStorage().put(jsonObject.getString("pckFs"), "");
                            }
                        }
                        onReceiveListner.onReceivedMessage(receiveString);
                    } else if (actn == ServerConstants.TYPE_CONFIRMATION) {
                        String key = jsonObject.getString("pckId");
                        if (serverCommunication.getSendPacketStorage().containsKey(key)) {
                            serverCommunication.getSendPacketStorage().remove(key);
                        }
                    } else if (actn < 400) {

                        // here
                        if (actn == ServerConstants.TYPE_UPDATE_ADD_STATUS_COMMENT
                                || actn == ServerConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT
                                || actn == ServerConstants.TYPE_UPDATE_LIKE_STATUS
                                || actn == ServerConstants.TYPE_UPDATE_UNLIKE_STATUS) {
                            if (jsonObject.has("pckFs")) {
                                if (serverCommunication.getReceivedPacketStorage().containsKey(jsonObject.getString("pckFs"))) {
                                    continue;
                                } else {
                                    serverCommunication.getReceivedPacketStorage().put(jsonObject.getString("pckFs"), "");
                                }
                            }
                        }
                        onReceiveListner.onReceivedMessage(receiveString);

                    }
                }
            } catch (Exception e) {
                logger.info("System Error==>" + e.toString());
            }
        }

    }

    private void sendConfirmationMessage(String pckFs) {
        try {
            serverCommunication.sendPacketConfirmation(pckFs + "," + getSessionId());
        } catch (Exception e) {
            logger.info("CommunicationProvider sendConfirmationMessage - Exception : " + e.toString());
        }
    }

    public ServerCommunication getServerCommunication() {
        return serverCommunication;
    }

    public void setServerCommunication(ServerCommunication serverCommunication) {
        this.serverCommunication = serverCommunication;
    }

    public String getSessionId() {
        return sId;
    }

    public void setSessionId(String sId) {
        this.sId = sId;
    }

    public OnReceiveListner getOnReceiveListner() {
        return onReceiveListner;
    }

    public void setOnReceiveListner(OnReceiveListner onReceiveListner) {
        this.onReceiveListner = onReceiveListner;
    }

    public String getBrowserInitialize() {
        return browserInitialize;
    }

    public void setBrowserInitialize(String browserInitialize) {
        this.browserInitialize = browserInitialize;
    }
}
