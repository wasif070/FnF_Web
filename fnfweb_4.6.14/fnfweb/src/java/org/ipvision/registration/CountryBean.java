/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.registration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author user
 */
@ManagedBean(name = "countrybean")
@SessionScoped
public class CountryBean implements Serializable {

   

    public CountryBean() {
        super();
    }
 String country_code = "";
    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    private static Map<String, String> countries_code = new HashMap<String, String>();
    private static ArrayList<String> countries_list = new ArrayList<String>();
    private String localeCode = "";

   

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public void countryLocaleCodeChanged(ValueChangeEvent e) {
        //assign new value to localeCode
        localeCode = e.getNewValue().toString();
    }

    public Map<String, String> getCountryInMap() {
        return this.countries_code;
    }

    public ArrayList<String> getCountries_list() {
        for (String key : countries_code.keySet()) {
            countries_list.add(key);
        }
        return countries_list;
    }

    public static void setCountries_list(ArrayList<String> countries_list) {
        CountryBean.countries_list = countries_list;
    }

    public String getCountryCode(String countryName) {
        String ccode = countries_code.get(countryName);
        setCountry_code(ccode);
        return ccode;
    }

    static {
        countries_code = new LinkedHashMap<String, String>();
        countries_code.put("Afghanistan", "+93"); //label, value
        countries_code.put("Albania", "+355");
        countries_code.put("Algeria", "+213");
        countries_code.put("American Samoa", "+1684");
        countries_code.put("Andorra", "+376");
        countries_code.put("Angola", "+244");
        countries_code.put("Anguilla", "+1264");
        countries_code.put("Antarctica", "+672");
        countries_code.put("Antigua and Barbuda", "+1268");
        countries_code.put("Argentina", "+54");
        countries_code.put("Armenia", "+374");
        countries_code.put("Aruba", "+297");
        countries_code.put("Australia", "+61");
        countries_code.put("Austria", "+43");
        countries_code.put("Azerbaijan", "+994");
        countries_code.put("Bahamas, The", "+1242");
        countries_code.put("Bahrain", "+973");
        countries_code.put("Bangladesh", "+880");
        countries_code.put("Barbados", "+1246");
        countries_code.put("Belarus", "+375");
        countries_code.put("Belgium", "+32");
        countries_code.put("Belize", "+501");
        countries_code.put("Benin", "+229");
        countries_code.put("Bermuda", "+1441");
        countries_code.put("Bhutan", "+975");
        countries_code.put("Bolivia", "+591");
        countries_code.put("Bonaire, Sint Eustatius...", "+599");
        countries_code.put("Bosnia and Herzegovina", "+387");
        countries_code.put("Botswana", "+267");
        countries_code.put("Brazil", "+55");
        countries_code.put("British Virgin Islands", "+1284");
        countries_code.put("Brunei", "+673");
        countries_code.put("Bulgaria", "+359");
        countries_code.put("Burkina Faso", "+226");
        countries_code.put("Burundi", "+257");
        countries_code.put("Cambodia", "+855");
        countries_code.put("Cameroon", "+237");
        countries_code.put("Canada", "+1");
        countries_code.put("Cape Verde", "+238");
        countries_code.put("Cayman Islands", "+1345");
        countries_code.put("Central African Republic", "+236");
        countries_code.put("Chad", "+235");
        countries_code.put("Chile", "+56");
        countries_code.put("China", "+86");
        countries_code.put("Colombia", "+57");
        countries_code.put("Comoros", "+269");
        countries_code.put("Congo", "+242");
        countries_code.put("Congo (DRC)", "+243");
        countries_code.put("Cook Islands", "+682");
        countries_code.put("Costa Rica", "+506");
        countries_code.put("C'te d'Ivoire", "+225");
        countries_code.put("Croatia", "+385");
        countries_code.put("Cuba", "+53");
        countries_code.put("Cyprus", "+357");
        countries_code.put("Czech Republic", "+420");
        countries_code.put("Denmark", "+45");
        countries_code.put("Djibouti", "+253");
        countries_code.put("Dominica", "+1767");
        countries_code.put("Dominican Republic", "+18");
        countries_code.put("Ecuador", "+593");
        countries_code.put("Egypt", "+20");
        countries_code.put("El Salvador", "+503");
        countries_code.put("Equatorial Guinea", "+240");
        countries_code.put("Eritrea", "+291");
        countries_code.put("Estonia", "+372");
        countries_code.put("Ethiopia", "+251");
        countries_code.put("Falkland Islands", "+500");
        countries_code.put("Faroe Islands", "+298");
        countries_code.put("Fiji", "+679");
        countries_code.put("Finland", "+358");
        countries_code.put("France", "+33");
        countries_code.put("French Guiana", "+594");
        countries_code.put("French Polynesia", "+689");
        countries_code.put("Gabon", "+241");
        countries_code.put("Gambia, The", "+220");
        countries_code.put("Georgia", "+995");
        countries_code.put("Germany", "+49");
        countries_code.put("Ghana", "+233");
        countries_code.put("Gibraltar", "+350");
        countries_code.put("Greece", "+30");
        countries_code.put("Greenland", "+299");
        countries_code.put("Grenada", "+1473");
        countries_code.put("Guadeloupe", "+590");
        countries_code.put("Guam", "+1671");
        countries_code.put("Guatemala", "+502");
        countries_code.put("Guinea", "+224");
        countries_code.put("Guinea-Bissau", "+245");
        countries_code.put("Guyana", "+592");
        countries_code.put("Haiti", "+509");
        countries_code.put("Holy See (Vatican City)", "+3");
        countries_code.put("Honduras", "+504");
        countries_code.put("Hong Kong SAR", "+852");
        countries_code.put("Hungary", "+36");
        countries_code.put("Iceland", "+354");
        countries_code.put("India", "+91");
        countries_code.put("Indonesia", "+62");
        countries_code.put("Iran", "+98");
        countries_code.put("Iraq", "+964");
        countries_code.put("Ireland", "+353");
        countries_code.put("Israel", "+972");
        countries_code.put("Italy", "+39");
        countries_code.put("Jamaica", "+1876");
        countries_code.put("Japan", "+81");
        countries_code.put("Jordan", "+962");
        countries_code.put("Kazakhstan", "+7");
        countries_code.put("Kenya", "+254");
        countries_code.put("Kiribati", "+686");
        countries_code.put("Korea", "+82");
        countries_code.put("Kuwait", "+965");
        countries_code.put("Kyrgyzstan", "+996");
        countries_code.put("Laos", "+856");
        countries_code.put("Latvia", "+371");
        countries_code.put("Lebanon", "+961");
        countries_code.put("Lesotho", "+266");
        countries_code.put("Liberia", "+231");
        countries_code.put("Libya", "+218");
        countries_code.put("Liechtenstein", "+423");
        countries_code.put("Lithuania", "+370");
        countries_code.put("Luxembourg", "+352");
        countries_code.put("Macao SAR", "+853");
        countries_code.put("Macedonia, FYRO", "+389");
        countries_code.put("Madagascar", "+261");
        countries_code.put("Malawi", "+265");
        countries_code.put("Malaysia", "+60");
        countries_code.put("Maldives", "+960");
        countries_code.put("Mali", "+223");
        countries_code.put("Malta", "+356");
        countries_code.put("Marshall Islands", "+692");
        countries_code.put("Martinique", "+596");
        countries_code.put("Mauritania", "+222");
        countries_code.put("Mauritius", "+230");
        countries_code.put("Mayotte", "+269");
        countries_code.put("Mexico", "+52");
        countries_code.put("Micronesia", "+691");
        countries_code.put("Moldova", "+373");
        countries_code.put("Monaco", "+377");
        countries_code.put("Mongolia", "+976");
        countries_code.put("Montenegro", "+382");
        countries_code.put("Montserrat", "+1664");
        countries_code.put("Morocco", "+212");
        countries_code.put("Mozambique", "+258");
        countries_code.put("Myanmar", "+95");
        countries_code.put("Namibia", "+264");
        countries_code.put("Nauru", "+674");
        countries_code.put("Nepal", "+977");
        countries_code.put("Netherlands", "+31");
        countries_code.put("New Caledonia", "+687");
        countries_code.put("New Zealand", "+64");
        countries_code.put("Nicaragua", "+505");
        countries_code.put("Niger", "+227");
        countries_code.put("Nigeria", "+234");
        countries_code.put("Niue", "+683");
        countries_code.put("Northern Mariana Islands", "+1670");
        countries_code.put("North Korea", "+850");
        countries_code.put("Norway", "+47");
        countries_code.put("Oman", "+968");
        countries_code.put("Pakistan", "+92");
        countries_code.put("Palau", "+680");
        countries_code.put("Panama", "+507");
        countries_code.put("Papua New Guinea", "+675");
        countries_code.put("Paraguay", "+595");
        countries_code.put("Peru", "+51");
        countries_code.put("Philippines", "+63");
        countries_code.put("Poland", "+48");
        countries_code.put("Portugal", "+351");
        countries_code.put("Puerto Rico", "+1787");
        countries_code.put("Qatar", "+974");
        countries_code.put("Reunion", "+262");
        countries_code.put("Romania", "+40");
        countries_code.put("Russia", "+7");
        countries_code.put("Rwanda", "+250");
        countries_code.put("Saint Helena, Ascension...", "+290");
        countries_code.put("Saint Kitts and Nevis", "+1869");
        countries_code.put("Saint Lucia", "+1758");
        countries_code.put("Saint Pierre and Miquelon", "+508");
        countries_code.put("Saint Vincent and the Gre...", "+1784");
        countries_code.put("Samoa", "+685");
        countries_code.put("San Marino", "+378");
        countries_code.put("S?o Tom? and Pr?ncipe", "+239");
        countries_code.put("Saudi Arabia", "+966");
        countries_code.put("Senegal", "+221");
        countries_code.put("Serbia", "+381");
        countries_code.put("Seychelles", "+248");
        countries_code.put("Sierra Leone", "+232");
        countries_code.put("Singapore", "+65");
        countries_code.put("Slovakia", "+421");
        countries_code.put("Slovenia", "+386");
        countries_code.put("Solomon Islands", "+677");
        countries_code.put("Somalia", "+252");
        countries_code.put("South Africa", "+27");
        countries_code.put("Spain", "+34");
        countries_code.put("Sri Lanka", "+94");
        countries_code.put("Sudan", "+249");
        countries_code.put("Suriname", "+597");
        countries_code.put("Swaziland", "+268");
        countries_code.put("Sweden", "+46");
        countries_code.put("Switzerland", "+41");
        countries_code.put("Syria", "+963");
        countries_code.put("Taiwan", "+886");
        countries_code.put("Tajikistan", "+992");
        countries_code.put("Tanzania", "+255");
        countries_code.put("Thailand", "+66");
        countries_code.put("Timor-Leste", "+670");
        countries_code.put("Togo", "+228");
        countries_code.put("Tokelau", "+690");
        countries_code.put("Tonga", "+676");
        countries_code.put("Trinidad and Tobago", "+1868");
        countries_code.put("Tunisia", "+216");
        countries_code.put("Turkey", "+90");
        countries_code.put("Turkmenistan", "+993");
        countries_code.put("Turks and Caicos Islands", "+1649");
        countries_code.put("Tuvalu", "+688");
        countries_code.put("Uganda", "+256");
        countries_code.put("Ukraine", "+380");
        countries_code.put("United Arab Emirates", "+971");
        countries_code.put("United Kingdom", "+44");
        countries_code.put("United States", "+1");
        countries_code.put("Uruguay", "+598");
        countries_code.put("US Virgin Islands", "+1340");
        countries_code.put("Uzbekistan", "+998");
        countries_code.put("Vanuatu", "+678");
        countries_code.put("Venezuela", "+58");
        countries_code.put("Vietnam", "+84");
        countries_code.put("Wallis and Futuna", "+681");
        countries_code.put("Yemen", "+967");
        countries_code.put("Zambia", "+260");
        countries_code.put("Zimbabwe", "+263");
    }
}
