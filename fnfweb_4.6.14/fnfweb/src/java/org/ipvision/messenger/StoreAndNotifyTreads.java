package org.ipvision.messenger;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "storeAndNotifyTreads")
@SessionScoped
public class StoreAndNotifyTreads implements Serializable {
    
    @ManagedProperty(value = "#{chatCommunication}")
    ChatCommunication chatCommunication;
//    @ManagedProperty(value = "#{friendChatSenderThread}")
    public FriendChatSenderThread friendChatSenderThread = null;
//    @ManagedProperty(value = "#{groupChatSenderThread}")
    public GroupChatSenderThread groupChatSenderThread = null;
    // should remove this method
    public void notify_or_start_thread() {
        if (friendChatSenderThread == null || !friendChatSenderThread.isRunning()) {
            friendChatSenderThread = new FriendChatSenderThread();
            friendChatSenderThread.setRunning(true);
            friendChatSenderThread.setChatCommunication(chatCommunication);
            friendChatSenderThread.start();
        }
        
      // friendChatSenderThread.setCounter(1);
        if (friendChatSenderThread.getState() == Thread.State.WAITING) {
            synchronized (friendChatSenderThread) {
                friendChatSenderThread.notify();
            }
        }
    }
    
    public void notify_or_start_thread_group() {
        if (groupChatSenderThread != null || !groupChatSenderThread.isRunning()) {
            groupChatSenderThread = new GroupChatSenderThread();
            groupChatSenderThread.setRunning(true);
            groupChatSenderThread.setChatCommunication(chatCommunication);
            groupChatSenderThread.start();
        }
        groupChatSenderThread.setCounter(1);
        if (groupChatSenderThread.getState() == Thread.State.WAITING) {
            synchronized (groupChatSenderThread) {
                groupChatSenderThread.notify();
            }
        }
    }
    
    public void stopService() {
        if (friendChatSenderThread != null) {
            friendChatSenderThread.setRunning(false);
        }
        
        if (groupChatSenderThread != null) {
            groupChatSenderThread.setRunning(false);
        }
    }
    
    public ChatCommunication getChatCommunication() {
        return chatCommunication;
    }
    
    public void setChatCommunication(ChatCommunication chatCommunication) {
        this.chatCommunication = chatCommunication;
    }
    
    public FriendChatSenderThread getFriendChatSenderThread() {
        return friendChatSenderThread;
    }
    
    public void setFriendChatSenderThread(FriendChatSenderThread friendChatSenderThread) {
        this.friendChatSenderThread = friendChatSenderThread;
    }
    
    public GroupChatSenderThread getGroupChatSenderThread() {
        return groupChatSenderThread;
    }
    
    public void setGroupChatSenderThread(GroupChatSenderThread groupChatSenderThread) {
        this.groupChatSenderThread = groupChatSenderThread;
    }
}
