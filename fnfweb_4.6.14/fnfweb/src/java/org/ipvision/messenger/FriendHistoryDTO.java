/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class FriendHistoryDTO implements Serializable {

    private int pt;
    private String uId;
    private String fndId;
//    private int numDays;
    private String pckId;
    private long mDate;

    public FriendHistoryDTO() {
    }

    public int getPacketType() {
        return pt;
    }

    public void setPacketType(int packetType) {
        this.pt = packetType;
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public String getFriendName() {
        return fndId;
    }

    public void setFriendName(String friendName) {
        this.fndId = friendName;
    }

//    public int getNumDays() {
//        return numDays;
//    }
//
//    public void setNumDays(int numDays) {
//        this.numDays = numDays;
//    }
    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public long getMessageDate() {
        return mDate;
    }

    public void setMessageDate(long messageDate) {
        this.mDate = messageDate;
    }
}
