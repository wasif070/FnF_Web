/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserProfile;

/**
 *
 * @author user
 */
@ManagedBean(name = "chatWindowView")
@ViewScoped
public class ChatWindowView implements Serializable {

    @ManagedProperty(value = "#{chatWindowActivity}")
    private ChatWindowActivity chatWindowActivity;
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    String friendName;
    String userName;
    String message;
    UserProfile userProfile;
    private ArrayList<MessageDTO> messageList = new ArrayList<MessageDTO>();

    public void sendMessage() {
        if (message != null && message.length() > 0) {
            chatWindowActivity.sendMessage(message, friendName);
        }
        message = "";
    }

    public boolean checkRefreshDiv(String friendName) {
        if (chatWindowActivity.checkFriendChat(friendName)) {
            setMessageList(chatWindowActivity.getFriendMessageList(friendName));
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<MessageDTO> getFriendMessageList(String name) {
        return chatWindowActivity.getFriendMessageList(name);
    }

    public ChatWindowActivity getChatWindowActivity() {
        return chatWindowActivity;
    }

    public void setChatWindowActivity(ChatWindowActivity chatWindowActivity) {
        this.chatWindowActivity = chatWindowActivity;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MessageDTO> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<MessageDTO> messageList) {
        this.messageList = messageList;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userIdentity) {
        userProfile = loggedUserProfile.getFriendlist().get(userIdentity);
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }
}
