package org.ipvision.messenger;

import java.io.Serializable;
import java.net.DatagramPacket;
import java.util.HashMap;
import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.utils.MessengerConstants;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "chatProvider")
@SessionScoped
public class ChatProvider extends Thread implements Serializable {

    static Logger logger = Logger.getLogger(ChatProvider.class.getName());
    @ManagedProperty(value = "#{chatCommunication}")
    private ChatCommunication chatCommunication;
    
    
    int packetType;
    String packetId = "";
    String userName;
    String friendName;
    long groupId;
    String message;
    String finalMessage;
    Random random;
    boolean isError = false;
    OnReceiveListner onReceiveListner;
    String hashKey = "";
    boolean isCountOneMsg = false;
    boolean isCountOneGroupMsg = false;
    public HashMap<String, String> packetIdHashMap = new HashMap<String, String>();
    public HashMap<Long, String> groupPacketIdHashMap = new HashMap<Long, String>();
    volatile boolean running = true;

    public void stopService() {
        running = false;
        try {
            Thread.sleep(1000L);
        } catch (Exception e) {
        }
        if (chatCommunication.getChatSocket() != null) {
            chatCommunication.getChatSocket().close();
        }
    }

    @Override
    public void run() {
        logger.info("Chat Provider Started....");
        while (running) {
            try {
                byte[] buf = new byte[MessengerConstants.BUFFER_SIZE];
                DatagramPacket incomingPacket = new DatagramPacket(buf, buf.length);
                chatCommunication.getChatSocket().receive(incomingPacket);
                buf = incomingPacket.getData();
                makeMessage(buf);
                switch (packetType) {
                    case MessengerConstants.CHAT_TYPING:
                    case MessengerConstants.CHAT_IDEL:
                        onReceiveListner.onReceivedMessage(finalMessage);
                        break;
                    case MessengerConstants.CHAT_FRIEND:
                        isCountOneMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneMsg = false;
                        } else {
                            isCountOneMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneMsg) {
                            isCountOneMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;
                    case MessengerConstants.FRIEND_OFFLINE_CHAT:
                        isCountOneMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneMsg = false;
                        } else {
                            isCountOneMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneMsg) {
                            isCountOneMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;
                    case MessengerConstants.CHAT_FRIEND_HISTORY:
                        isCountOneMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneMsg = false;
                        } else {
                            isCountOneMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneMsg) {
                            isCountOneMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;
                    case MessengerConstants.CHAT_GROUP:
                        isCountOneGroupMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneGroupMsg = false;
                        } else {
                            isCountOneGroupMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneGroupMsg) {
                            isCountOneGroupMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;

                    case MessengerConstants.GROUP_OFFLINE_CHAT:
                        isCountOneGroupMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneGroupMsg = false;
                        } else {
                            isCountOneGroupMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneGroupMsg) {
                            isCountOneGroupMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;

                    case MessengerConstants.CHAT_GROUP_HISTORY:
                        isCountOneGroupMsg = true;
                        if (packetIdHashMap.containsKey(packetId)) {
                            isCountOneGroupMsg = false;
                        } else {
                            isCountOneGroupMsg = true;
                            packetIdHashMap.put(packetId, packetId);
                        }
                        if (isCountOneGroupMsg) {
                            isCountOneGroupMsg = false;
                            onReceiveListner.onReceivedMessage(finalMessage);
                        }
                        break;
                    case MessengerConstants.CHAT_CONFIRMATION:
                        onReceiveListner.onReceivedMessage(finalMessage);
                        break;
                }
            } catch (Exception e) {
                logger.info("Communication error-->" + e);
                e.printStackTrace();
            }
        }
    }

    public synchronized void makeMessage(byte[] RecievedBuffer) {
        String packetString = new String(RecievedBuffer);
        logger.info("ReceivedChat==>" + packetString.trim());
        System.out.println("ReceivedChat==>" + packetString.trim());
        JSONObject messageObject = null;
        try {
            messageObject = new JSONObject(packetString.trim());
            if (messageObject.has("pt")) {
                packetType = messageObject.getInt("pt");
            }
            if (messageObject.has("uId")) {
                userName = messageObject.getString("uId");
            }
            if (messageObject.has("fndId")) {
                friendName = messageObject.getString("fndId");
            }
            hashKey = friendName;
            if (messageObject.has("mg")) {
                message = messageObject.getString("mg");
            }
            if (messageObject.has("grpId")) {
                groupId = messageObject.getLong("grpId");
            }

            if (messageObject.has("pckId"))
            {
                packetId = messageObject.getString("pckId");
                if (packetType == MessengerConstants.CHAT_FRIEND || packetType == MessengerConstants.FRIEND_OFFLINE_CHAT) {
                    sendConfirmationMSG(MessengerConstants.CHAT_CONFIRMATION, packetId);
                } else if (packetType == MessengerConstants.CHAT_FRIEND_HISTORY) {
                    sendConfirmationMSG(MessengerConstants.CHAT_CONFIRMATION_HISTORY, packetId);
                } else if (packetType == MessengerConstants.CHAT_GROUP || packetType == MessengerConstants.GROUP_OFFLINE_CHAT) {
                    sendConfirmationMSG(MessengerConstants.CHAT_CONFIRMATION_GROUP, packetId);
                } else if (packetType == MessengerConstants.CHAT_GROUP_HISTORY) {
                    sendConfirmationMSG(
                            MessengerConstants.CHAT_GROUP_CONFIRMATION_HISTORY,
                            packetId);
                } else if (packetType == MessengerConstants.CHAT_CONFIRMATION
                        || packetType == MessengerConstants.CHAT_HISTORY_RECEIVED_CONFIRMATION) {

                    if (chatCommunication.getFriendChatStorage().containsKey(packetId)) {

                        chatCommunication.getFriendChatStorage().remove(packetId);
                    }

                    if (chatCommunication.getGroupChatStorage().containsKey(packetId)) {
                        chatCommunication.getGroupChatStorage().remove(packetId);
                    }
                }
            }
        } catch (Exception e) {
            MessengerConstants.debugLog("exception", e.toString());
        }
        finalMessage = packetString;
    }

    public OnReceiveListner getOnReceiveListner() {
        return onReceiveListner;
    }

    public void setOnReceiveListner(OnReceiveListner onReceiveListner) {
        this.onReceiveListner = onReceiveListner;
    }

    private void sendConfirmationMSG(int packetType, String packetId) {
        String finalBytePacket = chatCommunication.confirmationMessagePacket(packetType, packetId);
        chatCommunication.sendPacketConfirmation(finalBytePacket);
    }

    public ChatCommunication getChatCommunication() {
        return chatCommunication;
    }

    public void setChatCommunication(ChatCommunication chatCommunication) {
        this.chatCommunication = chatCommunication;
    }


    
}
