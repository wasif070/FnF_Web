/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class GroupHistoryDTO implements Serializable {
    private int pt;//pt
    private String uId;
    private long grpId;
    private long mgDate;
    private String pckId;
    public GroupHistoryDTO() {
    }

    public int getPacketType() {
        return pt;
    }

    public void setPacketType(int packetType) {
        this.pt = packetType;
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public long getGroupId() {
        return grpId;
    }

    public void setGroupId(long groupId) {
        this.grpId = groupId;
    }

    public long getMessageDate() {
        return mgDate;
    }

    public void setMessageDate(long messageDate) {
        this.mgDate = messageDate;
    }
    
    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }
}
