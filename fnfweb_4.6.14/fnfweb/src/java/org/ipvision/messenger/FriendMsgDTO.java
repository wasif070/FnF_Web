/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class FriendMsgDTO implements Serializable {

    private String uId;
    private String fndId;
    private String mg;
    private int pt;
    private String pckId;

    public FriendMsgDTO() {
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public String getFriendName() {
        return fndId;
    }

    public void setFriendName(String friendName) {
        this.fndId = friendName;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public int getPacketType() {
        return pt;
    }

    public void setPacketType(int packetType) {
        this.pt = packetType;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }
}
