package org.ipvision.messenger;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "groupChatSenderThread")
@SessionScoped
public class GroupChatSenderThread extends Thread implements Serializable {

    @ManagedProperty(value = "#{chatCommunication}")
    private ChatCommunication chatCommunication;
    int counter = 0;

    boolean running = true;

    @Override
    public void run() {
        while (running) {
            try {
                if (!chatCommunication.getGroupChatStorage().isEmpty()) {
                    for (String key : chatCommunication.getGroupChatStorage().keySet()) {

                        ResendPacketDTO resendPacketDTO = chatCommunication.getGroupChatStorage().get(key);
                        if (counter > 7) {
                            chatCommunication.getGroupChatStorage().remove(key);
                        } else {
                            chatCommunication.chatSocket.send(resendPacketDTO.getPacket());
                            counter++;
                            setCounter(counter);
                        }
                        Thread.sleep(500);
                    }
                } else {
                    Thread.sleep(1000);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public ChatCommunication getChatCommunication() {
        return chatCommunication;
    }

    public void setChatCommunication(ChatCommunication chatCommunication) {
        this.chatCommunication = chatCommunication;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
