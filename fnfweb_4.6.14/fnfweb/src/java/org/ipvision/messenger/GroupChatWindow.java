/*
 * and open the template in the editor.
 * To change this template, choose Tools | Templates
 */
package org.ipvision.messenger;

import com.google.gson.Gson;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.ipvision.groups.GroupDTO;
import org.ipvision.groups.GroupMember;
import org.ipvision.messenger.groups.SessionGroupMsgDTO;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.utils.MessengerConstants;
import org.ipvision.utils.ServerConstants;
import org.ipvision.utils.Sorter;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "groupChatWindow")
@SessionScoped
public class GroupChatWindow implements OnReceiveListner, Serializable {

    @ManagedProperty(value = "#{chatMainBean}")
    private ChatMainBean chatMainBean;
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    private ArrayList<GroupMessageDTO> messageList = new ArrayList<GroupMessageDTO>();
    public HashMap<Long, ArrayList<GroupMessageDTO>> groupChatHashMap = new HashMap<Long, ArrayList<GroupMessageDTO>>();
    public HashMap<Long, String> chatSession = new HashMap<Long, String>();
    public HashMap<Long, ArrayList<GroupMessageDTO>> newChatHashMap = new HashMap<Long, ArrayList<GroupMessageDTO>>();
    public HashMap<Long, Boolean> chatRefresh = new HashMap<Long, Boolean>();
    public HashMap<Long, Integer> chatNoOfMessage = new HashMap<Long, Integer>();
    private boolean refreshDiv;
    private boolean refreshNotification;
    DatagramSocket chatSocket;
    String friendName;
    String message;
    long groupId;
    int packetType;
    String finalBytePacket;
    int numDays = 0;
    long hashKey = 0;
    GroupDTO groupDetails;
    private ArrayList<GroupDTO> newGroupList;

    public GroupChatWindow() {
    }

    public String getSessionGroups() {
        String groupListJsonStr = "false";
        ArrayList<SessionGroupMsgDTO> sessionMsgDTOs = new ArrayList<SessionGroupMsgDTO>();
        Gson gson = new Gson();
        for (Map.Entry<Long, String> entry : chatSession.entrySet()) {
            SessionGroupMsgDTO smdto = new SessionGroupMsgDTO();
            smdto.setGroupId(entry.getKey());
            smdto.setGroupName(entry.getValue().toString());
            smdto.setMessageList(groupChatHashMap.get(entry.getKey()));
            sessionMsgDTOs.add(smdto);
        }
        groupListJsonStr = gson.toJson(sessionMsgDTOs).toString();
        return groupListJsonStr;
    }

    public String addSession(long gid, String gName) {
        if (!chatSession.containsKey(gid)) {
            chatSession.put(gid, gName);
        }
        ArrayList<GroupMessageDTO> mList = new ArrayList<GroupMessageDTO>();
        mList = groupChatHashMap.get(gid);
        Gson gson = new Gson();
        if (mList != null && mList.size() > 0) {
            newChatHashMap.remove(gid);
            return gson.toJson(mList).toString();
        }
        return "false";

    }

    public void removeSession(long gid) {
        if (chatSession.containsKey(gid)) {
            chatSession.remove(gid);
        }
    }

    public String getNewMessages() {
        ArrayList<SessionGroupMsgDTO> mList = new ArrayList<SessionGroupMsgDTO>();
        Gson gson = new Gson();

        for (Map.Entry<Long, ArrayList<GroupMessageDTO>> entry : newChatHashMap.entrySet()) {
            SessionGroupMsgDTO smdto = new SessionGroupMsgDTO();
            long gid = entry.getKey();
            smdto.setGroupId(groupId);
            smdto.setMessageList(newChatHashMap.get(gid));
            if (loggedUserProfile.getGroupList().containsKey(gid)) {
                GroupDTO gdto = loggedUserProfile.getGroupList().get(gid);
                loggedUserProfile.getGroupList().get(gid).setNoOfMsg(0);
                smdto.setGroupName(gdto.getGroupName());
            }
            mList.add(smdto);
            newChatHashMap.remove(gid);
        }
        if (mList != null && mList.size() > 0) {
            return gson.toJson(mList).toString();
        } else {
            return "false";
        }
    }

//    public String getChatUpdate() {
//        String friendListJsonStr = "false";
//        Gson gson = new Gson();
//        if (newFriendList != null && newFriendList.size() > 0) {
//            friendListJsonStr = gson.toJson(newFriendList).toString();
//            newFriendList = new ArrayList<UserProfile>();
//            return friendListJsonStr;
//        }
//        return friendListJsonStr;
//    }
    public ArrayList<GroupMessageDTO> getGroupMessageList(long groupId) {
        if (chatNoOfMessage.containsKey(groupId)) {
            chatNoOfMessage.remove(groupId);
        }
        if (chatRefresh.containsKey(groupId)) {
            if (chatRefresh.get(groupId)) {
                chatRefresh.remove(groupId);
            }
        }
        messageList = new ArrayList<GroupMessageDTO>();
        messageList = groupChatHashMap.get(groupId);
        return messageList;
    }

    public boolean checkRefreshDiv() {
        if (refreshDiv) {
            refreshDiv = false;
            return true;
        } else {
            return false;
        }
    }

    public boolean checkGroupChat(long groupId) {
        if (chatRefresh.containsKey(groupId)) {
            if (chatRefresh.get(groupId)) {
                chatRefresh.remove(groupId);
//                System.out.println("True");
                return true;
            } else {
//                System.out.println("False");
                return false;
            }
        } else {
//            System.out.println("False");
            return false;
        }
    }

    public int getNumberOfMessage(long groupId) {
        if (chatNoOfMessage.containsKey(groupId)) {
            return chatNoOfMessage.get(groupId);
        } else {
            return 0;
        }
    }

    public String getChatUpdate() {
        String groupListJsonStr = "false";
        Gson gson = new Gson();
        if (newGroupList != null && newGroupList.size() > 0) {
            groupListJsonStr = gson.toJson(newGroupList).toString();
            newGroupList = new ArrayList<GroupDTO>();
            return groupListJsonStr;
        }
        return groupListJsonStr;
    }

    public boolean checkChatNotification() {
        if (refreshNotification) {
            refreshNotification = false;
            return true;
        } else {
            return false;
        }
    }

    public void setLayout(long groupId) {
        messageList = groupChatHashMap.get(groupId);
    }

    public void sendServletMessage(long groupId,String msg, long gId) {
        packetType = MessengerConstants.CHAT_GROUP;
        GroupDTO groupDTO = loggedUserProfile.getGroupList().get(gId);
        message = msg;
        groupId = gId;
        friendName = groupDTO.getGroupMembers();
        if (message != null && message.length() > 0) {
            setMessageView();
            String packetId = groupId + MessengerConstants.getRandromPacketId();
            finalBytePacket = chatMainBean.getChatCommunication().makeGroupMessagePacket(packetType, loggedUserProfile.getUserIdentity(), friendName, groupId, message, packetId);
            chatMainBean.getStoreAndNotifyTreads().notify_or_start_thread();
            chatMainBean.getChatCommunication().sendPacketGroup(finalBytePacket, packetId,gId);
        }
        message = "";
    }

    public String getNewMessage(long gId) {
        if (chatNoOfMessage.containsKey(gId)) {
            chatNoOfMessage.remove(gId);
        }
        ArrayList<GroupMessageDTO> mList = new ArrayList<GroupMessageDTO>();
        mList = newChatHashMap.get(gId);
        Gson gson = new Gson();
        if (mList != null && mList.size() > 0) {
            newChatHashMap.remove(gId);
            return gson.toJson(mList).toString();
        }
        return "false";
    }

    public void sendMessage() {
        packetType = MessengerConstants.CHAT_GROUP;
        if (message != null && message.length() > 0) {
            setMessageView();
            String packetId = groupId + MessengerConstants.getRandromPacketId();
            finalBytePacket = chatMainBean.getChatCommunication().makeGroupMessagePacket(packetType, loggedUserProfile.getUserIdentity(), friendName, groupId, message, packetId);
            chatMainBean.getStoreAndNotifyTreads().notify_or_start_thread();
            chatMainBean.getChatCommunication().sendPacketGroup(finalBytePacket, packetId,groupId);
        }
        message = "";
    }

    private void setMessageView() {
        GroupMessageDTO messageDTO = new GroupMessageDTO();
        messageDTO.setUserName(loggedUserProfile.getUserIdentity());
        messageDTO.setGroupId(groupId);
        messageDTO.setMessage(MessengerConstants.replaceEmoticons(message));
        messageDTO.setMessageDate(System.currentTimeMillis());
        messageDTO.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
        hashKey = groupId;

        messageList = groupChatHashMap.get(hashKey);
        if (messageList == null || messageList.size() < 1) {
            messageList = new ArrayList<GroupMessageDTO>();
        }
        messageList.add(messageDTO);
        groupChatHashMap.put(hashKey, messageList);
    }

    public void sendHistoryRequest(long gid, int nDays) {
        groupId = gid;
        numDays = nDays;
        if (groupChatHashMap.containsKey(groupId)) {
            groupChatHashMap.remove(groupId);
        }
        String packetId = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        packetType = MessengerConstants.CHAT_GROUP_HISTORY;
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -numDays - 1);
        Date today30 = cal.getTime();
        finalBytePacket = chatMainBean.getChatCommunication().makeGroupHistoryPacket(
                packetType, loggedUserProfile.getUserIdentity(), groupId, today30.getTime(), packetId);
        chatMainBean.getChatCommunication().sendPacketHistory(finalBytePacket, packetId);
    }

    public void chatHistory() {
        refreshDiv = true;
        if (groupChatHashMap.containsKey(groupId)) {
            groupChatHashMap.remove(groupId);
        }
        String packetId = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        packetType = MessengerConstants.CHAT_GROUP_HISTORY;
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -numDays - 1);
        Date today30 = cal.getTime();
        finalBytePacket = chatMainBean.getChatCommunication().makeGroupHistoryPacket(
                packetType, loggedUserProfile.getUserIdentity(), groupId, today30.getTime(), packetId);
        chatMainBean.getChatCommunication().sendPacketHistory(finalBytePacket, packetId);
    }

    @Override
    public void onReceivedMessage(String msg) {
        JSONObject receivedPacketDTO = null;
        try {
            receivedPacketDTO = new JSONObject(msg);
            if (receivedPacketDTO.has("pt")) {
                packetType = receivedPacketDTO.getInt("pt");
            } else {
                packetType = 0;
            }
            GroupDTO groupDTO;
            switch (packetType) {
                case MessengerConstants.CHAT_GROUP:
                    GroupMessageDTO msgDto = new GroupMessageDTO();
                    msgDto.setGroupId(receivedPacketDTO.getLong("grpId"));
                    msgDto.setUserName(receivedPacketDTO.getString("uId"));
                    msgDto.setMessage(MessengerConstants.replaceEmoticons(receivedPacketDTO.getString("mg")));
                    msgDto.setMessageDate(System.currentTimeMillis());
                    msgDto.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
//                    msgDto.setMessageDate(receivedPacketDTO.getLong("messageDate"));
//                    msgDto.setMessageDateString(MessengerConstants.getDate(receivedPacketDTO.getLong("messageDate"), MessengerConstants.CHAT_DATE_FORMAT));
                    hashKey = msgDto.getGroupId();
                    ArrayList<GroupMessageDTO> temp = groupChatHashMap.get(hashKey);
                    if (temp == null) {
                        temp = new ArrayList<GroupMessageDTO>();
                    }
                    temp.add(msgDto);
                    groupChatHashMap.put(hashKey, temp);
                    if (hashKey == groupId) {
                        temp = newChatHashMap.get(hashKey);
                        if (temp == null) {
                            temp = new ArrayList<GroupMessageDTO>();
                        }
                        temp.add(msgDto);
                        newChatHashMap.put(hashKey, temp);
                    }
                    chatRefresh.put(hashKey, true);
                    groupDTO = loggedUserProfile.getGroupList().get(hashKey);
                    if (loggedUserProfile.getGroupList().containsKey(hashKey)) {

                        groupDTO.setNoOfMsg(groupDTO.getNoOfMsg() + 1);
                        if (newGroupList == null) {
                            newGroupList = new ArrayList<GroupDTO>();
                        }
                        newGroupList.add(groupDTO);
                    }
                    chatSession.put(hashKey, groupDTO.getGroupName());
//                    if (chatNoOfMessage.containsKey(hashKey)) {
//                        chatNoOfMessage.put(hashKey, chatNoOfMessage.get(hashKey) + 1);
//                    } else {
//                        chatNoOfMessage.put(hashKey, 1);
//                    }
                    refreshDiv = true;
                    refreshNotification = true;
                    break;
                case MessengerConstants.GROUP_OFFLINE_CHAT:
                    try {
                        if (receivedPacketDTO.getInt("total_pak") > 0) {
                            JSONArray array = new JSONArray(receivedPacketDTO.getString("groupMessage"));
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonMsg = array.getJSONObject(i);
                                GroupMessageDTO mDto = new GroupMessageDTO();
                                mDto.setGroupId(jsonMsg.getLong("grpId"));
                                mDto.setUserName(jsonMsg.getString("uId"));
                                mDto.setMessage(MessengerConstants.replaceEmoticons(jsonMsg.getString("mg")));
                                mDto.setMessageDate(jsonMsg.getLong("mgDate"));
                                mDto.setMessageDateString(MessengerConstants.getDate(jsonMsg.getLong("mgDate"), MessengerConstants.CHAT_DATE_FORMAT));
                                hashKey = mDto.getGroupId();
                                ArrayList<GroupMessageDTO> tList = groupChatHashMap.get(hashKey);
                                if (tList == null || tList.size() < 1) {
                                    tList = new ArrayList<GroupMessageDTO>();
                                }
                                tList.add(mDto);
                                groupChatHashMap.put(hashKey, tList);
                                chatRefresh.put(hashKey, true);
                                if (loggedUserProfile.getGroupList().containsKey(hashKey)) {
                                    groupDTO = loggedUserProfile.getGroupList().get(hashKey);
                                    groupDTO.setNoOfMsg(groupDTO.getNoOfMsg() + 1);
                                    if (newGroupList == null) {
                                        newGroupList = new ArrayList<GroupDTO>();
                                    }
                                    newGroupList.add(groupDTO);
                                }
//                                if (chatNoOfMessage.containsKey(hashKey)) {
//                                    chatNoOfMessage.put(hashKey, chatNoOfMessage.get(hashKey) + 1);
//                                } else {
//                                    chatNoOfMessage.put(hashKey, 1);
//                                }
                            }
                            ArrayList<GroupMessageDTO> tList = groupChatHashMap.get(hashKey);
                            if (tList != null && tList.size() > 0) {
                                groupChatHashMap.put(hashKey, Sorter.sortArrayListASC(tList, "getMessageDate"));
                            }

                            refreshDiv = true;
                            refreshNotification = true;

                        }
                    } catch (Exception e) {
                        MessengerConstants.debugLog("Chat", "jsonException: " + e.toString());
                    }
                    break;
//                case MessengerConstants.CHAT_GROUP_HISTORY:
//                    try {
//                        if (receivedPacketDTO.getInt("total_pak") > 0) {
//                            JSONArray array = new JSONArray(receivedPacketDTO.getString("message"));
//                            for (int i = 0; i < array.length(); i++) {
//                                JSONObject jsonMsg = array.getJSONObject(i);
//                                GroupMessageDTO mDto = new GroupMessageDTO();
//                                mDto.setGroupId(getGroupId());
//                                mDto.setUserName(jsonMsg.getString("userName"));
//                                mDto.setMessage(MessengerConstants.replaceEmoticons(jsonMsg.getString("message")));
//                                mDto.setMessageDate(jsonMsg.getLong("messageDate"));
//                                mDto.setMessageDateString(MessengerConstants.getDate(jsonMsg.getLong("messageDate"), MessengerConstants.CHAT_DATE_FORMAT));
//                                hashKey = getGroupId();
//                                ArrayList<GroupMessageDTO> tList = groupChatHashMap.get(hashKey);
//                                if (tList == null || tList.size() < 1) {
//                                    tList = new ArrayList<GroupMessageDTO>();
//                                }
//                                tList.add(mDto);
//                                groupChatHashMap.put(hashKey, tList);
//
//                                tList = newChatHashMap.get(hashKey);
//                                if (tList == null || tList.size() < 1) {
//                                    tList = new ArrayList<GroupMessageDTO>();
//                                }
//                                tList.add(mDto);
//                                newChatHashMap.put(hashKey, tList);
//                            }
//
//                            ArrayList<GroupMessageDTO> tList = groupChatHashMap.get(hashKey);
//                            if (tList != null && tList.size() > 0) {
//                                groupChatHashMap.put(hashKey, Sorter.sortArrayListASC(tList, "getMessageDate"));
//                            }
//                            chatRefresh.put(hashKey, true);
//                        }
//                    } catch (Exception e) {
//                        MessengerConstants.debugLog("Chat", "jsonException: " + e.toString());
//                    }
//                    break;
            }
        } catch (Exception e) {
            MessengerConstants.debugLog("exception", e.toString());
        }
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GroupMessageDTO> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<GroupMessageDTO> messageList) {
        this.messageList = messageList;
    }

    public boolean isRefreshDiv() {
        return refreshDiv;
    }

    public void setRefreshDiv(boolean refreshDiv) {
        this.refreshDiv = refreshDiv;
    }

    public ChatMainBean getChatMainBean() {
        return chatMainBean;
    }

    public void setChatMainBean(ChatMainBean chatMainBean) {
        this.chatMainBean = chatMainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public int getNumDays() {
        return numDays;
    }

    public void setNumDays(int numDays) {
        this.numDays = numDays;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public GroupDTO getGroupDetails() {
        return groupDetails;
    }

    public String setGroupDetails(String groupId) {
        if (loggedUserProfile.getGroupList().containsKey(Long.parseLong(groupId))) {
            groupDetails = loggedUserProfile.getGroupList().get(Long.parseLong(groupId));
            String gmembers = "";
            ArrayList<GroupMember> mList = new ArrayList<GroupMember>();
            if (groupDetails.getMembers() != null && groupDetails.getMembers().size() > 0) {
                Set set = groupDetails.getMembers().entrySet();
                Iterator i = set.iterator();
                while (i.hasNext()) {
                    Map.Entry me = (Map.Entry) i.next();
                    GroupMember dto = (GroupMember) me.getValue();
                    mList.add(dto);
                    gmembers += dto.getUserIdentity() + ",";
                }
            }
            groupDetails.setGroupMembers(gmembers);
            groupDetails.setMemberList(mList);
            return "";
        } else {
            return "Sorry! The group is not found!";
        }
    }
}
