/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger.groups;

import java.util.ArrayList;
import org.ipvision.messenger.GroupMessageDTO;

/**
 *
 * @author user
 */
public class SessionGroupMsgDTO {

    private Long groupId;
    private String groupName;
    private ArrayList<GroupMessageDTO> messageList;

    public SessionGroupMsgDTO() {
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public ArrayList<GroupMessageDTO> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<GroupMessageDTO> messageList) {
        this.messageList = messageList;
    }
}
