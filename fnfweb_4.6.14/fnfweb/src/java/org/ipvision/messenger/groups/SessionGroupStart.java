/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger.groups;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ipvision.messenger.GroupChatWindow;
import org.ipvision.users.LoggedUserProfile;

/**
 *
 * @author user
 */
@WebServlet(name = "SessionGroupStart", urlPatterns = {"/SessionGroupStart"})
public class SessionGroupStart extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    // to be changed
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            GroupChatWindow groupChatWindow = (GroupChatWindow) request.getSession().getAttribute("groupChatWindow");
            ServletMsgDTO smg = new ServletMsgDTO();
            if(groupChatWindow!=null){
            LoggedUserProfile loggedUserProfile = (LoggedUserProfile) request.getSession().getAttribute("loggedUserProfile");
            
            String groups = groupChatWindow.getSessionGroups();
            
            smg.setUserName(loggedUserProfile.getUserIdentity());
            smg.setGroups(groups);
            }else{
                smg.setGroups(null);
            }
            
            Gson gson = new Gson();
            out.print(gson.toJson(smg).toString());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
