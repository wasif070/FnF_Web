/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger.groups;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ipvision.messenger.GroupChatWindow;
import org.ipvision.messenger.GroupMessageDTO;
import org.ipvision.utils.MessengerConstants;

/**
 *
 * @author user
 */
@WebServlet(name = "SendGroupMessage", urlPatterns = {"/SendGroupMessage"})
public class SendGroupMessage extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            GroupChatWindow groupChatWindow = (GroupChatWindow) request.getSession().getAttribute("groupChatWindow");
            GroupMessageDTO mDto = new GroupMessageDTO();
            if (groupChatWindow != null) {
                String msg = request.getParameter("message");
                long groupId = 0;
                if (request.getParameter("groupId") != null) {
                    try {
                        groupId = Long.parseLong(request.getParameter("groupId"));
                    } catch (Exception e) {
                    }
                }
                mDto.setMessage(MessengerConstants.replaceEmoticons(msg));
                mDto.setGroupId(groupId);
                mDto.setUserName(request.getParameter("userName"));
                mDto.setMessageDate(System.currentTimeMillis());
                mDto.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
                groupChatWindow.sendServletMessage(groupId,msg, groupId);
            } else {
                mDto.setMessage(null);
            }

            Gson gson = new Gson();
            out.print(gson.toJson(mDto).toString());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
