/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger.friends;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ipvision.messenger.ChatWindowActivity;
import org.ipvision.messenger.MessageDTO;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.utils.MessengerConstants;

/**
 *
 * @author user
 */
@WebServlet(name = "SendMessage", urlPatterns = {"/SendMessage"})
public class SendMessage extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            ChatWindowActivity chatWindowActivity = (ChatWindowActivity) request.getSession().getAttribute("chatWindowActivity");
            MessageDTO mDto = new MessageDTO();
            if (chatWindowActivity != null) {
                LoggedUserProfile loggedUserProfile = (LoggedUserProfile) request.getSession().getAttribute("loggedUserProfile");
                String msg = request.getParameter("message");
                mDto.setMessage(MessengerConstants.replaceEmoticons(msg));
                mDto.setFriendName(request.getParameter("friendName"));
                mDto.setUserName(loggedUserProfile.getUserIdentity());
                mDto.setMessageDate(System.currentTimeMillis());
                mDto.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
                chatWindowActivity.sendServletMessage(msg, mDto.getFriendName());
            } else {
                mDto.setMessage(null);
            }
            Gson gson = new Gson();
            out.print(gson.toJson(mDto).toString());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
