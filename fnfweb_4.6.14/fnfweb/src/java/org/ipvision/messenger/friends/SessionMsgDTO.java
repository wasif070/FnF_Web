/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger.friends;

import java.util.ArrayList;
import org.ipvision.messenger.MessageDTO;

/**
 *
 * @author user
 */
public class SessionMsgDTO {

    private String friendName;
    private ArrayList<MessageDTO> messageList;

    public SessionMsgDTO() {
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public ArrayList<MessageDTO> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<MessageDTO> messageList) {
        this.messageList = messageList;
    }
}
