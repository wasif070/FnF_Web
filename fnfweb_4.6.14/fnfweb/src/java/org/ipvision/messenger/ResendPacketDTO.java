/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ipvision.messenger;

import java.net.DatagramPacket;

/**
 *
 * @author user
 */
public class ResendPacketDTO {
    private int counter;
    
    private   DatagramPacket packet;

    public int getCounter() {
        return counter++;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setPacket(DatagramPacket packet) {
        this.packet = packet;
    }
    
    
    
}
