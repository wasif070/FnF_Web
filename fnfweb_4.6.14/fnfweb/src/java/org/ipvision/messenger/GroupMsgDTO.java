/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class GroupMsgDTO implements Serializable {

    private int pt;
    private String uId;
    private String fndId;
    private long grpId;
    private String mg;
    private String pckId;
      public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public GroupMsgDTO() {
    }

    public int getPacketType() {
        return pt;
    }

    public void setPacketType(int packetType) {
        this.pt = packetType;
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public String getFriendName() {
        return fndId;
    }

    public void setFriendName(String friendName) {
        this.fndId = friendName;
    }

    public long getGroupId() {
        return grpId;
    }

    public void setGroupId(long groupId) {
        this.grpId = groupId;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

  
    
}
