/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class GroupMessageDTO implements Serializable {
    private String uId;
    private String mg;
    private long mgDate;
    private String messageDateString;
    private long grpId;

    public GroupMessageDTO() {
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public long getMessageDate() {
        return mgDate;
    }

    public void setMessageDate(long messageDate) {
        this.mgDate = messageDate;
    }

    public String getMessageDateString() {
        return messageDateString;
    }

    public void setMessageDateString(String messageDateString) {
        this.messageDateString = messageDateString;
    }
    public long getGroupId() {
        return grpId;
    }

    public void setGroupId(long groupId) {
        this.grpId = groupId;
    }
    
    
}
