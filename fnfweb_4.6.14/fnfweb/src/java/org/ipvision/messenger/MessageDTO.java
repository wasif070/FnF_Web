/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class MessageDTO implements Serializable {

    private String uId;
    private String fndId;
    private String mg;
    private long mgDate;
     private String messageDateString;

    public MessageDTO() {
    }

    public String getUserName() {
        return uId;
    }

    public void setUserName(String userName) {
        this.uId = userName;
    }

    public String getFriendName() {
        return fndId;
    }

    public void setFriendName(String friendName) {
        this.fndId = friendName;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public long getMessageDate() {
        return mgDate;
    }

    public void setMessageDate(long messageDate) {
        this.mgDate = messageDate;
    }

    public String getMessageDateString() {
        return messageDateString;
    }

    public void setMessageDateString(String messageDateString) {
        this.messageDateString = messageDateString;
    }
}
