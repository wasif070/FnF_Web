/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.ipvision.communication.ServerCommunication;
import org.ipvision.groups.GroupDTO;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.MessengerConstants;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "chatCommunication")
@SessionScoped
public class ChatCommunication implements Serializable {

    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;

    @ManagedProperty(value = "#{serverCommunication}")
    private ServerCommunication serverCommunication;

    private Map<String, ResendPacketDTO> friendChatStorage = new ConcurrentHashMap<String, ResendPacketDTO>();
    private Map<String, ResendPacketDTO> groupChatStorage = new ConcurrentHashMap<String, ResendPacketDTO>();
    DatagramSocket chatSocket;
    DatagramPacket finalPacket;
    static Logger logger = Logger.getLogger(ChatCommunication.class.getName());

    public String makeStatusPacket(int packetType, String userName, String frinedName) {
        RegisterMsgDTO messageObject = null;
        try {
            messageObject = new RegisterMsgDTO();
            messageObject.setUserName(userName);
            messageObject.setFriendName(frinedName);
            messageObject.setPacketType(packetType);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject).toString();
    }

    public String makeRegisterPacket(int packetType, String userName) {
        RegisterMsgDTO messageObject = null;
        try {
            String packet_id = ServerConstants.getRandromPacketId(userName);
            messageObject = new RegisterMsgDTO();
            messageObject.setUserName(userName);
            messageObject.setFriendName(null);
            messageObject.setPacketType(packetType);
            messageObject.setPacketId(packet_id);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject).toString();
    }

    public String makeFriendMessagePacket(int packetType, String userName, String friendName, String message, String packetId) {
        FriendMsgDTO messageObject = null;
        try {
            messageObject = new FriendMsgDTO();
            messageObject.setUserName(userName);
            messageObject.setFriendName(friendName);
            messageObject.setPacketType(packetType);
            messageObject.setMessage(message);
            messageObject.setPacketId(packetId);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject);
    }

    public String confirmationMessagePacket(int packetType, String packetId) {
        ConfirmationMsgDTO messageObject = null;
        try {
            messageObject = new ConfirmationMsgDTO();
            messageObject.setPacketType(packetType);
            messageObject.setPacketId(packetId);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject);
    }

    public String makeGroupMessagePacket(int packetType, String userName, String friendName, long groupId, String message, String packetId) {
        GroupMsgDTO messageObject = null;
        try {
            messageObject = new GroupMsgDTO();
            messageObject.setUserName(userName);
            messageObject.setFriendName(friendName);
            messageObject.setGroupId(groupId);
            messageObject.setPacketType(packetType);
            messageObject.setMessage(message);
            messageObject.setPacketId(packetId);

        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject);
    }

    public String makeGroupHistoryPacket(int packetType, String userName, long groupId, long messageDate, String packetId) {
        GroupHistoryDTO messageObject = null;
        try {
            messageObject = new GroupHistoryDTO();
            messageObject.setUserName(userName);
            messageObject.setGroupId(groupId);
            messageObject.setPacketType(packetType);
            messageObject.setMessageDate(messageDate);
            messageObject.setPacketId(packetId);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject);
    }

    public String makeFriendHistoryPacket(int packetType, String userName, String friendName, long messageDate, String packetId) {
        FriendHistoryDTO messageObject = null;
        try {
            messageObject = new FriendHistoryDTO();
            messageObject.setUserName(userName);
            messageObject.setFriendName(friendName);
            messageObject.setPacketType(packetType);
            messageObject.setMessageDate(messageDate);
            messageObject.setPacketId(packetId);
        } catch (Exception e) {
        }
        Gson gson = new Gson();
        return gson.toJson(messageObject);
    }

    boolean chatPortsAvailable(UserProfile userProfile, long currentTime, int tryCount, boolean sendRequest) {
       
        if (userProfile.getChatPortUpdateTime() > currentTime - 1000 * 60 * 3) {

            return true;
        } else if (tryCount < 1) {

            return false;
        } else {
            if (sendRequest) {
                String packet_id = loggedUserProfile.getUserIdentity() + currentTime;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("actn", ServerConstants.TYPE_START_FRIEND_CHAT);
                    jsonObject.put("pckId", packet_id);
                    jsonObject.put("uId", loggedUserProfile.getUserIdentity());
                    jsonObject.put("fndId", userProfile.getUserIdentity());
                    jsonObject.put("sId",loggedUserProfile.getSessionId());
                    serverCommunication.sendChatRequestPacket(jsonObject.toString(), packet_id);
                } catch (Exception e) {
                }
            }
            try {
                Thread.sleep(10);
            } catch (Exception e) {
            }
            return chatPortsAvailable(userProfile, currentTime, --tryCount, false);

        }

    }

    boolean chatPortsAvailable(GroupDTO groupDTO, long currentTime, int tryCount, boolean sendRequest) {
       
        if (groupDTO.getChatPortUpdateTime() > currentTime - 1000 * 60 * 3) {

            return true;
        } else if (tryCount < 1) {

            return false;
        } else {
            if (sendRequest) {
                String packet_id = loggedUserProfile.getUserIdentity() + currentTime;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("actn", ServerConstants.TYPE_START_GROUP_CHAT);
                    jsonObject.put("pckId", packet_id);
                    jsonObject.put("uId", loggedUserProfile.getUserIdentity());
                    jsonObject.put("grpId", groupDTO.getGroupId());
                    jsonObject.put("sId",loggedUserProfile.getSessionId());
                    serverCommunication.sendChatRequestPacket(jsonObject.toString(), packet_id);
                } catch (Exception e) {
                }
            }
            try {
                Thread.sleep(10);
            } catch (Exception e) {
            }
            return chatPortsAvailable(groupDTO, currentTime, --tryCount, false);

        }

    }

    
//to be changed
    public void sendPacketFriend(String dataPacket, String packetId, String friendId) {
        try {

            UserProfile userProfile = loggedUserProfile.getFriendlist().get(friendId);

            if (!chatPortsAvailable(userProfile, System.currentTimeMillis(), 1000, true)) {
                 System.out.println("chatPortsAvailable false");
                return;
            }
            System.out.println("chatPortsAvailable passed");

            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = userProfile.getChatIp();

            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, userProfile.getFRIEND_CHAT_PORT());

            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();

            resendPacketDTO.setPacket(finalPacket);
            friendChatStorage.put(packetId, resendPacketDTO);

        } catch (Exception e) {
            logger.info("ChatCommunication-->", e);
        }
    }

//        public void sendStartFriendChat(String friendId) {
//        try {
//            
////            "fndId":"webapp",
////		"uId":"anwar",
////		"actn":175,
////		"sId":"1397628903092anwar",
////		"pckId":"5mPDW8vqanwar"
//            InetAddress serverIP;
//            
//          JSONObject jsonObject = new JSONObject();
//      
//            jsonObject.put("actn", ServerConstants.TYPE_START_FRIEND_CHAT);
//                     jsonObject.put("fndId", friendId);
//            jsonObject.put("uId", friendId);
//             jsonObject.put("sId", friendId);
//             jsonObject.put("pckId", friendId);
//            byte[] sendingBytePacket = jsonObject.toString().getBytes();
//            serverIP = InetAddress.getByName(MessengerConstants.CHAT_SERVER_IP);
//            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, MessengerConstants.FRIEND_CHAT_PORT);
//            friendChatStorage.put(packetId, finalPacket);
//        } catch (UnknownHostException e) {
//            logger.info("ChatCommunication-->", e);
//        } catch (IOException e) {
//            logger.info("ChatCommunication-->", e);
//        }
//    }
//    
    public void sendPacketGroup(String dataPacket, String packetId,long groupId) {
        try {
            
              GroupDTO groupDTO = loggedUserProfile.getGroupList().get(groupId);

            if (!chatPortsAvailable(groupDTO, System.currentTimeMillis(), 1000, true)) {
                 System.out.println("chatPortsAvailable false");
                return;
            }
            System.out.println("chatPortsAvailable passed");

            
            
            
            InetAddress serverIP=groupDTO.getChatIp();
            byte[] sendingBytePacket = dataPacket.getBytes();
           
            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, groupDTO.getGROUP_CHAT_PORT());
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();

            resendPacketDTO.setPacket(finalPacket);
            friendChatStorage.put(packetId, resendPacketDTO);

          //  groupChatStorage.put(packetId, finalPacket);
            //    chatSocket.send(finalPacket);
        } catch (Exception e) {
            logger.info("ChatCommunication-->", e);
        }
    }

    public void sendPacketConfirmation(String dataPacket) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(MessengerConstants.CHAT_SERVER_IP);
            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, MessengerConstants.CHAT_CONFIRMATION_PORT);
            chatSocket.send(finalPacket);
        } catch (UnknownHostException e) {
//            System.out.println(e);
        } catch (IOException e) {
//            System.out.println(e);
        }
    }

    public void sendPacketReg(String dataPacket) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(MessengerConstants.CHAT_SERVER_IP);
            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, MessengerConstants.CHAT_REGISTRATION_PORT);
            System.out.println("send packet regis>" + new String(finalPacket.getData()));
            chatSocket.send(finalPacket);
//            System.out.println("send to--->reg message->>" + serverIP + ":" + MessengerConstants.CHAT_REGISTRATION_PORT + " Packet: " + dataPacket);
        } catch (UnknownHostException e) {
            logger.info("ChatCommunication-->", e);
        } catch (IOException e) {
            logger.info("ChatCommunication-->", e);
        }
    }

    public void sendPacketHistory(String dataPacket, String packetId) {
        try {
            InetAddress serverIP;
            byte[] sendingBytePacket = dataPacket.getBytes();
            serverIP = InetAddress.getByName(MessengerConstants.CHAT_SERVER_IP);

            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, serverIP, MessengerConstants.CHAT_HISTORY_PORT);
            chatSocket.send(finalPacket);
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();

            resendPacketDTO.setPacket(finalPacket);
            friendChatStorage.put(packetId, resendPacketDTO);

        } catch (UnknownHostException e) {
            logger.info("ChatCommunication-->", e);
        } catch (IOException e) {
            logger.info("ChatCommunication-->", e);
        }
    }

    public DatagramSocket getChatSocket() {
        return chatSocket;
    }

    public void setChatSocket(DatagramSocket chatSocket) {
        this.chatSocket = chatSocket;
    }

    public DatagramPacket getFinalPacket() {
        return finalPacket;
    }

    public void setFinalPacket(DatagramPacket finalPacket) {
        this.finalPacket = finalPacket;
    }

    public Map<String, ResendPacketDTO> getFriendChatStorage() {
        return friendChatStorage;
    }

    public void setFriendChatStorage(Map<String, ResendPacketDTO> friendChatStorage) {
        this.friendChatStorage = friendChatStorage;
    }

    public Map<String, ResendPacketDTO> getGroupChatStorage() {
        return groupChatStorage;
    }

    public void setGroupChatStorage(Map<String, ResendPacketDTO> groupChatStorage) {
        this.groupChatStorage = groupChatStorage;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ChatCommunication.logger = logger;
    }

    public ServerCommunication getServerCommunication() {
        return serverCommunication;
    }

    public void setServerCommunication(ServerCommunication serverCommunication) {
        this.serverCommunication = serverCommunication;
    }

}
