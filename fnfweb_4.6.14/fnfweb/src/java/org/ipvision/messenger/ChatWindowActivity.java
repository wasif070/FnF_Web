/*
 * and open the template in the editor.
 * To change this template, choose Tools | Templates
 */
package org.ipvision.messenger;

import com.google.gson.Gson;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.ipvision.communication.ServerCommunication;
import org.ipvision.emoticons.EmoticonDTO;
import org.ipvision.emoticons.EmoticonLoader;
import org.ipvision.messenger.friends.SessionMsgDTO;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.MessengerConstants;
import org.ipvision.utils.ServerConstants;
import org.ipvision.utils.Sorter;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "chatWindowActivity")
@SessionScoped
public class ChatWindowActivity implements OnReceiveListner, Serializable {

//    static Logger logger = Logger.getLogger(ChatWindowActivity.class.getName());
    @ManagedProperty(value = "#{chatMainBean}")
    private ChatMainBean chatMainBean;
    @ManagedProperty(value = "#{groupChatWindow}")
    private GroupChatWindow groupChatWindow;
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    
  
    
    
    private ArrayList<MessageDTO> messageList = new ArrayList<MessageDTO>();
    public HashMap<String, ArrayList<MessageDTO>> chatHistoryHashMap = new HashMap<String, ArrayList<MessageDTO>>();
    public HashMap<String, String> chatSession = new HashMap<String, String>();
    public HashMap<String, ArrayList<MessageDTO>> newMessageHashMap = new HashMap<String, ArrayList<MessageDTO>>();
    public HashMap<String, Boolean> chatRefresh = new HashMap<String, Boolean>();
    public HashMap<String, Integer> chatNoOfMessage = new HashMap<String, Integer>();
    private boolean refreshDiv;
    private boolean refreshNotification;
    DatagramSocket chatSocket;
    String friendName;
    String message;
    int packetType;
    String finalBytePacket;
    int numDays = 0;
    String hashKey = "";
    ArrayList<EmoticonDTO> emoticonList;
    private ArrayList<UserProfile> newFriendList;

    public ChatWindowActivity() {
        refreshDiv = true;
    }

    public String getSessionFriends() {
        String friendListJsonStr = "false";
        ArrayList<SessionMsgDTO> sessionMsgDTOs = new ArrayList<SessionMsgDTO>();
        Gson gson = new Gson();
        Iterator it = chatSession.entrySet().iterator();
        while (it.hasNext()) {
            SessionMsgDTO smdto = new SessionMsgDTO();
            Map.Entry pairs = (Map.Entry) it.next();
            smdto.setFriendName(pairs.getValue().toString());
            smdto.setMessageList(chatHistoryHashMap.get(pairs.getValue().toString()));
            sessionMsgDTOs.add(smdto);
        }
        friendListJsonStr = gson.toJson(sessionMsgDTOs).toString();
        return friendListJsonStr;
    }

    public String addSession(String fName) {
       
        
         
        
        
        
        
        if (!chatSession.containsKey(fName)) {
            chatSession.put(fName, fName);
        }
        ArrayList<MessageDTO> mList = new ArrayList<MessageDTO>();
        mList = chatHistoryHashMap.get(fName);
        Gson gson = new Gson();
        if (mList != null && mList.size() > 0) {
            newMessageHashMap.remove(fName);
            return gson.toJson(mList).toString();
        }
        return "false";

    }

    public void removeSession(String fName) {
        if (chatSession.containsKey(fName)) {
            chatSession.remove(fName);
        }
    }

    public String getNewMessages() {
        ArrayList<SessionMsgDTO> mList = new ArrayList<SessionMsgDTO>();
        Gson gson = new Gson();
        Iterator it = newMessageHashMap.entrySet().iterator();
        while (it.hasNext()) {
            SessionMsgDTO smdto = new SessionMsgDTO();
            Map.Entry pairs = (Map.Entry) it.next();
            smdto.setFriendName(pairs.getKey().toString());
            smdto.setMessageList(newMessageHashMap.get(pairs.getKey().toString()));
            if (loggedUserProfile.getFriendlist().containsKey(pairs.getKey().toString())) {
                loggedUserProfile.getFriendlist().get(pairs.getKey().toString()).setNoOfMsg(0);
            }
            mList.add(smdto);
            it.remove();
        }
        if (mList != null && mList.size() > 0) {
            return gson.toJson(mList).toString();
        } else {
            return "false";
        }
    }

    public String getChatUpdate() {
        String friendListJsonStr = "false";
        Gson gson = new Gson();
        if (newFriendList != null && newFriendList.size() > 0) {
            friendListJsonStr = gson.toJson(newFriendList).toString();
            newFriendList = new ArrayList<UserProfile>();
            return friendListJsonStr;
        }
        return friendListJsonStr;
    }

    public void sendStatusMessage(String fName) {
        friendName = fName;
        chatMainBean.getChatProvider().setOnReceiveListner(this);
        packetType = MessengerConstants.CHAT_TYPING;
        finalBytePacket = chatMainBean.getChatCommunication().makeStatusPacket(packetType, loggedUserProfile.getUserIdentity(), friendName);
        System.out.println(finalBytePacket);
        chatMainBean.getChatCommunication().sendPacketReg(finalBytePacket);
    }

    public void sendServletMessage(String msg, String fName) {
        message = msg;
        friendName = fName;
        chatMainBean.getChatProvider().setOnReceiveListner(this);
        packetType = MessengerConstants.CHAT_FRIEND;
        if (message != null && message.length() > 0) {
            setMessageView();
            String packetId = loggedUserProfile.getUserIdentity() + friendName + MessengerConstants.getRandromPacketId();
            finalBytePacket = chatMainBean.getChatCommunication().makeFriendMessagePacket(packetType, loggedUserProfile.getUserIdentity(), friendName, message, packetId);
            chatMainBean.getStoreAndNotifyTreads().notify_or_start_thread();
            chatMainBean.getChatCommunication().sendPacketFriend(finalBytePacket, packetId,fName);
        }
        message = "";
    }

    public void sendHistoryRequest(String fName, int nDays) {
        friendName = fName;
        numDays = nDays;
        if (chatHistoryHashMap.containsKey(friendName)) {
            chatHistoryHashMap.remove(friendName);
        }
        String packetId = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        packetType = MessengerConstants.CHAT_FRIEND_HISTORY;
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -numDays - 1);
        Date today30 = cal.getTime();
        finalBytePacket = chatMainBean.getChatCommunication().makeFriendHistoryPacket(
                packetType, loggedUserProfile.getUserIdentity(), friendName, today30.getTime(), packetId);
        chatMainBean.getChatCommunication().sendPacketHistory(finalBytePacket, packetId);
    }

    public String getEmoticonList() {
        String html = "<table>";
        ArrayList<EmoticonDTO> temp = EmoticonLoader.getInstance().getEmoticonList();
        if (temp != null && temp.size() > 0) {
            for (int i = 0; i < temp.size(); i++) {
                EmoticonDTO emo = temp.get(i);
                if (i % 8 == 0) {
                    if (i == 0) {
                        html += "<tr>";
                    } else {
                        html += "</tr><tr>";
                    }
                }
                html += "<td><div class=\"small\"><img  class=\"img\" src=\"" + emo.getUrl() + "\" title=\"" + emo.getName() + "\" onclick=\"smileyCode('" + emo.getSymbol() + "')\" /></div></td>";
            }
        }
        html += "</tr></table>";
        return html;
    }

    public String getStickerList() {
        String html = "<table>";
        ArrayList<EmoticonDTO> temp = EmoticonLoader.getInstance().getStickerList();
        if (temp != null && temp.size() > 0) {
            for (int i = 0; i < temp.size(); i++) {
                EmoticonDTO emo = temp.get(i);
                if (i % 3 == 0) {
                    if (i == 0) {
                        html += "<tr>";
                    } else {
                        html += "</tr><tr>";
                    }
                }
                html += "<td><div class=\"sticker\"><img  class=\"img\" src=\"" + emo.getUrl() + "\" title=\"" + emo.getName() + "\" onclick=\"smileyCode('" + emo.getSymbol() + "')\" /></div></td>";
            }
        }
        html += "</tr></table>";
        return html;
    }

    public String getNewMessage(String name) {
        if (chatNoOfMessage.containsKey(name)) {
            chatNoOfMessage.remove(name);
        }
        ArrayList<MessageDTO> mList = new ArrayList<MessageDTO>();
        mList = newMessageHashMap.get(name);
        Gson gson = new Gson();
        if (mList != null && mList.size() > 0) {
            newMessageHashMap.remove(name);
            return gson.toJson(mList).toString();
        }
        return "false";
    }

    public String getMsgStatus() {
        ArrayList<StatusDTO> array = new ArrayList<StatusDTO>();
        for (String s : chatSession.keySet()) {
            StatusDTO statusDTO = new StatusDTO();
            statusDTO.setFriendName(s);
            statusDTO.setStatusVal(chatSession.get(s));
            array.add(statusDTO);
        }
        if (array.size() > 0) {
            Gson gson = new Gson();
            return gson.toJson(array).toString();
        }
        return "false";
    }

    public ArrayList<MessageDTO> getFriendMessageList(String name) {
        if (chatNoOfMessage.containsKey(name)) {
            chatNoOfMessage.remove(name);
        }
        messageList = new ArrayList<MessageDTO>();
        messageList = chatHistoryHashMap.get(name);
        return messageList;
    }

    public boolean checkRefreshDiv() {
        if (refreshDiv) {
            refreshDiv = false;
            return true;
        } else {
            return false;
        }
    }

    public boolean checkChatNotification() {
        if (refreshNotification) {
            refreshNotification = false;
            return true;
        } else {
            return false;
        }
    }

    public int getNumberOfMessage(String friendIdentity) {
        if (chatNoOfMessage.containsKey(friendIdentity)) {
            return chatNoOfMessage.get(friendIdentity);
        } else {
            return 0;
        }
    }

    public boolean checkFriendChat(String friendIdentity) {
        if (chatRefresh.containsKey(friendIdentity)) {
            if (chatRefresh.get(friendIdentity)) {
                chatRefresh.remove(friendIdentity);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void setLayout(String name) {
        messageList = chatHistoryHashMap.get(name);
    }

    public void setListener() {
        chatMainBean.getChatProvider().setOnReceiveListner(this);
    }

    public void sendMessage() {
        chatMainBean.getChatProvider().setOnReceiveListner(this);
        packetType = MessengerConstants.CHAT_FRIEND;
        if (message != null && message.length() > 0) {
            setMessageView();
            String packetId = loggedUserProfile.getUserIdentity() + friendName + MessengerConstants.getRandromPacketId();
            finalBytePacket = chatMainBean.getChatCommunication().makeFriendMessagePacket(packetType, loggedUserProfile.getUserIdentity(), friendName, message, packetId);
            chatMainBean.getStoreAndNotifyTreads().notify_or_start_thread();
            chatMainBean.getChatCommunication().sendPacketFriend(finalBytePacket, packetId,friendName);
        }
        message = "";
    }

    public void sendMessage(String message, String friendName) {
        packetType = MessengerConstants.CHAT_FRIEND;
        if (message != null && message.length() > 0) {
            setMessageView(message, friendName);
            String packetId = loggedUserProfile.getUserIdentity() + friendName + MessengerConstants.getRandromPacketId();
            finalBytePacket = chatMainBean.getChatCommunication().makeFriendMessagePacket(packetType, loggedUserProfile.getUserIdentity(), friendName, message, packetId);
            chatMainBean.getStoreAndNotifyTreads().notify_or_start_thread();
            chatMainBean.getChatCommunication().sendPacketFriend(finalBytePacket, packetId,friendName);
        }
        setMessage("");
    }

    private void setMessageView(String message, String friendName) {

        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUserName(loggedUserProfile.getUserIdentity());
        messageDTO.setFriendName(friendName);
        messageDTO.setMessage(MessengerConstants.replaceEmoticons(message));
        messageDTO.setMessageDate(System.currentTimeMillis());
        messageDTO.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
        hashKey = friendName;

        messageList = chatHistoryHashMap.get(hashKey);
        if (messageList == null || messageList.size() < 1) {
            messageList = new ArrayList<MessageDTO>();
        }
        messageList.add(messageDTO);
        chatHistoryHashMap.put(hashKey, messageList);
        refreshDiv = true;
        refreshNotification = false;
    }

    private void setMessageView() {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUserName(loggedUserProfile.getUserIdentity());
        messageDTO.setFriendName(friendName);
        messageDTO.setMessage(MessengerConstants.replaceEmoticons(message));
        messageDTO.setMessageDate(System.currentTimeMillis());
        messageDTO.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
        hashKey = friendName;

        messageList = chatHistoryHashMap.get(hashKey);
        if (messageList == null || messageList.size() < 1) {
            messageList = new ArrayList<MessageDTO>();
        }
        messageList.add(messageDTO);
        chatHistoryHashMap.put(hashKey, messageList);
    }

    public void chatHistory() {
        if (chatHistoryHashMap.containsKey(friendName)) {
            chatHistoryHashMap.remove(friendName);
        }
        String packetId = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        packetType = MessengerConstants.CHAT_FRIEND_HISTORY;
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -numDays - 1);
        Date today30 = cal.getTime();
        finalBytePacket = chatMainBean.getChatCommunication().makeFriendHistoryPacket(
                packetType, loggedUserProfile.getUserIdentity(), friendName, today30.getTime(), packetId);
        chatMainBean.getChatCommunication().sendPacketHistory(finalBytePacket, packetId);
    }

    @Override
    public void onReceivedMessage(String msg) {
        refreshDiv = true;
        JSONObject receivedPacketDTO = null;
        try {
            receivedPacketDTO = new JSONObject(msg);
            if (receivedPacketDTO.has("pt")) {
                packetType = receivedPacketDTO.getInt("pt");
            } else {
                packetType = 0;
            }
            switch (packetType) {
                case MessengerConstants.CHAT_FRIEND:
                    MessageDTO msgDto = new MessageDTO();
                    msgDto.setFriendName(receivedPacketDTO.getString("fndId"));
                    msgDto.setUserName(receivedPacketDTO.getString("uId"));
                    msgDto.setMessage(MessengerConstants.replaceEmoticons(receivedPacketDTO.getString("mg")));
                    msgDto.setMessageDate(System.currentTimeMillis());
                    msgDto.setMessageDateString(MessengerConstants.getDate(System.currentTimeMillis(), MessengerConstants.CHAT_DATE_FORMAT));
//                    msgDto.setMessageDate(receivedPacketDTO.getLong("messageDate"));
//                    msgDto.setMessageDateString(MessengerConstants.getDate(receivedPacketDTO.getLong("messageDate"), MessengerConstants.CHAT_DATE_FORMAT));
                    hashKey = msgDto.getFriendName();
                    if (msgDto.getFriendName().equalsIgnoreCase(loggedUserProfile.getUserIdentity())) {
                        hashKey = msgDto.getUserName();
                    }
                    ArrayList<MessageDTO> temp = chatHistoryHashMap.get(hashKey);
                    if (temp == null) {
                        temp = new ArrayList<MessageDTO>();
                    }
                    temp.add(msgDto);
                    chatHistoryHashMap.put(hashKey, temp);
                    chatSession.put(hashKey, hashKey);

                    temp = newMessageHashMap.get(hashKey);
                    if (temp == null) {
                        temp = new ArrayList<MessageDTO>();
                    }
                    temp.add(msgDto);
                    newMessageHashMap.put(hashKey, temp);

                    chatRefresh.put(hashKey, true);
                    if (loggedUserProfile.getFriendlist().containsKey(hashKey)) {
                        UserProfile friendProfile = loggedUserProfile.getFriendlist().get(hashKey);
                        friendProfile.setNoOfMsg(friendProfile.getNoOfMsg() + 1);
                        if (newFriendList == null) {
                            newFriendList = new ArrayList<UserProfile>();
                        }
                        newFriendList.add(friendProfile);
                    }
                    refreshDiv = true;
                    refreshNotification = true;
                    break;
                case MessengerConstants.CHAT_TYPING:
                    chatSession.put(friendName, "Typing...");
                    break;
                case MessengerConstants.CHAT_IDEL:
                    chatSession.put(friendName, "Idel");
                    // Need to push 
                    break;
                case MessengerConstants.FRIEND_OFFLINE_CHAT:
                    try {
                        if (receivedPacketDTO.getInt("total_pak") > 0) {
                            JSONArray array = new JSONArray(receivedPacketDTO.getString("friendsMessage"));
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonMsg = array.getJSONObject(i);
                                MessageDTO mDto = new MessageDTO();
                                mDto.setFriendName(jsonMsg.getString("fndId"));
                                mDto.setUserName(jsonMsg.getString("uId"));
                                mDto.setMessage(MessengerConstants.replaceEmoticons(jsonMsg.getString("mg")));
                                mDto.setMessageDate(jsonMsg.getLong("mgDate"));
                                mDto.setMessageDateString(MessengerConstants.getDate(jsonMsg.getLong("mgDate"), MessengerConstants.CHAT_DATE_FORMAT));
                                hashKey = mDto.getFriendName();
                                if (mDto.getFriendName().equalsIgnoreCase(loggedUserProfile.getUserIdentity())) {
                                    hashKey = mDto.getUserName();
                                }
                                ArrayList<MessageDTO> tList = chatHistoryHashMap.get(hashKey);
                                if (tList == null || tList.size() < 1) {
                                    tList = new ArrayList<MessageDTO>();
                                }
                                tList.add(mDto);
                                chatHistoryHashMap.put(hashKey, tList);
                                chatRefresh.put(hashKey, true);
                                if (loggedUserProfile.getFriendlist().containsKey(hashKey)) {
                                    UserProfile friendProfile = loggedUserProfile.getFriendlist().get(hashKey);
                                    friendProfile.setNoOfMsg(friendProfile.getNoOfMsg() + 1);
                                    if (newFriendList == null) {
                                        newFriendList = new ArrayList<UserProfile>();
                                    }
                                    newFriendList.add(friendProfile);
                                }
                            }
                            ArrayList<MessageDTO> tList = chatHistoryHashMap.get(hashKey);
                            if (tList != null && tList.size() > 0) {
                                chatHistoryHashMap.put(hashKey, Sorter.sortArrayListASC(tList, "getMessageDate"));
                            }
                            refreshDiv = true;
                            refreshNotification = true;
                        }
                    } catch (Exception e) {
                        MessengerConstants.debugLog("Chat", "jsonException: " + e.toString());
                    }
                    break;
//                case MessengerConstants.CHAT_FRIEND_HISTORY:
//                    try {
//                        if (receivedPacketDTO.getInt("total_pak") > 0) {
//                            JSONArray array = new JSONArray(receivedPacketDTO.getString("message"));
//                            for (int i = 0; i < array.length(); i++) {
//                                JSONObject jsonMsg = array.getJSONObject(i);
//                                MessageDTO mDto = new MessageDTO();
//                                mDto.setFriendName(getFriendName());
//                                mDto.setUserName(jsonMsg.getString("userName"));
//                                mDto.setMessage(MessengerConstants.replaceEmoticons(jsonMsg.getString("message")));
//                                mDto.setMessageDate(jsonMsg.getLong("messageDate"));
//                                mDto.setMessageDateString(MessengerConstants.getDate(jsonMsg.getLong("messageDate"), MessengerConstants.CHAT_DATE_FORMAT));
//                                hashKey = getFriendName();
//                                if (mDto.getFriendName().equalsIgnoreCase(loggedUserProfile.getUserIdentity())) {
//                                    hashKey = mDto.getUserName();
//                                }
//                                ArrayList<MessageDTO> tList = chatHistoryHashMap.get(hashKey);
//                                if (tList == null || tList.size() < 1) {
//                                    tList = new ArrayList<MessageDTO>();
//                                }
//                                tList.add(mDto);
//                                chatHistoryHashMap.put(hashKey, tList);
//                                temp = newMessageHashMap.get(hashKey);
//                                if (temp == null) {
//                                    temp = new ArrayList<MessageDTO>();
//                                }
//                                temp.add(mDto);
//                                newMessageHashMap.put(hashKey, temp);
//                            }
//                            ArrayList<MessageDTO> tList = chatHistoryHashMap.get(hashKey);
//                            if (tList != null && tList.size() > 0) {
//                                chatHistoryHashMap.put(hashKey, Sorter.sortArrayListASC(tList, "getMessageDate"));
//                            }
//
//
//                            chatRefresh.put(hashKey, true);
//                        }
//                    } catch (Exception e) {
//                        MessengerConstants.debugLog("Chat", "jsonException: " + e.toString());
//                    }
//                    break;
                case MessengerConstants.CHAT_GROUP:
                    groupChatWindow.onReceivedMessage(msg);
                    break;
                case MessengerConstants.GROUP_OFFLINE_CHAT:
                    groupChatWindow.onReceivedMessage(msg);
                    break;
                case MessengerConstants.CHAT_GROUP_HISTORY:
                    groupChatWindow.onReceivedMessage(msg);
                    break;
            }
        } catch (Exception e) {
            MessengerConstants.debugLog("exception", e.toString());
        }
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MessageDTO> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<MessageDTO> messageList) {
        this.messageList = messageList;
    }

    public boolean isRefreshDiv() {

        return refreshDiv;
    }

    public void setRefreshDiv(boolean refreshDiv) {
        this.refreshDiv = refreshDiv;
    }

    public ChatMainBean getChatMainBean() {
        return chatMainBean;
    }

    public void setChatMainBean(ChatMainBean chatMainBean) {
        this.chatMainBean = chatMainBean;
    }

    public int getNumDays() {
        return numDays;
    }

    public void setNumDays(int numDays) {
        this.numDays = numDays;
    }

    public HashMap<String, ArrayList<MessageDTO>> getChatHistoryHashMap() {
        return chatHistoryHashMap;
    }

    public void setChatHistoryHashMap(HashMap<String, ArrayList<MessageDTO>> chatHistoryHashMap) {
        this.chatHistoryHashMap = chatHistoryHashMap;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public GroupChatWindow getGroupChatWindow() {
        return groupChatWindow;
    }

    public void setGroupChatWindow(GroupChatWindow groupChatWindow) {
        this.groupChatWindow = groupChatWindow;
    }
}
