/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

/**
 *
 * @author Anwar
 */
public class StatusDTO {
    private String fndId;
    private String statusVal;

    public String getFriendName() {
        return fndId;
    }

    public void setFriendName(String friendName) {
        this.fndId = friendName;
    }

    public String getStatusVal() {
        return statusVal;
    }

    public void setStatusVal(String statusVal) {
        this.statusVal = statusVal;
    }
    
}
