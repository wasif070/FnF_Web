/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

/**
 *
 * @author user
 */
public class ConfirmationMsgDTO {
    private int pt;
    private String pckId;

    public ConfirmationMsgDTO() {
    }

    public int getPacketType() {
        return pt;
    }

    public void setPacketType(int packetType) {
        this.pt = packetType;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }
}
