/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.messenger;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.SocketException;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author user
 */
@ManagedBean(name = "chatMainBean")
@SessionScoped
public class ChatMainBean implements Serializable {
    // Chat Communication

    @ManagedProperty(value = "#{chatCommunication}")
    private ChatCommunication chatCommunication;
    @ManagedProperty(value = "#{chatProvider}")
    private ChatProvider chatProvider;
    @ManagedProperty(value = "#{storeAndNotifyTreads}")
    StoreAndNotifyTreads storeAndNotifyTreads;
    //End Chat
    boolean isStarted = false;
    static Logger logger = Logger.getLogger(ChatMainBean.class.getName());

    public void setCommunicationSocket() {
        try {
            chatCommunication.setChatSocket(new DatagramSocket());
        } catch (SocketException se) {
            logger.info("ChatCommunication-->", se);
        } catch (IOException e) {
            logger.info("ChatCommunication-->", e);
        }
        chatProvider.setChatCommunication(chatCommunication);
        chatProvider.start();
        storeAndNotifyTreads.setChatCommunication(chatCommunication);

    }

    public ChatCommunication getChatCommunication() {
        return chatCommunication;
    }

    public void setChatCommunication(ChatCommunication chatCommunication) {
        this.chatCommunication = chatCommunication;
    }

    public ChatProvider getChatProvider() {
        return chatProvider;
    }

    public void setChatProvider(ChatProvider chatProvider) {
        this.chatProvider = chatProvider;
    }

    public StoreAndNotifyTreads getStoreAndNotifyTreads() {
        return storeAndNotifyTreads;
    }

    public void setStoreAndNotifyTreads(StoreAndNotifyTreads storeAndNotifyTreads) {
        this.storeAndNotifyTreads = storeAndNotifyTreads;
    }

    public boolean isIsStarted() {
        return isStarted;
    }

    public void setIsStarted(boolean isStarted) {
        this.isStarted = isStarted;
    }
}
