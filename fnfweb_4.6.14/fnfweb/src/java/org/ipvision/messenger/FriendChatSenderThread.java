package org.ipvision.messenger;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "friendChatSenderThread")
@SessionScoped
public class FriendChatSenderThread extends Thread implements Serializable {

    @ManagedProperty(value = "#{chatCommunication}")
    private ChatCommunication chatCommunication;
  
  
    boolean running = true;
   // int counter=0;
//    private static FriendChatSenderThread friendChatSenderThread;

//    public static FriendChatSenderThread getInstance() {
//        if (friendChatSenderThread == null) {
//            friendChatSenderThread = new FriendChatSenderThread();
//        }
//        return friendChatSenderThread;
//    }

    @Override
    public void run() {
        while (running) {
            try {
                if (!chatCommunication.getFriendChatStorage().isEmpty()) {
                    for (String key : chatCommunication.getFriendChatStorage().keySet()) {
                        ResendPacketDTO resendPacketDTO=chatCommunication.getFriendChatStorage().get(key);
                       
                        if (resendPacketDTO.getCounter() > 7 && chatCommunication.getFriendChatStorage().containsKey(key)) {
                            chatCommunication.getFriendChatStorage().remove(key);
                        } else {
                            chatCommunication.chatSocket.send(resendPacketDTO.getPacket());
                         
                            System.out.println("counter >"+ resendPacketDTO.getCounter());
                        }
                        Thread.sleep(500);
                    }
                } else {
                    Thread.sleep(1000);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public ChatCommunication getChatCommunication() {
        return chatCommunication;
    }

    public void setChatCommunication(ChatCommunication chatCommunication) {
        this.chatCommunication = chatCommunication;
    }

//    public int getCounter() {
//        return counter;
//    }
//
//    public void setCounter(int counter) {
//        this.counter = counter;
//    }
}
