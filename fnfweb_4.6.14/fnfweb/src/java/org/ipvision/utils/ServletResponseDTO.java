/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.utils;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class ServletResponseDTO implements Serializable{

    private boolean sucs;
    private String mg;
    private Integer numberOfNewRequest;
    private String myId;
    private Integer updateType;
    
    

    public Integer getUpdateType() {
        return updateType;
    }

    public void setUpdateType(Integer updateType) {
        this.updateType = updateType;
    }
    
    

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }




    public ServletResponseDTO() {
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean sucs) {
        this.sucs = sucs;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String mg) {
        this.mg = mg;
    }

    public Integer getNumberOfNewRequest() {
        return numberOfNewRequest;
    }

    public void setNumberOfNewRequest(Integer numberOfNewRequest) {
        this.numberOfNewRequest = numberOfNewRequest;
    }
}
