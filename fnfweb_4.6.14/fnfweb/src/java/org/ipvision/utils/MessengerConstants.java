/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.utils;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.ipvision.emoticons.EmoticonDTO;
import org.ipvision.emoticons.EmoticonLoader;
import org.ipvision.login.LoginAction;

/**
 *
 * @author user
 */
public class MessengerConstants {

    static Logger logger = Logger.getLogger(LoginAction.class.getName());
    private static boolean debugOn = false; // Need to be true to see debug,error log
    //Packet Type 1=>Register, 2=>Leave from chat,3=>Personal Message to Friend, 4=>Group mg,5 =>Group chat leave
    public static final int CHAT_REGISTER = 1;
    public static final int CHAT_FRIEND = 3;
    public static final int CHAT_UNREGISTERED = 2;
    public static final int CHAT_GROUP = 4;
    public static final int CHAT_GROUP_LEAVE = 5;
    public static final int CHAT_FRIEND_HISTORY = 6;
    public static final int CHAT_GROUP_HISTORY = 7;
    public static final int CHAT_CONFIRMATION = 8;
    public static final int CHAT_CONFIRMATION_GROUP = 9;
    public static final int CHAT_CONFIRMATION_HISTORY = 10;
    public static final int CHAT_GROUP_CONFIRMATION_HISTORY = 11;
    public static final int CHAT_HISTORY_RECEIVED_CONFIRMATION = 12;
    public static final int FRIEND_OFFLINE_CHAT = 13;
    public static final int GROUP_OFFLINE_CHAT = 14;
    public static final int CHAT_TYPING = 10;
    public static final int CHAT_IDEL = 11;

    public static final int BUFFER_SIZE = 5120;
    public static final String CHAT_DATE_FORMAT = "dd/MM/yyyy hh:mm a";
    //Chat Communication Port
   // public static String CHAT_SERVER_IP = "38.108.92.141";//"192.168.1.45";//
    
     public static String CHAT_SERVER_IP = "38.108.92.154";//"192.168.1.45";//
    
  
    public static int CHAT_REGISTRATION_PORT = 1500;
    public static int FRIEND_CHAT_PORT = 1502;
    public static int GROUP_CHAT_PORT = 1505;
    public static int CHAT_HISTORY_PORT = 1510;
    public static int CHAT_CONFIRMATION_PORT = 1512;
    public static String EMOTICON_URL = "http://24fnf.com/emoticon/";
    //

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date. 
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static void debugLog(String logTag, String s) {
        if (debugOn) {
            logger.debug(logTag + "===>" + s);
        }
    }

    public static void errorLog(String logTag, String s) {
        if (debugOn) {
            logger.debug(logTag + "===>" + s);
        }
    }

    public static String replaceEmoticons(String msg) {
        String imgTag = "<img src=\"{PH}\" alt=\"{AL}\" title=\"{AL}\" />";
        String simgTag = "<img src=\"{PH}\" alt=\"{AL}\" title=\"{AL}\" width='25' height='25' />";
        String placeHolder = "{PH}";
        String altHolder = "{AL}";

        HashMap<String, EmoticonDTO> codeMap = EmoticonLoader.getInstance().emoticonHashMap;
        if (codeMap != null) {
            for (String key : codeMap.keySet()) {
                EmoticonDTO emo = codeMap.get(key);
                if (msg.contains(key.toString())) {
                    switch (emo.getType()) {
                        case 0:
                            simgTag = simgTag.replace(altHolder, emo.getName());
                            msg = msg.replace(key.toString(), simgTag.replace(placeHolder, emo.getUrl()));
                            break;
                        case 1:
                            imgTag = imgTag.replace(altHolder, emo.getName());
                            msg = msg.replace(key.toString(), imgTag.replace(placeHolder, emo.getUrl()));
                            break;
                    }

                }
            }
        }
        return msg;
    }

    public static String getRandromPacketId() {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);
        return key;
    }
}
