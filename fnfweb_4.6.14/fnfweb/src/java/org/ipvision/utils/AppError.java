/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.utils;

/**
 *
 * @author Administrator
 */
public class AppError {

    public static int NORMAL_ERROR = 7;
    public static int SEVERE_ERROR = 9;
    public static int MAILING_ERROR = 6;
    public static int DUPLICATE_MAIL = 4;
    public static int DUPLICATE_IP = 5;
    public static int NO_ERROR = 0;
    public static int VALIDATION_ERROR = 1;
    public static int DB_ERROR = 3;
    public static int OTHERS_ERROR = 3;
    public static int VIEW_EXPIRED = 8;
    public int errorType;
    public String errorMessage;

    public AppError() {
        errorType = NO_ERROR;
        // errorMessage="No error detected.";
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }
}
