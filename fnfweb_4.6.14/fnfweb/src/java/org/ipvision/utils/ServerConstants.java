/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.utils;

import java.security.SecureRandom;

/**
 *
 * @author user
 */
public class ServerConstants {

    

    
    public static final String API_VERSION = "101";
    public static double EMOTICON_VERSION = 0;
    public static final int DEVICE_CODE = 5;
// public static final String AUTH_SERVER_IP = "192.168.1.101";//"38.108.92.144";//
    public static final String AUTH_SERVER_IP = "38.108.92.243";
 
    

    
    public static int CHAT_PORT = 9493;
    public static Integer AUTH_SERVER_PORT = 9499;
    public static int CONFIRMATION_PORT = 9495;
    public static int UPDATE_PORT = 9496;
    public static int REQUEST_PORT = 9497;
    public static int CALLING_PORT = 9498;
    public static int KEEP_ALIVE_PORT = 9500;  
    public static int AUTHENTICATION_PORT = 9499;  
    
    
    public static int SMS_SERVER_PORT = 9494;

    public final static int STATUS_IN_PROGRESS = 201;
    public final static int STATUS_SENT = 202;
    public final static int STATUS_NOT_DELEVERED = 203;
    public final static int STATUS_NOT_ENOUGH_BALANCE = 204;

    public final static int TYPE_FRIEND_SMS = 81;
    public final static int TYPE_GROUP_SMS = 82;
    public final static int TYPE_KNOW_SMS_STATUS = 83;

    public static final int TYPE_CONFIRMATION = 200; // "signin";   
    public static final int TYPE_INVALID_LOGIN_SESSION = 19;
    public static final int TYPE_SIGN_IN = 20; // "signin";    
    public static final int TYPE_SIGN_UP = 21; // "signup";
    public static final int TYPE_SIGN_OUT = 22; //"signout";    
    public static final int TYPE_CONTACT_LIST = 23; // "contactList";
    public static final int TYPE_USER_ID_AVAILABLE = 24; // "userIdentityAvailable";
    public static final int TYPE_USER_PROFILE = 25; // "user_profile";    
    public static final int TYPE_KEEP_ALIVE = 26; // "keepAlive";
    public static final int TYPE_WHAT_IS_IN_UR_MIND = 31; // "whatisInYourMind";
    public static final int TYPE_CONTACT_SEARCH = 34; // "contactSearch";
    public static final int TYPE_CALL_LOG = 35; // "callLog";//----------------------------------------- changes on 20.03.2014 --------------------------------
    public static final int TYPE_CALL_LOG_SINGLE_FRIEND = 36; // "callLogSingleFriend";//----------------------------------------- changes on 20.03.2014 --------------------------------
    public static final int TYPE_PROFILE_IMAGE_TEXT = 37; // "profileImage";
    public static final int TYPE_ADD_ADDRESSBOOK_FRIEND = 38; // "addAddressBookFriend";
    public static final int TYPE_PASSWORD_RECOVERY = 39; // "passwordRecovery";
    public static final int TYPE_PASSWORD_RESET = 40; // "passwordReset";
    public static final int TYPE_EMOTICONS = 42; // "emoticons";
    public static final int TYPE_REMOVE_PROFILE_IMAGE = 43; // "removeProfileImage";
    public static final int TYPE_BALANCE = 44; // "balance";
    public static final int TYPE_CREATE_GROUP = 50;//"create_group";
    public static final int TYPE_NEW_GROUP = 51;//"new_group";
    public static final int TYPE_LEAVE_GROUP = 53;// "leave_group";
    public static final int TYPE_SEND_SMS = 61;//”send_sms”
    public static final int TYPE_FNF_HEADERS = 62;// "fnf_headers";/*Types in SMS */ 
    public static final int TYPE_UPDATE_IMAGE_FROM_WEB = 63;//”update_image_from_web” 
    public static final int TYPE_GROUP_LIST = 70;//----------------------------------------- changes on 20.03.2014 --------------------------------
    public static final int TYPE_ADDRESS_BOOK = 71;
    public static final int TYPE_PRIVACY_SETTINGS = 74;   //"privacy_settings"
    public static final int TYPE_MULTIPLE_SESSION_WARNING = 75;
    public static final int TYPE_SESSION_VALIDATION = 76;
    public static final int TYPE_SERVER_REPORT = 77;
     public static final int TYPE_PRESENCE = 78;
 
     public static final int TYPE_COMMENTS_FOR_STATUS = 84;
    public static final int TYPE_UPLOAD_ALBUM_IMAGE = 85;
    public static final int TYPE_STICKERS = 86;
    public static final int TYPE_NEWS_FEED = 88;
    public static final int TYPE_COMMENTS_FOR_IMAGE = 89;
    public static final int TYPE_UPDATE_UPLOAD_ALBUM_IMAGE = 90;
    public static final int TYPE_STICKERS_BACKGROUND_IMAGES = 91;
    public static final int TYPE_LIKES_FOR_STATUS = 92;
    public static final int TYPE_LIKES_FOR_IMAGE = 93;
    public static final int TYPE_MY_BOOK = 94;
    public static final int TYPE_USER_DETAILS = 95;
    public static final int TYPE_MY_ALBUMS = 96;
    public static final int TYPE_MY_ALBUM_IMAGES = 97;
    public static final int TYPE_GROUP_MEMBERS_LIST = 99;
    public static final int TYPE_FRIENDS_PRESENCE_LIST = 98;
    
    
   
    public static final int TYPE_ADD_FRIEND = 127; //"add_friend";
    public static final int TYPE_UPDATE_ADD_FRIEND = 327; //"add_friend";
    public static final int TYPE_DELETE_FRIEND = 128; // "delete_friend";
    public static final int TYPE_UPDATE_DELETE_FRIEND = 328; // "delete_friend";
    public static final int TYPE_ACCEPT_FRIEND = 129; //"accept_friend";
    public static final int TYPE_UPDATE_ACCEPT_FRIEND = 329; //"accept_friend";
    public static final int TYPE_CHANGE_PASSWORD = 130; // "change_password";
    public static final int TYPE_UPDATE_CHANGE_PASSWORD = 330; // "change_password";
    public static final int TYPE_CALL_START = 132; // "call_start";  
    public static final int TYPE_UPDATE_CALL_START = 332; // "call_start";
    public static final int TYPE_CALL_END = 133; // "call_end";
    public static final int TYPE_UPDATE_CALL_END = 333; // "call_end";
    public static final int TYPE_EMOTICONS_ALL = 141; // "emoticons_all";
    public static final int TYPE_UPDATE_EMOTICONS_ALL = 341; // "emoticons_all";
    public static final int TYPE_DELETE_GROUP = 152;// "delete_group";
    public static final int TYPE_UPDATE_DELETE_GROUP = 352;// "delete_group";
    public static final int TYPE_REMOVE_GROUP_MEMBER = 154;  //"remove_group_member";  
    public static final int TYPE_UPDATE_REMOVE_GROUP_MEMBER = 354;  //"remove_group_member"; 
    public static final int TYPE_ADD_GROUP_MEMBER = 156;// "add_group_member";
    public static final int TYPE_UPDATE_ADD_GROUP_MEMBER = 356;// "add_group_member";
    public static final int TYPE_EDIT_GROUP_MEMBER = 158;//  "edit_group_member";
    public static final int TYPE_UPDATE_EDIT_GROUP_MEMBER = 358;//  "edit_group_member";
    public static final int TYPE_ADD_ADDRESS_BOOK_FRIEND = 172;
    public static final int TYPE_UPDATE_ADD_ADDRESS_BOOK_FRIEND = 372;
    public static final int TYPE_ADD_MULTIPLE_FRIEND = 173; //"add_multiple_friend";
    public static final int TYPE_UPDATE_ADD_MULTIPLE_FRIEND = 373; //"add_multiple_friend";

    public static final int TYPE_SEND_REGISTER = 174; // "want to start a call";
    public static final int TYPE_UPDATE_SEND_REGISTER = 374; // "want to start a call";

    
    public static final int TYPE_START_CHAT = 175; // "want to start a call";
    public static final int TYPE_UPDATE_START_CHAT = 375; // "want to start a call";

    public static final int TYPE_DELETE_ALBUM_IMAGE = 176;
    public static final int TYPE_UPDATE_DELETE_ALBUM_IMAGE = 376;

    public static final int TYPE_ADD_STATUS = 177;
    public static final int TYPE_UPDATE_ADD_STATUS = 377;

    public static final int TYPE_EDIT_STATUS = 178;
    public static final int TYPE_UPDATE_EDIT_STATUS = 378;

    public static final int TYPE_DELETE_STATUS = 179;
    public static final int TYPE_UPDATE_DELETE_STATUS = 379;

    public static final int TYPE_ADD_IMAGE_COMMENT = 180;
    public static final int TYPE_UPDATE_ADD_IMAGE_COMMENT = 380;

    public static final int TYPE_ADD_STATUS_COMMENT = 181;
    public static final int TYPE_UPDATE_ADD_STATUS_COMMENT = 381;

    public static final int TYPE_DELETE_IMAGE_COMMENT = 182;
    public static final int TYPE_UPDATE_DELETE_IMAGE_COMMENT = 382;

    public static final int TYPE_DELETE_STATUS_COMMENT = 183;
    public static final int TYPE_UPDATE_DELETE_STATUS_COMMENT = 383;

    public static final int TYPE_LIKE_STATUS = 184;
    public static final int TYPE_UPDATE_LIKE_STATUS = 384;

    public static final int TYPE_LIKE_IMAGE = 185;
    public static final int TYPE_UPDATE_LIKE_IMAGE = 385;

    public static final int TYPE_UNLIKE_STATUS = 186;
    public static final int TYPE_UPDATE_UNLIKE_STATUS = 386;

    public static final int TYPE_UNLIKE_IMAGE = 187;
    public static final int TYPE_UPDATE_UNLIKE_IMAGE = 387;
    
    
    public static final int TYPE_START_FRIEND_CHAT = 175; // "want to start friend chat";
public static final int TYPE_START_GROUP_CHAT = 188; // "want to start group chat";

//------------------------------------------------- ACTION (start) ------------------------------------------------    
//    public static final String REQUEST = "REQUEST";
//    public static final String CALL = "CALL";
//    public static final String UPDATE = "UPDATE";
//    public static final String RESPONSE = "RESPONSE";
//    public static final String CONFIRMATION = "CONFIRMATION";
//    public static final String PRESENCE = "PRESENCE";
//    public static final String AUTHENTICATION = "AUTHENTICATION";
//    public static final String ACTION_SIGN_OUT = "SIGN_OUT";
////------------------------------------------------- ACTION (end) --------------------------------------------------
//    //------------------------------------------------- TYPE (start) --------------------------------------------- 
//    public static final String ADD_FRIEND = "add_friend";
//    public static final String DELETE_FRIEND = "delete_friend";
//    public static final String ACCEPT_FRIEND = "accept_friend";
//    public static final String CHANGE_PASSWORD = "change_password";
//    public static final String WHAT_IS_IN_UR_MIND_TYPE = "wim";
//    public static final String USER_PROFILE = "user_profile";
//    public static final String CALL_START = "call_start";
//    public static final String CALL_END = "call_end";
//    public static final String CONTACT_LIST = "contactList";
//    public static final String CONTACT_SEARCH = "contactSearch";
//    public static final String USER_ID_AVAILABLE = "userIdentityAvailable";
//    public static final String CALL_LOG = "callLog";
//    public static final String PASSWORD_RECOVERY = "passwordRecovery";
//    public static final String PASSWORD_RESET = "passwordReset";
//    public static final String PRIVACY_SETTINGS = "privacy_settings";
//    public static final String SIGN_UP = "signup";
//    public static final String SIGN_OUT = "signout";
//    public static final String KEEP_ALIVE = "keepAlive";
//    public static final byte[] KEEP_ALIVE_BYTE = KEEP_ALIVE.getBytes();
//    public static final String CALLLogSingleFriend = "callLogSingleFriend";
//    public static final String WHAT_IS_IN_YOUR_MIND = "wim";
//    public static final String REMOVE_PROFILE_IMAGE = "removeProfileImage";
//    public static final String PROFILE_IMAGE_TEXT = "prIm";
//    public static final String GROUP_LIST = "groupList";
//    public static final String LEAVE_GROUP = "leave_group";
//    public static final String DELETE_GROUP = "delete_group";
//    public static final String REMOVE_GROUP_MEMBER = "remove_group_member";
//    public static final String CREATE_GROUP = "create_group";
//    public static final String ADD_GROUP_MEMBER = "add_group_member";
//    public static final String EDIT_GROUP_MEMBER = "edit_group_member";
//    public static final String NEW_GROUP = "new_group";
//    public static final String SEND_SMS = "send_sms";
//    public static final String ADD_MULTIPLE_FRIEND = "add_multiple_friend";
//    public static final String ADDRESS_BOOK = "addressBook";
//    public static final String ADD_ADDRESS_BOOK_FRIEND = "addAddressBookFriend";
//    public static final String EMOTICONS = "emoticons";
//    public static final String FNF_HEADER_VERSION = "fnfHd";
//    public static final String FNF_HEADERS = "fnf_headers";
    //End Type
    public static final int BUFFER_SIZE = 30000;
    public static final int CONS_FIRST_NAME = 1;
    public static final int CONS_LAST_NAME = 2;
    public static final int CONS_GENDER = 3;
    public static final int CONS_COUNTRY = 4;
    public static final int CONS_MOBILE_PHONE = 5;
    public static final int CONS_EMAIL = 6;
    public static final int CONS_PRESENCE = 7;
    public static final int CONS_WHAT_IS_IN_UR_MIND = 8;
    public static final int CONS_FRIENDSHIP_STATUS = 9;
    public static final int CONS_CALLING_CODE = 10;
    public static final int CONS_PREV_PASS = 11;
    public static final int CONS_NEW_PASS = 12;
    public static final int CONS_PROFILE_IMAGE = 13;
    public static final int CONS_BIRTHDAY = 14;
    public static final int CONS_COVER_IMAGE = 15;
    public static final int CONS_PRIVACY = 16;
    
    
    public static int SHOW_ALL = 1;
    public static int SHOW_ONLY_FRIEND = 2;
    public static int DONOT_SHOW = 3;
    // friendship status
    public static int ACCEPTED_FRIEND = 1;//"1";
    public static int INCOMING_FRIEND_REQUEST = 2;// "2";
    public static int OUTGOING_FRIEND_REQUEST = 3;//"3";
    
    public static int PRESENCER_ONLINE = 2;//"3";
    public static int PRESENCER_OFFLINE = 1;//"3";
    
    
    
    
    public static final int DEVICE_PC = 1;
    public static final int DEVICE_ANDROID = 2;
    public static final int DEVICE_I_PHONE = 3;
    public static final int DEVICE_WINDOWS = 4;
    public static final int DEVICE_WEB = 5;
    
    public static String getRandromPacketId() {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);
        return key;
    }

    public static String getRandromPacketId(String uId) {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);
        if (uId != null) {
            key = key + uId;
        }
        return key;
    }
}
