/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.emoticons;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.ipvision.utils.MessengerConstants;

/**
 *
 * @author user
 */
public class EmoticonLoader {

    private static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    public HashMap<String, EmoticonDTO> emoticonHashMap;
    public ArrayList<EmoticonDTO> emoticonList;
    public ArrayList<EmoticonDTO> stickerList;
    static EmoticonLoader settingLoader = null;
    static Logger logger = Logger.getLogger(EmoticonLoader.class.getName());

    public EmoticonLoader() {
    }

    public static EmoticonLoader getInstance() {
        if (settingLoader == null) {
            createLoader();
        }
        return settingLoader;
    }

    private synchronized static void createLoader() {
        if (settingLoader == null) {
            settingLoader = new EmoticonLoader();
        }
    }

    public void getsetdata() {
        emoticonHashMap = new HashMap<String, EmoticonDTO>();
        emoticonList = new ArrayList<EmoticonDTO>();
        stickerList = new ArrayList<EmoticonDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select * from emoticon";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                EmoticonDTO dto = new EmoticonDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                dto.setSymbol(rs.getString("symbol"));
                dto.setType(rs.getInt("type"));
                dto.setUrl(MessengerConstants.EMOTICON_URL + rs.getString("url"));
                emoticonHashMap.put(dto.getSymbol(), dto);
                if (dto.getType() == 0) {
                    emoticonList.add(dto);
                } else if (dto.getType() == 1) {
                    stickerList.add(dto);
                }
               
            }
        } catch (Exception e) {
            logger.info("Exception Emoticon List-->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            getsetdata();
        }
    }

    public synchronized ArrayList<EmoticonDTO> getEmoticonList() {
        checkForReload();
        return emoticonList;
    }

    public synchronized ArrayList<EmoticonDTO> getStickerList() {
        checkForReload();
        return stickerList;
    }

    public EmoticonDTO getEmoticonBySymbol(String sybmol) {
        if (emoticonHashMap.containsKey(sybmol)) {
            return emoticonHashMap.get(sybmol);
        }
        return null;
    }

    public static void main(String[] args) {
//        try {
//            InetAddress giriAddress = java.net.InetAddress.getByName("www.24fnf.com");
//            String address = giriAddress.getHostAddress();
//            System.out.println("Address:::" + address);
//        } catch (Exception e) {
//            System.out.println("E-->" + e);
//        }
    }
}
