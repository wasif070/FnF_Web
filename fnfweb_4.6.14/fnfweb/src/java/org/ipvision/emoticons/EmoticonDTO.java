/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.emoticons;

/**
 *
 * @author user
 */
public class EmoticonDTO {

    private int id;
    private String name;
    private String symbol;
    private String url;
    private int type;

    public EmoticonDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
