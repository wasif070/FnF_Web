/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.login;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.ipvision.book.BookAction;
import org.ipvision.communication.KeepAliveThread;
import org.ipvision.communication.MainBean;
import org.ipvision.friends.FriendsBean;
import org.ipvision.groups.GroupAction;
import org.ipvision.groups.GroupDTO;
import org.ipvision.messenger.ChatMainBean;

import org.ipvision.net.OnReceiveListner;
import org.ipvision.registration.CountryBean;
import org.ipvision.sms.SMSAction;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserHomeAction;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.AppConstants;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "loginAction")
@SessionScoped
public class LoginAction implements OnReceiveListner, Serializable {
    // AullAuthontication System

    static Logger logger = Logger.getLogger(LoginAction.class.getName());
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
    @ManagedProperty(value = "#{chatMainBean}")
    private ChatMainBean chatMainBean;

    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    @ManagedProperty(value = "#{userHomeAction}")
    private UserHomeAction userHomeAction;

    @ManagedProperty(value = "#{bookAction}")
    private BookAction bookAction;

    @ManagedProperty(value = "#{friendsBean}")
    private FriendsBean friendsBean;
    @ManagedProperty(value = "#{groupAction}")
    private GroupAction groupAction;
    @ManagedProperty(value = "#{smsAction}")
    private SMSAction smsAction;
    @ManagedProperty(value = "#{keepAliveThread}")
    KeepAliveThread keepAliveThread;

    @ManagedProperty(value = "#{countrybean}")
    CountryBean countrybean;

    String uId;
    private String cnty = "Bangladesh";
    String usrPw;
    String secCo;
    String retypePassword;
    boolean isComplete = false;
    String errorMessage;
    String signOutMessage = "";
    boolean isSignOutfromAnotherDevice = false;

    String country_code = "+880";
    private String pnN = "";

    public String getCountry() {
        return cnty;
    }

    public void setCountry(String cnty) {
        this.cnty = cnty;
    }

    public LoginAction() {
    }

    public String signin() {
        JSONObject jsonObject = new JSONObject();
        if (mainBean.getServerCommunication().getServerSocket() == null) {
            mainBean.setCommunicationSocket();
        }
        mainBean.getCommunicationProvider().setOnReceiveListner(this);
        if (chatMainBean.getChatCommunication().getChatSocket() == null) {
            chatMainBean.setCommunicationSocket();
        }
        String packet_id = uId + System.currentTimeMillis();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_SIGN_IN);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("mblDc", country_code);
            jsonObject.put("mbl", pnN);

            jsonObject.put("usrPw", usrPw);
            jsonObject.put("vsn", ServerConstants.API_VERSION);
            jsonObject.put("dvc", ServerConstants.DEVICE_CODE);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendAuthenticationPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (loggedUserProfile.getSessionId() != null && loggedUserProfile.getSessionId().length() > 0) {
                    isComplete = false;
                    getContactListRequest();
                    getGroupListRequest();
                    // getNewsFeedRequest();
                    // getMyBookRequest();
                    //getMyAlbumRequest();
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        return "";
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
        return "/secure/user/welcome.xhtml" + AppConstants.REDIRECT;
    }

    public String forgotPassword() {
        JSONObject jsonObject = new JSONObject();
        mainBean.getCommunicationProvider().setOnReceiveListner(this);
        String packet_id = uId + System.currentTimeMillis();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_PASSWORD_RECOVERY);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("uId", uId);
            jsonObject.put("usrPw", usrPw);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        return "";
                    }
                }
            } catch (InterruptedException ex) {
            }
        }

        return "/open/user/reset_password.xhtml" + AppConstants.REDIRECT;
    }

    public void resetPassword() {
        JSONObject jsonObject = new JSONObject();
        mainBean.getCommunicationProvider().setOnReceiveListner(this);
        String packet_id = uId + System.currentTimeMillis();
        if (!usrPw.equals(retypePassword)) {
            errorMessage = "Password & Retype Password doesn't match";
        } else if (usrPw.contains(",")) {
            errorMessage = "Password doesn't contains any comma.";
        } else {
            try {
                jsonObject.put("actn", ServerConstants.TYPE_PASSWORD_RESET);
                jsonObject.put("pckId", packet_id);
                jsonObject.put("secCo", secCo);
                jsonObject.put("newPassword", usrPw);
                mainBean.getNotifyThread().notify_or_start_thread();
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
            } catch (Exception e) {
            }

            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    public String signout() {
        JSONObject jsonObject = new JSONObject();
        mainBean.getCommunicationProvider().setOnReceiveListner(this);
        String packet_id = ServerConstants.getRandromPacketId(uId);
        try {
            jsonObject.put("actn", ServerConstants.TYPE_SIGN_OUT);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendAuthenticationPacket(jsonObject.toString(), packet_id);

            keepAliveThread.stopMe();
            synchronized (keepAliveThread) {
                keepAliveThread.notify();
            }

            chatMainBean.getStoreAndNotifyTreads().stopService();
            chatMainBean.getChatProvider().stopService();

            mainBean.getNotifyThread().stopService();
            mainBean.getCommunicationProvider().stopService();
            loggedUserProfile.setSessionId(null);

            FacesContext sess = FacesContext.getCurrentInstance();
            HttpServletRequest ss = (HttpServletRequest) sess.getExternalContext().getRequest();
            HttpSession sss = ss.getSession(false);
            sss.invalidate();

        } catch (Exception e) {
        }
        return "/open/user/login.xhtml" + AppConstants.REDIRECT;
    }

    public void redirectToLoginPage() {
        try {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ExternalContext extContext = ctx.getExternalContext();
            String url = extContext.encodeActionURL(ctx.getApplication().getViewHandler().getActionURL(ctx, "/open/user/login.xhtml"));
            try {
                extContext.redirect(url);
            } catch (IOException ioe) {
                throw new FacesException(ioe);
            }

        } catch (Exception e) {
        }
    }

    public void isFourceSignOut() {
        if (keepAliveThread != null) {
            keepAliveThread.setCurrentLoginTime(System.currentTimeMillis());
        }

        if (isSignOutfromAnotherDevice || loggedUserProfile == null || loggedUserProfile.getSessionId() == null) {
            isSignOutfromAnotherDevice = false;
            loggedUserProfile.setSessionId(null);
            FacesContext sess = FacesContext.getCurrentInstance();
            HttpServletRequest ss = (HttpServletRequest) sess.getExternalContext().getRequest();
            HttpSession sss = ss.getSession(false);
            sss.invalidate();
            redirectToLoginPage();
        }
    }

    public void removePreviousSession() {
        isSignOutfromAnotherDevice = false;
        loggedUserProfile.setSessionId(null);
        loggedUserProfile.setUserIdentity(null);
    }

    @Override
    public void onReceivedMessage(String msg) {

        System.out.println("received>>>>>>>>>>>>>>" + loggedUserProfile.getUserIdentity() + msg);

//        logger.info("LoginActionReceiver==>" + msg.trim());
        try {
            JSONObject jsonObject = new JSONObject(msg.trim());
            int actn = jsonObject.getInt("actn");
            if (actn == ServerConstants.TYPE_SIGN_OUT || actn == ServerConstants.TYPE_MULTIPLE_SESSION_WARNING || actn == ServerConstants.TYPE_INVALID_LOGIN_SESSION) {
                if (jsonObject.getString("sId").equals(loggedUserProfile.getSessionId())) {
                    if (jsonObject.has("mg")) {
                        signOutMessage = jsonObject.getString("mg");
                    }
                    keepAliveThread.stopMe();
                    synchronized (keepAliveThread) {
                        keepAliveThread.notify();
                    }
                    mainBean.getNotifyThread().stopService();
                    mainBean.getCommunicationProvider().stopService();

                    chatMainBean.getStoreAndNotifyTreads().stopService();
                    chatMainBean.getChatProvider().stopService();
                    isSignOutfromAnotherDevice = true;
                } else {
                    isSignOutfromAnotherDevice = false;
                }
            } else if (actn == ServerConstants.TYPE_PRESENCE) {
                userHomeAction.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_SIGN_IN) {
                if (jsonObject.getBoolean("sucs")) {
                    loggedUserProfile.setMobilePhoneDialingCode(jsonObject.getString("mblDc"));
                    loggedUserProfile.setCountry(jsonObject.getString("cnty"));
                    loggedUserProfile.setEmail(jsonObject.getString("el"));
                    loggedUserProfile.setFirstName(jsonObject.getString("fn"));
                    loggedUserProfile.setLastName(jsonObject.getString("ln"));
                    loggedUserProfile.setGender(jsonObject.getString("gr"));
                    loggedUserProfile.setMobilePhone(jsonObject.getString("mbl"));
                    loggedUserProfile.setPresence(jsonObject.getInt("psnc"));
                    loggedUserProfile.setProfileImage(jsonObject.getString("prIm"));
                    loggedUserProfile.setSessionId(jsonObject.getString("sId"));
                    System.out.println(jsonObject.getString("sId"));
                    loggedUserProfile.setWhatisInYourMind(jsonObject.getString("wim"));
                    loggedUserProfile.setMailServerHost(jsonObject.getString("ms"));
                    loggedUserProfile.setMailServerPort(jsonObject.getInt("mp"));
                    loggedUserProfile.setMailServerUserId(jsonObject.getString("re"));
                    loggedUserProfile.setCoverImage(jsonObject.getString("cIm"));
                    if (jsonObject.has("pvc")) {
                        JSONArray pvc = jsonObject.getJSONArray("pvc");

                        loggedUserProfile.setEmailPrivacy(pvc.getInt(0));
                        loggedUserProfile.setMobilePhonePrivacy(pvc.getInt(1));
                        loggedUserProfile.setProfileImagePrivacy(pvc.getInt(2));
                        loggedUserProfile.setBirthDayPrivacy(pvc.getInt(3));
                        loggedUserProfile.setCoverImagePrivacy(pvc.getInt(4));

                    }
                    loggedUserProfile.setUserIdentity(uId);
                    loggedUserProfile.setPassword(usrPw);
                    loggedUserProfile.setFriendlist(null);
                    loggedUserProfile.setGroupList(null);
                    mainBean.getCommunicationProvider().setSessionId(loggedUserProfile.getSessionId());

//                        if (jsonObject.has("smIp")) {
//                            ServerConstants.SMS_SERVER = jsonObject.getString("smIp");
//                            ServerConstants.SMS_SERVER_PORT = jsonObject.getInt("smP");
//                        }
                    if (jsonObject.has("bDay")) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                        String dateInString = jsonObject.getString("bDay");
                        try {
                            Date date = formatter.parse(dateInString);
                            loggedUserProfile.setBirthday(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

//                        if (jsonObject.has("chIp")) {
//                            MessengerConstants.CHAT_SERVER_IP = jsonObject.getString("chIp");
//                            MessengerConstants.CHAT_REGISTRATION_PORT = jsonObject.getInt("chRp");
//                            MessengerConstants.CHAT_CONFIRMATION_PORT = jsonObject.getInt("chCp");
//                            MessengerConstants.CHAT_HISTORY_PORT = jsonObject.getInt("chHp");
//                            MessengerConstants.FRIEND_CHAT_PORT = jsonObject.getInt("fChp");
//                            MessengerConstants.GROUP_CHAT_PORT = jsonObject.getInt("gChp");
//                        }
                    keepAliveThread.setSessionId(loggedUserProfile.getSessionId());

                    if (!keepAliveThread.isRunning()) {
                        keepAliveThread.setCurrentLoginTime(System.currentTimeMillis());
                        keepAliveThread.setRunning(true);
                        keepAliveThread.start();
                    }

                    isComplete = true;
                } else {
                    errorMessage = "Please Try Again!";
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    }
                }
            } else if (actn == ServerConstants.TYPE_PASSWORD_RECOVERY) {
                if (jsonObject.getBoolean("sucs")) {
                    errorMessage = "We have sent a mail to your el address. Please check your el. and input your secret code.";
                    isComplete = true;
                } else {
                    errorMessage = "Please Try Again!";
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    }
                }
            } else if (actn == ServerConstants.TYPE_PASSWORD_RESET) {
                if (jsonObject.getBoolean("sucs")) {
                    errorMessage = "Your usrPw has changed successfully.";
                    isComplete = true;
                } else {
                    errorMessage = "Please Try Again!";
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    }
                }
                isComplete = true;
            } else if (actn == ServerConstants.TYPE_CONTACT_LIST
                   || actn == ServerConstants.TYPE_FRIENDS_PRESENCE_LIST
                    || actn == ServerConstants.TYPE_USER_PROFILE
                    || actn == ServerConstants.TYPE_CHANGE_PASSWORD
                    || actn == ServerConstants.TYPE_WHAT_IS_IN_UR_MIND
                    || actn == ServerConstants.TYPE_REMOVE_PROFILE_IMAGE
                    || actn == ServerConstants.TYPE_PRIVACY_SETTINGS) {
                userHomeAction.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_CONTACT_SEARCH
                    || actn == ServerConstants.TYPE_ADD_FRIEND
                    || actn == ServerConstants.TYPE_ADD_MULTIPLE_FRIEND
                    || actn == ServerConstants.TYPE_ADDRESS_BOOK
                    || actn == ServerConstants.TYPE_ADD_ADDRESS_BOOK_FRIEND
                    || actn == ServerConstants.TYPE_ACCEPT_FRIEND
                    || actn == ServerConstants.TYPE_DELETE_FRIEND) {
                friendsBean.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_GROUP_LIST
                    || actn == ServerConstants.TYPE_GROUP_MEMBERS_LIST
                    || actn == ServerConstants.TYPE_CREATE_GROUP
                    || actn == ServerConstants.TYPE_REMOVE_GROUP_MEMBER
                    || actn == ServerConstants.TYPE_LEAVE_GROUP
                    || actn == ServerConstants.TYPE_DELETE_GROUP
                    || actn == ServerConstants.TYPE_ADD_GROUP_MEMBER
                    || actn == ServerConstants.TYPE_EDIT_GROUP_MEMBER
                    || actn == ServerConstants.TYPE_NEW_GROUP
                    || actn == ServerConstants.TYPE_UPDATE_REMOVE_GROUP_MEMBER
                    || actn == ServerConstants.TYPE_UPDATE_DELETE_GROUP
                    || actn == ServerConstants.TYPE_UPDATE_ADD_GROUP_MEMBER
                    || actn == ServerConstants.TYPE_UPDATE_EDIT_GROUP_MEMBER) {
                groupAction.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_START_FRIEND_CHAT) {
                if (jsonObject.getBoolean("sucs")) {
                    UserProfile userProfile = loggedUserProfile.getFriendlist().get(jsonObject.getString("fndId"));
                    userProfile.setCHAT_CONFIRMATION_PORT(jsonObject.getInt("chCp"));//
                    userProfile.setCHAT_REGISTRATION_PORT(jsonObject.getInt("chRp"));
                    userProfile.setCHAT_HISTORY_PORT(jsonObject.getInt("chRp"));
                    userProfile.setGROUP_CHAT_PORT(jsonObject.getInt("gChp"));
                    userProfile.setFRIEND_CHAT_PORT(jsonObject.getInt("fChp"));
                    InetAddress serverIP = InetAddress.getByName(jsonObject.getString("chIp"));
                    userProfile.setChatIp(serverIP);
                    userProfile.setChatPortUpdateTime(System.currentTimeMillis());

                }

            } else if (actn == ServerConstants.TYPE_START_GROUP_CHAT) {
                if (jsonObject.getBoolean("sucs")) {
                    GroupDTO groupDTO = loggedUserProfile.getGroupList().get(jsonObject.getLong("grpId"));
                    groupDTO.setCHAT_CONFIRMATION_PORT(jsonObject.getInt("chCp"));//
                    groupDTO.setCHAT_REGISTRATION_PORT(jsonObject.getInt("chRp"));
                    groupDTO.setCHAT_HISTORY_PORT(jsonObject.getInt("chRp"));
                    groupDTO.setGROUP_CHAT_PORT(jsonObject.getInt("gChp"));
                    groupDTO.setFRIEND_CHAT_PORT(jsonObject.getInt("fChp"));
                    InetAddress serverIP = InetAddress.getByName(jsonObject.getString("chIp"));
                    groupDTO.setChatIp(serverIP);
                    groupDTO.setChatPortUpdateTime(System.currentTimeMillis());

                }

            } else if (actn == ServerConstants.TYPE_FRIEND_SMS
                    || actn == ServerConstants.TYPE_GROUP_SMS
                    || actn == ServerConstants.TYPE_KNOW_SMS_STATUS) {
                smsAction.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_UPDATE_ADD_FRIEND
                    || actn == ServerConstants.TYPE_UPDATE_DELETE_FRIEND
                    || actn == ServerConstants.TYPE_UPDATE_ACCEPT_FRIEND) {
                friendsBean.onReceivedMessage(msg);
            } else if (actn == ServerConstants.TYPE_NEWS_FEED
                    || actn == ServerConstants.TYPE_ADD_STATUS
                    || actn == ServerConstants.TYPE_UPDATE_ADD_STATUS
                    || actn == ServerConstants.TYPE_DELETE_STATUS
                    || actn == ServerConstants.TYPE_EDIT_STATUS
                    || actn == ServerConstants.TYPE_ADD_STATUS_COMMENT
                    || actn == ServerConstants.TYPE_DELETE_STATUS_COMMENT
                    || actn == ServerConstants.TYPE_LIKE_STATUS
                    || actn == ServerConstants.TYPE_UPDATE_LIKE_STATUS
                    || actn == ServerConstants.TYPE_UNLIKE_STATUS
                    || actn == ServerConstants.TYPE_UPDATE_UNLIKE_STATUS
                    || actn == ServerConstants.TYPE_COMMENTS_FOR_STATUS
                    || actn == ServerConstants.TYPE_LIKES_FOR_STATUS
                    || actn == ServerConstants.TYPE_MY_BOOK
                    || actn == ServerConstants.TYPE_UPDATE_ADD_STATUS_COMMENT
                    || actn == ServerConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT
                    || actn == ServerConstants.TYPE_UPDATE_DELETE_STATUS) {
                bookAction.onReceivedMessage(msg);

            } else {
                if (jsonObject.has("mg")) {
                    errorMessage = jsonObject.getString("mg");
                } else {
                    isComplete = true;
                    errorMessage = "Invalid Session";
                }
            }
        } catch (Exception e) {
            logger.info("LoginAction==>" + e.toString());
            System.out.println("LoginAction==>" + e.toString());
        }
    }

    private void getEmoticonRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_EMOTICONS);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("emtnV", 0);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    private void getHeadersRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_FNF_HEADERS);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    private void getContactListRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_CONTACT_LIST);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("ut", -1);
            jsonObject.put("cut", -1);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    private void getGroupListRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_GROUP_LIST);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("ut", -1);
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    private void getMyBookRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_MY_BOOK);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    private void getMyAlbumRequest() {
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("actn", ServerConstants.TYPE_MY_ALBUMS);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendRequestPacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }
    }

    public void getCountryCode(String countryName) {
        String ccode = countrybean.getCountryInMap().get(countryName);
        setCountry_code(ccode);
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getPassword() {
        return usrPw;
    }

    public void setPassword(String usrPw) {
        this.usrPw = usrPw;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public UserHomeAction getUserHomeAction() {
        return userHomeAction;
    }

    public void setUserHomeAction(UserHomeAction userHomeAction) {
        this.userHomeAction = userHomeAction;
    }

    public FriendsBean getFriendsBean() {
        return friendsBean;
    }

    public void setFriendsBean(FriendsBean friendsBean) {
        this.friendsBean = friendsBean;
    }

    public GroupAction getGroupAction() {
        return groupAction;
    }

    public void setGroupAction(GroupAction groupAction) {
        this.groupAction = groupAction;
    }

    public KeepAliveThread getKeepAliveThread() {
        return keepAliveThread;
    }

    public void setKeepAliveThread(KeepAliveThread keepAliveThread) {
        this.keepAliveThread = keepAliveThread;
    }

    public SMSAction getSmsAction() {
        return smsAction;
    }

    public void setSmsAction(SMSAction smsAction) {
        this.smsAction = smsAction;
    }

    public ChatMainBean getChatMainBean() {
        return chatMainBean;
    }

    public void setChatMainBean(ChatMainBean chatMainBean) {
        this.chatMainBean = chatMainBean;
    }

    public String getSignOutMessage() {
        return signOutMessage;
    }

    public void setSignOutMessage(String signOutMessage) {
        this.signOutMessage = signOutMessage;
    }

    public String getSecretCode() {
        return secCo;
    }

    public void setSecretCode(String secCo) {
        this.secCo = secCo;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public BookAction getBookAction() {
        return bookAction;
    }

    public void setBookAction(BookAction bookAction) {
        this.bookAction = bookAction;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhoneNo() {
        return pnN;
    }

    public void setPhoneNo(String pnN) {
        this.pnN = pnN;
    }

    public CountryBean getCountrybean() {
        return countrybean;
    }

    public void setCountrybean(CountryBean countrybean) {
        this.countrybean = countrybean;
    }

}
