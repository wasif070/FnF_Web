/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.groups;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ipvision.utils.ServletResponseDTO;

/**
 *
 * @author user
 */
@WebServlet(name = "GetMyGroups", urlPatterns = {"/GetMyGroups"})
public class GetMyGroups extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            GroupAction groupAction = (GroupAction) request.getSession().getAttribute("groupAction");
            ServletResponseDTO servletResponseDTO = new ServletResponseDTO();
            if (groupAction != null) {
                String groupSearchValue = request.getParameter("groupSearchValue");
                groupAction.setGroupSearchValue(groupSearchValue);

                ArrayList<GroupDTO> friendList = groupAction.getGroupList();

                if (friendList == null || friendList.size() < 1) {
                    servletResponseDTO.setMessage(null);
                    servletResponseDTO.setSuccess(false);
                } else {
                    Gson gson = new Gson();
                    String mg = gson.toJson(friendList);
                    servletResponseDTO.setMessage(mg);
                    servletResponseDTO.setSuccess(true);
                }
            } else {
                servletResponseDTO.setMessage(null);
                servletResponseDTO.setSuccess(false);
            }

            Gson gson = new Gson();
            out.printf(gson.toJson(servletResponseDTO).toString());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
