/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.groups;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.ipvision.communication.MainBean;
import org.ipvision.messenger.GroupChatWindow;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.users.LoggedUserProfile;
import org.ipvision.users.UserProfile;
import org.ipvision.utils.AppConstants;
import org.ipvision.utils.ServerConstants;
import org.ipvision.utils.Sorter;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "groupAction")
@SessionScoped
public class GroupAction implements OnReceiveListner, Serializable {
// AullAuthontication System

    static Logger logger = Logger.getLogger(GroupAction.class.getName());
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
    //End
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    @ManagedProperty(value = "#{groupChatWindow}")
    private GroupChatWindow groupChatWindow;
    private ArrayList<GroupDTO> groupList;
    private ArrayList<GroupMember> memberList;
    private boolean isRefreshDiv = false;
    boolean isComplete = false;
    String errorMessage = "";
    GroupDTO groupDetails;
    String gNm;
    String currentMemberid = "";
    String newGroupId = "";
    long grpId;
    HashMap<String, ArrayList<GroupMember>> selectedList = new HashMap<String, ArrayList<GroupMember>>();
    private String groupSearchValue;
    private ArrayList<GroupDTO> newGroupList;

    public GroupAction() {
    }

    public ArrayList<GroupDTO> getGroupList() {
//        System.out.println("GroupList");
        HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();
        groupList = new ArrayList<GroupDTO>();
        if (groupHashMap != null && groupHashMap.size() > 0) {
            Set set = groupHashMap.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                GroupDTO dto = (GroupDTO) me.getValue();
                if (groupSearchValue != null && groupSearchValue.length() > 0) {
                    if (dto.getGroupName().startsWith(groupSearchValue)) {
                        groupList.add(dto);
                    }
                } else {
                    groupList.add(dto);
                }
            }
        }
        return Sorter.sortStrArrayListASC(groupList, "getGroupName");
    }

    public ArrayList<GroupDTO> getSearchGroupList() {
        HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();
        groupList = new ArrayList<GroupDTO>();
        if (groupHashMap != null && groupHashMap.size() > 0) {
            Set set = groupHashMap.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                GroupDTO dto = (GroupDTO) me.getValue();
                groupList.add(dto);
            }
        }
        return Sorter.sortStrArrayListASC(groupList, "getGroupName");
    }

    public String createGroup() {
        isComplete = false;
        errorMessage = "";
        try {
            ArrayList<GroupMember> mList = selectedList.get(newGroupId);
            if (mList != null && mList.size() > 0) {
                String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("actn", ServerConstants.TYPE_CREATE_GROUP);
                jsonObject.put("pckId", key);
                jsonObject.put("sId", loggedUserProfile.getSessionId());
                jsonObject.put("gNm", gNm);

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < mList.size(); i++) {
                    JSONObject jsoninnerObject1 = new JSONObject();
                    GroupMember gm = mList.get(i);
                    jsoninnerObject1.accumulate("uId", gm.getUserIdentity());
                    jsoninnerObject1.accumulate("admin", gm.isAdmin());
                    jsonArray.put(jsoninnerObject1);
                }
                jsonObject.put("groupMembers", jsonArray);

                mainBean.getNotifyThread().notify_or_start_thread();
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
            } else {
                errorMessage = "Please select Group members";
                return "";
            }
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        return "";
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
        return "/secure/group/details.xhtml" + AppConstants.REDIRECT + "&grpId=" + grpId;
    }

    public void clearNewGroupMembers() {
        if (selectedList.containsKey(newGroupId)) {
            selectedList.get(newGroupId).clear();
        }
    }

    public boolean isTempMember(String uIdentity) {
        if (selectedList.containsKey(newGroupId)) {
            ArrayList<GroupMember> mList = selectedList.get(newGroupId);
            if (mList != null && mList.size() > 0) {
                for (int i = 0; i < mList.size(); i++) {
                    GroupMember gm = mList.get(i);
                    if (gm.getUserIdentity().equalsIgnoreCase(uIdentity)) {
                        return true;
                    }
                }
            }
        } else {
            return false;
        }
        return false;
    }

    public void addTempMember(String uIdentity, boolean isAdmin) {
        ArrayList<GroupMember> mList = selectedList.get(newGroupId);
        if (mList == null || mList.size() < 1) {
            mList = new ArrayList<GroupMember>();
        }
        GroupMember groupMember = new GroupMember();
        UserProfile up = loggedUserProfile.getFriendlist().get(uIdentity);
        groupMember.setAdmin(isAdmin);
        groupMember.setFirstName(up.getFirstName());
        groupMember.setLastName(up.getLastName());
        groupMember.setMobilePhone(up.getMobilePhone());
        groupMember.setPresence(up.getPresence());
        groupMember.setUserIdentity(up.getUserIdentity());
        groupMember.setGender(up.getGender());
        groupMember.setProfileImage(up.getProfileImage());
        mList.add(groupMember);
        selectedList.put(newGroupId, mList);
    }

    public void setSelectedMembers() {
        selectedList.put(newGroupId, groupDetails.getMemberList());
    }

    public void removeTempMember(String uIdentity) {
        ArrayList<GroupMember> mList = selectedList.get(newGroupId);
        if (mList != null && mList.size() > 0) {
            for (int i = 0; i < mList.size(); i++) {
                GroupMember gm = mList.get(i);
                if (gm.getUserIdentity().equalsIgnoreCase(uIdentity)) {
                    mList.remove(i);
                    break;
                }
            }
        }
        selectedList.put(newGroupId, mList);
    }

    public boolean isGroupAdmin(long gId) {
        if (loggedUserProfile.getGroupList().containsKey(gId)) {
            GroupDTO groupDto = loggedUserProfile.getGroupList().get(gId);
            HashMap<String, GroupMember> grmember = groupDto.getMembers();
            if (grmember.containsKey(loggedUserProfile.getUserIdentity())) {
                GroupMember gm = grmember.get(loggedUserProfile.getUserIdentity());
                if (gm.isAdmin()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public String addGroupMembers() {
        try {
            String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("actn", ServerConstants.TYPE_ADD_GROUP_MEMBER);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("grpId", groupDetails.getGroupId());

            JSONArray jsonArray = new JSONArray();
            ArrayList<GroupMember> mList = selectedList.get(newGroupId);
            for (int i = 0; i < mList.size(); i++) {
                JSONObject jsoninnerObject1 = new JSONObject();
                GroupMember gm = mList.get(i);
                if (!groupDetails.getMembers().containsKey(gm.getUserIdentity())) {
                    jsoninnerObject1.accumulate("uId", gm.getUserIdentity());
                    jsoninnerObject1.accumulate("admin", gm.isAdmin());
                    jsonArray.put(jsoninnerObject1);
                }
            }
            if (jsonArray != null && jsonArray.length() > 0) {
                jsonObject.put("groupMembers", jsonArray);
                mainBean.getNotifyThread().notify_or_start_thread();
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
            } else {
                isComplete = true;
            }
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        return "";
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
        return "/secure/group/details.xhtml" + AppConstants.REDIRECT + "&grpId=" + groupDetails.getGroupId();
    }

    public void removeTempServerMember(String uIdentity) {
        ArrayList<GroupMember> mList = selectedList.get(newGroupId);
        if (mList != null && mList.size() > 0) {
            for (int i = 0; i < mList.size(); i++) {
                GroupMember gm = mList.get(i);
                if (gm.getUserIdentity().equalsIgnoreCase(uIdentity)) {
                    mList.remove(i);
                    break;
                }
            }
        }
        selectedList.put(newGroupId, mList);
        try {
            String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_REMOVE_GROUP_MEMBER);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("grpId", groupDetails.getGroupId());
            JSONArray jsonArray = new JSONArray();
            JSONObject inerobj = new JSONObject();
            inerobj.put("uId", uIdentity);
            jsonArray.put(inerobj);
            jsonObject.put("groupMembers", jsonArray);
            currentMemberid = uIdentity;
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);
        } catch (Exception e) {
        }
    }

    public void removeMembers(String uId) {
        isComplete = false;
        errorMessage = null;
        try {
            String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_REMOVE_GROUP_MEMBER);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("grpId", groupDetails.getGroupId());
            JSONArray jsonArray = new JSONArray();
            JSONObject inerobj = new JSONObject();
            inerobj.put("uId", uId);
            jsonArray.put(inerobj);
            jsonObject.put("groupMembers", jsonArray);
            currentMemberid = uId;
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);

            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        } catch (Exception e) {
        }
    }

    public String leaveFromGroup(long grpId) {
        try {
            String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_LEAVE_GROUP);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("grpId", grpId);

            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);

            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        } catch (Exception e) {
        }
        return "/secure/user/welcome.xhtml" + AppConstants.REDIRECT;
    }

    public ArrayList<GroupMember> getMemberList() {
        HashMap<String, UserProfile> friendHashMap = loggedUserProfile.getFriendlist();
        memberList = new ArrayList<GroupMember>();
        if (friendHashMap != null && friendHashMap.size() > 0) {
            Set set = friendHashMap.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                UserProfile dto = (UserProfile) me.getValue();
                if (dto.getFriendShipStatus() == 1) {
                    GroupMember gm = new GroupMember();
                    gm.setAdmin(false);
                    gm.setFirstName(dto.getFirstName());
                    gm.setGender(dto.getGender());
                    gm.setLastName(dto.getLastName());
                    gm.setMobilePhone(dto.getMobilePhone());
                    gm.setPresence(dto.getPresence());
                    gm.setProfileImage(dto.getProfileImage());
                    gm.setUserIdentity(dto.getUserIdentity());
                    memberList.add(gm);
                }
            }
        }
        return memberList;
    }

    public String deleteGroup() {
        isComplete = false;
        try {
            String key = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("actn", ServerConstants.TYPE_DELETE_GROUP);
            jsonObject.put("pckId", key);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("grpId", groupDetails.getGroupId());
            mainBean.getNotifyThread().notify_or_start_thread();
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), key);

            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        } catch (Exception e) {
        }

        return "/secure/user/welcome.xhtml" + AppConstants.REDIRECT;
    }

    public String goToAddMember(long gId) {
        if (isGroupAdmin(gId)) {
            return "/secure/user/group/add_member.xhtml" + AppConstants.REDIRECT + "&grpId=" + gId;
        } else {
            return "/secure/user/group/details.xhtml" + AppConstants.REDIRECT + "&grpId=" + gId;
        }
    }

    public String goToGroupDetails(long gId) {
        return "/secure/user/group/details.xhtml" + AppConstants.REDIRECT + "&grpId=" + gId;
    }

    public String setGroupDetails(String grpId) {
        if (loggedUserProfile.getGroupList().containsKey(Long.parseLong(grpId))) {
            groupDetails = loggedUserProfile.getGroupList().get(Long.parseLong(grpId));
            ArrayList<GroupMember> mList = new ArrayList<GroupMember>();
            HashMap<String, UserProfile> myFriends = loggedUserProfile.getFriendlist();
            if (groupDetails.members != null && groupDetails.members.size() > 0) {
                Set set = groupDetails.members.entrySet();
                Iterator i = set.iterator();
                while (i.hasNext()) {
                    Map.Entry me = (Map.Entry) i.next();
                    GroupMember dto = (GroupMember) me.getValue();
                    if (myFriends.containsKey(dto.getUserIdentity())) {
                        UserProfile fProfile = myFriends.get(dto.getUserIdentity());
                        dto.setPresence(fProfile.getPresence());
                        dto.setProfileImage(fProfile.getProfileImage());
                    } else if (dto.getUserIdentity().equals(loggedUserProfile.getUserIdentity())) {
                        dto.setProfileImage(loggedUserProfile.getProfileImage());
                    }
                    mList.add(dto);
                }
            }
            groupDetails.setMemberList(mList);
            return "";
        }
        return "Sorry! The group is not found!";
    }

    public void synchronizeMapAndList(GroupDTO groupDetails) {
        Map<String, GroupMember> map = groupDetails.getMembers();
        List<GroupMember> list = groupDetails.getMemberList();

        for (Iterator<GroupMember> it = list.iterator(); it.hasNext();) {
            GroupMember m = it.next();
            if (!map.containsKey(m.getUserIdentity())) {
                list.remove(m);
            }

        }

    }

    public GroupMember getMemberProfile(String friendIdentiy) {
        HashMap<String, GroupMember> myFriends = groupDetails.getMembers();
        if (myFriends.containsKey(friendIdentiy)) {
            return myFriends.get(friendIdentiy);
        }
        return null;
    }

    public GroupDTO getGroupDetails() {
        return groupDetails;
    }

    public boolean checkGroup() {
        if (isRefreshDiv) {
            isRefreshDiv = false;
            return true;
        } else if (groupChatWindow.checkChatNotification()) {
            return true;
        } else {
            return false;
        }
    }

    public String getGroupUpdate() {
        String groupListJsonStr = "false";
        Gson gson = new Gson();
        if (isRefreshDiv) {
            isRefreshDiv = false;
            if (newGroupList != null && newGroupList.size() > 0) {
                groupListJsonStr = gson.toJson(newGroupList).toString();
                newGroupList = new ArrayList<GroupDTO>();
                return groupListJsonStr;
            }
            return groupListJsonStr;
        } else if (groupChatWindow.checkChatNotification()) {
            groupListJsonStr = groupChatWindow.getChatUpdate();
            return groupListJsonStr;
        } else {
            return groupListJsonStr;
        }
    }

    @Override
    public void onReceivedMessage(String msg) {

        try {
//            logger.info("GroupAction===>" + msg.trim());
            JSONObject jsonObject = new JSONObject(msg.trim());
            int actn = jsonObject.getInt("actn");

            System.out.println("action>" + actn);
//            if (jsonObject.getBoolean("sucs")) {
            if (actn == ServerConstants.TYPE_GROUP_LIST) {
                if (jsonObject.has("groupList")) {
                    JSONArray groupJsonArray = jsonObject.getJSONArray("groupList");
                    HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();
                    for (int j = 0; j < groupJsonArray.length(); j++) {
                        JSONObject jGroup = groupJsonArray.getJSONObject(j);
                        GroupDTO groupDTO = new GroupDTO();
                        groupDTO.setGroupId(jGroup.getLong("grpId"));
                        groupDTO.setGroupName(jGroup.getString("gNm"));
                        groupDTO.setSuperAdmin(jGroup.getString("sAd"));
                          //  JSONArray memberJsonArray = groupJsonArray.getJSONObject(j).getJSONArray("groupMembers");

                        //   HashMap<String, GroupMember> members = groupDTO.getMembers();
                        //  String gmembers = "";
//                            for (int i = 0; i < memberJsonArray.length(); i++) {
//                                JSONObject jgroupMember = memberJsonArray.getJSONObject(i);
//                                GroupMember groupMember = new GroupMember();
//                                groupMember.setAdmin(jgroupMember.getBoolean("admin"));
//                                groupMember.setFirstName(jgroupMember.getString("fn"));
//                                groupMember.setLastName(jgroupMember.getString("ln"));
//                                groupMember.setMobilePhone(jgroupMember.getString("mbl"));
//                                groupMember.setPresence(jgroupMember.getInt("psnc"));
//                                groupMember.setUserIdentity(jgroupMember.getString("uId"));
//                                groupMember.setGender(jgroupMember.getString("gr"));
//                                groupMember.setProfileImage("");
//                                if (members.containsKey(groupMember.getUserIdentity())) {
//                                    members.remove(groupMember.getUserIdentity());
//                                }
//                                members.put(groupMember.getUserIdentity(), groupMember);
//                                gmembers += groupMember.getUserIdentity() + ",";
//                            }
                        // groupDTO.setGroupMembers(gmembers);
                        if (groupHashMap.containsKey(groupDTO.getGroupId())) {

                            groupHashMap.get(groupDTO.getGroupId()).setGroupName(groupDTO.getGroupName());
                            groupHashMap.get(groupDTO.getGroupId()).setGroupId(groupDTO.getGroupId());
                            groupHashMap.get(groupDTO.getGroupId()).setSuperAdmin(groupDTO.getSuperAdmin());

                        } else {
                            groupHashMap.put(groupDTO.getGroupId(), groupDTO);
                        }
                        if (newGroupList == null) {
                            newGroupList = new ArrayList<GroupDTO>();
                        }
                        newGroupList.add(groupDTO);
                    }
                    //loggedUserProfile.setGroupList(groupHashMap);
                    isRefreshDiv = true;
                    // isComplete = true;
                }

                //  received>>>>>>>>>>>>>>>{"actn":99,"seq":"6/57","pckFs":2484,"groupMembers":[{"uId":"wasir","admin":true,"psnc":2,"fn":"A  Mr.","ln":"wasir","gr":"Male","mbl":"1736861305","grpId":636},{"uId":"anwar","admin":true,"psnc":1,"fn":"Anwar","ln":"IPVision","gr":"Male","mbl":"1670854678","grpId":636}],"tr":113}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            } else if (actn == ServerConstants.TYPE_GROUP_MEMBERS_LIST) {
                if (jsonObject.has("groupMembers")) {
                    JSONArray memberJsonArray = jsonObject.getJSONArray("groupMembers");

                    for (int j = 0; j < memberJsonArray.length(); j++) {
                        
                     //   
                               
                                
                        JSONObject member = memberJsonArray.getJSONObject(j);
                        
                         if(member.getInt("ists")==1){
                         continue;
                         }
                        GroupDTO groupDTO = new GroupDTO();
                        groupDTO.setGroupId(member.getLong("grpId"));
                        groupDTO.setGroupName("");
                        groupDTO.setSuperAdmin("");

                        GroupMember groupMember = new GroupMember();
                        groupMember.setAdmin(member.getBoolean("admin"));
                        groupMember.setFirstName(member.getString("fn"));
                        groupMember.setLastName(member.getString("ln"));
                        groupMember.setMobilePhone(member.getString("mbl"));
                        groupMember.setPresence(member.getInt("psnc"));
                        groupMember.setUserIdentity(member.getString("uId"));
                        groupMember.setGender(member.getString("gr"));
                        groupMember.setProfileImage("");
//                                if (members.containsKey(groupMember.getUserIdentity())) {
//                                    members.remove(groupMember.getUserIdentity());
//                                }
//                                members.put(groupMember.getUserIdentity(), groupMember);
//                                gmembers += groupMember.getUserIdentity() + ",";
//                            }

                        HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();

                        if (groupHashMap.containsKey(groupDTO.getGroupId())) {
                            GroupDTO groupDTO_old = groupHashMap.get(groupDTO.getGroupId());
                            String gmembers = groupDTO_old.getGroupMembers();
                            gmembers += groupMember.getUserIdentity() + ",";
                            groupDTO_old.setGroupMembers(gmembers);
                            groupDTO_old.getMembers().put(groupMember.getUserIdentity(), groupMember);
                        } else {
                            String gmembers = "";
                            gmembers += groupMember.getUserIdentity() + ",";
                            groupDTO.setGroupMembers(gmembers);
                            groupDTO.getMembers().put(groupMember.getUserIdentity(), groupMember);

                            groupHashMap.put(groupDTO.getGroupId(), groupDTO);
                        }

                        //  if (newGroupList == null) {
                        //      newGroupList = new ArrayList<GroupDTO>();
                        //  }
                        //    newGroupList.add(groupDTO);
                    }

                    //  isRefreshDiv = true;
                    // isComplete = true;
                }

            } else if (actn == ServerConstants.TYPE_REMOVE_GROUP_MEMBER) {
                //   {"sucs":true,"actn":154,"uId":"nazmul","pckFs":2624,"grpId":829} 
                // {"sucs":true,"actn":154,"pckId":"NgQz9C6anazmul12"} 
                try {
                    if (jsonObject.has("pckId")) {
                        if (jsonObject.getBoolean("sucs")) {

                            groupDetails.getMembers().remove(currentMemberid);
                            errorMessage = "Member " + currentMemberid + " is removed from this group.";
                            setGroupDetails("" + groupDetails.getGroupId());
                            isComplete = true;
                        } else if (jsonObject.getString("msg").contains("is  not a member of this group")) {

                            groupDetails.getMembers().remove(currentMemberid);
                            errorMessage = "Member " + currentMemberid + " is removed from this group.";
                            setGroupDetails("" + groupDetails.getGroupId());
                            isComplete = true;
                        } else {

                            errorMessage = jsonObject.getString("msg");
                        }
                    } else {
                        GroupDTO groupDTO = null;
                        if (loggedUserProfile.getGroupList().containsKey(jsonObject.getLong("grpId"))) {
                            groupDTO = loggedUserProfile.getGroupList().get(jsonObject.getLong("grpId"));
                            groupDTO.getMembers().remove(jsonObject.getString("uId"));
                            synchronizeMapAndList(groupDTO);
                        }

                    }
                    //  isRefreshDiv = true;
                } catch (Exception e) {
                }
            } else if (actn == ServerConstants.TYPE_UPDATE_REMOVE_GROUP_MEMBER) {
                //   {"sucs":true,"actn":154,"uId":"nazmul","pckFs":2624,"grpId":829}  
                try {

                    GroupDTO groupDTO = loggedUserProfile.getGroupList().get(jsonObject.getLong("grpId"));

                    groupDTO.getMembers().remove(jsonObject.getString("uId"));

                    ArrayList<GroupMember> memberList = groupDTO.getMemberList();

                    for (GroupMember m : memberList) {

                        if (m.getUserIdentity().equals(jsonObject.getString("uId"))) {

                            memberList.remove(m);

                        }

                    }

                } catch (Exception e) {
                }
            } else if (actn == ServerConstants.TYPE_DELETE_GROUP) {
                //   {"sucs":true,"actn":154,"uId":"nazmul","pckFs":2624,"grpId":829}  
                try {
                    if (jsonObject.getBoolean("sucs")) {
                        loggedUserProfile.getGroupList().remove(groupDetails.getGroupId());
                        GroupDTO groupDTO = new GroupDTO();
                        groupDTO.setGroupId(groupDetails.getGroupId());
                        groupDTO.setDeleted(true);
                        if (newGroupList == null) {
                            newGroupList = new ArrayList<GroupDTO>();
                        }
                        newGroupList.add(groupDTO);
                        isComplete = true;
                        isRefreshDiv = true;
                    } else {
                        errorMessage = jsonObject.getString("msg");
                    }
                    isRefreshDiv = true;
                } catch (Exception e) {
                }
            } else if (actn == ServerConstants.TYPE_UPDATE_DELETE_GROUP) {
                //   {"sucs":true,"actn":154,"uId":"nazmul","pckFs":2624,"grpId":829}  
                try {
                    loggedUserProfile.getGroupList().remove(jsonObject.getLong("grpId"));

                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.setGroupId(groupDetails.getGroupId());
                    groupDTO.setDeleted(true);
                    if (newGroupList == null) {
                        newGroupList = new ArrayList<GroupDTO>();
                    }
                    newGroupList.add(groupDTO);

                    isRefreshDiv = true;
                } catch (Exception e) {
                }
            } else if (actn == ServerConstants.TYPE_LEAVE_GROUP) {
                if (jsonObject.getBoolean("sucs")) {
                    loggedUserProfile.getGroupList().remove(groupDetails.getGroupId());
                    groupDetails.getMembers().remove(currentMemberid);
                } else {
                    errorMessage = jsonObject.getString("msg");
                }

                isComplete = true;
            } else if (actn == ServerConstants.TYPE_CREATE_GROUP) {
                if (jsonObject.getBoolean("sucs")) {
                    HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();
                    if (jsonObject.has("grpId")) {

                        if (groupHashMap.containsKey(jsonObject.getLong("grpId"))) {
                            return;
                        }

                        ArrayList<GroupMember> mList = selectedList.get(newGroupId);
                        if (mList == null || mList.size() < 1) {
                            mList = new ArrayList<GroupMember>();
                        }

                        GroupMember owner = new GroupMember();
                        owner.setUserIdentity(loggedUserProfile.getUserIdentity());
                        owner.setFirstName(loggedUserProfile.getFirstName());
                        owner.setLastName(loggedUserProfile.getLastName());
                        owner.setAdmin(true);
                        owner.setGender(loggedUserProfile.getGender());
                        owner.setPresence(loggedUserProfile.getPresence());
                        owner.setMobilePhone(loggedUserProfile.getMobilePhoneDialingCode() + loggedUserProfile.getMobilePhone());
                        owner.setProfileImage(loggedUserProfile.getProfileImage());
                        mList.add(owner);
                        selectedList.put(newGroupId, mList);

                        GroupDTO gDTO = new GroupDTO();
                        gDTO.setGroupId(jsonObject.getLong("grpId"));
                        gDTO.setGroupName(gNm);
                        gDTO.setSuperAdmin(jsonObject.getString("sAd"));
                        gDTO.setMemberList(selectedList.get(newGroupId));
                        HashMap<String, GroupMember> members = new HashMap<String, GroupMember>();

                        mList = gDTO.getMemberList();
                        if (mList != null && mList.size() > 0) {
                            for (int k = 0; k < mList.size(); k++) {
                                GroupMember gm = mList.get(k);
                                members.put(gm.getUserIdentity(), gm);
                            }
                            gDTO.setMembers(members);
                        }
                        groupHashMap.put(gDTO.getGroupId(), gDTO);
                        grpId = gDTO.getGroupId();
                        setGroupDetails("" + grpId);
                        selectedList.remove(newGroupId);
                        gNm = "";

                        if (newGroupList == null) {
                            newGroupList = new ArrayList<GroupDTO>();
                        }
                        newGroupList.add(gDTO);
                    }
                    loggedUserProfile.setGroupList(groupHashMap);
                    errorMessage = "Group is added successfully.";
                    isRefreshDiv = true;
                } else {
                    errorMessage = jsonObject.getString("msg");
                }
                isComplete = true;
            } else if (actn == ServerConstants.TYPE_NEW_GROUP) {
                //{"sucs":true,
//                "actn":51,
//                "pckFs":2814,
//                "grpId":828,//
//                "gNm":"me and my friend",//
//                "sAd":"nazmul2468",//
//                "groupMembers":[
//                {"uId":"nazmul2468",//
//                "admin":true,//
//                "psnc":2,//
//                "fn":"nazmul",
//                "ln":"nazmul",
//                "gr":"Male"}
//                ,{"uId":"nazmul","admin":false,"psnc":2,"fn":"Nazmul","ln":"Haque","gr":"Male"}]}                                          
                try {

                    HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();

                    ArrayList<GroupMember> mList = new ArrayList<GroupMember>();
                    HashMap<String, GroupMember> members = new HashMap<String, GroupMember>();

                    JSONArray memberJsonArray = jsonObject.getJSONArray("groupMembers");

                    for (int j = 0; j < memberJsonArray.length(); j++) {

                        try {
                            JSONObject member = memberJsonArray.getJSONObject(j);

                            GroupMember groupMember = new GroupMember();

                            groupMember.setUserIdentity(member.getString("uId"));
                            groupMember.setAdmin(member.getBoolean("admin"));
                            groupMember.setPresence(member.getInt("psnc"));
                            groupMember.setFirstName(member.getString("fn"));
                            groupMember.setLastName(member.getString("ln"));
                            groupMember.setGender(member.getString("gr"));
                            mList.add(groupMember);
                            members.put(groupMember.getUserIdentity(), groupMember);

                        } catch (Exception e) {
                        }

                    }
                    GroupDTO gDTO = new GroupDTO();
                    gDTO.setGroupId(jsonObject.getLong("grpId"));
                    gDTO.setGroupName(jsonObject.getString("gNm"));
                    gDTO.setSuperAdmin(jsonObject.getString("sAd"));
                    gDTO.setMemberList(mList);
                    gDTO.setMembers(members);
                    groupHashMap.put(gDTO.getGroupId(), gDTO);
                    grpId = gDTO.getGroupId();
                    setGroupDetails("" + grpId);
                    if (newGroupList == null) {
                        newGroupList = new ArrayList<GroupDTO>();
                    }
                    newGroupList.add(gDTO);

                    loggedUserProfile.setGroupList(groupHashMap);

                    isRefreshDiv = true;
                } catch (Exception e) {
                }

            } else if (actn == ServerConstants.TYPE_UPDATE_ADD_GROUP_MEMBER) {
                //{"sucs":true,
//                "actn":51,
//                "pckFs":2814,
//                "grpId":828,//
//                "gNm":"me and my friend",//
//                "sAd":"nazmul2468",//
//                "groupMembers":[
//                {"uId":"nazmul2468",//
//                "admin":true,//
//                "psnc":2,//
//                "fn":"nazmul",
//                "ln":"nazmul",
//                "gr":"Male"}
//                ,{"uId":"nazmul","admin":false,"psnc":2,"fn":"Nazmul","ln":"Haque","gr":"Male"}]}                                          
                try {
////received>>>>>>>>>>>>>>>{"sucs":true,"actn":356,"pckFs":9473,"grpId":830,"gNm":"me and my friend","sAd":"nazmul2468","groupMembers":[{"uId":"nazmul","admin":false,"psnc":2,"fn":"Nazmul","ln":"Haque","gr":"Male"},{"uId":"nazmul1116840","admin":false,"psnc":2,"fn":"nazmul","ln":"nazmul","gr":"Male"},{"uId":"nazmul2468","admin":true,"psnc":2,"fn":"nazmul","ln":"nazmul","gr":"Male"}]}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         

                    HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();

                    ArrayList<GroupMember> mList = new ArrayList<GroupMember>();
                    HashMap<String, GroupMember> members = new HashMap<String, GroupMember>();

                    JSONArray memberJsonArray = jsonObject.getJSONArray("groupMembers");

                    for (int j = 0; j < memberJsonArray.length(); j++) {

                        try {
                            JSONObject member = memberJsonArray.getJSONObject(j);

                            GroupMember groupMember = new GroupMember();

                            groupMember.setUserIdentity(member.getString("uId"));
                            groupMember.setAdmin(member.getBoolean("admin"));
                            groupMember.setPresence(member.getInt("psnc"));
                            groupMember.setFirstName(member.getString("fn"));
                            groupMember.setLastName(member.getString("ln"));
                            groupMember.setGender(member.getString("gr"));
                            mList.add(groupMember);
                            members.put(groupMember.getUserIdentity(), groupMember);

                        } catch (Exception e) {
                        }

                    }

                    if (jsonObject.has("gNm")) {

                        GroupDTO gDTO = new GroupDTO();
                        gDTO.setGroupId(jsonObject.getLong("grpId"));
                        gDTO.setGroupName(jsonObject.getString("gNm"));
                        gDTO.setSuperAdmin(jsonObject.getString("sAd"));
                        gDTO.setMemberList(mList);
                        gDTO.setMembers(members);
                        groupHashMap.put(gDTO.getGroupId(), gDTO);
                        grpId = gDTO.getGroupId();
                        setGroupDetails("" + grpId);
                        if (newGroupList == null) {
                            newGroupList = new ArrayList<GroupDTO>();
                        }
                        newGroupList.add(gDTO);

                        loggedUserProfile.setGroupList(groupHashMap);

                        isRefreshDiv = true;

                    } else {
                        GroupDTO gDTO = groupHashMap.get(jsonObject.getLong("grpId"));

                        gDTO.getMemberList().addAll(mList);
                        gDTO.getMembers().putAll(members);
                        grpId = gDTO.getGroupId();
                        setGroupDetails("" + grpId);

                    }

                } catch (Exception e) {
                }

            } else if (actn == ServerConstants.TYPE_ADD_GROUP_MEMBER) {
                HashMap<Long, GroupDTO> groupHashMap = loggedUserProfile.getGroupList();

                if (jsonObject.getBoolean("sucs")) {
                    GroupDTO gDTO = loggedUserProfile.getGroupList().get(groupDetails.getGroupId());
                    ArrayList<GroupMember> tempList = gDTO.getMemberList();
                    if (tempList == null || tempList.size() < 1) {
                        tempList = new ArrayList<GroupMember>();
                    }
                    tempList.addAll(selectedList.get(newGroupId));
                    HashMap<String, GroupMember> members = new HashMap<String, GroupMember>();
                    ArrayList<GroupMember> mList = gDTO.getMemberList();
                    if (mList != null && mList.size() > 0) {
                        for (int k = 0; k < mList.size(); k++) {
                            GroupMember gm = mList.get(k);
                            members.put(gm.getUserIdentity(), gm);
                        }
                        gDTO.setMembers(members);
                    }
                    groupHashMap.put(gDTO.getGroupId(), gDTO);
                    grpId = gDTO.getGroupId();
                    setGroupDetails("" + grpId);
                    selectedList.remove(newGroupId);
                    gNm = "";

                    if (newGroupList == null) {
                        newGroupList = new ArrayList<GroupDTO>();
                    }
                    newGroupList.add(gDTO);

                    loggedUserProfile.setGroupList(groupHashMap);
                    errorMessage = "Group is added successfully.";

                    isRefreshDiv = true;
                } else {
                    errorMessage = jsonObject.getString("msg");
                }

                isComplete = true;
            }
//            } else {
//                if (jsonObject.has("mg")) {
//                    errorMessage = jsonObject.getString("mg");
//                }
//            }
        } catch (Exception e) {
//            System.out.println("GroupAction==>" + e.toString());
        }
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getGroupName() {
        return gNm;
    }

    public void setGroupName(String gNm) {
        this.gNm = gNm;
    }

    public GroupChatWindow getGroupChatWindow() {
        return groupChatWindow;
    }

    public void setGroupChatWindow(GroupChatWindow groupChatWindow) {
        this.groupChatWindow = groupChatWindow;
    }

    public String getNewGroupId() {
        return newGroupId;
    }

    public void setNewGroupId(String gid) {
        if (gid != null && gid.length() > 0) {
            this.newGroupId = gid;
        } else {
            this.newGroupId = loggedUserProfile.getUserIdentity();
        }
    }

    public void setMemberList(ArrayList<GroupMember> memberList) {
        this.memberList = memberList;
    }

    public String getGroupSearchValue() {
        return groupSearchValue;
    }

    public void setGroupSearchValue(String groupSearchValue) {
        this.groupSearchValue = groupSearchValue;
    }
}
