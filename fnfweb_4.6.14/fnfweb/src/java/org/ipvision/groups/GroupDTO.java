/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.groups;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.myfaces.shared_tomahawk.util.ConcurrentLRUCache;

/**
 *
 * @author user
 */
public class GroupDTO implements Serializable {

    private long grpId;
    private String gNm;
    private String sAd;
    HashMap<String, GroupMember> members = new HashMap<String, GroupMember>();
    private ArrayList<GroupMember> memberList =new ArrayList<GroupMember>();
    private String groupMembers;
    private int noOfMsg;
      private long chatPortUpdateTime;
    private  int CHAT_REGISTRATION_PORT ;
    private  int FRIEND_CHAT_PORT ;
    private  int GROUP_CHAT_PORT;
    private  int CHAT_HISTORY_PORT;
    private  int CHAT_CONFIRMATION_PORT;
     private  InetAddress chatIp;
    
     private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public GroupDTO() {
    }

    public long getGroupId() {
        return grpId;
    }

    public void setGroupId(long grpId) {
        this.grpId = grpId;
    }

    public String getGroupName() {
        return gNm;
    }

    public void setGroupName(String gNm) {
        this.gNm = gNm;
    }

    public String getSuperAdmin() {
        return sAd;
    }

    public void setSuperAdmin(String sAd) {
        this.sAd = sAd;
    }

    public HashMap<String, GroupMember> getMembers() {
        if (members == null) {
            members = new HashMap<String, GroupMember>();
        }
        return members;
    }

    public void setMembers(HashMap<String, GroupMember> members) {
        this.members = members;
    }

    public ArrayList<GroupMember> getMemberList() {
        return memberList;
    }

    public void setMemberList(ArrayList<GroupMember> memberList) {
        this.memberList = memberList;
    }

    public String getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(String groupMembers) {
        this.groupMembers = groupMembers;
    }

    public int getNoOfMsg() {
        return noOfMsg;
    }

    public void setNoOfMsg(int noOfMsg) {
        this.noOfMsg = noOfMsg;
    }

    public long getChatPortUpdateTime() {
        return chatPortUpdateTime;
    }

    public void setChatPortUpdateTime(long chatPortUpdateTime) {
        this.chatPortUpdateTime = chatPortUpdateTime;
    }

    public int getCHAT_REGISTRATION_PORT() {
        return CHAT_REGISTRATION_PORT;
    }

    public void setCHAT_REGISTRATION_PORT(int CHAT_REGISTRATION_PORT) {
        this.CHAT_REGISTRATION_PORT = CHAT_REGISTRATION_PORT;
    }

    public int getFRIEND_CHAT_PORT() {
        return FRIEND_CHAT_PORT;
    }

    public void setFRIEND_CHAT_PORT(int FRIEND_CHAT_PORT) {
        this.FRIEND_CHAT_PORT = FRIEND_CHAT_PORT;
    }

    public int getGROUP_CHAT_PORT() {
        return GROUP_CHAT_PORT;
    }

    public void setGROUP_CHAT_PORT(int GROUP_CHAT_PORT) {
        this.GROUP_CHAT_PORT = GROUP_CHAT_PORT;
    }

    public int getCHAT_HISTORY_PORT() {
        return CHAT_HISTORY_PORT;
    }

    public void setCHAT_HISTORY_PORT(int CHAT_HISTORY_PORT) {
        this.CHAT_HISTORY_PORT = CHAT_HISTORY_PORT;
    }

    public int getCHAT_CONFIRMATION_PORT() {
        return CHAT_CONFIRMATION_PORT;
    }

    public void setCHAT_CONFIRMATION_PORT(int CHAT_CONFIRMATION_PORT) {
        this.CHAT_CONFIRMATION_PORT = CHAT_CONFIRMATION_PORT;
    }

    public InetAddress getChatIp() {
        return chatIp;
    }

    public void setChatIp(InetAddress chatIp) {
        this.chatIp = chatIp;
    }
    
}
