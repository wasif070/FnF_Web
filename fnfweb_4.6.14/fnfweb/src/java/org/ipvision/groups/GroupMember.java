/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.groups;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class GroupMember implements Serializable {

    String uId;
    boolean admin;
    int psnc;
    String fn;
    String ln;
    String gr;
    String mbl;
    String prIm;

    public GroupMember() {
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public int getPresence() {
        return psnc;
    }

    public void setPresence(int psnc) {
        this.psnc = psnc;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String ln) {
        this.ln = ln;
    }

    public String getGender() {
        return gr;
    }

    public void setGender(String gr) {
        this.gr = gr;
    }

    public String getMobilePhone() {
        return mbl;
    }

    public void setMobilePhone(String mbl) {
        this.mbl = mbl;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String prIm) {
        this.prIm = prIm;
    }
}
