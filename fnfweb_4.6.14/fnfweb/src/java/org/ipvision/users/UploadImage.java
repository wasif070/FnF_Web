/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.users;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.log4j.Logger;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONObject;

public class UploadImage extends HttpServlet {

    static Logger logger = Logger.getLogger(UploadImage.class.getName());

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        String sId = "";
        String uId = "";

        LoggedUserProfile loggedUserProfile = (LoggedUserProfile) request.getSession().getAttribute("loggedUserProfile");

        sId = loggedUserProfile.getSessionId();
        uId = loggedUserProfile.getUserIdentity();

        byte[] imageByte = null;
        PrintWriter out = response.getWriter();
        FileItemFactory factory = new org.apache.commons.fileupload.disk.DiskFileItemFactory();
        org.apache.commons.fileupload.servlet.ServletFileUpload upload = new org.apache.commons.fileupload.servlet.ServletFileUpload(factory);
        List items = null;
        try {
            items = upload.parseRequest(request);
        } catch (Exception e) {
            // logger.info("FNFLOGGER_EX_21: Exception in ImageUploadHandler no:1>" + e);
            e.printStackTrace();
        }
        Iterator itr = items.iterator();
        while (itr.hasNext()) {
            org.apache.commons.fileupload.FileItem item = (org.apache.commons.fileupload.FileItem) itr.next();
            if (item.isFormField()) {

            } else {
                String name = item.getFieldName();
                String value = item.getString();

                try {
                    imageByte = item.get();
                } catch (Exception e) {
                } finally {
                }
            }
        }
       String responseStr = uploader_method("http://38.108.92.154/auth/rest/ImageUploadHandler", sId, uId, imageByte,ServerConstants.AUTHENTICATION_PORT);
       
        
        //  String responseStr = uploader_method("http://localhost:8080/auth/rest/CoverImageUploadHandler", sId, uId, imageByte,ServerConstants.AUTHENTICATION_PORT);
       
        try {
            JSONObject jsonObject = new JSONObject(responseStr.trim());
            if (jsonObject.has("sucs")) {
                if (jsonObject.getBoolean("sucs")) {
                    if (jsonObject.has("prIm")) {
                        String prim = jsonObject.getString("prIm");
                        loggedUserProfile.setProfileImage(prim);
                    }
                }
            }
        } catch (Exception e) {
        }
        out.println(responseStr);
    }

    public String uploader_method(String imageserverurl, String sId, String useridenity, byte[] imageData,int authPort) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;

        InputStream is = null;
        OutputStream os = null;
        boolean ret = false;
        String StrMessage = "";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        //  DownLoaderHelps dHelps = new DownLoaderHelps();

        int bytesRead, bytesAvailable, bufferSize;

        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        String responseFromServer = "";
        try {
            URL url = new URL(imageserverurl);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs  
            conn.setDoInput(true);
            // Allow Outputs  
            conn.setDoOutput(true);
            // Don't use a cached copy.  
            conn.setUseCaches(false);
            // Use a post method.  
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            dos = new DataOutputStream(conn.getOutputStream());
           
            
            
              dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"authPort\"" + lineEnd + lineEnd + authPort + lineEnd);
            
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"sId\"" + lineEnd + lineEnd + sId + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uId\"" + lineEnd + lineEnd + useridenity + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + "test.jpg" + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            // while (bytesRead > 0) {
            dos.write(imageData, 0, imageData.length);
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            dos.flush();
            dos.close();
        } catch (MalformedURLException ex) {
        } catch (IOException ioe) {
        }
        //------------------ read the SERVER RESPONSE  

        String mainstr = "";

        try {
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String str;

            while ((str = br.readLine()) != null) {
                mainstr += str;
            }

        } catch (IOException ioex) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mainstr;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
