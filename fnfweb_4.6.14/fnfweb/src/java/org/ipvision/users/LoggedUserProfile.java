/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.users;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.ipvision.groups.GroupDTO;

/**
 *
 * @author user
 */
@ManagedBean(name = "loggedUserProfile")
@SessionScoped
public class LoggedUserProfile implements Serializable {

    private String mblDc;
    private String el;
    private Date bDay;
    private String gr;
    private String mbl;
    private String fn;
    private String ln;
    private String cnty;
    private int psnc;
    private String wim;
    private boolean sucs;
    private String sId;
    private String prIm;
    private String cIm;
    private String uId;
    private String usrPw;
    private String oldPassword;
    private String retypePassword;
    private int ePr;
    private int mblPhPr;
    private int prImPr;
    private int bdPr;
     private int cImPr;
    private int mailServerPort;
    private  String mailServerHost;
    private  String mailServerUserId;
    private HashMap<String, UserProfile> friendlist = new HashMap<String, UserProfile>();
    private HashMap<Long, GroupDTO> groupList =  new HashMap<Long, GroupDTO>();    
    private HashMap<String, String> presenceMap =  new HashMap<String, String>();

    public HashMap<String, String> getPresenceMap() {
        return presenceMap;
    }

    public void setPresenceMap(HashMap<String, String> presenceMap) {
        this.presenceMap = presenceMap;
    }

    
    
    public LoggedUserProfile() {
    }

    
     public String getCoverImage() {
        return cIm;
    }

    public void setCoverImage(String cIm) {
        this.cIm = cIm;
    }
    
    
    
    public String getMobilePhoneDialingCode() {
        return mblDc;
    }

    public void setMobilePhoneDialingCode(String mblDc) {
        this.mblDc = mblDc;
    }

    public String getEmail() {
        return el;
    }

    public void setEmail(String el) {
        this.el = el;
    }

    public String getGender() {
        return gr;
    }

    public void setGender(String gr) {
        this.gr = gr;
    }

    public String getMobilePhone() {
        return mbl;
    }

    public void setMobilePhone(String mbl) {
        this.mbl = mbl;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String ln) {
        this.ln = ln;
    }

    public String getCountry() {
        return cnty;
    }

    public void setCountry(String cnty) {
        this.cnty = cnty;
    }

    public int getPresence() {
        return psnc;
    }

    public void setPresence(int psnc) {
        this.psnc = psnc;
    }

    public String getWhatisInYourMind() {
        return wim;
    }

    public void setWhatisInYourMind(String wim) {
        this.wim = wim;
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean sucs) {
        this.sucs = sucs;
    }

    public String getSessionId() {
        return sId;
    }

    public void setSessionId(String sId) {
        this.sId = sId;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String prIm) {
        this.prIm = prIm;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getPassword() {
        return usrPw;
    }

    public void setPassword(String usrPw) {
        this.usrPw = usrPw;
    }

    public HashMap<String, UserProfile> getFriendlist() {
        if (friendlist == null) {
            friendlist = new HashMap<String, UserProfile>();
        }
        return friendlist;
    }

    public void setFriendlist(HashMap<String, UserProfile> friendlist) {
        this.friendlist = friendlist;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public HashMap<Long, GroupDTO> getGroupList() {
        if (groupList == null) {
            groupList = new HashMap<Long, GroupDTO>();
        }
        return groupList;
    }

    public void setGroupList(HashMap<Long, GroupDTO> groupList) {
        this.groupList = groupList;
    }

    public int getEmailPrivacy() {
        return ePr;
    }

    public void setEmailPrivacy(int ePr) {
        this.ePr = ePr;
    }

    public int getMobilePhonePrivacy() {
        return mblPhPr;
    }

    public void setMobilePhonePrivacy(int mblPhPr) {
        this.mblPhPr = mblPhPr;
    }

    public int getProfileImagePrivacy() {
        return prImPr;
    }

    public void setProfileImagePrivacy(int prImPr) {
        this.prImPr = prImPr;
    }

           public int getCoverImagePrivacy() {
        return cImPr;
    }

    public void setCoverImagePrivacy(int cImPr) {
        this.cImPr = cImPr;
    }
    public int getBirthDayPrivacy() {
        return bdPr;
    }

    public void setBirthDayPrivacy(int bdPr) {
        this.bdPr = bdPr;
    }

    public Date getBirthday() {
        return bDay;
    }

    public void setBirthday(Date bDay) {
        this.bDay = bDay;
    }

    public int getMailServerPort() {
        return mailServerPort;
    }

    public void setMailServerPort(int mailServerPort) {
        this.mailServerPort = mailServerPort;
    }

    public String getMailServerHost() {
        return mailServerHost;
    }

    public void setMailServerHost(String mailServerHost) {
        this.mailServerHost = mailServerHost;
    }

    public String getMailServerUserId() {
        return mailServerUserId;
    }

    public void setMailServerUserId(String mailServerUserId) {
        this.mailServerUserId = mailServerUserId;
    }
    
    
}
