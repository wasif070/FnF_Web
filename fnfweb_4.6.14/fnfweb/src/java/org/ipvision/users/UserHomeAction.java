/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.users;

import com.google.gson.Gson;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.ipvision.communication.MainBean;
import org.ipvision.friends.FriendsBean;
import org.ipvision.messenger.ChatWindowActivity;
import org.ipvision.net.OnReceiveListner;
import org.ipvision.registration.CountryBean;
import org.ipvision.utils.AppConstants;
import org.ipvision.utils.ServerConstants;
import org.richfaces.json.JSONArray;
import org.richfaces.json.JSONObject;

/**
 *
 * @author user
 */
@ManagedBean(name = "userHomeAction")
@SessionScoped
public class UserHomeAction implements OnReceiveListner, Serializable {
    // AullAuthontication System

    static Logger logger = Logger.getLogger(UserHomeAction.class.getName());
    @ManagedProperty(value = "#{mainBean}")
    private MainBean mainBean;
    //End
    @ManagedProperty(value = "#{loggedUserProfile}")
    private LoggedUserProfile loggedUserProfile;
    @ManagedProperty(value = "#{countrybean}")
    CountryBean countrybean;
//    @ManagedProperty(value = "#{friendsBean}")
//    private FriendsBean friendsBean;
    @ManagedProperty(value = "#{chatWindowActivity}")
    private ChatWindowActivity chatWindowActivity;
    UserProfile userProfile;
    private ArrayList<UserProfile> requestedFriendList;
    private int numberOfNewRequest;
    private boolean refreshDiv = false;
    boolean isComplete = false;
    String errorMessage = "";
    private int friendStatus;
    private String friendSearchValue;
    private ArrayList<UserProfile> newFriendList;
    String realPath = FacesContext.getCurrentInstance()
            .getExternalContext().getRequestContextPath();

    public UserHomeAction() {
    }

    public String goToSendSMS(String friendIdentity) {
        return "/secure/sms/send.xhtml" + AppConstants.REDIRECT + "&friend=" + friendIdentity;
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(int friendStatus) {
        this.friendStatus = friendStatus;
    }

    public String getFriendSearchValue() {
        return friendSearchValue;
    }

    public void setFriendSearchValue(String friendSearchValue) {
        this.friendSearchValue = friendSearchValue;
    }

    public boolean checkFriendRequest() {
        if (numberOfNewRequest > 0) {
            return true;
        }
        return false;
    }

    public ArrayList<UserProfile> getFriendList() {
        HashMap<String, UserProfile> friendHashMap = loggedUserProfile.getFriendlist();
        ArrayList<UserProfile> friendList = new ArrayList<UserProfile>();
        requestedFriendList = new ArrayList<UserProfile>();
        System.out.println("added new");
        if (friendHashMap != null && friendHashMap.size() > 0) {
            Set set = friendHashMap.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                UserProfile dto = (UserProfile) me.getValue();
                if (friendStatus > 0) {
                    if (friendStatus == dto.getPresence() && dto.getFriendShipStatus() == AppConstants.ACCEPTED_FRIEND) {
                        if (friendSearchValue != null && friendSearchValue.length() > 0) {
                            if (dto.getUserIdentity().startsWith(friendSearchValue) || dto.getFirstName().startsWith(friendSearchValue)) {
                                friendList.add(dto);
                            }
                        } else {
                            friendList.add(dto);
                        }
                    } else if (friendStatus == AppConstants.PENDDING && (dto.getFriendShipStatus() == AppConstants.INCOMING_FRIEND_REQUEST || dto.getFriendShipStatus() == AppConstants.OUTGOING_FRIEND_REQUEST)) {
                        if (friendSearchValue != null && friendSearchValue.length() > 0) {
                            if (dto.getUserIdentity().startsWith(friendSearchValue) || dto.getFirstName().startsWith(friendSearchValue)) {
                                friendList.add(dto);
                            }
                        } else {
                            friendList.add(dto);
                        }
                    }
                } else {
                    if (friendSearchValue != null && friendSearchValue.length() > 0) {
                        if (dto.getUserIdentity().startsWith(friendSearchValue) || dto.getFirstName().startsWith(friendSearchValue)) {
                            friendList.add(dto);
                        }
                    } else {
                        friendList.add(dto);
                    }
                }

                if (dto.getFriendShipStatus() == AppConstants.INCOMING_FRIEND_REQUEST) {
                    requestedFriendList.add(dto);
                    System.out.println("added 1");
                }
            }
        }
        numberOfNewRequest = requestedFriendList.size();
        return friendList;
    }

    public boolean checkNewContact() {
        if (refreshDiv) {
            return true;
        } //        else if (friendsBean.checkRefreshDiv()) {
        //            return true;
        //        } 
        else if (chatWindowActivity.checkChatNotification()) {
            return true;
        } else {
            return false;
        }
    }

    public String getMyFriends() {
        String friendListJsonStr = "false";
        Gson gson = new Gson();
        if (refreshDiv) {
            refreshDiv = false;
            if (newFriendList != null && newFriendList.size() > 0) {
                friendListJsonStr = gson.toJson(newFriendList).toString();
                newFriendList = new ArrayList<UserProfile>();
                return friendListJsonStr;
            }
            return friendListJsonStr;
        } //        else if (friendsBean.checkRefreshDiv()) {
        //            newFriendList = friendsBean.getFriendsUpdate();
        //            if (newFriendList != null && newFriendList.size() > 0) {
        //                for (int i = 0; i < newFriendList.size(); i++) {
        //                    UserProfile u = newFriendList.get(i);
        //                    if (u.getFriendShipStatus() == ServerConstants.INCOMING_FRIEND_REQUEST) {
        //                        numberOfNewRequest++;
        //                         requestedFriendList.add(u);
        //                         System.out.println("added 2");
        //                    }
        //                }
        //                friendListJsonStr = gson.toJson(newFriendList).toString();
        //            }
        //            return friendListJsonStr;
        //
        //        } 
        else if (chatWindowActivity.checkChatNotification()) {
            friendListJsonStr = chatWindowActivity.getChatUpdate();
            return friendListJsonStr;
        } else {
            return friendListJsonStr;
        }
    }

    public MainBean getMainBean() {
        return mainBean;
    }

    public void setMainBean(MainBean mainBean) {
        this.mainBean = mainBean;
    }

    public LoggedUserProfile getLoggedUserProfile() {
        return loggedUserProfile;
    }

    public void setLoggedUserProfile(LoggedUserProfile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    //Action
    public void updateProfile() {
        errorMessage = "";
        JSONObject jsonObject = new JSONObject();
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        try {
            jsonObject.put("actn", ServerConstants.TYPE_USER_PROFILE);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("nOfHd", 8);
            jsonObject.put("mblDc", countrybean.getCountry_code().toString());
            jsonObject.put("gr", loggedUserProfile.getGender().toString());
            jsonObject.put("fn", loggedUserProfile.getFirstName().trim());
            jsonObject.put("ln", loggedUserProfile.getLastName().trim());
            jsonObject.put("cnty", loggedUserProfile.getCountry().toString());
            jsonObject.put("el", loggedUserProfile.getEmail().trim());
            jsonObject.put("mbl", loggedUserProfile.getMobilePhone().toString());
            if (loggedUserProfile.getBirthday() != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                jsonObject.put("bDay", formatter.format(loggedUserProfile.getBirthday()));
            }
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    errorMessage = "Profile Updated Successfully!";
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public void updatePrivacy() {
        errorMessage = "";
        JSONObject jsonObject = new JSONObject();
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        try {
            jsonObject.put("actn", ServerConstants.TYPE_PRIVACY_SETTINGS);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("nOfHd", 5);
            jsonObject.put("ePr", loggedUserProfile.getEmailPrivacy());
            jsonObject.put("mblPhPr", loggedUserProfile.getMobilePhonePrivacy());
            jsonObject.put("prImPr", loggedUserProfile.getProfileImagePrivacy());
            jsonObject.put("bdPr", loggedUserProfile.getBirthDayPrivacy());
            jsonObject.put("cImPr", loggedUserProfile.getCoverImagePrivacy());
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    errorMessage = "Privacy Settings Updated Successfully!";
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public void changePassword() {
        errorMessage = "";
        isComplete = false;
        JSONObject jsonObject = new JSONObject();
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());

        if (!loggedUserProfile.getPassword().equals(loggedUserProfile.getRetypePassword())) {
            errorMessage = "Password & Retype Password doesn't match";
        } else if (loggedUserProfile.getPassword().contains(",")) {
            errorMessage = "Password doesn't contains any comma.";
        } else {
            try {
                jsonObject.put("actn", ServerConstants.TYPE_CHANGE_PASSWORD);
                jsonObject.put("pckId", packet_id);
                jsonObject.put("sId", loggedUserProfile.getSessionId());
                jsonObject.put("nOfHd", 1);
                jsonObject.put("oPw", loggedUserProfile.getOldPassword());
                jsonObject.put("nPw", loggedUserProfile.getPassword());
                mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
            } catch (Exception e) {
            }

            while (true) {
                try {
                    Thread.sleep(100);
                    if (isComplete) {
                        errorMessage = "Password Changed Successfully!";
                        isComplete = false;
                        break;
                    } else {
                        if (errorMessage == null || errorMessage.length() <= 0) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    public void updateWhatsInMind() {
        errorMessage = "";
        JSONObject jsonObject = new JSONObject();
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        try {
            jsonObject.put("actn", ServerConstants.TYPE_WHAT_IS_IN_UR_MIND);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("nOfHd", 1);
            jsonObject.put("wim", loggedUserProfile.getWhatisInYourMind());
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public void removeProfileImage() {
        errorMessage = "";
        JSONObject jsonObject = new JSONObject();
        String packet_id = ServerConstants.getRandromPacketId(loggedUserProfile.getUserIdentity());
        try {
            jsonObject.put("actn", ServerConstants.TYPE_REMOVE_PROFILE_IMAGE);
            jsonObject.put("pckId", packet_id);
            jsonObject.put("sId", loggedUserProfile.getSessionId());
            jsonObject.put("prIm", "");
            mainBean.getServerCommunication().sendUpdatePacket(jsonObject.toString(), packet_id);
        } catch (Exception e) {
        }

        while (true) {
            try {
                Thread.sleep(100);
                if (isComplete) {
                    isComplete = false;
                    break;
                } else {
                    if (errorMessage == null || errorMessage.length() <= 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public String setProfile(String uId) {
        if (loggedUserProfile.getFriendlist().containsKey(uId)) {
            userProfile = loggedUserProfile.getFriendlist().get(uId);
            return "";
        } else {

            userProfile = null;
            return "Sorry! The Friend Profile is not found.";
        }
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public void onReceivedMessage(String msg) {
        try {
            JSONObject jsonObject = new JSONObject(msg);
            int actn = jsonObject.getInt("actn");
            if (actn == ServerConstants.TYPE_PRESENCE) {
                setPresence(msg);
                refreshDiv = true;
            } else if (actn == ServerConstants.TYPE_CONTACT_LIST) {
                if (jsonObject.getBoolean("sucs")) {

                    if (jsonObject.has("contactList")) {
                        JSONArray friendsJsonArray = jsonObject.getJSONArray("contactList");
                        HashMap<String, UserProfile> friendHashMap = loggedUserProfile.getFriendlist();
                         Map<String, String> presenceMap = loggedUserProfile.getPresenceMap();

                        for (int j = 0; j < friendsJsonArray.length(); j++) {
                            JSONObject jProfile = friendsJsonArray.getJSONObject(j);
                            if (jProfile.getInt("ists") == 1) {
                                continue;
                            }

                            UserProfile friendProfile = new UserProfile();
                            friendProfile.setCountry(jProfile.getString("cnty"));
                            friendProfile.setFirstName(jProfile.getString("fn"));
                            friendProfile.setFriendShipStatus(jProfile.getInt("frnS"));
                            friendProfile.setGender(jProfile.getString("gr"));
                            friendProfile.setLastName(jProfile.getString("ln"));
                            
                            if (jProfile.has("pvc")) {
                                JSONArray jprivacy = jProfile.getJSONArray("pvc");
                                if (jprivacy.getInt(0) == ServerConstants.SHOW_ALL || jprivacy.getInt(0) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    friendProfile.setEmail(jProfile.getString("el"));
                                } else {
                                    friendProfile.setEmail("");
                                }

                                if (jprivacy.getInt(1) == ServerConstants.SHOW_ALL || jprivacy.getInt(1) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    friendProfile.setMobilePhone(jProfile.getString("mbl"));
                                } else {
                                    friendProfile.setMobilePhone("");
                                }

                                if (jprivacy.getInt(2) == ServerConstants.SHOW_ALL || jprivacy.getInt(2) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    if (jProfile.getString("prIm") != null && jProfile.getString("prIm").length() > 4) {
                                        friendProfile.setProfileImage(jProfile.getString("prIm"));
                                    } else {
                                        if (friendProfile.getGender().equalsIgnoreCase("Male")) {
                                            friendProfile.setProfileImage(realPath + "/clients_image/male.png");
                                        } else {
                                            friendProfile.setProfileImage(realPath + "/clients_image/female.png");
                                        }
                                    }
                                } else {
                                    if (friendProfile.getGender().equalsIgnoreCase("Male")) {
                                        friendProfile.setProfileImage(realPath + "/clients_image/male.png");
                                    } else {
                                        friendProfile.setProfileImage(realPath + "/clients_image/female.png");
                                    }
                                }
                                if (jProfile.has("bDay") && (jprivacy.getInt(3) == ServerConstants.SHOW_ALL || jprivacy.getInt(3) == ServerConstants.SHOW_ONLY_FRIEND)) {
                                    friendProfile.setBirthday(jProfile.getString("bDay"));
                                } else {
                                    friendProfile.setBirthday("");
                                }
                            }

                            friendProfile.setUserIdentity(jProfile.getString("uId"));
                            friendProfile.setWhatisInYourMind(jProfile.getString("wim"));
                            
                        friendProfile.setPresence(ServerConstants.PRESENCER_OFFLINE);
                        if(presenceMap.containsKey(jProfile.getString("uId"))){
                        friendProfile.setPresence(ServerConstants.PRESENCER_ONLINE);
                        friendProfile.setDevice(presenceMap.get(jProfile.getString("uId")));
                        }
                            friendHashMap.put(jProfile.getString("uId"), friendProfile);

                            if (newFriendList == null) {
                                newFriendList = new ArrayList<UserProfile>();
                            }
                            if (friendProfile.getFriendShipStatus() == ServerConstants.INCOMING_FRIEND_REQUEST) {
                                numberOfNewRequest++;
                                requestedFriendList.add(friendProfile);
                                System.out.println("added 3");
                            }
                            newFriendList.add(friendProfile);

                            refreshDiv = true;
                        }

                    }
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
            } else if (actn == ServerConstants.TYPE_FRIENDS_PRESENCE_LIST) {

                //{"actn":98,"uId":"ring8801728119927,ring8801717931608","pckFs":7702}
                String presenceData = jsonObject.getString("uId");

                String presenceArray[] = presenceData.split(",");
                Map<String, String> presenceMap = loggedUserProfile.getPresenceMap();
                HashMap<String, UserProfile> friendHashMap = loggedUserProfile.getFriendlist();

                for (String s : presenceArray) {
                    String device_userId[] = s.split(";");
                    if (friendHashMap.containsKey(device_userId[1])) {
                        UserProfile friendProfile = friendHashMap.get(device_userId[1]);                       
                        friendProfile.setPresence(ServerConstants.PRESENCER_ONLINE);
                        friendProfile.setDevice(device_userId[0]);
                        if (newFriendList == null) {
                            newFriendList = new ArrayList<UserProfile>();
                        }

                        newFriendList.add(friendProfile);

                        refreshDiv = true;

                    } else {
                        presenceMap.put(device_userId[1], device_userId[0]);
                    }

                }

              

            } else if (actn == ServerConstants.TYPE_USER_PROFILE) {
                if (jsonObject.getBoolean("sucs")) {
                    isComplete = true;
                    JSONArray friendsJsonArray = jsonObject
                            .getJSONArray("inOH");
                    for (int j = 0; j < friendsJsonArray.length(); j++) {
                        int number = friendsJsonArray.getInt(j);
                        switch (number) {
                            case ServerConstants.CONS_FIRST_NAME:
                                loggedUserProfile.setFirstName(jsonObject.getString("fn"));
                                break;
                            case ServerConstants.CONS_LAST_NAME:
                                loggedUserProfile.setLastName(jsonObject.getString("ln"));
                                break;
                            case ServerConstants.CONS_GENDER:
                                if (jsonObject.has("gr")) {
                                    loggedUserProfile.setGender(jsonObject.getString("gr"));
                                }
                                break;
                            case ServerConstants.CONS_COUNTRY:
                                loggedUserProfile.setCountry(jsonObject.getString("cnty"));
                                break;
                            case ServerConstants.CONS_MOBILE_PHONE:
                                loggedUserProfile.setMobilePhone(jsonObject.getString("mbl"));
                                break;
                            case ServerConstants.CONS_EMAIL:
                                loggedUserProfile.setEmail(jsonObject.getString("el"));
                                break;
                            case ServerConstants.CONS_PRESENCE:
                                loggedUserProfile.setPresence(jsonObject.getInt("psnc"));
                                break;
                            case ServerConstants.CONS_PROFILE_IMAGE:
                                String imagePath = "";
                                if (jsonObject.has("prIm")) {
                                    imagePath = jsonObject.getString("prIm");
                                }
                                loggedUserProfile.setProfileImage(imagePath);
                                break;
                            case ServerConstants.CONS_BIRTHDAY:
                                if (jsonObject.has("bDay")) {
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                                    String dateInString = jsonObject.getString("bDay");
                                    try {
                                        Date date = formatter.parse(dateInString);
                                        loggedUserProfile.setBirthday(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        errorMessage = "Thank you. Your FnF Profile Updated Successfully!";
                    }
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
            } else if (actn == ServerConstants.TYPE_WHAT_IS_IN_UR_MIND) {
                if (jsonObject.getBoolean("sucs")) {
                    isComplete = true;
                    errorMessage = "Thank you. Your FnF Status Updated Successfully!";
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
//                        loggedUserProfile.setWhatisInYourMind(jsonObject.getString("wim"));
            } else if (actn == ServerConstants.TYPE_REMOVE_PROFILE_IMAGE) {
                if (jsonObject.getBoolean("sucs")) {
                    loggedUserProfile.setProfileImage("");
                    isComplete = true;
                    errorMessage = "Your profile image removed successfully";
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
//                        loggedUserProfile.setWhatisInYourMind(jsonObject.getString("wim"));
            } else if (actn == ServerConstants.TYPE_CHANGE_PASSWORD) {
                if (jsonObject.getBoolean("sucs")) {
                    isComplete = true;
                    errorMessage = "Thank you. Your FnF Password Updated Successfully!";
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
            } else if (actn == ServerConstants.TYPE_PRIVACY_SETTINGS) {
                if (jsonObject.getBoolean("sucs")) {
                    isComplete = true;
                    errorMessage = "Thank you. Privacy Settings Updated Successfully!";
                } else {
                    if (jsonObject.has("mg")) {
                        errorMessage = jsonObject.getString("mg");
                    } else {
                        errorMessage = "Action not performed. Please try again.";
                    }
                }
            } else {
                if (jsonObject.has("mg")) {
                    errorMessage = jsonObject.getString("mg");
                }
            }
        } catch (Exception e) {
            logger.info("UserHomeAction==>", e);
        }
    }

    private void setPresence(String msg) {
        //{"actn":78,"inOH":[16],"uId":"ring8801717931608","pckFs":18934,"pvc":[2,2,1,2,1]}   
        try {
            JSONObject jsonObject = new JSONObject(msg);
            String uId = jsonObject.getString("uId");
            JSONArray friendsJsonArray = jsonObject.getJSONArray("inOH");
            for (int j = 0; j < friendsJsonArray.length(); j++) {
                int number = friendsJsonArray.getInt(j);
                if (loggedUserProfile.getFriendlist().containsKey(uId)) {
                    UserProfile fProfile = loggedUserProfile.getFriendlist().get(uId);
                    switch (number) {
                         case ServerConstants.CONS_PRIVACY:
                                           
                                JSONArray jprivacy = jProfile.getJSONArray("pvc");
                                if (jprivacy.getInt(0) == ServerConstants.SHOW_ALL || jprivacy.getInt(0) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    fProfile.setEmail(jProfile.getString("el"));
                                } else {
                                    fProfile.setEmail("");
                                }

                                if (jprivacy.getInt(1) == ServerConstants.SHOW_ALL || jprivacy.getInt(1) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    fProfile.setMobilePhone(jProfile.getString("mbl"));
                                } else {
                                    fProfile.setMobilePhone("");
                                }

                                if (jprivacy.getInt(2) == ServerConstants.SHOW_ALL || jprivacy.getInt(2) == ServerConstants.SHOW_ONLY_FRIEND) {
                                    if (jProfile.getString("prIm") != null && jProfile.getString("prIm").length() > 4) {
                                        fProfile.setProfileImage(jProfile.getString("prIm"));
                                    } else {
                                        if (fProfile.getGender().equalsIgnoreCase("Male")) {
                                            fProfile.setProfileImage(realPath + "/clients_image/male.png");
                                        } else {
                                            fProfile.setProfileImage(realPath + "/clients_image/female.png");
                                        }
                                    }
                                } else {
                                    if (fProfile.getGender().equalsIgnoreCase("Male")) {
                                        fProfile.setProfileImage(realPath + "/clients_image/male.png");
                                    } else {
                                        fProfile.setProfileImage(realPath + "/clients_image/female.png");
                                    }
                                }
                                if (jProfile.has("bDay") && (jprivacy.getInt(3) == ServerConstants.SHOW_ALL || jprivacy.getInt(3) == ServerConstants.SHOW_ONLY_FRIEND)) {
                                    friendProfile.setBirthday(jProfile.getString("bDay"));
                                } else {
                                    friendProfile.setBirthday("");
                                }
                            
                            
                            
                            
                            
                            break;
                        case ServerConstants.CONS_FIRST_NAME:
                            fProfile.setFirstName(jsonObject.getString("fn"));
                            break;
                        case ServerConstants.CONS_LAST_NAME:
                            fProfile.setLastName(jsonObject.getString("ln"));
                            break;
                        case ServerConstants.CONS_GENDER:
                            fProfile.setGender(jsonObject.getString("gr"));
                            break;
                        case ServerConstants.CONS_COUNTRY:
                            fProfile.setCountry(jsonObject.getString("cnty"));
                            break;
                        case ServerConstants.CONS_MOBILE_PHONE:
                            fProfile.setMobilePhone(jsonObject.getString("mbl"));
                            break;
                        case ServerConstants.CONS_EMAIL:
                            fProfile.setEmail(jsonObject.getString("el"));
                            break;
                        case ServerConstants.CONS_PRESENCE:
                            fProfile.setPresence(jsonObject.getInt("psnc"));
                            if(fProfile.getPresence()==ServerConstants.PRESENCER_ONLINE){
                            
                            fProfile.setDevice(""+jsonObject.getInt("dvc"));
                            }
                            
                            break;
                        case ServerConstants.CONS_WHAT_IS_IN_UR_MIND:
                            fProfile.setWhatisInYourMind(jsonObject.getString("wim"));
                            break;
                        case ServerConstants.CONS_FRIENDSHIP_STATUS:
                            fProfile.setFriendShipStatus(jsonObject.getInt("frnS"));
                            break;

                        case ServerConstants.CONS_PROFILE_IMAGE:
                            String imagePath = "";
                            if (jsonObject.has("prIm")) {
                                imagePath = jsonObject.getString("prIm");
                            }
                            fProfile.setProfileImage(imagePath);
                            break;
                        default:
                            break;
                    }
                    loggedUserProfile.getFriendlist().put(fProfile.getUserIdentity(), fProfile);
                    if (newFriendList == null) {
                        newFriendList = new ArrayList<UserProfile>();
                    }
                    newFriendList.add(fProfile);
                } else if (uId.equalsIgnoreCase(loggedUserProfile.getUserIdentity())) {
                    switch (number) {
                        case ServerConstants.CONS_PROFILE_IMAGE:
                            String imagePath = "";
                            if (jsonObject.has("prIm")) {
                                imagePath = jsonObject.getString("prIm");
                            }
                            loggedUserProfile.setProfileImage(imagePath);
                            break;
                    }
                }
            }
        } catch (Exception e) {
//            System.out.println("CommunicationProvider==>setPresence - Exception : " + e.toString());
        }
    }

    public CountryBean getCountrybean() {
        return countrybean;
    }

    public void setCountrybean(CountryBean countrybean) {
        this.countrybean = countrybean;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

//    public FriendsBean getFriendsBean() {
//        return friendsBean;
//    }
//
//    public void setFriendsBean(FriendsBean friendsBean) {
//        this.friendsBean = friendsBean;
//    }
    public ChatWindowActivity getChatWindowActivity() {
        return chatWindowActivity;
    }

    public void setChatWindowActivity(ChatWindowActivity chatWindowActivity) {
        this.chatWindowActivity = chatWindowActivity;
    }

    public ArrayList<UserProfile> getRequestedFriendList() {
        return requestedFriendList;
    }

    public void setRequestedFriendList(ArrayList<UserProfile> requestedFriendList) {
        this.requestedFriendList = requestedFriendList;
    }

    public int getNumberOfNewRequest() {
        return numberOfNewRequest;
    }

    public void setNumberOfNewRequest(int numberOfNewRequest) {
        this.numberOfNewRequest = numberOfNewRequest;
    }

    public boolean isRefreshDiv() {
        return refreshDiv;
    }

    public void setRefreshDiv(boolean refreshDiv) {
        this.refreshDiv = refreshDiv;
    }

    public ArrayList<UserProfile> getNewFriendList() {
        return newFriendList;
    }

    public void setNewFriendList(ArrayList<UserProfile> newFriendList) {
        this.newFriendList = newFriendList;
    }

}
