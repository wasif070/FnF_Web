/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.users;

import java.io.Serializable;
import java.net.InetAddress;

/**
 *
 * @author user
 */
public class UserProfile implements Serializable {

    private String uId;
    private String fn;
    private String ln;
    private String gr;
    private String cnty;
    private String mbl;
    private String bDay;
    private String el;
    private int psnc;
    private String prIm;
    private String wim;
    private int frnS;
    private int noOfMsg;
    private boolean deleted;    
    private String dvc;
    
    
    
    
    private long chatPortUpdateTime;
    private  int CHAT_REGISTRATION_PORT ;
    private  int FRIEND_CHAT_PORT ;
    private  int GROUP_CHAT_PORT;
    private  int CHAT_HISTORY_PORT;
    private  int CHAT_CONFIRMATION_PORT;
    
    private  InetAddress chatIp;

    
    
    public String getDevice() {
        return dvc;
    }

    public void setDevice(String dvc) {
        this.dvc = dvc;
    }
    
    
    
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
  
    
    
    

    public UserProfile() {
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getLastName() {
        return ln;
    }

    public void setLastName(String ln) {
        this.ln = ln;
    }

    public String getGender() {
        return gr;
    }

    public void setGender(String gr) {
        this.gr = gr;
    }

    public String getCountry() {
        return cnty;
    }

    public void setCountry(String cnty) {
        this.cnty = cnty;
    }

    public String getMobilePhone() {
        return mbl;
    }

    public void setMobilePhone(String mbl) {
        this.mbl = mbl;
    }

    public String getEmail() {
        return el;
    }

    public void setEmail(String el) {
        this.el = el;
    }

    public int getPresence() {
        return psnc;
    }

    public void setPresence(int psnc) {
        this.psnc = psnc;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String prIm) {
        this.prIm = prIm;
    }

    public String getWhatisInYourMind() {
        return wim;
    }

    public void setWhatisInYourMind(String wim) {
        this.wim = wim;
    }

    public int getFriendShipStatus() {
        return frnS;
    }

    public void setFriendShipStatus(int frnS) {
        this.frnS = frnS;
    }

    public int getNoOfMsg() {
        return noOfMsg;
    }

    public void setNoOfMsg(int noOfMsg) {
        this.noOfMsg = noOfMsg;
    }

    public String getBirthday() {
        return bDay;
    }

    public void setBirthday(String bDay) {
        this.bDay = bDay;
    }

    public long getChatPortUpdateTime() {
        return chatPortUpdateTime;
    }

    public void setChatPortUpdateTime(long chatPortUpdateTime) {
        this.chatPortUpdateTime = chatPortUpdateTime;
    }

    public int getCHAT_REGISTRATION_PORT() {
        return CHAT_REGISTRATION_PORT;
    }

    public void setCHAT_REGISTRATION_PORT(int CHAT_REGISTRATION_PORT) {
        this.CHAT_REGISTRATION_PORT = CHAT_REGISTRATION_PORT;
    }

    public int getFRIEND_CHAT_PORT() {
        return FRIEND_CHAT_PORT;
    }

    public void setFRIEND_CHAT_PORT(int FRIEND_CHAT_PORT) {
        this.FRIEND_CHAT_PORT = FRIEND_CHAT_PORT;
    }

    public int getGROUP_CHAT_PORT() {
        return GROUP_CHAT_PORT;
    }

    public void setGROUP_CHAT_PORT(int GROUP_CHAT_PORT) {
        this.GROUP_CHAT_PORT = GROUP_CHAT_PORT;
    }

    public int getCHAT_HISTORY_PORT() {
        return CHAT_HISTORY_PORT;
    }

    public void setCHAT_HISTORY_PORT(int CHAT_HISTORY_PORT) {
        this.CHAT_HISTORY_PORT = CHAT_HISTORY_PORT;
    }

    public int getCHAT_CONFIRMATION_PORT() {
        return CHAT_CONFIRMATION_PORT;
    }

    public void setCHAT_CONFIRMATION_PORT(int CHAT_CONFIRMATION_PORT) {
        this.CHAT_CONFIRMATION_PORT = CHAT_CONFIRMATION_PORT;
    }

    public InetAddress getChatIp() {
        return chatIp;
    }

    public void setChatIp(InetAddress chatIp) {
        this.chatIp = chatIp;
    }
    
    
    
}
