/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.ipvmail;

import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class MailDAO {

    private PreparedStatement ps = null;
    protected DBConnection dbConnection = null;
    static Logger logger = Logger.getLogger(MailDAO.class.getName());

    public MailDTO getProfileInfo(String param_mail_address) {
        MailDTO mailDTO = new MailDTO();
        
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select james_fullname, james_phone from james_users where james_id = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, param_mail_address);

            ResultSet rs = ps.executeQuery();
            if (rs != null && rs.next()) {
                mailDTO.setFullname(rs.getString("james_fullname"));
                mailDTO.setPhone(rs.getString("james_phone"));
            }
            
        } catch (SQLException ex) {
            logger.info("Error while fetching full name from DB for userId --> " + param_mail_address + " :: " + ex);
        } catch (Exception ex) {
            logger.info("Error while fetching full name from DB for userId --> " + param_mail_address + " :: " + ex);
        } finally {
            if (dbConnection.connection != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                } catch (Exception ex) {
                    logger.info("Error while making DBConnection free --> " + ex);
                }
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                logger.info("Error while closing ps --> " + e);
            }
        }
        return mailDTO;
    }
}
