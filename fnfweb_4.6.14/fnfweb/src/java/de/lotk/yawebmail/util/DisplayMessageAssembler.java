/*
 * @(#)DisplayMessageAssembler.java 1.00 2006/03/15
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 15.03.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;

import com.sun.mail.util.BASE64DecoderStream;

import de.lotk.yawebmail.bean.DisplayMessageBean;
import java.util.Enumeration;
//import de.lotk.yawebmail.exceptions.MessageRetrieveException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Header;

/**
 * Erzeugt eine Display-Message aus einer javax.mail.Message.
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class DisplayMessageAssembler {

    // ---------------------------------------------------------- private Methoden
    /**
     * Saeubert den Content-ID-String. Ggf. mussen umschliessende Brackets
     * entfernt werden.
     *
     * @param contentID Zu saeubernde Content-ID
     * @return             <code>String</code> mit gesaeuberter Content-ID
     */
    private static String cleanContentID(String contentID) {

        String cid = contentID.trim();

        if (cid.startsWith("<") && cid.endsWith(">")) {

            cid = cid.substring(1, (cid.length() - 1));
        }

        return (cid);
    }

    /**
     * Fuegt einen DisplayPart hinzu.
     *
     * @param part Aktuell zu behandelnder Part
     * @param displayParts List, in die die DisplayParts einsortiert werden.
     * @param multiparts Map, in die Multiparts gepackt werden.
     */
    private static void addDisplayPart(Part part, List<Part> displayParts,
            Map<Multipart, List<Integer>> multiparts) {
        Part to_be_removed = null;
        // Wenn dieser Part ein BodyPart (Teil eines Multipart) ist, Parent heraus-
        // finden und DisplayPart-Nummer in multipart-Map merken
        if (part instanceof BodyPart) {
            try {
//                part.getHeader("Content-Type")[0].contains("text/plain");
                Enumeration<Header> e = part.getAllHeaders();
                while (e.hasMoreElements()) {
                    Header h = ((Header) e.nextElement());
                    String name = h.getName();
                    String value = h.getValue();
//                    System.out.println(e.nextElement());
//                    String param = ;
                    System.out.println("name --> " + name);
                    System.out.println("value --> " + value);

                    for (Part v_part : displayParts) {
                        System.out.println("--> " + v_part.getHeader("Content-Type")[0]);
                        v_part.getHeader("Content-Type")[0].contains("text/plain");
                        part.getHeader("Content-Type")[0].contains("text/html");
                        if (part.getHeader("Content-Type")[0].contains("text/html")) {
                            if (v_part.getHeader("Content-Type")[0].contains("text/plain")) {
                                to_be_removed = v_part;
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }

            BodyPart actPart = (BodyPart) part;
            Multipart parent = actPart.getParent();

            if (parent != null) {

                // Unter "multipart.get(parent)" muss eine Liste existieren, zu der wir
                // die DisplayPart-Nummer (= Groesse der DisplayPart-List) adden.
                ((List<Integer>) multiparts.get(parent)).add(new Integer(displayParts.size()));
            }
        }

        if(to_be_removed != null){
            displayParts.remove(to_be_removed);
        }
        displayParts.add(part);
    }

    /**
     * Packt die verschachtelten Parts einer Message in eine flache List
     * (DisplayParts) und in eine Map (InlineParts).
     *
     * @param part Aktuell zu behandelnder Part
     * @param displayParts List, in die die DisplayParts einsortiert werden.
     * @param inlineParts Map, in die die InlineParts gepackt werden.
     * @param multiparts Map, in die Multiparts gepackt werden.
     */
    private static void deflateMessageParts(Part part, List<Part> displayParts,
            Map<String, Part> inlineParts, Map<Multipart, List<Integer>> multiparts) throws IOException, MessagingException {

        Object partContent = null;

        try {
            partContent = part.getContent();
        } catch (Exception e) {
            addDisplayPart(part, displayParts, multiparts);
        }

        // Wenn Part-Content MultiPart ist, uns selbst fuer jeden Part rekursiv
        // aufrufen.
        if (partContent instanceof Multipart) {
            Multipart actMultipart = (Multipart) partContent;
            multiparts.put(actMultipart, (new ArrayList<Integer>()));

            for (int tt = 0; tt < actMultipart.getCount(); tt++) {
                deflateMessageParts(actMultipart.getBodyPart(tt), displayParts, inlineParts, multiparts);
            }
        } else if ((partContent instanceof Message) && (part.getContentType().toLowerCase().indexOf("rfc822-headers") < 0)) {
            deflateMessageParts((Message) partContent, displayParts, inlineParts, multiparts);
        } // For content-types that are unknown to the DataHandler system, an input
        // stream is returned as the content...
        //
        // BASE64DecoderStreams duerfen nicht in einen String geparst werden!
        else if ((partContent instanceof InputStream)
                && (!(partContent instanceof BASE64DecoderStream))) {

            BufferedInputStream bufReader
                    = new BufferedInputStream((InputStream) partContent);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] inBuf = new byte[4096];
            int len = 0;
            while ((len = bufReader.read(inBuf)) > 0) {

                baos.write(inBuf, 0, len);
            }
            bufReader.close();

            part.setContent(baos.toString(), part.getContentType());
            addDisplayPart(part, displayParts, multiparts);
        } // Ist Part ein InlinePart? Dann in die DisplayParts-List packen.
        // 1.) Part muss MimeBodyPart sein
        // 2.) Part muss eine Content-ID haben.
        // 3.) Parent-Part muss "Content-Type: multipart/related;" haben.
        // 4.) Wenn "Content-Disposition" am Start, muss es "inline" sein.
        else if ((part instanceof MimeBodyPart)
                && (((MimeBodyPart) part).getContentID() != null)
                && (((MimeBodyPart) part).getContentID().trim().length() >= 1)
                && (((MimeBodyPart) part).getParent().getContentType() != null)
                && (((MimeBodyPart) part).getParent().getContentType().toLowerCase().indexOf("multipart/related") >= 0)
                && ((part.getDisposition() == null) || part.getDisposition().equalsIgnoreCase(Part.INLINE))) {

            inlineParts.put(cleanContentID(((MimePart) part).getContentID()), part);
        } // Sonst den aktuellen Part einfach in die DisplayParts-List packen.
        else {

            addDisplayPart(part, displayParts, multiparts);
        }
    }

    /**
     * Erstellt eine DisplayMessageBean, die eine fehlerhafte Mail anzeigt.
     *
     * @param message Fehlerhafte Message
     * @param displayMessage Aktuelle DisplayMessageBean
     * @param displayParts List, in die die DisplayParts einsortiert werden.
     * @param inlineParts Map, in die die InlineParts gepackt werden.
     * @param multiparts Map, in die Multiparts gepackt werden.
     * @param e Exception, die beim Einlesen geflogen ist
     */
    private static void assemblePartsForFaultySourceMessage(Message message,
            DisplayMessageBean displayMessage, List<Part> displayParts, Map inlineParts, Map multiparts, Exception e) throws MessagingException {

        // Alle vielleicht schon ansatzweise gefuellten Collections zuruecksetzen
        displayParts.clear();
        inlineParts.clear();
        multiparts.clear();

        // Part erstellen, der auf das Problem hinweist und den Quelltext anfuegt.
        StringBuffer mt = new StringBuffer("Message faulty!\n\n");
        mt.append("The requested messages is faulty because of this reason:\n");
        mt.append(e.getMessage()).append("\n\n");
        mt.append("This is the faulty source of the requested message:\n\n");
        mt.append(displayMessage.getMessageSource());

        // Info-Text-Message erstellen
        Message infoMessage = new MimeMessage((Session) null);
        infoMessage.setText(mt.toString());

        // Info-Text-Message in die Display-Parts packen
        displayParts.add(infoMessage);
    }

    // ----------------------------------------------------- oeffentliche Methoden
    /**
     * Baut aus einem Message-Objekt ein DisplayMessage-Objekt.
     *
     * @param message Message, aus der das DisplayMessage-Objekt gebaut werden
     * soll.
     */
    public static DisplayMessageBean assembleDisplayMessage(Message message) {

        DisplayMessageBean displayMessage = new DisplayMessageBean();

        return (refurbishGivenDisplayMessage(displayMessage, message));
    }

    /**
     * Setzt die Properties einen existierenden DisplayMessage-Objektes aus der
     * uebergebenen javax.mail.Message.
     *
     * @param displayMessage Bereits existierende Display-Message
     * @param message Message, aus der das DisplayMessage-Objekt gebaut werden
     * soll.
     */
    public static DisplayMessageBean refurbishGivenDisplayMessage(DisplayMessageBean displayMessage,
            Message message) {

        displayMessage.setOriginMessage(message);
        // Parts aus der Message in eine flache ArrayList (DisplayParts) und eine
        // HashMap (InlineParts) packen. Ausserdem die Parts von Multipart-Objekten
        // in einer Map merken (Key: Multipart-Objekt / Value: List)
        List<Part> displayParts = new ArrayList<Part>();
        Map<String, Part> inlineParts = new HashMap<String, Part>();
        Map<Multipart, List<Integer>> multiparts
                = new HashMap<Multipart, List<Integer>>();

        try {
            deflateMessageParts(message, displayParts, inlineParts, multiparts);
        } catch (Exception e) {
            try {
                assemblePartsForFaultySourceMessage(message, displayMessage, displayParts, inlineParts, multiparts, e);
            } catch (MessagingException me) {
                e.printStackTrace();
                me.printStackTrace();
//                throw (new MessageRetrieveException("Konnte Message-Parts nicht beziehen", e));
            }
        }

        displayMessage.setDisplayParts(displayParts);
        displayMessage.setInlineParts(inlineParts);
        displayMessage.setMultiparts(multiparts);
        int counter = 1;
        for (Part part : displayParts) {
            try {
                System.out.println(counter++ + "--> " + getText(part));
            } catch (MessagingException ex) {
                System.err.println("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr11111111111111111");
            } catch (IOException ex) {
                System.err.println("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
            }
        }

        return (displayMessage);
    }

    private static String getText(Part p) throws
            MessagingException, IOException {
        boolean textIsHtml = false;
        if (p.isMimeType("text/*")) {
            String s = (String) p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null) {
                        text = getText(bp);
                    }
                    return text;
//                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    if (s != null) {
                        return s;
                    }
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null) {
                    return s;
                }
            }
        }

        return null;
    }
}
