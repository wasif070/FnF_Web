/*
 * @(#)MailboxConnection.java 1.00 2005/02/05
 *
 * Copyright (c) 2005, Stephan Sann
 *
 * 05.02.2005 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.business;

import com.sun.mail.imap.protocol.FLAGS;
import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.TreeSet;

import javax.mail.AuthenticationFailedException;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;

import com.sun.mail.util.MailSSLSocketFactory;

import de.lotk.yawebmail.application.Constants;
import de.lotk.yawebmail.application.Lifecycle;
import de.lotk.yawebmail.bean.LoginDataBean;
import de.lotk.yawebmail.bean.RetrieveMessagesResultBean;
import de.lotk.yawebmail.bean.SessionContainerBean;
import de.lotk.yawebmail.enumerations.MailboxProtocolEnum;
import de.lotk.yawebmail.exceptions.AccessDeniedException;
import de.lotk.yawebmail.exceptions.ConnectionEstablishException;
import de.lotk.yawebmail.exceptions.LogoutException;
import de.lotk.yawebmail.exceptions.MailboxFolderException;
import de.lotk.yawebmail.exceptions.MessageDeletionException;
import de.lotk.yawebmail.exceptions.MessageMovementException;
import de.lotk.yawebmail.exceptions.MessageRetrieveException;
import de.lotk.yawebmail.exceptions.YawebmailCertificateException;
import de.lotk.yawebmail.util.JavamailUtils;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.mail.Address;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.UIDFolder;
import javax.mail.search.SearchTerm;
import org.apache.log4j.Logger;

/**
 * Default-Implementierung des Interfaces MailboxConnection.
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class DefaultMailboxConnection implements Lifecycle, Serializable {

    private static Logger logger = Logger.getLogger(DefaultMailboxConnection.class.getName());
    // ---------------------------------------------------------------- Konstanten
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8364316467231432686L;
    // --------------------------------------------------------- Instanz-Variablen
    /**
     * Speichert die Login-Daten
     */
    private LoginDataBean loginData = null;
    /**
     * Speichert einen JavaMail-Store
     */
    private Store store = null;
    /**
     * Speichert einen JavaMail-Folder
     */
    private Folder folder = null;

    private UIDFolder uidfolder = null;
    /**
     * Speichert den FolderSeparator
     */
    private Character separator = null;
    /**
     * Speichert ein TreeSet mit allen Foldern des Accounts
     */
    private TreeSet allFolders = null;

    private TreeSet allFoldersList = null;
    /**
     * Holds a TrustManager for secure connections
     */
    private YawebmailTrustManager trustManager = null;

    private int noOfMailBoxConnectionUsers = 0;

    // --------------------------------------------------------------- Konstruktor
    /**
     * Initialisiert eine DefaultMailboxConnection-Instanz mit Login-Daten.
     *
     * @param loginData Die Login-Daten
     * @param trustManager TrustManager to use for secure connections
     */
    public DefaultMailboxConnection(LoginDataBean loginData, YawebmailTrustManager trustManager) {
        this.loginData = loginData;
        this.trustManager = trustManager;
    }

    public DefaultMailboxConnection(LoginDataBean loginData, YawebmailTrustManager trustManager, Character separator) {
        this.loginData = loginData;
        this.trustManager = trustManager;
        this.separator = separator;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    private void createStore() {

        try {

            MailboxProtocolEnum curMbPrtcl = this.loginData.getMailboxProtocol();

            // Get a Properties and Session object
            Properties props = JavamailUtils.getProperties();

            // Trustmanager needed?
            if (curMbPrtcl.isUseOfSsl()) {

                MailSSLSocketFactory socketFactory = new MailSSLSocketFactory();
                socketFactory.setTrustManagers(new TrustManager[]{this.trustManager});

                props.put(("mail." + curMbPrtcl.getProtocolId() + ".ssl.socketFactory"),
                        socketFactory);
            }

            Session session = Session.getInstance(props, null);
            session.setDebug(false);

            this.store = session.getStore(curMbPrtcl.getProtocolId());
        } catch (NoSuchProviderException e) {

            throw (new RuntimeException(("Unknown Protocol: "
                    + this.loginData.getMailboxProtocol()), e));
        } catch (GeneralSecurityException gse) {
            throw (new RuntimeException(("Security-problem: " + gse.getMessage()),
                    gse));
        }
    }

    /**
     * Setzt den in der Instanzvariable folder gespeicherten Folder.
     *
     * @param mode Mode, in dem der Folder geoeffnet werden soll.
     */
    private void openFolder(int mode) throws MailboxFolderException {

        // Versuchen den Folder zu oeffnen
        try {
            noOfMailBoxConnectionUsers++;
            logger.info("noOfMailBoxConnectionUsers --> " + noOfMailBoxConnectionUsers);
//            System.out.println("noOfMailBoxConnectionUsers --> " + noOfMailBoxConnectionUsers);
            this.folder.open(mode);
        } catch (MessagingException e) {

            throw (new MailboxFolderException(("Probleme beim Oeffnen des Folders "
                    + this.folder.getName()), e, this.folder.getName()));
        }
    }

    /**
     * Fuellt das private TreeSet mit allen Foldern des Accounts (Connection
     * muss fuer diese Aktion geoeffnet sein).
     */
    private void updateAllFoldersTreeSet() throws MessagingException {
        Folder[] allFoldersArray = this.store.getDefaultFolder().list("*");
        TreeSet<Folder> allFoldersTreeSet = new TreeSet<Folder>(new FolderByFullNameComparator());
        TreeSet<Folder> allFoldersListTreeSet = new TreeSet<Folder>(new FolderByFullNameComparator());

        for (Folder allFoldersArray1 : allFoldersArray) {
            allFoldersTreeSet.add(allFoldersArray1);
            if (shouldBeAddedToFoldersList(allFoldersArray1.getName())) {
                allFoldersListTreeSet.add(allFoldersArray1);
            }
        }

        this.allFolders = allFoldersTreeSet;
        this.allFoldersList = allFoldersListTreeSet;
    }

    private static boolean shouldBeAddedToFoldersList(String name) {
        return (!(name.equalsIgnoreCase("INBOX")
                || name.equalsIgnoreCase("SENT")
                || name.equalsIgnoreCase("DRAFTS")
                || name.equalsIgnoreCase("TRASH")));
    }

    /**
     * Assembles a RetrieveMessagesResultBean.
     *
     * @param messages Messages to set
     * @param overallMessageCount Overall message count to set.
     * @return  <code>RetrieveMessagesResultBean</code>-object.
     */
    private RetrieveMessagesResultBean assembleRetrieveMessagesResult(Message[] messages, int overallMessageCount) {
//        UIDFolder uIDFolder = (UIDFolder) this.folder;
//        Long[] uidList = new Long[messages.length];
//        try {
//            for (int index = 0; index < uidList.length; index++) {
//                uidList[index] = uIDFolder.getUID(messages[index]);
//            }
//        } catch (MessagingException ex) {
//            System.err.println("" + ex);
//        } catch (Exception e) {
//            System.err.println("" + e);
//        }
        RetrieveMessagesResultBean result = new RetrieveMessagesResultBean();
        result.setMessages(messages);
//        result.setUidListOfMessages(uidList);
        result.setOverallMessageCount(overallMessageCount);

        return (result);
    }

    // ----------------------------------------------------- oeffentliche Methoden

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#setLoginData(de.lotk.yawebmail.bean.LoginDataBean)
     */
    public void setLoginData(LoginDataBean loginData) {
        this.loginData = loginData;
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#login()
     */
    public void login() throws ConnectionEstablishException,
            AccessDeniedException, YawebmailCertificateException {

//------------------------------------ creating store (javax.mail.Store) ----------------------------
        if (this.store == null) {
            this.createStore();
        }
//------------------------------------ Perform Login (Connecting to server) ----------------------------
        try {
            if (!this.store.isConnected()) {
                this.store.connect(
                        this.loginData.getMailboxHost(),
                        this.loginData.getMailboxPort(),
                        this.loginData.getMailboxUser(),
                        this.loginData.getMailboxPassword());
            }
            
//            System.out.println(""+this.store.getURLName().getUsername());

            // ggf. Separator setzen
            if (this.separator == null) {
                this.separator = Character.valueOf(this.store.getDefaultFolder().getSeparator());
            }

            // ggf. Liste mit allen Foldern des Accounts fuellen
            if (this.allFolders == null || this.allFoldersList == null) {
                this.updateAllFoldersTreeSet();
            }
        } catch (AuthenticationFailedException afe) {
            throw (new AccessDeniedException("username/password mismatch", afe));
        } catch (MessagingException me) {

            Exception nextException = me.getNextException();

            if ((nextException != null) && (nextException instanceof SSLHandshakeException)) {

                Throwable cause = ((SSLHandshakeException) nextException).getCause();

                if ((cause != null)
                        && (cause instanceof YawebmailCertificateException)) {

                    throw ((YawebmailCertificateException) cause);
                }
            }

            throw (new ConnectionEstablishException("Verbindung gescheitert.", me,
                    this.loginData.getMailboxHost(),
                    this.loginData.getMailboxPort()));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#login(java.lang.String)
     */
    public void login(String folderName) throws ConnectionEstablishException,
            AccessDeniedException, MailboxFolderException,
            YawebmailCertificateException {
        this.login();
        this.changeFolder(folderName);
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#logout()
     */
    private boolean signingOut = false;

    public boolean isSigningOut() {
        return signingOut;
    }

    public void setSigningOut(boolean signingOut) {
        this.signingOut = signingOut;
    }

//    @Override
    public void logout() throws LogoutException {

        // Ggf. Folder schliessen
        if ((this.folder != null) && this.folder.isOpen()) {

            try {
                logger.info("noOfMailBoxConnectionUsers (b4 close) --> " + noOfMailBoxConnectionUsers);
//                System.out.println("noOfMailBoxConnectionUsers (b4 close) --> " + noOfMailBoxConnectionUsers);
                noOfMailBoxConnectionUsers--;
                if (noOfMailBoxConnectionUsers == 0) {
                    logger.info("noOfMailBoxConnectionUsers == 0 --> Folder closed");
                    this.folder.close(true);
                }

            } catch (MessagingException me) {

                throw (new LogoutException("Problem beim Schliessen des Folders.", me));
            }
            if (noOfMailBoxConnectionUsers == 0) {
                this.folder = null;
            }

        }

        // Ggf. Store schliessen
        if (this.store != null && isSigningOut()) {
            try {
                this.store.close();
            } catch (MessagingException me) {
                throw (new LogoutException("Problem beim Schliessen des Stores.", me));
            }

            this.store = null;
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#validateLoginData()
     */
    public void validateLoginData() throws ConnectionEstablishException,
            AccessDeniedException, YawebmailCertificateException {

        this.login();

        // Wenn es ein Problem beim Logout gibt, dann ist das nicht so tragisch...
        try {
            this.logout();
        } catch (LogoutException e) {
            // empty
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getCurrentFolder()
     */
//    @Override
    public Folder getCurrentFolder() throws MailboxFolderException {
        if (this.folder != null) {
            return (this.folder);
        } else {
            throw (new MailboxFolderException("Es existiert z.Zt. kein Folder.", null));
        }
    }

    /**
     * Setzt die Instanzvariable folder auf den gewuenschten Folder
     *
     * @param folderName Name des gewuenschten Folders.
     */
//    @Override
    public void changeFolder(String folderName) throws MailboxFolderException {
        try {

            if (Constants.LEERSTRING.equals(folderName)) {
                this.folder = this.store.getDefaultFolder();
            } else {
                this.folder = this.store.getFolder(folderName);
            }

            if (this.folder == null) {
                throw (new MailboxFolderException(("Invalid folder: " + folderName), null));
            } else {
                this.uidfolder = (UIDFolder) this.folder;
            }
        } catch (MessagingException me) {

            throw (new MailboxFolderException(("Probleme mit Folder: " + folderName),
                    me, folderName));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getMessages()
     */
    public RetrieveMessagesResultBean getMessages(final String par_search_keyword) throws MailboxFolderException, MessageRetrieveException {

        // Open folder
        this.openFolder(Folder.READ_ONLY);

        // Get messages and return them
        try {
            if (par_search_keyword != null && par_search_keyword.length() > 0) {
                // creates a search criterion
                SearchTerm searchCondition = new SearchTerm() {
//                    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm a");

                    @Override
                    public boolean match(Message message) {
                        try {
                            if (message.getSubject().contains(par_search_keyword)) {
                                return true;
                            }

                            for (Address adr : message.getAllRecipients()) {
                                if (adr.toString().contains(par_search_keyword)) {
                                    return true;
                                }
                            }

                            for (Address adr : message.getFrom()) {
                                if (adr.toString().contains(par_search_keyword)) {
                                    return true;
                                }
                            }

//                            if (formatter.format(message.getSentDate()).contains(par_search_keyword)) {
//                                return true;
//                            }
                        } catch (MessagingException ex) {
                            ex.printStackTrace();
                        }
                        return false;
                    }
                };
                Message[] searchResult = this.folder.search(searchCondition);
                return (this.assembleRetrieveMessagesResult(searchResult, searchResult.length));
            } else {
                return (this.assembleRetrieveMessagesResult(this.folder.getMessages(), this.folder.getMessageCount()));
            }

        } catch (MessagingException me) {

            String tempExceptionMessage
                    = ("Could not retrieve messages from folder \""
                    + this.folder.getName() + "\".");
            throw (new MessageRetrieveException(tempExceptionMessage, me));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getMessages(int, int, boolean)
     */
    private static String getText(Part p) throws MessagingException, IOException {
//        boolean textIsHtml = false;
        if (p.isMimeType("text/*")) {
            String s = (String) p.getContent();
//            textIsHtml = p.isMimeType("text/html");
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null) {
                        text = getText(bp);
                    }
                    return text;
//                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    if (s != null) {
                        return s;
                    }
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null) {
                    return s;
                }
            }
        }

        return "";
    }

    public RetrieveMessagesResultBean getMessages(int aStartNumber, int anEndNumber, boolean adjustParameters, final String par_search_keyword) throws MailboxFolderException,
            MessageRetrieveException {

        // Open folder (should happen before getting the message-count)
        this.openFolder(Folder.READ_ONLY);

        try {

            if (par_search_keyword != null && par_search_keyword.length() > 0) {
                // creates a search criterion
                SearchTerm searchCondition = new SearchTerm() {
//                    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm a");

                    @Override
                    public boolean match(Message message) {
                        try {
                            if (message.getSubject().contains(par_search_keyword)) {
                                return true;
                            }

                            for (Address adr : message.getAllRecipients()) {
                                if (adr.toString().contains(par_search_keyword)) {
                                    return true;
                                }
                            }

                            for (Address adr : message.getFrom()) {
                                if (adr.toString().contains(par_search_keyword)) {
                                    return true;
                                }
                            }

//                            if (formatter.format(message.getSentDate()).contains(par_search_keyword)) {
//                                return true;
//                            }

                            if (getText(message).contains(par_search_keyword)) {
                                return true;
                            }
                        } catch (MessagingException ex) {
                            ex.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                };
                Message[] searchResult = this.folder.search(searchCondition);
                return (this.assembleRetrieveMessagesResult(searchResult, searchResult.length));
            } else {
                // The given numbers may not be larger than the amount of messages
                int tempMessageCount = this.folder.getMessageCount();

                if ((aStartNumber > tempMessageCount) || (anEndNumber > tempMessageCount)) {

                    if (adjustParameters) {

                        // In case the start-number is bigger than the amount of messages, we
                        // return an empty Message-Array.
                        if (aStartNumber > tempMessageCount) {

                            return (this.assembleRetrieveMessagesResult((new Message[0]), this.folder.getMessageCount()));
                        }
                        if (anEndNumber > tempMessageCount) {
                            anEndNumber = tempMessageCount;
                        }
                    } else {
                        String tempExceptionMessage = "Given message-numbers exceed the amount of messages";
                        throw (new MessageRetrieveException(tempExceptionMessage));
                    }
                }
                return (this.assembleRetrieveMessagesResult(this.folder.getMessages(aStartNumber, anEndNumber), this.folder.getMessageCount()));
            }

            // Get messages and return them
//            Message[] tempMessages = this.folder.getMessages(aStartNumber, anEndNumber);
//            return (this.assembleRetrieveMessagesResult(tempMessages, this.folder.getMessageCount()));
        } catch (MessagingException me) {
            String tempExceptionMessage = ("Could not retrieve messages from folder \"" + this.folder.getName() + "\".");
            throw (new MessageRetrieveException(tempExceptionMessage, me));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getMessage(int)
     */
    public Message getMessage(long messageUIDNumber) throws MailboxFolderException, MessageRetrieveException {

        this.openFolder(Folder.READ_ONLY);
        Message message = null;

        try {
            message = this.uidfolder.getMessageByUID(messageUIDNumber);
        } catch (MessagingException me) {
            System.err.println("" + me);
        }

        return (message);
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getEnvelopes()
     */
//    @Override
    public RetrieveMessagesResultBean getEnvelopes(String par_search_keyword) throws MailboxFolderException, MessageRetrieveException {
        try {
            // Wenn der aktuelle Folder der Default-Folder ist, leeres Array zurueck
            if (this.folder.getParent() == null) {
                return (this.assembleRetrieveMessagesResult(new Message[0], 0));
            }
//-------------------------- Retreiving Mails from Server -----------------------------------
            RetrieveMessagesResultBean rmr = this.getMessages(par_search_keyword);
            Message[] messages = rmr.getMessages();

            if (messages.length >= 1) {
                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.ENVELOPE);
                this.folder.fetch(messages, fp);
            }

            // Messages zurueckliefern
            return (rmr);
        } catch (MessagingException me) {

            throw (new MessageRetrieveException(("Konnte Envelopes aus Folder \"" + this.folder.getName() + "\" nicht beziehen."), me));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getEnvelopes(int, int, boolean)
     */
    public RetrieveMessagesResultBean getEnvelopes(int aStartIndex, int anEndIndex, boolean adjustParameters, String par_search_keyword) throws MailboxFolderException,
            MessageRetrieveException {

        try {
            // Wenn der aktuelle Folder der Default-Folder ist, leeres Array zurueck
            if (this.folder.getParent() == null) {
                return (this.assembleRetrieveMessagesResult((new Message[0]), 0));
            }

            // Messages beziehen
            RetrieveMessagesResultBean rmr = this.getMessages(aStartIndex, anEndIndex, adjustParameters, par_search_keyword);
            Message[] messages = rmr.getMessages();

            // load envelopes (only in case there is at lease one message)
            if (messages.length >= 1) {
                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.ENVELOPE);
                this.folder.fetch(messages, fp);
            }

            // Messages zurueckliefern
            return (rmr);
        } catch (MessagingException me) {

            throw (new MessageRetrieveException(("Konnte Envelopes aus Folder \""
                    + this.folder.getName() + "\" nicht beziehen."), me));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#setDeletedFlag(int)
     */
    public void setDeletedFlag(long messageNumber) throws MailboxFolderException,
            MessageDeletionException {

        // TODO schoener machen!
        long[] uebergabe = new long[1];
        uebergabe[0] = messageNumber;

        this.setMultipleDeletedFlags(uebergabe);
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#setMultipleDeletedFlags(int[])
     */
    public void setMultipleDeletedFlags(long[] messageNumbers) throws MailboxFolderException, MessageDeletionException {
        try {
            this.openFolder(Folder.READ_WRITE);
            UIDFolder uIDFolder = (UIDFolder) this.folder;

            Message[] msg = uIDFolder.getMessagesByUID(messageNumbers);
            ArrayList<Message> msgArray = new ArrayList<Message>();
            if (msg != null) {
                for (Message m : msg) {
                    if (m != null) {
                        msgArray.add(m);
//                        System.out.println("sub :: " + m.getSubject());
                    } else {
                        System.err.println("m is null, Message doesn't exist");
                    }
                }
                int i = 0;
                Message[] msgListToSetFlag = new Message[msgArray.size()];
                for (Message objMsg : msgArray) {
                    msgListToSetFlag[i] = msgArray.get(i);
                    i++;
                }
                Flags flags = new Flags();
                flags.add(Flags.Flag.DELETED);
                this.folder.setFlags(msgListToSetFlag, flags, true);
            } else {
                System.out.println("Message doesn't exist");
            }
        } catch (MessagingException me) {
            System.out.println("Errorrrrrrrrrrrrrrrrrr");
        }
//        }
    }

    public void setMultipleDeletedFlags(Message[] paramMsg) throws MailboxFolderException, MessageDeletionException {

        try {
            this.openFolder(Folder.READ_WRITE);
            Flags flags = new Flags();
            flags.add(Flags.Flag.DELETED);
            for (Message m : paramMsg) {
                m.setFlag(Flags.Flag.DELETED, true);
            }
        } catch (Exception me) {
            System.out.println("Errorrrrrrrrrrrrrrrrrr -->" + me);
        }
//        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getFolderSeparator()
     */
    public char getFolderSeparator() throws Exception {

        // Haben wir einen Folder am Start?
        if (this.separator != null) {

            return (this.separator.charValue());
        } // Sonst koennen wir nicht helfen.
        else {

            throw (new Exception("FolderSeparator nicht beziehbar."));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#getAllFolders()
     */
    public TreeSet getAllFolders() throws Exception {
        if (this.allFolders != null) {
            return (this.allFolders);
        } // Sonst koennen wir nicht helfen.
        else {
            throw (new Exception("Ordnerliste nicht beziehbar."));
        }
    }

    public TreeSet getAllFoldersList() {
        return allFoldersList;
    }

    public void setAllFoldersList(TreeSet allFoldersList) {
        this.allFoldersList = allFoldersList;
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#createFolder(java.lang.String)
     */
    public void createFolder(String folderName) throws MailboxFolderException {
        try {
            Folder newFolder = this.store.getFolder(folderName);
            if (!newFolder.exists()) {
                if (!newFolder.create(Folder.HOLDS_MESSAGES)) {
                    throw (new Exception("Folder creation failed"));
                }
                // Liste aller Ordner aktualisieren
                this.updateAllFoldersTreeSet();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw (new MailboxFolderException(e.getMessage(), e, folderName));
        }
    }

    public void deleteFolder(String folderName) throws MailboxFolderException {

        try {

            Folder folderToDelete = this.store.getFolder(folderName);

            if (folderToDelete.exists()) {

                if (!folderToDelete.delete(true)) {

                    throw (new Exception("Folder konnte nicht geloescht werden."));
                }

                // Liste aller Ordner aktualisieren
                this.updateAllFoldersTreeSet();
            }
        } catch (Exception e) {

            e.printStackTrace();
            throw (new MailboxFolderException(e.getMessage(), e, folderName));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.business.MailboxConnection#moveMessages(int[], java.lang.String)
     */
    public void moveMessages(long[] messageNumbers, String targetFolderName) throws MailboxFolderException, MessageMovementException {
        try {
            this.openFolder(Folder.READ_WRITE);
//            UIDFolder uIDFolder = (UIDFolder) this.folder;

            Message[] messages = this.uidfolder.getMessagesByUID(messageNumbers);

            Folder targetFolderObj = this.store.getFolder(targetFolderName);
            this.folder.copyMessages(messages, targetFolderObj);

            Flags flags = new Flags();
            flags.add(Flags.Flag.DELETED);
            this.folder.setFlags(messages, flags, true);
        } catch (MessagingException e) {
            throw (new MessageMovementException("Problem beim Verschieben.", e));
        }
    }

    public void saveMessagesAsDraft(Message[] paramMessage) throws MailboxFolderException, MessageMovementException {
        try {
            if (!this.folder.isOpen()) {
                this.openFolder(Folder.READ_WRITE);
            }

            this.folder.appendMessages(paramMessage);
            this.folder.close(true);
        } catch (MessagingException e) {
            throw (new MessageMovementException("Problem beim Verschieben.", e));
        }
    }

    /* (non-Javadoc)
     * @see de.lotk.yawebmail.Lifecycle#destroy()
     */
    public void destroy() {
        try {
            this.logout();
        } catch (LogoutException e) {
            // empty
        }

        this.loginData = null;
    }

//    @Override
    public LoginDataBean getLoginData() {
        return loginData;
    }

//    @Override
    public boolean checkStore() {
        boolean return_val = true;
        if (this.store == null) {
            return_val = false;
        }
        return return_val;
    }

    public int setSeenFlag(long myMsgID, boolean param_seen_flag) {
        int return_val = 0;
        try {
            if (!this.folder.isOpen()) {
                this.openFolder(Folder.READ_WRITE);
            }
            this.uidfolder.getMessageByUID(myMsgID).setFlag(Flags.Flag.SEEN, param_seen_flag);
        } catch (MessagingException me) {
            return_val = -1;
            System.out.println("me --> " + me.getMessage());
        } catch (NullPointerException ne) {
            return_val = -1;
            System.out.println("NULL_POINTER_EXCEPTION --> " + ne);
        } catch (Exception e) {
            return_val = -1;
            System.out.println("e --> " + e);
        }
        return return_val;
    }

    public int setSeenFlag(long[] myMsgID, boolean param_seen_flag) {
        int return_val = 0;
        try {
            if (!this.folder.isOpen()) {
                this.openFolder(Folder.READ_WRITE);
            }

            Flags flag = new Flags();
            flag.add(Flags.Flag.SEEN);

            Message[] msg = this.uidfolder.getMessagesByUID(myMsgID);
            ArrayList<Message> msgArrayList = new ArrayList<Message>();
            for (Message m : msg) {
                msgArrayList.add(m);
            }
            Message[] msgArray = new Message[msgArrayList.size()];
            int i = 0;
            for (Message m : msgArrayList) {
                msgArray[i] = msgArrayList.get(i);
            }

            this.folder.setFlags(msgArray, flag, param_seen_flag);

        } catch (MessagingException me) {
            return_val = -1;
            System.out.println("me --> " + me.getMessage());
        } catch (NullPointerException ne) {
            return_val = -1;
            System.out.println("NULL_POINTER_EXCEPTION --> " + ne);
        } catch (Exception e) {
            return_val = -1;
            System.out.println("e --> " + e);
        }
        return return_val;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DefaultMailboxConnection defaultMailboxConnection = new DefaultMailboxConnection(this.loginData, this.trustManager, this.separator);
        return defaultMailboxConnection; //To change body of generated methods, choose Tools | Templates.
    }

    public void setAllFolders(TreeSet allFolders) {
        TreeSet<Folder> allFoldersListTreeSet = new TreeSet<Folder>(new FolderByFullNameComparator());
        for (Object allFoldersArray1 : allFolders) {
            if (shouldBeAddedToFoldersList(((Folder) allFoldersArray1).getName())) {
                allFoldersListTreeSet.add((Folder) allFoldersArray1);
            }
        }
        setAllFoldersList(allFoldersListTreeSet);
        this.allFolders = allFolders;
    }

    public int getTotalNumberOfMessagesOfCurrentFolder() {
        try {
            if (this.folder != null && !this.folder.isOpen()) {
                this.openFolder(Folder.READ_ONLY);
            }
            int totalMessageCount = this.folder.getMessageCount();
            this.folder.close(true);
            return totalMessageCount;

        } catch (MessagingException ex) {
            System.out.println("Error while counting overAll message numbers");
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isRefreshNeeded(boolean isAutoRefresh, int msgCount) {
        try {
            if (!isAutoRefresh) {
                return true;
            }
            if (this.folder != null && !this.folder.isOpen()) {
                this.openFolder(Folder.READ_ONLY);
            }
            if (this.folder.hasNewMessages() || this.folder.getMessageCount() != msgCount) {
                return true;
            }
            this.folder.close(true);

        } catch (MessagingException ex) {
            System.out.println("Error while counting overAll message numbers in --> isRefreshNeeded(int msgCount)");
        } catch (Exception e) {
        }

        return false;
    }
}
