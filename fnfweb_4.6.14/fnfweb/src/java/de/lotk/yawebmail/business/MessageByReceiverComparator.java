/*
 * @(#)MessageBySenderComparator.java 1.00 2005/06/06
 *
 * Copyright (c) 2005, Stephan Sann
 *
 * 06.06.2005 ssann        Vers. 1.0     created
 */


package de.lotk.yawebmail.business;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MessageByReceiverComparator<T extends Message> extends
        ReversibleComparator<T> {
    
  private static final long serialVersionUID = 5556264714168821793L;
  
  protected static final Log LOG = LogFactory.getLog(MessageByReceiverComparator.class);
  
  public MessageByReceiverComparator() {
  }
  
  public MessageByReceiverComparator(boolean reverse) {
    this.reverse = reverse;
  }
  
  public int compare(T m1, T m2) {

    int rueck = 0;

    try {

      Address[] receiverEins = m1.getAllRecipients();
      Address[] receiverZwei = m2.getAllRecipients();
      
      boolean einsDa = ((receiverEins != null) && (receiverEins.length >= 1));
      boolean zweiDa = ((receiverZwei != null) && (receiverZwei.length >= 1));
      
      if(einsDa && zweiDa) {
        String einsCleaned = receiverEins[0].toString().replaceAll("\"", "");
        String zweiCleaned = receiverZwei[0].toString().replaceAll("\"", "");
        
        rueck = einsCleaned.compareToIgnoreCase(zweiCleaned);
      }
      
      else if((! einsDa) && zweiDa) {
        rueck = (-50);
      }
      else if(einsDa && (! zweiDa)) {
        rueck = 50;
      }
      else {
        rueck = 0;
      }
      
      if(this.reverse) {
        rueck = rueck * (-1);
      }

      return(rueck);
    }
    catch(MessagingException me) {
      LOG.error("[compare] Problem getting the Receiver.", me);
      return(0);
    }
  }

}
