/*
 * @(#)OverviewOfflineMimeMessage.java 1.00 2006/05/11
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 11.05.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.business;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import de.lotk.yawebmail.application.Constants;

public class OverviewOfflineMimeMessage extends OfflineMimeMessage {

  // --------------------------------------------------------- Instanz-Variablen
    /**
     * Subject-Tooltip
     */
    private String subjectTooltip = Constants.LEERSTRING;

    private boolean containsAttachment = false;

    public OverviewOfflineMimeMessage(MimeMessage mimeMessage) throws MessagingException {
        super(mimeMessage);
    }

    public OverviewOfflineMimeMessage(Session session) throws MessagingException {
        super(session);
    }

    public String getSubjectTooltip() {
        return subjectTooltip;
    }

    public void setSubjectTooltip(String subjectTooltip) {
        this.subjectTooltip = subjectTooltip;
    }

    public boolean isContainsAttachment() {
        return containsAttachment;
    }

    public void setContainsAttachment(boolean containsAttachment) {
        this.containsAttachment = containsAttachment;
    }

}
