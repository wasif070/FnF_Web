/*
 * @(#)DisplayMessage.java 1.00 2006/03/03
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 03.03.2006 ssann                  Vers. 1.0     created
 */
package de.lotk.yawebmail.bean;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.mail.Address;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;

import de.lotk.yawebmail.application.Lifecycle;
import de.lotk.yawebmail.business.OverviewOfflineMimeMessage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;

public class DisplayMessageBean implements Lifecycle, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5647805002355682456L;

    /**
     * Originaere Message
     */
    private Message originMessage = null;

//    private Message messageObjectFromServer = null;
    /**
     * Anzuzeigende Parts
     */
    private List displayParts = null;

    /**
     * Inline-Parts
     */
    private Map inlineParts = null;

    /**
     * Map mit den Mutlipart-Objekten (und deren Child-Parts)
     */
    private Map multiparts = null;
    /**
     * Ist diese Message selektiert?
     */
//    private boolean selected = true;
    private boolean selected = false;

    private boolean seen = true;

    private boolean containsAttachment = false;

    private String cssClassName;

    private long uID;

    public long getuID() {
        return uID;
    }

    public void setuID(long uID) {
        this.uID = uID;
    }

    /**
     * @return Returns the originMessage.
     */
    public Message getOriginMessage() {
        return originMessage;
    }

    /**
     * @param originMessage The originMessage to set.
     */
    public void setOriginMessage(Message originMessage) {
        this.originMessage = originMessage;
    }

//    public Message getMessageObjectFromServer() {
//        return messageObjectFromServer;
//    }
//
//    public void setMessageObjectFromServer(Message messageObjectFromServer) {
//        this.messageObjectFromServer = messageObjectFromServer;
//    }
    /**
     * @return Returns the displayParts.
     */
    public List getDisplayParts() {
        return displayParts;
    }

    /**
     * @param displayParts The displayParts to set.
     */
    public void setDisplayParts(List displayParts) {
        this.displayParts = displayParts;
    }

    /**
     * @return Returns the inlineParts.
     */
    public Map getInlineParts() {
        return inlineParts;
    }

    /**
     * @param inlineParts The inlineParts to set.
     */
    public void setInlineParts(Map inlineParts) {
        this.inlineParts = inlineParts;
    }

    /**
     * @return Returns the multiparts.
     */
    public Map getMultiparts() {

        return this.multiparts;
    }

    /**
     * @param multiparts The multiparts to set.
     */
    public void setMultiparts(Map multiparts) {
        this.multiparts = multiparts;
    }

    /**
     * @return Returns the selected.
     */
    public boolean isSelected() {

        return (this.selected);
    }

    /**
     * @param selected The selected to set.
     */
    public void setSelected(boolean selected) {

        this.selected = selected;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public boolean getContainsAttachment() {
        try {
            OverviewOfflineMimeMessage oomm = (OverviewOfflineMimeMessage) this.getOriginMessage();
            return oomm.isContainsAttachment();
        } catch (Exception ex) {
        }
        return false;
    }

    public void setContainsAttachment(boolean containsAttachment) {
        this.containsAttachment = containsAttachment;
    }

    public String getCssClassName() {
        cssClassName = isSeen() ? "messageLink" : "messageLinkBold";
        return cssClassName;
    }

    public void setCssClassName(String cssClassName) {
        this.cssClassName = cssClassName;
    }

    public Address[] getToRecipients() {
        try {
            return (this.originMessage.getRecipients(Message.RecipientType.TO));
        } catch (Exception e) {
            e.printStackTrace();
            return (null);
        }
    }

    /**
     * Liefert die CC-Recipients
     */
    public Address[] getCcRecipients() {

        try {

            return (this.originMessage.getRecipients(Message.RecipientType.CC));
        } catch (MessagingException e) {

            e.printStackTrace();
            return (null);
        }
    }

    /**
     * Liefert die BCC-Recipients
     */
    public Address[] getBccRecipients() {

        try {

            return (this.originMessage.getRecipients(Message.RecipientType.BCC));
        } catch (MessagingException e) {

            e.printStackTrace();
            return (null);
        }
    }

    /**
     * Liefert den Message-Quelltext
     */
    public String getMessageSource() {

        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.originMessage.writeTo(baos);

            return (baos.toString());
        } catch (Exception e) {

            e.printStackTrace();
            return (e.getMessage());
        }
    }

    /**
     * Liefert alle Header-Lines als Liste.
     */
    public List getAllHeaderLinesAsList() {

        try {

            List<String> allHeaderLines = new ArrayList<String>();
            Enumeration headerEnum = this.originMessage.getAllHeaders();

            while (headerEnum.hasMoreElements()) {

                Header currentHeader = (Header) headerEnum.nextElement();
                String headerLine
                        = currentHeader.getName() + ": " + currentHeader.getValue();

                allHeaderLines.add(headerLine);
            }

            return (allHeaderLines);
        } catch (MessagingException e) {

            throw (new RuntimeException("Konnte Header-Lines nicht beziehen.", e));
        }
    }

    public String senderList() {
        String return_val = "";
        StringBuilder sb = new StringBuilder();
        try {
            Address[] actFrom = this.originMessage.getFrom();
//            for (int index = 0; index < actFrom.length; index++) {
            return_val = InternetAddress.toString(actFrom);
//                actFrom[index]. != null && actFrom[index].
//            }

        } catch (MessagingException ex) {
            Logger.getLogger(DisplayMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
        }
        return return_val;
    }

    public String receipentList() {
        String return_val = "";
        try {
            Address[] actTo = this.originMessage.getAllRecipients();
            return_val = InternetAddress.toString(actTo);
        } catch (MessagingException ex) {
            Logger.getLogger(DisplayMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
        }
        return return_val;
    }

    /**
     * Setzt die Bean zurueck
     */
    public void reset() {

        this.originMessage = null;
        this.displayParts = null;
        this.inlineParts = null;
        this.multiparts = null;
        this.selected = false;
    }

    /* (non-Javadoc)
     * @see de.lotk.webftp.Lifecycle#destroy()
     */
    public void destroy() {
        this.reset();
    }

}
