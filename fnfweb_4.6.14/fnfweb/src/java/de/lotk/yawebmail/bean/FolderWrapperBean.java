/*
 * @(#)FolderWrapperBean.java 1.00 2006/02/28
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 28.02.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.pop3.POP3Folder;

import de.lotk.yawebmail.application.Lifecycle;
import javax.mail.Flags;
import javax.mail.UIDFolder;

/**
 * Bean die einen Folder wrappt und zu diesem verschiedene Hilfmethoden zur
 * Verfuegung stellt.
 * <br/><br/>
 *
 * Das per "setMessages" uebergebene Array wird in ein Array von DisplayMessages
 * konvertiert, so dass zusaetzliche Infos zu den Mails gemerkt werden koennen.
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class FolderWrapperBean implements Lifecycle, Serializable {

    private static final long serialVersionUID = -3400739251118573376L;

    private Folder folder = null;

    /**
     * Hier merken wir uns die Subfolder des aktuellen javax-mail-folders
     */
    private Folder[] subfolders = null;

    /**
     * Hier merken wir uns den Type des aktuellen javax-mail-folders
     */
    private int folderType = (-1);

    /**
     * Hier merken wir uns das Message-Array als Display-Messages
     */
    private DisplayMessageBean[] displayMessages = null;

    private Message[] messages = null;

    private Map<Message, Long> uidMap = null;

    /**
     * Holds the (overall) amount of messages within the message-folder
     */
    private int overallMessageCount = 0;

    private boolean selectAllCheckBoxClicked = false;


    private String counter = "0";

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public boolean isHasUnreadMessages() {
        return Integer.parseInt(getCounter()) > 0;
    }

    public boolean isSelectAllCheckBoxClicked() {
        return selectAllCheckBoxClicked;
    }

    public void setSelectAllCheckBoxClicked(boolean selectAllCheckBoxClicked) {
        this.selectAllCheckBoxClicked = selectAllCheckBoxClicked;
    }

    public Folder getFolder() {
        return folder;
    }

    public boolean isInboxFolder() {
        return folder.getFullName().equalsIgnoreCase("INBOX");
    }

    /**
     * @return the subfolders
     */
    public Folder[] getSubfolders() {
        return subfolders;
    }

    /**
     * @return the folderType
     */
    public int getFolderType() {

        return folderType;
    }

    /**
     * @return Returns the messages.
     */
    public DisplayMessageBean[] getDisplayMessages() {
        return (this.displayMessages);
    }

    /**
     * Gibt das originale Message-Array unserer Display-Messages zurueck.
     *
     * @param messages The messages to set.
     */
    public Message[] getMessages() {
        return (this.messages);
    }

    /**
     * @return the overallMessageCount
     */
    public int getOverallMessageCount() {
        return overallMessageCount;
    }

    /**
     * @param overallMessageCount the overallMessageCount to set
     */
    public void setOverallMessageCount(int overallMessageCount) {

        this.overallMessageCount = overallMessageCount;
    }

    public void setFolder(Folder folder) {

        try {

            this.folder = folder;
            this.folderType = folder.getType();

            if (folder instanceof POP3Folder) {
                this.subfolders = new Folder[0];
            } // Sonst Unterordner aus dem Folder lesen und speichern.
            else {
                this.subfolders = folder.list();
            }
        } catch (Exception e) {

            throw (new RuntimeException("Probleme beim Setzen des Folders.", e));
        }
    }

    public void setMessages(Message[] messages, Message[] messagesObjFrmServer) {
        UIDFolder uIDFolder = (UIDFolder) this.folder;

        if (uidMap == null || (messagesObjFrmServer != null && messagesObjFrmServer.length > 0)) {
            uidMap = new HashMap<Message, Long>();
        }

        this.messages = messages;

        this.displayMessages = new DisplayMessageBean[messages.length];

        for (int ii = 0; ii < messages.length; ii++) {
            this.displayMessages[ii] = new DisplayMessageBean();
            try {
                this.displayMessages[ii].setOriginMessage(messages[ii]);
            } catch (Exception e) {
            }
            try {
                if (messagesObjFrmServer != null) {
                    this.displayMessages[ii].setuID(uIDFolder.getUID(messagesObjFrmServer[ii]));
                    uidMap.put(messages[ii], this.displayMessages[ii].getuID());
                } else {
                    this.displayMessages[ii].setuID(uidMap.get(messages[ii]));
                }
                if (!messages[ii].isSet(Flags.Flag.SEEN)) {
                    this.displayMessages[ii].setSeen(false);
                }
            } catch (MessagingException ex) {
                System.out.println("ex --> " + ex);
            } catch (Exception e) {
                System.err.println("e --> " + e);
            }
        }
    }

    public boolean isSwappable() {
        return (this.folder instanceof IMAPFolder);
    }

    public boolean isSubfolderPossible() {
        return (this.folderType != Folder.HOLDS_MESSAGES);
    }

    public Map getSubfolderMap() throws MessagingException {
        Folder[] subfolders = this.getSubfolders();
        HashMap<String, String> subfolderMap = new HashMap<String, String>();

        for (int ii = 0; ii < subfolders.length; ii++) {
            subfolderMap.put(subfolders[ii].getName(), subfolders[ii].getFullName());
        }
        return (subfolderMap);
    }

    public long[] getUidNumbersOfSelectedDisplayMessages() {

        long[] workArray = new long[this.displayMessages.length];
        int amountSelectedMessages = 0;
        for (int ii = 0; ii < this.displayMessages.length; ii++) {
            if (this.displayMessages[ii].isSelected()) {
                workArray[amountSelectedMessages++] = this.displayMessages[ii].getuID();
            }
        }

        long[] messageNumbersOfSelectedDisplayMessages = new long[amountSelectedMessages];
        System.arraycopy(workArray, 0, messageNumbersOfSelectedDisplayMessages, 0, amountSelectedMessages);
        return (messageNumbersOfSelectedDisplayMessages);
    }

    public int[] getMsgNumbers() {
        int[] workArray = new int[this.displayMessages.length];
        int amountSelectedMessages = 0;
        for (DisplayMessageBean displayMessage : this.displayMessages) {
            if (displayMessage.isSelected()) {
                workArray[amountSelectedMessages++] = displayMessage.getOriginMessage().getMessageNumber();
            }
        }

        int[] messageNumbersOfSelectedDisplayMessages = new int[amountSelectedMessages];
        System.arraycopy(workArray, 0, messageNumbersOfSelectedDisplayMessages, 0, amountSelectedMessages);
        return (messageNumbersOfSelectedDisplayMessages);
    }

    public int getMessageCount() {
        return ((this.messages != null) ? this.messages.length : 0);
    }

    /**
     * Setzt die Bean zurueck
     */
    public void reset() {
        this.folder = null;
        this.subfolders = null;
        this.displayMessages = null;
        this.messages = null;
        this.overallMessageCount = 0;
    }

    /* (non-Javadoc)
     * @see de.lotk.webftp.Lifecycle#destroy()
     */
    public void destroy() {
        this.reset();
    }

    public boolean getIsDraft() {
        return this.folder.getName().equals("DRAFTS");
    }

    public String getCurrentFolderName() {
        return this.folder.getName();
    }
}
