/*
 * @(#)MailsListingController.java 1.00 2006/03/03
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 03.03.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.controller;

import java.util.Map;

import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.mail.Folder;
import javax.mail.Message;

import de.lotk.yawebmail.application.Constants;
import de.lotk.yawebmail.bean.DisplayMessageBean;
import de.lotk.yawebmail.bean.FolderManagementBean;
import de.lotk.yawebmail.bean.FolderWrapperBean;
import de.lotk.yawebmail.bean.MailBasisBean;
import de.lotk.yawebmail.bean.MailsListingBean;
import de.lotk.yawebmail.bean.SessionContainerBean;
import de.lotk.yawebmail.business.DefaultMailboxConnection;
import de.lotk.yawebmail.business.MessageByDateComparator;
import de.lotk.yawebmail.business.MessageByReceiverComparator;
import de.lotk.yawebmail.business.MessageBySenderComparator;
import de.lotk.yawebmail.business.MessageBySpamlevelComparator;
import de.lotk.yawebmail.business.MessageBySubjectComparator;
import de.lotk.yawebmail.business.ReversibleComparator;
import de.lotk.yawebmail.exceptions.AccessDeniedException;
import de.lotk.yawebmail.exceptions.ConnectionEstablishException;
import de.lotk.yawebmail.exceptions.LogoutException;
import de.lotk.yawebmail.exceptions.MailboxFolderException;
import de.lotk.yawebmail.exceptions.MessageDeletionException;
import de.lotk.yawebmail.exceptions.MessageMovementException;
import de.lotk.yawebmail.exceptions.YawebmailCertificateException;
import de.lotk.yawebmail.util.DisplayMessageAssembler;
import de.lotk.yawebmail.util.OfflineMessageAssembler;
import de.lotk.yawebmail.util.ReactionMailBasisAssembler;
import de.lotk.yawebmail.util.faces.ExceptionConverter;
import java.util.ArrayList;
import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIOutput;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.mail.UIDFolder;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 * Controller fuer das Mails-Listing
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class MailsListingController extends BaseMailboxActionController {

    /**
     * Used to sort messages by sender
     */
    public static final MessageByDateComparator<Message> MESSAGE_BY_DATE_COMPARATOR = new MessageByDateComparator<Message>();

    /**
     * Used to sort messages by sender
     */
    public static final MessageBySenderComparator<Message> MESSAGE_BY_SENDER_COMPARATOR = new MessageBySenderComparator<Message>();

    /**
     * Used to sort messages by sender
     */
    public static final MessageByReceiverComparator<Message> MESSAGE_BY_RECEIVER_COMPARATOR = new MessageByReceiverComparator<Message>();

    /**
     * Used to sort messages by spam-level
     */
    public static final MessageBySpamlevelComparator<Message> MESSAGE_BY_SPAMLEVEL_COMPARATOR = new MessageBySpamlevelComparator<Message>();

    /**
     * Used to sort messages by sender
     */
    public static final MessageBySubjectComparator<Message> MESSAGE_BY_SUBJECT_COMPARATOR = new MessageBySubjectComparator<Message>();

    /**
     * Handle auf die Folder-Tabelle
     */
    private UIData folderTable = null;

    private UIData folderTableWithLessOptions = null;

    /**
     * Handle auf die Nachrichten-Tabelle
     */
    private UIData messageTable = null;

    private UIData selectOperationOnMessageTable = null;

    private UIData moreOperationOnMessageTable = null;

    /**
     * @return the folderTable
     */
    public UIData getFolderTable() {
        this.folderTable = null;
        return folderTable;
    }

    /**
     * @param folderTable the folderTable to set
     */
    public void setFolderTable(UIData folderTable) {
        this.folderTable = folderTable;
    }

    public UIData getFolderTableWithLessOptions() {
        return folderTableWithLessOptions;
    }

    public void setFolderTableWithLessOptions(UIData folderTableWithLessOptions) {
        this.folderTableWithLessOptions = folderTableWithLessOptions;
    }

    /**
     * @return the messageTable
     */
    public UIData getMessageTable() {
        return messageTable;
    }

    /**
     * @param messageTable the messageTable to set
     */
    public void setMessageTable(UIData messageTable) {

        this.messageTable = messageTable;
    }

    public UIData getSelectOperationOnMessageTable() {
        return selectOperationOnMessageTable;
    }

    public void setSelectOperationOnMessageTable(UIData selectOperationOnMessageTable) {
        this.selectOperationOnMessageTable = selectOperationOnMessageTable;
    }

    public UIData getMoreOperationOnMessageTable() {
        return moreOperationOnMessageTable;
    }

    public void setMoreOperationOnMessageTable(UIData moreOperationOnMessageTable) {
        this.moreOperationOnMessageTable = moreOperationOnMessageTable;
    }

    /**
     * Aendert die Sortierreihenfolge auf Sortierung nach uebergebenen
     * Comparator- Typ. Wenn bereits nach uebergebenen Comparator-Typ sortiert
     * wurde, wird die bisherige Sortierung invertiert.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    private String sortByX(ReversibleComparator<Message> aComparator) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {

            // SessionContainerBean und aktuellen SortierComparator holen
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            ReversibleComparator<Message> currentComparator
                    = sessionContainer.getSortierComparator();

            // Wird bereits nach uebergebenem Comparator-Typ sortiert?
            if (aComparator.getClass() == currentComparator.getClass()) {

                // Sortierreihenfolge umdrehen
                currentComparator.setReverse(!currentComparator.isReverse());
            } // Sonst neuen Comparator gemaess Parameter in den SessionContainer setzen
            else {
                sessionContainer.setSortierComparator(aComparator);
            }

            // Neue Sortierung anmelden
            sessionContainer.setRenewSortorder(true);
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    /**
     * Liefert die aktuelle ComparatorKlasse, wenn vom Typ X
     *
     * @param sortierComparatorClass Klasse des Sortier-Comparators
     * @return Aktuelles <code>ReversibleComparator</code>-Objekt, wenn vom
     * uebergebenen Typ; sonst <code>null</code>.
     */
    private ReversibleComparator getSortByComparatorIfX(Class sortierComparatorClass) {

        try {

            // SessionContainerBean und aktuellen SortierComparator holen
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            ReversibleComparator sortierComparatorInstance
                    = sessionContainer.getSortierComparator();

            // Wird nach uebergebenem Comparator-Typ sortiert?
            if (sortierComparatorClass.isInstance(sortierComparatorInstance)) {

                return (sortierComparatorInstance);
            } // Sonst ist die Antwort NULL 
            else {

                return (null);
            }
        } catch (Exception e) {

            e.printStackTrace();
            throw (new RuntimeException(e.getMessage(), e));
        }
    }

    /**
     * Wird nach X aufsteigend sortiert?
     *
     * @param sortierComparatorClass Klasse des Sortier-Comparators
     * @return  <code>true</code>, wenn aufsteigend sortiert wird;
     * <code>false</code>, wenn absteigend sortiert wird.
     */
    private boolean isSortUpByX(Class sortierComparatorClass) {

        ReversibleComparator currentComparator
                = this.getSortByComparatorIfX(sortierComparatorClass);

        // Wenn wir nicht NULL zuruck bekommen haben, wird aktuell nach X sortiert.
        if (currentComparator != null) {

            // Nachsehen, ob aktueller Comparator aufsteigend sortiert wird.
            return (!currentComparator.isReverse());
        }

        return false;
    }

    private boolean isSortDownByX(Class sortierComparatorClass) {

        ReversibleComparator currentComparator = this.getSortByComparatorIfX(sortierComparatorClass);

        if (currentComparator != null) {

            return (currentComparator.isReverse());
        }

        return false;
    }

    private String changeFolderToX(String folderPath) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (folderPath != null) {

            try {
                SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
                sessionContainer.setCurrentOffset(0);
                sessionContainer.setCurrentMailboxFolder(folderPath);
                sessionContainer.setRenewEnvelopes(true);
                return (Constants.OUTCOME_MAILS_LISTING);

            } catch (Exception e) {
                facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
                return (Constants.OUTCOME_TECH_ERROR);
            }
        } else {

            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, new Exception("Parameter \"folderPath\" nicht uebergeben."), true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    /**
     * Checks the current offset after an operation that deletes messages from
     * the current folder. If the new amount of messages in the current folder
     * is equal or less the current offset, the current offset has to be
     * decreased.
     *
     * @param folderWrapper Current FolderWrapperBean
     * @param sessionContainer Current SessionContainerBean
     * @param selectedMessages Messages to be removed.
     */
    private void assureCorrectOffset(FolderWrapperBean folderWrapper, SessionContainerBean sessionContainer, long[] selectedMessages) {

        if ((sessionContainer.getCurrentOffset() > 0)
                && ((folderWrapper.getOverallMessageCount() - selectedMessages.length) <= sessionContainer.getCurrentOffset())) {
            sessionContainer.setCurrentOffset(sessionContainer.getCurrentOffset() - sessionContainer.getAmountOfMessagesPerPage());
        }
    }

    public String deleteMailsTemporarilyOrPermanently() {
        try {
            DefaultMailboxConnection mailboxConnection = this.getMailboxConnection();
            String currentMailboxFolder = this.getCurrentMailboxFolder();
            if (!currentMailboxFolder.equals("TRASH")) {
                return moveSelectedMailsToTrash(mailboxConnection, currentMailboxFolder);
            } else {
                return deleteSelectedMails(mailboxConnection, currentMailboxFolder);
            }
        } catch (Exception ex) {
            return "";
        }
    }

    private String deleteSelectedMails(DefaultMailboxConnection mailboxConnection, String currentMailboxFolder) {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            long[] messageNumbersOfSelectedDisplayMessages = folderWrapper.getUidNumbersOfSelectedDisplayMessages();
            if (messageNumbersOfSelectedDisplayMessages.length >= 1) {

                try {
                    mailboxConnection.login(currentMailboxFolder);
                    mailboxConnection.setMultipleDeletedFlags(messageNumbersOfSelectedDisplayMessages);

                    SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
                    sessionContainer.setRenewEnvelopes(true);

                    assureCorrectOffset(folderWrapper, sessionContainer, messageNumbersOfSelectedDisplayMessages);
                } catch (ConnectionEstablishException cee) {
                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXHOST, ExceptionConverter.getFacesMessage(facesContext, cee, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (AccessDeniedException ade) {
                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXPASSWORD, ExceptionConverter.getFacesMessage(facesContext, ade, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (MailboxFolderException mfe) {
                    facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, mfe, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } catch (MessageDeletionException mde) {
                    facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, mde, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } finally {
                    try {
                        mailboxConnection.logout();
                    } catch (LogoutException le) {
                        facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, le, true));
                        return (Constants.OUTCOME_TECH_ERROR);
                    }
                }
            }
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    private String moveSelectedMailsToTrash(DefaultMailboxConnection mailboxConnection, String currentMailboxFolder) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            FolderManagementBean folderManagement = (FolderManagementBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERMANAGEMENT);
            long[] messageNumbersOfSelectedDisplayMessages = folderWrapper.getUidNumbersOfSelectedDisplayMessages();

            if (messageNumbersOfSelectedDisplayMessages.length >= 1) {

//                DefaultMailboxConnection mailboxConnection = this.getMailboxConnection();
//                String currentMailboxFolder = this.getCurrentMailboxFolder();
                try {
                    folderManagement.setActionFolder("TRASH");
                    performMoveOperation(mailboxConnection, currentMailboxFolder, messageNumbersOfSelectedDisplayMessages, folderManagement, folderWrapper);
                } catch (ConnectionEstablishException cee) {
                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXHOST, ExceptionConverter.getFacesMessage(facesContext, cee, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (AccessDeniedException ade) {
                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXPASSWORD, ExceptionConverter.getFacesMessage(facesContext, ade, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (MailboxFolderException mfe) {
                    facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, mfe, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } catch (MessageMovementException mme) {
                    facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, mme, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } finally {
                    try {
                        mailboxConnection.logout();
                    } catch (LogoutException le) {
                        facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, le, true));
                        return (Constants.OUTCOME_TECH_ERROR);
                    }
                }
            }
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    private void performMoveOperation(DefaultMailboxConnection mailboxConnection,
            String currentMailboxFolder,
            long[] messageNumbersOfSelectedDisplayMessages,
            FolderManagementBean folderManagement,
            FolderWrapperBean folderWrapper) throws ConnectionEstablishException, AccessDeniedException, MailboxFolderException, YawebmailCertificateException, MessageMovementException, Exception {

        mailboxConnection.login(currentMailboxFolder);
        mailboxConnection.moveMessages(messageNumbersOfSelectedDisplayMessages, folderManagement.getActionFolder());

        SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
        sessionContainer.setRenewEnvelopes(true);
        assureCorrectOffset(folderWrapper, sessionContainer, messageNumbersOfSelectedDisplayMessages);
    }

    public String moveSelectedMails() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {

            // FolderWrapperBean und FolderManagementBean holen
            FolderWrapperBean folderWrapper
                    = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            FolderManagementBean folderManagement
                    = (FolderManagementBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERMANAGEMENT);

            // Message-Numbers der selektierten Display-Messages holen
            long[] messageNumbersOfSelectedDisplayMessages = folderWrapper.getUidNumbersOfSelectedDisplayMessages();

            // Wir tun nur was, wenn mindestens eine Mail selektiert wurde.
            if (messageNumbersOfSelectedDisplayMessages.length >= 1) {

                // MailboxConnection und aktuellen Mailbox-Folder aus der Session holen
                DefaultMailboxConnection mailboxConnection = this.getMailboxConnection();
                String currentMailboxFolder = this.getCurrentMailboxFolder();

                // Mails verschieben
                try {
                    performMoveOperation(mailboxConnection, currentMailboxFolder, messageNumbersOfSelectedDisplayMessages, folderManagement, folderWrapper);
//                    mailboxConnection.login(currentMailboxFolder);
//                    mailboxConnection.moveMessages(messageNumbersOfSelectedDisplayMessages, folderManagement.getActionFolder());
//
//                    // Nach dem Loeschen sind die gecachten Envelopes nicht mehr aktuell.
//                    SessionContainerBean sessionContainer
//                            = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
//                    sessionContainer.setRenewEnvelopes(true);
//
//                    // Ggf. aktuellen Offset anpassen
//                    assureCorrectOffset(folderWrapper, sessionContainer, messageNumbersOfSelectedDisplayMessages);
                } catch (ConnectionEstablishException cee) {

                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXHOST,
                            ExceptionConverter.getFacesMessage(facesContext, cee, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (AccessDeniedException ade) {

                    facesContext.addMessage(Constants.CLIENT_ID_MAILBOXPASSWORD,
                            ExceptionConverter.getFacesMessage(facesContext, ade, false));
                    return (Constants.OUTCOME_LOGON);
                } catch (MailboxFolderException mfe) {

                    facesContext.addMessage(null,
                            ExceptionConverter.getFacesMessage(facesContext, mfe, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } catch (MessageMovementException mme) {

                    facesContext.addMessage(null,
                            ExceptionConverter.getFacesMessage(facesContext, mme, true));
                    return (Constants.OUTCOME_TECH_ERROR);
                } finally {

                    try {

                        mailboxConnection.logout();
                    } catch (LogoutException le) {

                        facesContext.addMessage(null,
                                ExceptionConverter.getFacesMessage(facesContext, le, true));
                        return (Constants.OUTCOME_TECH_ERROR);
                    }
                }
            }
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
        return (Constants.OUTCOME_MAILS_LISTING);
    }
    
    public String displaySelectedMail(DisplayMessageBean dmb) {
        String view_name = Constants.OUTCOME_DISPLAY_MAIL;
        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);

            DefaultMailboxConnection mailboxConnection = this.getMailboxConnection();
            String currentMailboxFolder = this.getCurrentMailboxFolder();
            mailboxConnection.login(currentMailboxFolder);

//            DisplayMessageBean dmb = (DisplayMessageBean) this.messageTable.getRowData();
            dmb.setSeen(true);
            long messageUIDNumber = dmb.getuID();
//            long messageUIDNumber = 3;
            
            
            
            int return_val = mailboxConnection.setSeenFlag(messageUIDNumber, true);
            if (return_val == -1) {
                view_name = Constants.OUTCOME_MAILS_LISTING;
                sessionContainer.setRenewEnvelopes(true);
            } else {
                sessionContainer.setMessageNumberCurrentDisplayMail(messageUIDNumber);
            }
            folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
            mailboxConnection.logout();

            return view_name;
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    public String createNewMail() {
        String view_name = Constants.OUTCOME_CREATE_MAIL;
        try {
            MailBasisBean mailBasisBean = (MailBasisBean) this.getManagedBeanByName(Constants.NAME_MBEAN_MAILBASIS);
            mailBasisBean.reset();
            ((SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER)).setMessageNumberCurrentDraftMail(-1);
            return view_name;
        } catch (Exception e) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }
    
    public String processClickedMail(DisplayMessageBean dmb){
        String return_val = "";
        try {
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            if(folderWrapper.getIsDraft()){
                return_val = this.createMailFrmDraft(dmb);
            }else{
                return_val = this.displaySelectedMail(dmb);
            }
        } catch (Exception ex) {
            System.err.println("Error while mail is clicked");
            logger.info("Error while mail is clicked");
        }
        return return_val;
    }

    public String createMailFrmDraft(DisplayMessageBean dmb) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SessionContainerBean sessionContainer;
        DefaultMailboxConnection mailboxConnection = null;
        DisplayMessageBean displayMessage = null;
        long uID = dmb.getuID();
//        long uID = ((DisplayMessageBean) this.messageTable.getRowData()).getuID();
        try {
            sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            mailboxConnection = sessionContainer.getMailboxConnection();
            mailboxConnection.login(sessionContainer.getCurrentMailboxFolder());
            sessionContainer.setMessageNumberCurrentDraftMail(uID);
            Message message = mailboxConnection.getMessage(uID);
            MimeMessage offlineMimeMessage = OfflineMessageAssembler.assembleOfflineMimeMessage((MimeMessage) message);
            displayMessage = (DisplayMessageBean) this.getManagedBeanByName(Constants.NAME_MBEAN_DISPLAYMESSAGE);
            DisplayMessageAssembler.refurbishGivenDisplayMessage(displayMessage, offlineMimeMessage);
            displayMessage.setuID(uID);
        } catch (Exception e) {
        } finally {
            try {
                mailboxConnection.logout();
            } catch (LogoutException le) {
                // empty;
            }
        }

        try {
            MailBasisBean mailBasis = (MailBasisBean) this.getManagedBeanByName(Constants.NAME_MBEAN_MAILBASIS);
//            displayMessage = (DisplayMessageBean) this.getManagedBeanByName(Constants.NAME_MBEAN_DISPLAYMESSAGE);

            ReactionMailBasisAssembler.refurbishGivenDraftMailBasis(mailBasis, displayMessage);
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        return (Constants.OUTCOME_CREATE_MAIL);
    }

    public String checkForNewMail() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {

            // Session-Container holen
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);

            // Wir setzen nur das Flag im Session-Container, den Rest (neue Envelopes
            // holen) macht der RenderResponsePhaseListener.
            if (sessionContainer.isRenewEnvelopes()) {
                return "";
            }
            sessionContainer.setRenewEnvelopes(true);

            // Zurueck zur "mails listing"-Seite
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    public String autoCheckForNewMail() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            sessionContainer.setRenewEnvelopes(true);
            sessionContainer.setThisIsAutoCheckingForNewMail(true);
            return "";
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return "";
        }
    }

    public String searchForSpecificMail() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            sessionContainer.setRenewEnvelopes(true);
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    public String changeFolder() {
        MailsListingBean mailsListingBean;
        try {
            mailsListingBean = (MailsListingBean) this.getManagedBeanByName(Constants.NAME_MBEAN_MAILSLISTING);
            mailsListingBean.setSearchVal(null);
            ((SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER)).setMessageNumberCurrentDraftMail(-1);
        } catch (Exception ex) {
        }
        String folderName = ((Folder) this.folderTable.getRowData()).getFullName();
        return (this.changeFolderToX(folderName));
    }

    public String changeFolderX() {
        String folderName = ((Folder) this.folderTableWithLessOptions.getRowData()).getFullName();
        return (this.changeFolderToX(folderName));
    }

    /**
     * Wechselt den Folder in den Default-Ordner.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    Map<String, String> params;// = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    public String changeToDesiredFolder() {
        MailsListingBean mailsListingBean;
        String return_view = "";
        try {
            mailsListingBean = (MailsListingBean) this.getManagedBeanByName(Constants.NAME_MBEAN_MAILSLISTING);
            mailsListingBean.setSearchVal(null);

            ((SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER)).setMessageNumberCurrentDraftMail(-1);
        } catch (Exception ex) {
        }
        params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params != null && params.containsKey("folderName")) {
            if (!params.containsKey("mailsListing")) {
                return_view = (this.changeFolderToX(params.get("folderName")));
            }
            this.changeFolderToX(params.get("folderName"));
        } 
        return return_view;
    }

    /**
     * Legt einen neuen Subfolder an.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String createSubfolder() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {

            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderManagementBean folderManagement
                    = (FolderManagementBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERMANAGEMENT);
            sessionContainer.setMessageNumberCurrentDraftMail(-1);
            DefaultMailboxConnection mailboxConnection
                    = sessionContainer.getMailboxConnection();

            StringBuffer nameNewFolder = new StringBuffer();
            String currentMailboxFolder = sessionContainer.getCurrentMailboxFolder();

//            if (!currentMailboxFolder.equals(Constants.LEERSTRING)) {
//                nameNewFolder.append(currentMailboxFolder);
//                nameNewFolder.append(mailboxConnection.getFolderSeparator());
//            }
            nameNewFolder.append(folderManagement.getNewSubfolder());

            // Folder anlegen
            try {

                mailboxConnection.login();
                mailboxConnection.createFolder(nameNewFolder.toString());
            } finally {

                mailboxConnection.logout();
            }

//            sessionContainer.setRenewEnvelopes(true);
            folderManagement.setNewSubfolder("");

            // Zurueck zur "mails listing"-Seite
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (MailboxFolderException mfe) {

            facesContext.addMessage(Constants.CLIENT_ID_NEW_SUBFOLDER,
                    ExceptionConverter.getFacesMessage(facesContext, mfe, false));
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    /**
     * Loescht einen Subfolder.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String deleteSubfolder() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {

            // SessionContainerBean und FolderManagementBean holen
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderManagementBean folderManagement
                    = (FolderManagementBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERMANAGEMENT);

            // Aus FolderManagementBean den zu loeschenden Folder holen
            String folderToDelete = ((Folder) this.folderTable.getRowData()).getFullName();
//            String folderToDelete = folderManagement.getSubfolderToDelete();

            // Aus dem SessionContainer die Connection holen und Folder loeschen
            DefaultMailboxConnection mailboxConnection = sessionContainer.getMailboxConnection();

            // Folder loeschen
            try {
                mailboxConnection.login();
                mailboxConnection.deleteFolder(folderToDelete);
                if (mailboxConnection.getCurrentFolder().getFullName().equals(folderToDelete)) {
                    mailboxConnection.changeFolder("INBOX");
                }
            } finally {
                mailboxConnection.logout();
            }
            
            sessionContainer.setRenewEnvelopes(true);

            // Zurueck zur "mails listing"-Seite
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (MailboxFolderException mfe) {

            facesContext.addMessage(Constants.CLIENT_ID_SUBFOLDER_TO_DELETE,
                    ExceptionConverter.getFacesMessage(facesContext, mfe, false));
            return (Constants.OUTCOME_MAILS_LISTING);
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
    }

    public String toggleSelectionOfMails(boolean b) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            // FolderWrapperBean holen
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            // Display-Messages holen und selektieren
            DisplayMessageBean[] displayMessages = folderWrapper.getDisplayMessages();

            for (int ii = 0; ii < displayMessages.length; ii++) {
                displayMessages[ii].setSelected(b);
                System.out.println("ii--> " + ii);
            }
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    public String reRender(AjaxBehaviorEvent event) {
//        ((UIOutput) event.getComponent()).getValue();
//        String s = (String) (((UIOutput) event.getComponent()).getValue());
        boolean b = (Boolean) (((UIOutput) event.getComponent()).getValue());
        try {
            toggleSelectionOfMails(b);
        } catch (Exception ex) {
            System.err.println("error in public String reRender(AjaxBehaviorEvent event)  --> " + ex);
        }
        return "";
    }
    
    public String changeSelectionOfMails() {

        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

        String choice = params.get("choice");
        FolderManagementBean folderManagement = null;
        try {
            folderManagement = (FolderManagementBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERMANAGEMENT);
            if (choice.equalsIgnoreCase("None")) {
                folderManagement.setMailSelectionCheckBox(false);
                toggleSelectionOfMails(false);
            } else if (choice.equalsIgnoreCase("All")) {
                folderManagement.setMailSelectionCheckBox(true);
                toggleSelectionOfMails(true);
            } else if (choice.equalsIgnoreCase("Read")) {
                folderManagement.setMailSelectionCheckBox(false);
                selectParticularMails(true);
            } else if (choice.equalsIgnoreCase("Unread")) {
                folderManagement.setMailSelectionCheckBox(false);
                selectParticularMails(false);
            }
        } catch (Exception ex) {
        }
        return "";
    }

    public String changeMarkingOfMails() {

        String choice = "";
        String params = "";
        MailsListingBean mailsListingBean = null;

        FolderWrapperBean folderWrapper = null;
        DefaultMailboxConnection mailboxConnection = null;
        try {
            mailsListingBean = (MailsListingBean) this.getManagedBeanByName(Constants.NAME_MBEAN_MAILSLISTING);
            choice = mailsListingBean.getChoice();
            params = mailsListingBean.getParams();

            mailboxConnection = this.getMailboxConnection();
            String currentMailboxFolder = this.getCurrentMailboxFolder();
            mailboxConnection.login(currentMailboxFolder);
            folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);

            if (choice != null && choice.equalsIgnoreCase("Mark all as read")) {
                long[] uidList = new long[folderWrapper.getDisplayMessages().length];
                int index = 0;
                for (DisplayMessageBean dmb : folderWrapper.getDisplayMessages()) {
                    dmb.setSeen(true);
                    uidList[index++] = dmb.getuID();
                }
                mailboxConnection.setSeenFlag(uidList, true);
                folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
            } else if (choice != null && choice.equalsIgnoreCase("Mark all as unread")) {
                long[] uidList = new long[folderWrapper.getDisplayMessages().length];
                int index = 0;
                for (DisplayMessageBean dmb : folderWrapper.getDisplayMessages()) {
                    dmb.setSeen(false);
                    uidList[index++] = dmb.getuID();
                }
                mailboxConnection.setSeenFlag(uidList, false);
                folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
            } else if (choice != null && choice.equalsIgnoreCase("Mark as read") && params != null && params.length() > 0) {
                String[] messageNumbers = params.split(",");
                Integer[] tempMsgNo = new Integer[messageNumbers.length];
                int size = messageNumbers.length;
                for (int index = 0; index < size; index++) {
                    int msgNoInt = Integer.parseInt(messageNumbers[index]);
                    tempMsgNo[index] = msgNoInt;
                }
                ArrayList<Long> longArray = new ArrayList<Long>();
                for (DisplayMessageBean dmb : folderWrapper.getDisplayMessages()) {
                    int messageNumber = dmb.getOriginMessage().getMessageNumber();

                    for (int i = 0; i < messageNumbers.length; i++) {
                        if (tempMsgNo[i] != null && tempMsgNo[i] == messageNumber) {
                            tempMsgNo[i] = null;
                            dmb.setSeen(true);
                            longArray.add(dmb.getuID());
//                            long messageUIDNumber = dmb.getuID();
                            size--;
                        }
                    }
                    if (size == 0) {
                        break;
                    }
                }
                long[] lA = new long[longArray.size()];
                int index = 0;
                for (Long l : longArray) {
                    lA[index++] = l;
                }
                mailboxConnection.setSeenFlag(lA, true);
                folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
            } else if (choice != null && choice.equalsIgnoreCase("Mark as unread") && params != null && params.length() > 0) {
                String[] messageNumbers = params.split(",");
                Integer[] tempMsgNo = new Integer[messageNumbers.length];
                int size = messageNumbers.length;
                for (int index = 0; index < size; index++) {
                    int msgNoInt = Integer.parseInt(messageNumbers[index]);
                    tempMsgNo[index] = msgNoInt;
                }
                ArrayList<Long> longArray = new ArrayList<Long>();
                for (DisplayMessageBean dmb : folderWrapper.getDisplayMessages()) {
                    int messageNumber = dmb.getOriginMessage().getMessageNumber();
                    for (int i = 0; i < size; i++) {
                        if (tempMsgNo[i] != null && tempMsgNo[i] == messageNumber) {
                            tempMsgNo[i] = null;
                            dmb.setSeen(false);
                            longArray.add(dmb.getuID());

//                            long messageUIDNumber = dmb.getuID();
//                            mailboxConnection.setSeenFlag(messageUIDNumber, false);
                        }
                    }
                    if (size == 0) {
                        break;
                    }
                }
                long[] lA = new long[longArray.size()];
                int index = 0;
                for (Long l : longArray) {
                    lA[index++] = l;
                }
                mailboxConnection.setSeenFlag(lA, true);
                folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
            }
        } catch (Exception ex) {
        } finally {
            if (mailboxConnection != null) {
                try {
                    mailboxConnection.logout();
                } catch (LogoutException ex) {
//                    Logger.getLogger(MailsListingController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return "";
    }

    public String selectParticularMails(boolean paramIsRead) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            // FolderWrapperBean holen
            FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);

            // Display-Messages holen und selektieren
            DisplayMessageBean[] displayMessages = folderWrapper.getDisplayMessages();
            if (paramIsRead) {
                for (int ii = 0; ii < displayMessages.length; ii++) {
                    if (displayMessages[ii].isSeen()) {
                        displayMessages[ii].setSelected(true);
                    } else {
                        displayMessages[ii].setSelected(false);
                    }
                }
            } else {
                for (int ii = 0; ii < displayMessages.length; ii++) {
                    if (!displayMessages[ii].isSeen()) {
                        displayMessages[ii].setSelected(true);
                    } else {
                        displayMessages[ii].setSelected(false);
                    }
                }
            }
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    static Logger logger = Logger.getLogger(MailsListingController.class.getName());

    /**
     * Jumps to another paging-scope.
     *
     * @return  <code>String</code>-object with info where to navigate to.
     */
    public String demandPaging() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();

        // Get param "newOffset" from the request
        if (params.containsKey(Constants.REQ_PARAM_NEW_OFFSET)) {

            try {
                // Get current SessionContainerBean
                SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
                int tempNewOffset = Integer.parseInt((String) params.get(Constants.REQ_PARAM_NEW_OFFSET));

                // Set offset to new value
                sessionContainer.setCurrentOffset(tempNewOffset);
                // Instruct to re-load envelopes
                sessionContainer.setRenewEnvelopes(true);
            } catch (Exception e) {

                facesContext.addMessage(null,
                        ExceptionConverter.getFacesMessage(facesContext, e, true));
                return (Constants.OUTCOME_TECH_ERROR);
            }
        }

        // If we get here, everything seems to be okay.
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    /**
     * Aendert die Sortierreihenfolge auf Sortierung nach Subject. Wenn bereits
     * nach Subject sortiert wurde, wird die bisherige Sortierung invertiert.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String sortBySubject() {

        return (this.sortByX(MESSAGE_BY_SUBJECT_COMPARATOR));
    }

    public String sortBySender() {
        return (this.sortByX(MESSAGE_BY_SENDER_COMPARATOR));
    }

    public String sortByReceiver() {
        return (this.sortByX(MESSAGE_BY_RECEIVER_COMPARATOR));
    }

    /**
     * Aendert die Sortierreihenfolge auf Sortierung nach Datum. Wenn bereits
     * nach Datum sortiert wurde, wird die bisherige Sortierung invertiert.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String sortByDate() {

        return (this.sortByX(MESSAGE_BY_DATE_COMPARATOR));
    }

    /**
     * Aendert die Sortierreihenfolge auf Sortierung nach Spamlevel. Wenn
     * bereits nach Spamlevel sortiert wurde, wird die bisherige Sortierung
     * invertiert.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String sortBySpamlevel() {

        return (this.sortByX(MESSAGE_BY_SPAMLEVEL_COMPARATOR));
    }

    /**
     * Wird nach Subject aufsteigend sortiert?
     *
     * @return  <code>true</code>, wenn aufsteigend sortiert wird;
     * <code>false</code>, wenn absteigend sortiert wird.
     */
    public boolean isSortUpBySubject() {

        return (this.isSortUpByX(MessageBySubjectComparator.class));
    }

    /**
     * Wird nach Subject absteigend sortiert?
     *
     * @return  <code>true</code>, wenn absteigend sortiert wird;
     * <code>false</code>, wenn aufsteigend sortiert wird.
     */
    public boolean isSortDownBySubject() {

        return (this.isSortDownByX(MessageBySubjectComparator.class));
    }

    /**
     * Wird nach Sender aufsteigend sortiert?
     *
     * @return  <code>true</code>, wenn aufsteigend sortiert wird;
     * <code>false</code>, wenn absteigend sortiert wird.
     */
    public boolean isSortUpBySender() {
        return (this.isSortUpByX(MessageBySenderComparator.class));
    }

    public boolean isSortUpByReceiver() {
        return (this.isSortUpByX(MessageByReceiverComparator.class));
    }

    public boolean isSortDownBySender() {
        return (this.isSortDownByX(MessageBySenderComparator.class));
    }

    public boolean isSortDownByReceiver() {
        return (this.isSortDownByX(MessageByReceiverComparator.class));
    }

    public boolean isSortUpByDate() {

        return (this.isSortUpByX(MessageByDateComparator.class));
    }

    public boolean isSortDownByDate() {

        return (this.isSortDownByX(MessageByDateComparator.class));
    }

    /**
     * Wird nach Spamlevel aufsteigend sortiert?
     *
     * @return  <code>true</code>, wenn aufsteigend sortiert wird;
     * <code>false</code>, wenn absteigend sortiert wird.
     */
    public boolean isSortUpBySpamlevel() {

        return (this.isSortUpByX(MessageBySpamlevelComparator.class));
    }

    /**
     * Wird nach Spamlevel absteigend sortiert?
     *
     * @return  <code>true</code>, wenn absteigend sortiert wird;
     * <code>false</code>, wenn aufsteigend sortiert wird.
     */
    public boolean isSortDownBySpamlevel() {

        return (this.isSortDownByX(MessageBySpamlevelComparator.class));
    }

    /**
     * Determine the number of the last message on the page.
     *
     * @return  <code>int</code>-value of the number of the last message.
     */
    public int getNumberLastMessage() {

        try {

            // Get SessionContainerBean and FolderWrapperBean 
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderWrapperBean folderWrapper
                    = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
            if (sessionContainer.isAllMessagesOnOnePage()) {

                return (folderWrapper.getOverallMessageCount());
            } // Else calculate the number
            else {

                int currentOffsetPlusMessagesPerPage
                        = sessionContainer.getCurrentOffset()
                        + sessionContainer.getAmountOfMessagesPerPage();

                if (currentOffsetPlusMessagesPerPage
                        > folderWrapper.getOverallMessageCount()) {

                    return (folderWrapper.getOverallMessageCount());
                } else {

                    return (currentOffsetPlusMessagesPerPage);
                }
            }
        } catch (Exception e) {

            // Should not happen. But if so, throw RuntimeException
            throw (new RuntimeException(e.getMessage(), e));
        }
    }

    /**
     * Determine the max offset (based on the amount of messages in the current
     * folder).
     *
     * @return  <code>int</code>-value with the max offset.
     */
    public int getMaxOffset() {

        try {

            // Get SessionContainerBean and FolderWrapperBean 
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderWrapperBean folderWrapper
                    = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);

            // No messages - max offset = 0
            if (folderWrapper.getOverallMessageCount() < 1) {

                return (0);
            } else {

                int x = (folderWrapper.getOverallMessageCount() - 1)
                        / sessionContainer.getAmountOfMessagesPerPage();
                return (x * sessionContainer.getAmountOfMessagesPerPage());
            }
        } catch (Exception e) {

            // Should not happen. But if so, throw RuntimeException
            throw (new RuntimeException(e.getMessage(), e));
        }
    }

    /**
     * Determine if paging-forth is possible.
     *
     * @return  <code>true</code>, if paging-forth is possible;
     * <code>false</code>, if paging-forth is not possible.
     */
    public boolean isPagingForthPossible() {

        try {

            // Get SessionContainerBean and FolderWrapperBean 
            SessionContainerBean sessionContainer
                    = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            FolderWrapperBean folderWrapper
                    = (FolderWrapperBean) this.getManagedBeanByName(Constants.NAME_MBEAN_FOLDERWRAPPER);
//            System.out.println("002 --> " + folderWrapper.getDisplayMessages()[0].isSelected());
            // Paging-mode? With "all on one page" we return FALSE
            if (sessionContainer.isAllMessagesOnOnePage()) {

                return (false);
            } // Else calculate possibility
            else {

                return (folderWrapper.getOverallMessageCount()
                        > (sessionContainer.getCurrentOffset()
                        + sessionContainer.getAmountOfMessagesPerPage()));
            }
        } catch (Exception e) {

            // Should not happen. But if so, throw RuntimeException
            throw (new RuntimeException(e.getMessage(), e));
        }
    }

}
