/*
 * @(#)LogonController.java 1.00 2006/02/14
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 14.02.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.controller;

import com.ipvision.ipvmail.MailDAO;
import com.ipvision.ipvmail.MailDTO;
import de.lotk.yawebmail.application.Configuration;
import de.lotk.yawebmail.application.Constants;
import de.lotk.yawebmail.bean.LoginDataBean;
import de.lotk.yawebmail.bean.SessionContainerBean;
import de.lotk.yawebmail.bean.SmtpConnectionBean;
import de.lotk.yawebmail.business.DefaultMailboxConnection;
import de.lotk.yawebmail.business.MailboxConnectionFactory;
import de.lotk.yawebmail.enumerations.SmtpHostChoiceEnum;
import de.lotk.yawebmail.util.VersionMonitor;
import de.lotk.yawebmail.util.faces.ExceptionConverter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import org.apache.oro.text.perl.Perl5Util;
import org.ipvision.users.LoggedUserProfile;

/**
 * Controller fuer das Logon
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class LogonController extends BaseController {

    // --------------------------------------------------------- Klassen-Variablen
    /**
     * Perl5Util zum Suchen und Ersetzen
     */
    private static Perl5Util perl5Util = new Perl5Util();
    static Logger logger = Logger.getLogger(LogonController.class.getName());

    // ---------------------------------------------------------- private Methoden
    /**
     * Initiale Befuellung des SMTP-Connection-Objektes.
     *
     * @param smtpConnection Zu befuellendes SmtpConnectionBean-Objekt
     * @param loginData Die Login-Daten
     */
    private void prefillSmtpConnection(SmtpConnectionBean smtpConnection,
            LoginDataBean loginData) {

        smtpConnection.setSmtpAuthUser(loginData.getMailboxUser());

        // Durch Konfiguration erzwungener SMTP-Host
        if (Configuration.getSmtpHostChoice() == SmtpHostChoiceEnum.NONE) {
            smtpConnection.setSmtpHost(Configuration.getForcedSmtpHostName());
            smtpConnection.setSmtpPort(Configuration.getForcedSmtpHostPort());
        } // SMTP-Host frei oder innerhalb der Domain waehlbar
        else {

            String mailboxHost = loginData.getMailboxHost();

            // SMTP-Host innerhalb der Domain waehlbar
            if (Configuration.getSmtpHostChoice() == SmtpHostChoiceEnum.DOMAIN) {

                if (perl5Util.match("m#^(pop|imap)\\d?s?\\.#i", mailboxHost)) {

                    String smtpDomain
                            = perl5Util.substitute("s#^(pop|imap)\\d?s?\\.##i", mailboxHost);

                    smtpConnection.setSmtpSubdomainPrefix("smtp.");
                    smtpConnection.setSmtpDomain(smtpDomain);
                } else {

                    smtpConnection.setSmtpDomain(mailboxHost);
                }
            } // SMTP-Host frei waehlbar
            else {

                if (perl5Util.match("m#^(pop|imap)\\d?s?\\.#i", mailboxHost)) {
                    String smtpHost = perl5Util.substitute("s#^(pop|imap)\\d?s?#smtp#i", mailboxHost);
                    smtpConnection.setSmtpHost(smtpHost);
                } else {
                    smtpConnection.setSmtpHost(mailboxHost);
                }
            }
        }
    }

    public void redirectToList(){
        try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mailsListing.xhtml");//add your URL here, instead of list.do
       
        } catch (Exception e) {
        }
  }
    
    public String logon() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);
            LoggedUserProfile loggedUserProfile = (LoggedUserProfile) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGGED_USER_PROFILE);
          // loginData.setMailboxHost(loggedUserProfile.getMailServerHost());
          // loginData.setMailboxPort(loggedUserProfile.getMailServerPort());
            loginData.setMailboxUser(loggedUserProfile.getMailServerUserId());
            loginData.setMailboxPassword(loggedUserProfile.getPassword());
            
            

            // loginData.setMailboxUser("nazmul@dtalkbd.com");
           // loginData.setMailboxPassword("aaaaaaa");
            
            
            SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SESSIONCONTAINER);
            SmtpConnectionBean smtpConnection = (SmtpConnectionBean) this.getManagedBeanByName(Constants.NAME_MBEAN_SMTPCONNECTION);

            sessionContainer.setCurrentMailboxFolder(sessionContainer.getDefaultFolder());

            DefaultMailboxConnection mailboxConnection = null;
           // if ((sessionContainer.getMailboxConnection() == null)) {
                mailboxConnection = MailboxConnectionFactory.getInstance().createMailboxConnection(loginData, sessionContainer.getMailboxTrustManager());
                sessionContainer.setMailboxConnection(mailboxConnection);
           // }

            this.prefillSmtpConnection(smtpConnection, loginData);
           
//            String v_full_name = mailDAO.getProfileInfo(loginData.getMailboxUser());
            loginData.setMailboxUserFullName(loggedUserProfile.getFirstName()+" " +loggedUserProfile.getLastName());
            loginData.setMailboxUserPhone(loggedUserProfile.getMobilePhoneDialingCode()+loggedUserProfile.getMobilePhone());
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }
        return (Constants.OUTCOME_MAILS_LISTING);
    }

    public String showProfileInfo() {
        return Constants.OUTCOME_EDIT_PROFILE;
    }

    public String showPasswordChangeWindow() {
        return Constants.OUTCOME_CHANGE_PASSWORD;
    }

    public String showSettings() {
        return Constants.OUTCOME_SETTINGS;
    }

    public String changeProfileInfo() {
        logger.info("Inside change profile info -----------");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);
            if (loginData.getEditMailboxUserOldPassword().equals(loginData.getMailboxPassword())) {
                String urlParameters = "username=" + loginData.getMailboxUser()
                        + "&fullname=" + loginData.getEditMailboxUserFullNameCustom()
                        + "&phone=" + loginData.getEditMailboxUserPhoneCustom();

                String v_response_text = sendPost(Constants.EDIT_PROFILE_REQUEST_URL_SERVER, urlParameters);
                if (v_response_text.equals("SUCCESS")) {
                    facesContext.addMessage(
                            Constants.UPDATE_PROFILE_MSG,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Successfully updated",
                                    "Profile updated successfully")
                    );

                    loginData.setMailboxUserFullName(loginData.getEditMailboxUserFullNameCustom());
                    loginData.setMailboxUserPhone(loginData.getEditMailboxUserPhoneCustom());
//                    loginData.setMailboxPassword(loginData.getEditMailboxUserNewPassword());
                } else {
                    facesContext.addMessage(
                            Constants.UPDATE_PROFILE_MSG,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    v_response_text,
                                    v_response_text)
                    );
                }

            } else {
                logger.info("false --> password is incorrect -----------------------");
                facesContext.addMessage(
                        Constants.UPDATE_PROFILE_MSG,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Password is incorrect",
                                "Please enter correct password")
                );
            }

        } catch (Exception ex) {
            facesContext.addMessage(
                    Constants.UPDATE_PROFILE_MSG,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Profile update failed",
                            "Profile update failed ")
            );
            logger.info("Error while updating profile info --> " + ex);
        }
        return Constants.OUTCOME_EDIT_PROFILE;
    }

    public String changePassword() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);

            if (loginData.getEditMailboxUserOldPassword().equals(loginData.getMailboxPassword())) {
                if (loginData.getEditMailboxUserNewPassword() != null
                        && loginData.getEditMailboxUserConfirmPassword() != null
                        && loginData.getEditMailboxUserNewPassword().length() > 4
                        && loginData.getEditMailboxUserConfirmPassword().length() > 4) {
                    if (loginData.getEditMailboxUserNewPassword().equals(loginData.getEditMailboxUserConfirmPassword())) {
                        String urlParameters = "username=" + loginData.getMailboxUser()
                                + "&password=" + loginData.getEditMailboxUserNewPassword()
                                + "&confirm_password=" + loginData.getEditMailboxUserConfirmPassword();

                        String v_response_text = sendPost(Constants.EDIT_PROFILE_REQUEST_URL_SERVER, urlParameters);
                        if (v_response_text.equals("SUCCESS")) {
                            facesContext.addMessage(
                                    Constants.UPDATE_PROFILE_MSG,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "Successfully updated",
                                            "Password changed successfully")
                            );

//                            loginData.setMailboxUserFullName(loginData.getEditMailboxUserFullNameCustom());
                            loginData.setMailboxPassword(loginData.getEditMailboxUserNewPassword());
                        } else {
                            facesContext.addMessage(
                                    Constants.UPDATE_PROFILE_MSG,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                            v_response_text,
                                            v_response_text)
                            );
                        }
                    } else {
                        facesContext.addMessage(
                                Constants.UPDATE_PROFILE_MSG,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        "Password mismatched",
                                        "Password mismatched")
                        );
                    }
                } else {
                    facesContext.addMessage(
                            Constants.UPDATE_PROFILE_MSG,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Password length must be greater or equal to 5",
                                    "Password length must be greater or equal to 5")
                    );
                }
            } else {
                facesContext.addMessage(
                        Constants.UPDATE_PROFILE_MSG,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Old password is incorrect",
                                "Old password is incorrect")
                );
            }

        } catch (Exception ex) {
            facesContext.addMessage(
                    Constants.UPDATE_PROFILE_MSG,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Password change failed",
                            "Password change failed")
            );
            logger.info("Error while changing password --> " + ex);
        }
        return Constants.OUTCOME_CHANGE_PASSWORD;
    }

//    public String changeProfileInfo() {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        try {
//            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);
//
//            if (loginData.getEditMailboxUserOldPassword().equals(loginData.getMailboxPassword())) {
//                if (loginData.getEditMailboxUserNewPassword() != null
//                        && loginData.getEditMailboxUserConfirmPassword() != null
//                        && loginData.getEditMailboxUserNewPassword().length() > 4
//                        && loginData.getEditMailboxUserConfirmPassword().length() > 4) {
//                    if (loginData.getEditMailboxUserNewPassword().equals(loginData.getEditMailboxUserConfirmPassword())) {
//                        String urlParameters = "username=" + loginData.getMailboxUser()
//                                + "&fullname=" + loginData.getEditMailboxUserFullNameCustom()
//                                + "&password=" + loginData.getEditMailboxUserNewPassword()
//                                + "&confirm_password=" + loginData.getEditMailboxUserConfirmPassword();
//
//                        String v_response_text = sendPost(Constants.EDIT_PROFILE_REQUEST_URL_SERVER, urlParameters);
//                        if (v_response_text.equals("SUCCESS")) {
//                            facesContext.addMessage(
//                                    Constants.UPDATE_PROFILE_MSG,
//                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
//                                            "Successfully updated",
//                                            "Profile updated successfully")
//                            );
//                            
//                            loginData.setMailboxUserFullName(loginData.getEditMailboxUserFullNameCustom());
//                            loginData.setMailboxPassword(loginData.getEditMailboxUserNewPassword());
//                        } else {
//                            facesContext.addMessage(
//                                    Constants.UPDATE_PROFILE_MSG,
//                                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                            v_response_text,
//                                            v_response_text)
//                            );
//                        }
//                    } else {
//                        facesContext.addMessage(
//                                Constants.UPDATE_PROFILE_MSG,
//                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                        "Password mismatched",
//                                        "Password mismatched")
//                        );
//                    }
//                } else {
//                    facesContext.addMessage(
//                            Constants.UPDATE_PROFILE_MSG,
//                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                    "Password length must be greater or equal to 5",
//                                    "Password length must be greater or equal to 5")
//                    );
//                }
//            } else {
//                facesContext.addMessage(
//                        Constants.UPDATE_PROFILE_MSG,
//                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                "Old password is incorrect",
//                                "Old password is incorrect")
//                );
//            }
//
//        } catch (Exception ex) {
//            facesContext.addMessage(
//                    Constants.UPDATE_PROFILE_MSG,
//                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                            "Profile update failed",
//                            "Profile update failed ")
//            );
//            logger.info("Error while updating profile info --> " + ex);
//        }
//        return Constants.OUTCOME_EDIT_PROFILE;
//    }
    // HTTP POST request
    private String sendPost(String req_url, String param_url_params) throws Exception {
        logger.info("----------------------- Inside sendPost function -----------------------");
        String url = req_url;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Host", "38.127.68.201:8080");
//        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0");
        con.setRequestProperty("Accept", "*/*");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Accept-Encoding", "gzip, deflate");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//        con.setRequestProperty("Referer", "http://localhost:8084/ipvmail/logon.jsf");
//        con.setRequestProperty("Origin", "http://localhost:8084");

//        String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
//        String urlParameters = "username=reefat@dtalkbd.com&fullname=FULLNAME&password=NEWPASSWORD&confirm_password=CONFIRMPASSWORD";
        String urlParameters = param_url_params;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        urlParameters = urlParameters.replaceAll("\\+", "%2B");
//        urlParameters = urlParameters.replaceAll("\\ ", "%20%");
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

//        logger.info("----------------------- End sendPost function -----------------------");
        logger.info("----------------------- response : " + response.toString());
        //print result
//        System.out.println(response.toString());
        return response.toString();

    }

    public String configureAdvancedLogonProperties() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);

            loginData.setAdvancedLogonProperties(true);
            UIInput uicMbHost = (UIInput) facesContext.getViewRoot().findComponent(Constants.CLIENT_ID_MAILBOXHOST);
            loginData.setMailboxHost((String) uicMbHost.getSubmittedValue());
        } catch (Exception e) {

            facesContext.addMessage(null,
                    ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_LOGON);
    }

    /**
     * Formular zuruecksetzen.
     *
     * @return          <code>String</code>-Objekt mit Info, wohin navigiert wird.
     */
    public String reset() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            // LoginDataBean holen
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);

            // LoginDataBean zuruecksetzen
            loginData.reset();
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_LOGON);
    }

    public String resetEditProfileFields() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            LoginDataBean loginData = (LoginDataBean) this.getManagedBeanByName(Constants.NAME_MBEAN_LOGINDATA);
            loginData.resetEditProfileFields();
        } catch (Exception e) {
            facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
            return (Constants.OUTCOME_TECH_ERROR);
        }

        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
        return (Constants.OUTCOME_EDIT_PROFILE);
    }

//    resetEditProfileFields
//    public String signup() {
//        // Wenn wir hier angelangt sind, scheint alles i.O. zu sein.
//        return (Constants.OUTCOME_SIGNUP);
//    }

    /**
     * Wird beim Wechseln der Sprache involviert.
     *
     * @param event Aktuelles ValueChangeEvent.
     */
    public void changeLanguage(ValueChangeEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();

        // Wir machen nichts, ausser die Sprache zu wechseln.
        fc.getViewRoot().setLocale((Locale) event.getNewValue());

        // Und gleich wieder die View rendern.
        fc.renderResponse();
    }

    /**
     * Soll das Eingabefeld fuer den Mailbox-Host editierbar sein?
     *
     * @return  <code>true</code>, wenn das Feld editierbar sein soll;
     * <code>false</code>, wenn das Feld nicht editierbar sein soll.
     */
    public boolean isEditableMailboxHost() {
        return (!Configuration.isForcePreselectedMailboxHost());
    }

    /**
     * Soll das Eingabefeld fuer das Mailbox-Protokoll editierbar sein?
     *
     * @return  <code>true</code>, wenn das Feld editierbar sein soll;
     * <code>false</code>, wenn das Feld nicht editierbar sein soll.
     */
    public boolean isEditableMailboxProtocol() {
        return (!Configuration.isForcePreselectedMailboxProtocol());
    }

    /**
     * Is there a new version of yawebmail available?
     *
     * @return  <code>true</code>, if there is a new version available;
     * <code>false</code>, if not.
     */
    public boolean isNewYawebmailVersionAvailable() {
        return (VersionMonitor.isCurrentVersionGreaterThanConfiguredVersion());
    }
}
