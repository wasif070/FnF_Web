/*
 * @(#)RenderResponsePhaseListener.java 1.00 2006/02/21
 *
 * Copyright (c) 2006, Stephan Sann
 *
 * 21.02.2006 ssann        Vers. 1.0     created
 */
package de.lotk.yawebmail.listener;

import java.util.Arrays;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.mail.Message;
import javax.mail.internet.MimeMessage;

import de.lotk.yawebmail.application.Constants;
import de.lotk.yawebmail.bean.CheckCertsBean;
import de.lotk.yawebmail.bean.DisplayMessageBean;
import de.lotk.yawebmail.bean.FolderManagementBean;
import de.lotk.yawebmail.bean.FolderWrapperBean;
import de.lotk.yawebmail.bean.MailsListingBean;
import de.lotk.yawebmail.bean.RetrieveMessagesResultBean;
import de.lotk.yawebmail.bean.SessionContainerBean;
import de.lotk.yawebmail.business.DefaultMailboxConnection;
import de.lotk.yawebmail.controller.CheckCertsController;
import de.lotk.yawebmail.controller.LogonController;
import de.lotk.yawebmail.exceptions.AccessDeniedException;
import de.lotk.yawebmail.exceptions.ConnectionEstablishException;
import de.lotk.yawebmail.exceptions.LogoutException;
import de.lotk.yawebmail.exceptions.YawebmailCertificateException;
import de.lotk.yawebmail.util.DisplayMessageAssembler;
import de.lotk.yawebmail.util.OfflineMessageAssembler;
import de.lotk.yawebmail.util.faces.ExceptionConverter;
import de.lotk.yawebmail.util.faces.ManagedBeanUtils;

/**
 * Phase-Listener fuer die Render-Response-Phase
 *
 * @author Stephan Sann
 * @version 1.0
 */
public class RenderResponsePhaseListener extends BaseListener implements
        PhaseListener {

    // ---------------------------------------------------------------- Konstanten
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6690767913768137935L;

    private boolean schreibeEnvelopesInFolderWrapperBean(SessionContainerBean sessionContainer, FacesContext facesContext) throws Exception {

        MailsListingBean mailsListingBean = (MailsListingBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_MAILSLISTING);
        FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_FOLDERWRAPPER);

        if (sessionContainer.isRenewEnvelopes()) {
            DefaultMailboxConnection dmc = sessionContainer.getMailboxConnection();
            DefaultMailboxConnection mailboxConnection = (DefaultMailboxConnection) dmc.clone();

            try {
                mailboxConnection.login(sessionContainer.getCurrentMailboxFolder());
                if (mailboxConnection.isRefreshNeeded(sessionContainer.isThisIsAutoCheckingForNewMail(), folderWrapper.getMessageCount())) {
                    RetrieveMessagesResultBean rmr = null;

                    if (sessionContainer.isAllMessagesOnOnePage()) {
                        rmr = mailboxConnection.getEnvelopes(mailsListingBean.getSearchVal());
                    } else {
                        int tempStartNumber;
                        int tempEndNumber;
                        int count = mailboxConnection.getTotalNumberOfMessagesOfCurrentFolder();

                        if (count < sessionContainer.getAmountOfMessagesPerPage()) {
                            tempStartNumber = 1;
                            tempEndNumber = count;
                        } else {
                            int offset = sessionContainer.getCurrentOffset() / sessionContainer.getAmountOfMessagesPerPage();
                            tempStartNumber = count - ((offset + 1) * sessionContainer.getAmountOfMessagesPerPage()) + 1;
                            tempEndNumber = count - (offset * sessionContainer.getAmountOfMessagesPerPage());
                            if (tempStartNumber <= 0) {
                                tempStartNumber = 1;
                            }
                        }
                        rmr = mailboxConnection.getEnvelopes(tempStartNumber, tempEndNumber, true, mailsListingBean.getSearchVal());
                    }
                    Message[] originalMessages = rmr.getMessages();
                    Message[] messages = new Message[rmr.getMessages().length];// = rmr.getMessages();

                    Long[] uidList = rmr.getUidListOfMessages();
                    for (int ii = 0; ii < messages.length; ii++) {
                        messages[ii] = OfflineMessageAssembler.assembleOverviewOfflineMimeMessage((MimeMessage) rmr.getMessages()[ii]);
                    }

                    Arrays.sort(messages, sessionContainer.getSortierComparator());
                    Arrays.sort(originalMessages, sessionContainer.getSortierComparator());

                    FolderManagementBean folderManagement = (FolderManagementBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_FOLDERMANAGEMENT);
                    folderManagement.setMailSelectionCheckBox(Boolean.FALSE);
                    folderWrapper.setFolder(mailboxConnection.getCurrentFolder());

//                    folderWrapper.setMessages(messages,originalMessages);                    
                    folderWrapper.setMessages(messages, originalMessages);
                    folderWrapper.setOverallMessageCount(rmr.getOverallMessageCount());
                    if (mailboxConnection.getCurrentFolder().getFullName().equalsIgnoreCase(sessionContainer.getDefaultFolder())) {
                        folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
                    }
//                folderWrapper.setSelectAllCheckBoxClicked(false);
                    dmc.setAllFolders(mailboxConnection.getAllFolders());
                }
                sessionContainer.setRenewEnvelopes(false);
                sessionContainer.setThisIsAutoCheckingForNewMail(false);
//                }
            } catch (Exception e) {

                throw (e);
            } finally {
                try {
                    mailboxConnection.logout();
                } catch (LogoutException le) {
                    // empty;
                }
            }
        } else if (sessionContainer.isRenewSortorder()) {
            Message[] messages = folderWrapper.getMessages();
            Arrays.sort(messages, sessionContainer.getSortierComparator());
//            Arrays.sort(folderWrapper.getDisplayMessages(),sessionContainer.getSortierComparatorDMB());
            folderWrapper.setMessages(messages, null);
            sessionContainer.setRenewSortorder(false);
        } else {
            return true;
        }
        return false;
    }

    /**
     * Schreibt die ausgewaehlte Mail in die managed Bean "DisplayMessageBean".
     *
     * @param sessionContainer Der aktuelle SessionContainer
     * @param facesContext Der aktuelle Faces-Context
     */
    private void schreibeMessageInDisplayMessageBean(SessionContainerBean sessionContainer, FacesContext facesContext) throws Exception {

        DefaultMailboxConnection mailboxConnection = sessionContainer.getMailboxConnection();

        // Warum try/catch, wenn wir alle Exceptions re-thrown?
        // Damit wir finally aufraeumen koennen.
        try {

            mailboxConnection.login(sessionContainer.getCurrentMailboxFolder());

            Message message = mailboxConnection.getMessage(sessionContainer.getMessageNumberCurrentDisplayMail());

            MimeMessage offlineMimeMessage = OfflineMessageAssembler.assembleOfflineMimeMessage((MimeMessage) message);

            DisplayMessageBean displayMessage = (DisplayMessageBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_DISPLAYMESSAGE);

            DisplayMessageAssembler.refurbishGivenDisplayMessage(displayMessage, offlineMimeMessage);
        } catch (Exception e) {
            throw (e);
        } finally {
            try {
                mailboxConnection.logout();
            } catch (LogoutException le) {
                // empty;
            }
        }
    }

    // ----------------------------------------------------- oeffentliche Methoden

    /* (non-Javadoc)
     * @see javax.faces.event.PhaseListener#getPhaseId()
     */
    @Override
    public PhaseId getPhaseId() {

        // dieser Listener interessiert sich nur fuer die Render-Response-Phase 
        return (PhaseId.RENDER_RESPONSE);
    }

    /* (non-Javadoc)
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void afterPhase(PhaseEvent phaseEvent) {

    }

    @Override
    public void beforePhase(PhaseEvent phaseEvent) {

       
        FacesContext facesContext = phaseEvent.getFacesContext();
        String viewId = facesContext.getViewRoot().getViewId();
        if (!viewId.contains("mail/")) {
            return;
        }
 SessionContainerBean sessionContainer=null;
        try {

             sessionContainer = (SessionContainerBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_SESSIONCONTAINER);

            if (viewId.equals(Constants.VIEW_ID_MAILSLISTING) || viewId.equals(Constants.VIEW_ID_LOGON)) {
                boolean return_val = this.schreibeEnvelopesInFolderWrapperBean(sessionContainer, facesContext);
                if (return_val && viewId.equals(Constants.VIEW_ID_LOGON)) {
                    viewId = Constants.VIEW_ID_MAILSLISTING;
                    facesContext.getViewRoot().setViewId(viewId);
                }
            } else if (viewId.equals(Constants.VIEW_ID_DISPLAY_MAIL)) {
                this.schreibeMessageInDisplayMessageBean(sessionContainer, facesContext);
            }
        } catch (ConnectionEstablishException cee) {

            facesContext.addMessage(Constants.CLIENT_ID_MAILBOXHOST,
                    ExceptionConverter.getFacesMessage(facesContext, cee, false));

            ViewHandler vh = facesContext.getApplication().getViewHandler();
            UIViewRoot newRoot = vh.createView(facesContext,
                    Constants.VIEW_ID_LOGON);
            facesContext.setViewRoot(newRoot);
        } catch (AccessDeniedException ade) {
            
//            try {
//            sessionContainer.setMailboxConnection(null);
//            } catch (Exception e) {
//            }
//            
             try {
                        LogonController logonController = (LogonController) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_LOGIN_CONTROLLER);
                        logonController.logon();

                    } catch (Exception e) {
                    }
            
            
            
            facesContext.addMessage(Constants.CLIENT_ID_MAILBOXPASSWORD, ExceptionConverter.getFacesMessage(facesContext, ade, false));

            ViewHandler vh = facesContext.getApplication().getViewHandler();
            UIViewRoot newRoot = vh.createView(facesContext, viewId);
            facesContext.setViewRoot(newRoot);
        } catch (YawebmailCertificateException yce) {

            try {

                CheckCertsBean checkCerts
                        = (CheckCertsBean) ManagedBeanUtils.getManagedBeanByName(facesContext,
                                Constants.NAME_MBEAN_CHECKCERTS);

                checkCerts.setWhichCerts(CheckCertsController.WhichCertsEnum.MAILBOX);
                checkCerts.setServerCerts(yce.getCerts());

                ViewHandler vh = facesContext.getApplication().getViewHandler();
                UIViewRoot newRoot = vh.createView(facesContext,
                        Constants.VIEW_ID_CHECK_CERTS);
                facesContext.setViewRoot(newRoot);
            } catch (Exception e) {
                facesContext.addMessage(null,
                        ExceptionConverter.getFacesMessage(facesContext, e, true));

                ViewHandler vh = facesContext.getApplication().getViewHandler();
                UIViewRoot newRoot = vh.createView(facesContext,
                        Constants.VIEW_ID_TECH_ERROR);
                facesContext.setViewRoot(newRoot);
            }
        } catch (Exception e) {
            if (!viewId.equals(Constants.VIEW_ID_LOGON)) {
                facesContext.addMessage(null,
                        ExceptionConverter.getFacesMessage(facesContext, e, true));

                ViewHandler vh = facesContext.getApplication().getViewHandler();
                UIViewRoot newRoot = vh.createView(facesContext,
                        Constants.VIEW_ID_TECH_ERROR);
                facesContext.setViewRoot(newRoot);
            }
        }
    }
}

///*
// * @(#)RenderResponsePhaseListener.java 1.00 2006/02/21
// *
// * Copyright (c) 2006, Stephan Sann
// *
// * 21.02.2006 ssann        Vers. 1.0     created
// */
//package de.lotk.yawebmail.listener;
//
//import java.util.Arrays;
//
//import javax.faces.application.ViewHandler;
//import javax.faces.component.UIViewRoot;
//import javax.faces.context.FacesContext;
//import javax.mail.Message;
//import javax.mail.internet.MimeMessage;
//
//import de.lotk.yawebmail.application.Constants;
//import de.lotk.yawebmail.bean.CheckCertsBean;
//import de.lotk.yawebmail.bean.DisplayMessageBean;
//import de.lotk.yawebmail.bean.FolderManagementBean;
//import de.lotk.yawebmail.bean.FolderWrapperBean;
//import de.lotk.yawebmail.bean.MailsListingBean;
//import de.lotk.yawebmail.bean.RetrieveMessagesResultBean;
//import de.lotk.yawebmail.bean.SessionContainerBean;
//import de.lotk.yawebmail.business.DefaultMailboxConnection;
//import de.lotk.yawebmail.controller.CheckCertsController;
//import de.lotk.yawebmail.controller.LogonController;
//import de.lotk.yawebmail.exceptions.AccessDeniedException;
//import de.lotk.yawebmail.exceptions.ConnectionEstablishException;
//import de.lotk.yawebmail.exceptions.LogoutException;
//import de.lotk.yawebmail.exceptions.SessionExpiredException;
//import de.lotk.yawebmail.exceptions.YawebmailCertificateException;
//import de.lotk.yawebmail.util.DisplayMessageAssembler;
//import de.lotk.yawebmail.util.OfflineMessageAssembler;
//import de.lotk.yawebmail.util.faces.ExceptionConverter;
//import de.lotk.yawebmail.util.faces.ManagedBeanUtils;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ViewScoped;
//
///**
// * Phase-Listener fuer die Render-Response-Phase
// *
// * @author Stephan Sann
// * @version 1.0
// */
//@ManagedBean(name = "renderResponsePhaseListener")
//@ViewScoped
//public class RenderResponsePhaseListener extends BaseListener {
//
//    // ---------------------------------------------------------------- Konstanten
//    /**
//     * serialVersionUID
//     */
//    private static final long serialVersionUID = 6690767913768137935L;
//
//    private boolean schreibeEnvelopesInFolderWrapperBean(SessionContainerBean sessionContainer, FacesContext facesContext) throws Exception {
//
//        MailsListingBean mailsListingBean = (MailsListingBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_MAILSLISTING);
//        FolderWrapperBean folderWrapper = (FolderWrapperBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_FOLDERWRAPPER);
//
//        if (sessionContainer.isRenewEnvelopes()) {
//            DefaultMailboxConnection dmc = sessionContainer.getMailboxConnection();
//            DefaultMailboxConnection mailboxConnection = (DefaultMailboxConnection) dmc.clone();
//
//            try {
//                mailboxConnection.login(sessionContainer.getCurrentMailboxFolder());
//                if (mailboxConnection.isRefreshNeeded(sessionContainer.isThisIsAutoCheckingForNewMail(), folderWrapper.getMessageCount())) {
//                    RetrieveMessagesResultBean rmr = null;
//
//                    if (sessionContainer.isAllMessagesOnOnePage()) {
//                        rmr = mailboxConnection.getEnvelopes(mailsListingBean.getSearchVal());
//                    } else {
//                        int tempStartNumber;
//                        int tempEndNumber;
//                        int count = mailboxConnection.getTotalNumberOfMessagesOfCurrentFolder();
//
//                        if (count < sessionContainer.getAmountOfMessagesPerPage()) {
//                            tempStartNumber = 1;
//                            tempEndNumber = count;
//                        } else {
//                            int offset = sessionContainer.getCurrentOffset() / sessionContainer.getAmountOfMessagesPerPage();
//                            tempStartNumber = count - ((offset + 1) * sessionContainer.getAmountOfMessagesPerPage()) + 1;
//                            tempEndNumber = count - (offset * sessionContainer.getAmountOfMessagesPerPage());
//                            if (tempStartNumber <= 0) {
//                                tempStartNumber = 1;
//                            }
//                        }
//                        rmr = mailboxConnection.getEnvelopes(tempStartNumber, tempEndNumber, true, mailsListingBean.getSearchVal());
//                    }
//                    Message[] originalMessages = rmr.getMessages();
//                    Message[] messages = new Message[rmr.getMessages().length];// = rmr.getMessages();
//
//                    Long[] uidList = rmr.getUidListOfMessages();
//                    for (int ii = 0; ii < messages.length; ii++) {
//                        messages[ii] = OfflineMessageAssembler.assembleOverviewOfflineMimeMessage((MimeMessage) rmr.getMessages()[ii]);
//                    }
//
//                    Arrays.sort(messages, sessionContainer.getSortierComparator());
//                    Arrays.sort(originalMessages, sessionContainer.getSortierComparator());
//
//                    FolderManagementBean folderManagement = (FolderManagementBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_FOLDERMANAGEMENT);
//                    folderManagement.setMailSelectionCheckBox(Boolean.FALSE);
//                    folderWrapper.setFolder(mailboxConnection.getCurrentFolder());
//
////                    folderWrapper.setMessages(messages,originalMessages);                    
//                    folderWrapper.setMessages(messages, originalMessages);
//                    folderWrapper.setOverallMessageCount(rmr.getOverallMessageCount());
//                    if (mailboxConnection.getCurrentFolder().getFullName().equalsIgnoreCase(sessionContainer.getDefaultFolder())) {
//                        folderWrapper.setCounter(String.valueOf(mailboxConnection.getCurrentFolder().getUnreadMessageCount()));
//                    }
////                folderWrapper.setSelectAllCheckBoxClicked(false);
//                    dmc.setAllFolders(mailboxConnection.getAllFolders());
//                }
//                sessionContainer.setRenewEnvelopes(false);
//                sessionContainer.setThisIsAutoCheckingForNewMail(false);
////                }
//            } catch (Exception e) {
//
//                throw (e);
//            } finally {
//                try {
//                    mailboxConnection.logout();
//                } catch (LogoutException le) {
//                    // empty;
//                }
//            }
//        } else if (sessionContainer.isRenewSortorder()) {
//            Message[] messages = folderWrapper.getMessages();
//            Arrays.sort(messages, sessionContainer.getSortierComparator());
////            Arrays.sort(folderWrapper.getDisplayMessages(),sessionContainer.getSortierComparatorDMB());
//            folderWrapper.setMessages(messages, null);
//            sessionContainer.setRenewSortorder(false);
//        } else {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * Schreibt die ausgewaehlte Mail in die managed Bean "DisplayMessageBean".
//     *
//     * @param sessionContainer Der aktuelle SessionContainer
//     * @param facesContext Der aktuelle Faces-Context
//     */
//    private void schreibeMessageInDisplayMessageBean(SessionContainerBean sessionContainer, FacesContext facesContext) throws Exception {
//
//        DefaultMailboxConnection mailboxConnection = sessionContainer.getMailboxConnection();
//
//        // Warum try/catch, wenn wir alle Exceptions re-thrown?
//        // Damit wir finally aufraeumen koennen.
//        try {
//
//            mailboxConnection.login(sessionContainer.getCurrentMailboxFolder());
//
//            Message message = mailboxConnection.getMessage(sessionContainer.getMessageNumberCurrentDisplayMail());
//
//            MimeMessage offlineMimeMessage = OfflineMessageAssembler.assembleOfflineMimeMessage((MimeMessage) message);
//
//            DisplayMessageBean displayMessage = (DisplayMessageBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_DISPLAYMESSAGE);
//
//            DisplayMessageAssembler.refurbishGivenDisplayMessage(displayMessage, offlineMimeMessage);
//        } catch (Exception e) {
//            throw (e);
//        } finally {
//            try {
//                mailboxConnection.logout();
//            } catch (LogoutException le) {
//                // empty;
//            }
//        }
//    }
//
//    // ----------------------------------------------------- oeffentliche Methoden
//
//    /* (non-Javadoc)
//     * @see javax.faces.event.PhaseListener#getPhaseId()
//     */
//    public void beforePhase(String viewId) {
//        System.out.println(viewId);
//
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        if ((!viewId.equals(Constants.VIEW_ID_LOADING))) {
//
//            try {
//                SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(facesContext,
//                        Constants.NAME_MBEAN_SESSIONCONTAINER);
//
//                DefaultMailboxConnection mailboxConnection = sessionContainer.getMailboxConnection();
//
//                if (mailboxConnection == null) {
//                    try {
//                        LogonController logonController = (LogonController) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_LOGIN_CONTROLLER);
//                        logonController.logon();
//
//                    } catch (Exception e) {
//                    }
//                }
//            } catch (SessionExpiredException see) {
//                facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, see, false));
//
//                ViewHandler vh = facesContext.getApplication().getViewHandler();
//                // TODO konfigurierbar machen!
//                UIViewRoot newRoot = vh.createView(facesContext, Constants.VIEW_ID_LOGON);
//                facesContext.setViewRoot(newRoot);
//            } catch (Exception e) {
//
//                e.printStackTrace();
//
//                facesContext.addMessage(null, ExceptionConverter.getFacesMessage(facesContext, e, true));
//
//                ViewHandler vh = facesContext.getApplication().getViewHandler();
//
//                // TODO konfigurierbar machen!
//                UIViewRoot newRoot = vh.createView(facesContext, Constants.VIEW_ID_TECH_ERROR);
//                facesContext.setViewRoot(newRoot);
//            }
//        
//
//        try {
//
//            SessionContainerBean sessionContainer = (SessionContainerBean) this.getManagedBeanByName(facesContext, Constants.NAME_MBEAN_SESSIONCONTAINER);
//
//            if (viewId.equals(Constants.VIEW_ID_MAILSLISTING) || viewId.equals(Constants.VIEW_ID_LOGON)) {
//                boolean return_val = this.schreibeEnvelopesInFolderWrapperBean(sessionContainer, facesContext);
//                if (return_val && viewId.equals(Constants.VIEW_ID_LOGON)) {
//                    viewId = Constants.VIEW_ID_MAILSLISTING;
//                    facesContext.getViewRoot().setViewId(viewId);
//                }
//            } else if (viewId.equals(Constants.VIEW_ID_DISPLAY_MAIL)) {
//                this.schreibeMessageInDisplayMessageBean(sessionContainer, facesContext);
//            }
//        } catch (ConnectionEstablishException cee) {
//
//            facesContext.addMessage(Constants.CLIENT_ID_MAILBOXHOST,
//                    ExceptionConverter.getFacesMessage(facesContext, cee, false));
//
//            ViewHandler vh = facesContext.getApplication().getViewHandler();
//            UIViewRoot newRoot = vh.createView(facesContext,
//                    Constants.VIEW_ID_LOGON);
//            facesContext.setViewRoot(newRoot);
//        } catch (AccessDeniedException ade) {
//            facesContext.addMessage(Constants.CLIENT_ID_MAILBOXPASSWORD, ExceptionConverter.getFacesMessage(facesContext, ade, false));
//
//            ViewHandler vh = facesContext.getApplication().getViewHandler();
//            UIViewRoot newRoot = vh.createView(facesContext, Constants.VIEW_ID_LOGON);
//            facesContext.setViewRoot(newRoot);
//        } catch (YawebmailCertificateException yce) {
//
//            try {
//
//                CheckCertsBean checkCerts
//                        = (CheckCertsBean) ManagedBeanUtils.getManagedBeanByName(facesContext,
//                                Constants.NAME_MBEAN_CHECKCERTS);
//
//                checkCerts.setWhichCerts(CheckCertsController.WhichCertsEnum.MAILBOX);
//                checkCerts.setServerCerts(yce.getCerts());
//
//                ViewHandler vh = facesContext.getApplication().getViewHandler();
//                UIViewRoot newRoot = vh.createView(facesContext,
//                        Constants.VIEW_ID_CHECK_CERTS);
//                facesContext.setViewRoot(newRoot);
//            } catch (Exception e) {
//                facesContext.addMessage(null,
//                        ExceptionConverter.getFacesMessage(facesContext, e, true));
//
//                ViewHandler vh = facesContext.getApplication().getViewHandler();
//                UIViewRoot newRoot = vh.createView(facesContext,
//                        Constants.VIEW_ID_TECH_ERROR);
//                facesContext.setViewRoot(newRoot);
//            }
//        } catch (Exception e) {
//            if (!viewId.equals(Constants.VIEW_ID_LOGON)) {
//                facesContext.addMessage(null,
//                        ExceptionConverter.getFacesMessage(facesContext, e, true));
//
//                ViewHandler vh = facesContext.getApplication().getViewHandler();
//                UIViewRoot newRoot = vh.createView(facesContext,
//                        Constants.VIEW_ID_TECH_ERROR);
//                facesContext.setViewRoot(newRoot);
//            }
//        }}
//    }
//}
