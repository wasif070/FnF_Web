/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.*;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.*;

import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

//@WebServlet(name = "AttachmentUploadForHtml", urlPatterns = {"/AttachmentUploadForHtml"})
public class AttachmentUploadForHtml extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UploadedFilesList uploadedFilesList = new UploadedFilesList();
        uploadedFilesList.setRet_val_success(true);
        HashMap<String, String> tempFilesList = new HashMap<String, String>();
        PrintWriter out = response.getWriter();

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {
            // System.out.println("File Not Uploaded");
        } else {
            //System.out.println("not File Not Uploaded");
        }

        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List items = null;

        try {
            items = upload.parseRequest(request);
        } catch (FileUploadException e) {
            e.printStackTrace();
        }
        Iterator itr = items.iterator();
        itr = items.iterator();
        while (itr.hasNext()) {
            FileItem item = (FileItem) itr.next();
            // System.out.println(item);
            if (item.isFormField()) {
                String name = item.getFieldName();
                String value = item.getString();
            } else {
                try {
                    String itemName = item.getName();

                    String reg = "[.*]";
                    String replacingtext = "";
                    Pattern pattern = Pattern.compile(reg);
                    Matcher matcher = pattern.matcher(itemName);
                    StringBuffer buffer = new StringBuffer();

                    while (matcher.find()) {
                        matcher.appendReplacement(buffer, replacingtext);
                    }

                    ServletContext context = request.getSession().getServletContext();
                    String realContextPath = context.getRealPath(request.getContextPath());

                    if (realContextPath.endsWith("IP")) {
                        realContextPath = realContextPath.substring(0, realContextPath.length() - "IP".length() - 1);

                    } else if (realContextPath.endsWith("IP")) {

                        realContextPath = realContextPath.substring(0, realContextPath.length() - "IP".length() - 1);

                    }

                    String fileLocation = realContextPath + File.separator + "uploadedFiles" + File.separator;

                    File savedFile = new File(fileLocation + itemName);

                    File parent = savedFile.getParentFile();
                    if (!parent.exists() && !parent.mkdirs()) {
//                        out.println("<p style='color:red' id='sss'>unable to upload. ERR:110.</p>");
//                        return;
                        uploadedFilesList.setRet_val_success(false);
                        break;
                    }
                    item.write(savedFile);
                    tempFilesList.put(itemName, savedFile.getAbsolutePath());

                } catch (Exception e) {
                    uploadedFilesList.setRet_val_success(false);
                    break;
//                    out.println("<p style='color:red' id='sss'>unable to upload. ERR:103.</p>");
                } finally {
                }

            }

        }
        uploadedFilesList.setFilesList(tempFilesList);
        Gson gson = new Gson();
        out.println(gson.toJson(uploadedFilesList));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
