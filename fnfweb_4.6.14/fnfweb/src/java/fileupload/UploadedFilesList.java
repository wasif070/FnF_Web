/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fileupload;

import java.util.HashMap;

/**
 *
 * @author reefat
 */
public class UploadedFilesList {
    private boolean ret_val_success;
    private HashMap<String,String> filesList;

    public UploadedFilesList() {
        filesList = new HashMap<String, String>();
    }

    public boolean isRet_val_success() {
        return ret_val_success;
    }

    public void setRet_val_success(boolean ret_val_success) {
        this.ret_val_success = ret_val_success;
    }

    public HashMap<String, String> getFilesList() {
        return filesList;
    }

    public void setFilesList(HashMap<String, String> filesList) {
        this.filesList = filesList;
    }
}
